<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloTurno extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
  

    function getlistturnos($params){

        $columns = array( 
            0=>'tur.id',
            1=>'suc.nombre as sucursal',
            2=>'tur.fecha',
            3=>'tur.horaa',
            4=>'tur.horac',
            5=>'tur.nombre',
            6=>'tur.status',
            7=>'tur.sucursalId',  
        );
        $columnsss = array( 
            0=>'tur.id',
            1=>'suc.nombre',
            2=>'tur.fecha',
            3=>'tur.horaa',
            4=>'tur.horac',
            5=>'tur.nombre',
            6=>'tur.status',
            7=>'tur.sucursalId',  
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('turno tur');
        $this->db->join('sucursales suc', 'suc.id=tur.sucursalId');
        
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columnsss[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function getlistturnost($params){        
        $columns = array( 
            0=>'tur.id',
            1=>'suc.nombre as sucursal',
            2=>'tur.fecha',
            3=>'tur.horaa',
            4=>'tur.horac',
            5=>'tur.nombre',
            6=>'tur.status',
            7=>'tur.sucursalId',  
        );
        $columnsss = array( 
            0=>'tur.id',
            1=>'suc.nombre',
            2=>'tur.fecha',
            3=>'tur.horaa',
            4=>'tur.horac',
            5=>'tur.nombre',
            6=>'tur.status',
            7=>'tur.sucursalId',  
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('turno tur');
        $this->db->join('sucursales suc', 'suc.id=tur.sucursalId');
        
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsss as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();

        return $query->row()->total;
    }



}