<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloVentas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$idsucursal,$factura){
    	$strq = "INSERT INTO ventas(id_personal, id_cliente, metodo, subtotal, descuento,descuentocant, monto_total, efectivo, cambio, sucursal,factura) 
                VALUES ($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$idsucursal,$factura)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarventad($idventa,$producto,$cantidad,$precio,$tipo){
    	$strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad, precio,tipo) VALUES ($idventa,$producto,$cantidad,$precio,$tipo)";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function ingresarventa_temporal($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$idsucursal){
        $strq = "INSERT INTO ventas_temporal(id_personal, id_cliente, metodo, subtotal, descuento,descuentocant, monto_total, efectivo, cambio, sucursal) 
                VALUES ($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$idsucursal)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }

    function ingresarventad_temporal($idventa,$producto,$cantidad,$precio,$tipo){
        $strq = "INSERT INTO venta_detalle_temporal(id_venta, id_producto, cantidad, precio) VALUES ($idventa,$producto,$cantidad,$precio,$tipo)";
        $query = $this->db->query($strq);
        $this->db->close();
    }

    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function clientepordefecto(){
        $strq = "SELECT * FROM  clientes LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventas($id){
        $strq = "SELECT * FROM ventas where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function getventasd($id){
        $strq = "SELECT vendell.cantidad, pro.nombre, vendell.precio, vendell.tipo
        FROM venta_detalle as vendell
        inner join productos as pro on pro.productoid=vendell.id_producto
        where vendell.id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function ingresarcompra($uss,$prov,$total,$idsucursal,$personalId){
        $strq = "INSERT INTO compras(monto_total,idsucursal,personalId) VALUES ($total,$idsucursal,$personalId)";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        $this->db->close();
        return $id;
    }
    function ingresarcomprad($idcompra,$producto,$cantidad,$precio,$provedor_id){
        $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad, precio_compra, id_proveedor) VALUES ($idcompra,$producto,$cantidad,$precio,$provedor_id)";
        $query = $this->db->query($strq);
    }
    function turnos(){
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        $status='cerrado';
        foreach ($query->result() as $row) {
            $status =$row->status;
        }
        return $status;
    }
    function turnoss($sucursal){
        $strq = "SELECT * FROM turno where sucursalId=$sucursal ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function abrirturno($cantidad,$nombre,$fecha,$horaa,$suc){
        $strq = "INSERT INTO turno(fecha, horaa, cantidad, nombre, status, user,sucursalId) VALUES ('$fecha','$horaa','$cantidad','$nombre','abierto','user',$suc)";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function cerrarturno($id,$horaa){
        $fechac=date('Y-m-d');
        $strq = "UPDATE turno 
                SET 
                horac='$horaa',
                fechacierre='$fechac', 
                status='cerrado', 
                status_var=0
                WHERE id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }


    function corte($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven){
        if($idsucursal==0){
            $wheresuc=" ";
        }else{
            $wheresuc=" v.sucursal=".$idsucursal."  AND ";
        }
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" v.metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) AS vendedor, v.sucursal,v.descuentocant,v.metodo,su.nombre AS sucursal_txt
                FROM ventas AS v 
                LEFT JOIN personal AS p ON v.id_personal=p.personalId 
                INNER JOIN clientes AS cl ON cl.ClientesId=v.id_cliente
                INNER JOIN sucursales AS su ON su.id=v.sucursal
                WHERE $wheresuc $whereven  v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0 ORDER BY v.id_venta DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }


    function consultarturnoname($inicio,$fin){
        $strq = "SELECT * FROM turno where fecha>='$inicio' and fechacierre<='$fin'";
        $query = $this->db->query($strq);
        $this->db->close();
        //$nombre=$cfecha.'/'.$chora;
        return $query;
    }
    function cortesum($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven){
        if($idsucursal==0){
            $wheresuc=" ";
        }else{
            $wheresuc=" sucursal=".$idsucursal."  AND ";
        }
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where $wheresuc $whereven reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM ventas";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                ORDER BY ven.id_venta DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function filastur() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function cancalarventa($id){
        $fecha = date('Y-m-d');
        $strq = "UPDATE ventas SET cancelado=1,hcancelacion='$fecha' WHERE `id_venta`=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function ventadetalles($id){
        $strq = "SELECT * FROM venta_detalle where id_venta=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function regresarpro($id,$can){
        $strq = "UPDATE productos set stock=stock+$can where productoid=$id";
        $query = $this->db->query($strq);
        $this->db->close();
    }
    function filasturnos() {
        $strq = "SELECT COUNT(*) as total FROM turno";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginadosturnos($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM turno ORDER BY id DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultarturno($id){
        $strq = "SELECT * FROM turno where id=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturno($fecha,$horaa,$horac,$fechac,$sucursal){
        $strq = "SELECT sum(monto_total) as total 
                FROM ventas 
                where 
                    cancelado=0 and
                    sucursal=$sucursal and
                    reg between '$fecha $horaa' and '$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        }
        return $total;
    }
    function consultartotalturno2($fecha,$horaa,$horac,$fechac,$sucursal){
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra 
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where
                    v.sucursal=$sucursal and
                    v.cancelado=0 and 
                    v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->preciocompra;
        }
        return $total;
    }
    function consultartotalturnopro($fecha,$horaa,$horac,$fechac,$sucursal){
        $strq = "SELECT p.nombre as producto,vd.cantidad, vd.precio
                from venta_detalle as vd
                inner join productos as p on vd.id_producto=p.productoid
                inner join ventas as v on vd.id_venta=v.id_venta
                where 
                    v.sucursal=$sucursal and
                    v.cancelado=0 and 
                    v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function consultartotalturnopromas($fecha,$horaa,$horac,$fechac,$sucursal){
        $strq = "SELECT p.nombre as producto, sum(vd.cantidad) as total 
                from venta_detalle as vd 
                inner join productos as p on vd.id_producto=p.productoid 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where 
                    v.sucursal=$sucursal and
                    v.cancelado=0 and 
                    v.reg>='$fecha $horaa' and v.reg<='$fechac $horac' 
                    GROUP BY producto ORDER BY `total` DESC LIMIT 1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    /*
    function filaslcompras(){
        $strq = "SELECT COUNT(*) as total FROM compra_detalle";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    */
    /*
    function total_paginadoslcompras($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT compdll.id_detalle_compra,comp.reg,pro.nombre as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                inner join productos as pro on pro.productoid=compdll.id_producto 
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                ORDER BY compdll.id_detalle_compra DESC
                LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    */
    /*
    function lcomprasconsultar($inicio,$fin) {
        
        $strq = "SELECT compdll.id_detalle_compra,comp.reg,pro.nombre as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra 
                FROM compra_detalle as compdll 
                inner join compras as comp on comp.id_compra=compdll.id_compra 
                inner join productos as pro on pro.productoid=compdll.id_producto 
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                where comp.reg>='$inicio 00:00:00' and comp.reg<='$fin 23:59:59'
                ORDER BY compdll.id_detalle_compra DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    */
    function ventassearch($search){
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.subtotal, ven.monto_total,ven.cancelado
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                where ven.id_venta like '%".$search."%' or 
                      ven.reg like '%".$search."%' or
                      cli.Nom like '%".$search."%' or
                      per.nombre like '%".$search."%' or
                      ven.monto_total like '%".$search."%'
                ";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function consultartotal_venta($id_venta){
        $strq = "SELECT sum(monto_total) AS total FROM ventas WHERE id_venta=$id_venta";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        }
        return $total;
    }

    function consultartotal_venta2($id_venta,$sucursal){
        $strq = "SELECT ps.preciocompra*vd.cantidad AS preciocompra 
                FROM venta_detalle AS vd
                INNER JOIN productos_sucursal_precios AS ps ON ps.productoid=vd.id_producto
                INNER JOIN ventas AS v ON v.id_venta=vd.id_venta
                WHERE v.id_venta=$id_venta AND v.sucursal=$sucursal GROUP BY vd.id_detalle_venta";
        $query = $this->db->query($strq);
        $this->db->close();
        $total_aux=0;
        foreach ($query->result() as $row) {
            $total_aux = $total_aux+round($row->preciocompra, 2);
        }
        return $total_aux;
    }
    
    function pro_venta($inicio,$fin,$idsucursal){
        $strq = "SELECT v.id_venta, p.nombre as producto, sum(vd.cantidad) AS total_cantidad  FROM ventas AS v 
                INNER JOIN venta_detalle AS vd ON vd.id_venta=v.id_venta
                INNER JOIN productos AS p ON p.productoid=vd.id_producto
                WHERE v.sucursal=$idsucursal AND  v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0 GROUP BY vd.id_producto ORDER BY sum(vd.cantidad) DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query;
    }


    /////////////////////Corte a credito////////////////////////
    function corte_credito($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven){
        if($idsucursal==0){
            $wheresuc=" ";
        }else{
            $wheresuc=" v.sucursal=".$idsucursal."  AND ";
        }
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" v.metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, v.id_venta, v.id_cliente, v.id_personal, v.monto_total,v.efectivo, v.subtotal, p.personalId, concat(p.nombre,p.apellidos) AS vendedor, v.sucursal,v.descuentocant,v.metodo,su.nombre AS sucursal_txt
                FROM ventas AS v 
                LEFT JOIN personal AS p ON v.id_personal=p.personalId 
                INNER JOIN clientes AS cl ON cl.ClientesId=v.id_cliente
                INNER JOIN sucursales AS su ON su.id=v.sucursal
                WHERE $wheresuc $whereven v.metodo=4 AND v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function cortesum_credito($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven){
        if($idsucursal==0){
            $wheresuc=" ";
        }else{
            $wheresuc=" sucursal=".$idsucursal."  AND ";
        }
        if($tipo_venta==0){
            $whereven=" ";
        }else{
            $whereven=" metodo=".$tipo_venta."  AND ";
        }
        $strq = "SELECT sum(monto_total) as total , sum(subtotal) as subtotal, sum(descuentocant) as descuento 
                FROM ventas 
                where $wheresuc $whereven metodo=4 AND reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    //// Abono
    public function getall_total_abono($id){
        $sql = "SELECT SUM(cantidad) AS abono FROM venta_abono AS v 
                INNER JOIN personal AS p ON p.personalId = v.idpersona
                WHERE v.id_venta = $id AND v.activo=1";
        $query = $this->db->query($sql);
        $total_aux=0;
        foreach ($query->result() as $row) {
            $total_aux = $row->abono;
        }
        return $total_aux;
    }
    
    function getcompras($id){
        $strq = "SELECT * FROM compras where id_compra=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function getcomprasd($id){
        $strq = "SELECT com.cantidad, pro.nombre, com.precio_compra
        FROM compra_detalle as com
        inner join productos as pro on pro.productoid=com.id_producto
        where com.id_compra=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function get_gastos($inicio,$fin,$idsucursal){
        if($idsucursal==0){
            $wheresuc=" ";
        }else{
            $wheresuc=" sucursal=".$idsucursal."  AND ";
        }
        $strq = "SELECT SUM(monto) AS gasto
                FROM gastos
                WHERE $wheresuc fecha BETWEEN '$inicio' AND '$fin' AND activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function corte_pedidos($inicio,$fin){
        $strq = "SELECT c.id,cl.Nom AS nombre,DATE_FORMAT(c.fechapedido,  '%d / %m / %Y' ) AS fechapedido,us.Usuario,c.estatus,c.total,c.anticipo,DATE_FORMAT(c.fechaentrega,  '%d / %m / %Y' ) AS fechaentrega
                FROM pedidos AS c 
                LEFT JOIN usuarios AS us ON us.UsuarioID=c.usuario
                INNER JOIN clientes AS cl ON cl.ClientesId=c.cliente
                WHERE  c.activo=1 AND c.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' ORDER BY c.id DESC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function get_pedido_abono($inicio,$fin){
        $strq = "SELECT SUM(pago) AS pago
                FROM pedidos_pagos
                WHERE fecha BETWEEN '$inicio' AND '$fin' AND activo=1";
        $query = $this->db->query($strq);
        return $query;
    }
    
}