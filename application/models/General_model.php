<?php
class General_model extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function add_record($table,$data){
    	$this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function edit_record($cos,$id,$data,$table){
    	$this->db->set($data);
        $this->db->where($cos, $id);
        return $this->db->update($table);
    }

    public function get_records_condition($condition,$table){
        $sql = "SELECT * FROM $table WHERE $condition";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_record($col,$id,$table){
        $sql = "SELECT * FROM $table WHERE $col=$id";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function get_tabla($tabla,$values){
        $this->db->select('*');
        $this->db->from($tabla);
        $this->db->where($values);
        $query=$this->db->get();
        return $query->result();
    }

    public function getselectwhere_orden_asc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query->result();
    }
    public function getselectwhere_orden_desc($tables,$values,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($values);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title, 'DESC');
        $query=$this->db->get();
        return $query->result();
    } 
    public function get_where_orden_asc($tables,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->order_by($title, 'ASC');
        $query=$this->db->get();
        return $query->result();
    }
    public function get_where_orden_desc($tables,$title){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->order_by($title, 'DESC');
        $query=$this->db->get();
        return $query->result();
    } 

    public function getselectwhererow($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query->row();
    }

    function pro_venta($inicio,$fin,$idsucursal){
        $strq = "SELECT v.id_venta, p.nombre as producto, sum(vd.cantidad) AS total_cantidad  FROM ventas AS v 
                INNER JOIN venta_detalle AS vd ON vd.id_venta=v.id_venta
                INNER JOIN productos AS p ON p.productoid=vd.id_producto
                WHERE v.sucursal=$idsucursal AND  v.reg BETWEEN '$inicio 00:00:00' AND '$fin 23:59:59' AND v.cancelado=0 GROUP BY vd.id_producto ORDER BY sum(vd.cantidad) DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function editar_status_alert($pro,$suc){
        $sql = "UPDATE productos_sucursal_precios SET status=0 WHERE productoid = $pro AND idsucursal=$suc";
        $query = $this->db->query($sql);
        $this->db->close();
        return $query;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }

    function get_folio(){
        $strq = "SELECT * FROM pedidos ORDER BY id DESC LIMIT 1";
        $query = $this->db->query($strq);
        $folio=0;
        foreach ($query->result() as $x) {
            $folio =$x->folio;
        } 
        return $folio;
    }
    
}