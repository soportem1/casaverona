<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function categorias_all() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriadell($id){
        $strq = "UPDATE categoria SET activo=0 WHERE categoriaId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categoriaadd($nom){
        $strq = "INSERT INTO categoria(categoria) VALUES ('$nom')";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function updatenota($use,$nota){
        $strq = "UPDATE notas SET mensaje='$nota',usuario='$use',reg=NOW()";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticket(){
        $strq = "SELECT * FROM ticket";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup){
        $strq = "UPDATE ticket SET titulo='$titulo',mensaje='$mensaje1',mensaje2='$mensaje2',fuente='$fuente',tamano='$tamano',margensup='$margsup'";
        $query = $this->db->query($strq);
        $this->db->close();
    }
   

}