<?php
defined('BASEPATH') OR exit ('No direct script access allowed');
class ModelCatalogos extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }
    /*
SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,pro.precioventa,pro.stock FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
*/
    function get_productos($params){
        if($params['idsucursal']==1){
            $columns = array( 
                0=>'pro.productoid',
                1=>'pro.nombre',
                2=>'pro.img',
                3=>'ps.stock',
                4=>'ps.precioventa',
                5=>'cat.categoria',
                6=>'pro.codigo'
            );
        }else{
            $columns = array( 
                0=>'pro.productoid',
                1=>'pro.nombre',
                2=>'pro.img',
                3=>'pro.codigo',
                4=>'pro.descripcion',
                5=>'ps.precioventa',
                6=>'ps.stock',
                6=>'pro.codigo'
            );
        }
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos pro');
        //if($params['idsucursal']==1){
          //  $where = array('pro.activo'=>1);
        //}else{
            $this->db->join('productos_sucursal_precios ps','ps.productoid=pro.productoid and ps.idsucursal=1','left'); 
            $this->db->join('categoria cat','cat.categoriaId=pro.categoria','left'); 
            $where = array('pro.activo'=>1);
        //}
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_productos($params){
        $columns = array( 
            0=>'pro.productoid',
            1=>'pro.nombre',
            2=>'pro.img',
            3=>'pro.codigo'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('productos pro');
        if($params['idsucursal']==1){
            $where = array('pro.activo'=>1);
        }else{
            $this->db->join('productos_sucursal_precios ps','ps.productoid=pro.productoid','left'); 
            $where = array('pro.activo'=>1,'idsucursal'=>$params['idsucursal']);
        }
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///====== Categoria =========!!
    function get_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'categoriaId',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_categoria($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    ///====== Marcas =========!!
    function get_marcas($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'idmarca',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('marca');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_marcas($params){
        $columns = array( 
            0=>'*',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('marca');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_precios_sucur($idpro,$idsucu){
        $where="";
        if($idsucu==1){
            $where="";
        }else{
            $where=" AND p.idsucursal=$idsucu";
        }
        $strq = "SELECT p.precioventa,s.nombre FROM productos_sucursal_precios AS p
                 INNER JOIN sucursales AS s ON s.id=p.idsucursal  
                 where p.productoid=$idpro $where";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_existencias_sucur($idpro,$idsucu){
        $where="";
        if($idsucu==1){
            $where="";
        }else{
            $where=" AND p.idsucursal=$idsucu";
        }
        $strq = "SELECT p.stock,p.stock_paq,s.nombre FROM productos_sucursal_precios AS p
                 INNER JOIN sucursales AS s ON s.id=p.idsucursal  
                 where p.productoid=$idpro $where";
        $query = $this->db->query($strq);
        return $query->result();
    }
    /// == Personal == ///
    function get_personal($params){
        $columns = array( 
            0=>'per.personalId',
            1=>'per.nombre',
            2=>'per.apellidos',
            3=>'us.Usuario'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('personal per');
        $this->db->join('usuarios us','us.personalId=per.personalId','left'); 
        $where = array('per.tipo'=>1,'per.estatus'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_personal($params){
        $columns = array( 
            0=>'per.personalId',
            1=>'per.nombre',
            2=>'per.apellidos',
            3=>'us.Usuario'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal per');
        $this->db->join('usuarios us','us.personalId=per.personalId','left'); 
        $where = array('per.tipo'=>1,'per.estatus'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /// == Cliente == ///
    function get_clientes($params){
        $columns = array( 
            0=>'ClientesId',
            1=>'Nom',
            2=>'telefonoc',
            3=>'Municipio',
            4=>'Calle',
            5=>'noExterior',
            6=>'Localidad',
            7=>'Estado'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('clientes');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_clientes($params){
        $columns = array( 
            0=>'ClientesId',
            1=>'Nom',
            2=>'telefonoc',
            3=>'Municipio',
            4=>'Calle',
            5=>'noExterior',
            6=>'Localidad',
            7=>'Estado'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('clientes');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    
    /// == Proveedores == ///
    function get_proveedores($params){
        $columns = array( 
            0=>'id_proveedor',
            1=>'razon_social',
            2=>'domicilio',
            3=>'ciudad',
            4=>'telefono_local',
            5=>'telefono_celular',
            6=>'email_contacto',
            7=>'rfc'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('proveedores');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_proveedores($params){
        $columns = array( 
            0=>'id_proveedor',
            1=>'razon_social',
            2=>'domicilio',
            3=>'ciudad',
            4=>'telefono_local',
            5=>'telefono_celular',
            6=>'email_contacto',
            7=>'rfc'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('proveedores');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///====== Turnos =========!!
    function get_turnos($params){
        $columns = array( 
            0=>'*',
        );
        $columns2 = array( 
            0=>'categoriaId',
            1=>'nombre'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_turnos($params){
        $columns = array( 
            0=>'categoria',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('categoria');
        $where = array('activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /// == Gastos == ///
    function get_gastos($params){
        $columns = array( 
            0=>'gas.id',
            1=>'gas.nombre',
            2=>'gas.monto',
            3=>'gas.fecha',
            4=>'us.Usuario',
            5=>'su.nombre AS sucursal',
            6=>'DATE_FORMAT(gas.fecha,  "%d / %m / %Y" ) AS fecha_gas',
            7=>'su.id AS idsucursal',
        );
        $columns2 = array( 
            0=>'gas.id',
            1=>'gas.nombre',
            2=>'gas.monto',
            3=>'gas.fecha',
            4=>'us.Usuario',
            5=>'su.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('gastos gas');
        $this->db->join('usuarios us','us.UsuarioID=gas.usuario','left'); 
        $this->db->join('sucursales su','su.id=gas.sucursal','left'); 
        if($params['idsucursal']==1){
            $where = array('gas.activo'=>1);
        }else{
            $where = array('gas.activo'=>1,'gas.sucursal'=>$params['idsucursal']);
        }
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_gastos($params){
        $columns = array( 
            0=>'gas.id',
            1=>'gas.nombre',
            2=>'gas.monto',
            3=>'gas.fecha',
            4=>'us.Usuario',
            5=>'su.nombre',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('gastos gas');
        $this->db->join('usuarios us','us.UsuarioID=gas.usuario','left'); 
        $this->db->join('sucursales su','su.id=gas.sucursal','left'); 
        $where = array('gas.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_productos_traspasos($buscar,$idsucursal){
        $strq = "SELECT 
                        p.productoid,
                        p.nombre,
                        p.codigo,
                        p.codigo_pag,
                        ps.precioventa,
                        ps.especial1_precio,
                        ps.especial2_precio,
                        ps.mediomayoreo,
                        ps.mayoreo,
                        ps.precio_paq
                FROM productos as p
                INNER JOIN productos_sucursal_precios AS ps ON ps.productoid=p.productoid
                WHERE 
                    p.activo = 1 AND 
                    ps.idsucursal=$idsucursal AND 
                    (
                        p.nombre like '%$buscar%' OR 
                        p.codigo like '%$buscar%' OR
                        p.codigo_pag like '%$buscar%'
                    )";
        $query = $this->db->query($strq);
        return $query->result();
    }
    ///====== Cotizaciones =========!!
    function get_cotizaciones($params){
        $columns = array( 
            0=>'c.id',
            1=>'cl.Nom AS nombre',
            2=>'DATE_FORMAT(c.fecha,  "%d / %m / %Y" ) AS fecha',
            3=>'us.Usuario',
            4=>'c.total',
            5=>'c.estatus',
            6=>'c.uncluir',
            7=>'c.subtotal',
            8=>'c.iva',
            9=>'c.contacto',
            10=>'c.cliente'
        );
        $columns2 = array( 
            0=>'c.id',
            1=>'cl.Nom',
            2=>'c.fecha',
            3=>'us.Usuario',
            4=>'c.total',
            5=>'c.estatus',
            6=>'c.contacto',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('cotizacion c');
        $this->db->join('usuarios us','us.UsuarioID=c.usuario','left'); 
        $this->db->join('clientes cl','cl.ClientesId=c.cliente','left'); 
        $where = array('c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_cotizaciones($params){
        $columns = array( 
            0=>'c.id',
            1=>'cl.Nom',
            2=>'c.fecha',
            3=>'us.Usuario',
            4=>'c.total',
            5=>'c.contacto',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('cotizacion c');
        $this->db->join('usuarios us','us.UsuarioID=c.usuario','left'); 
        $this->db->join('clientes cl','cl.ClientesId=c.cliente','left'); 
        $where = array('c.activo'=>1);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_partidas($idcotizacion){
        $strq = "SELECT c.*,p.nombre,ps.precioventa,ps.especial1_precio,ps.especial2_precio,ps.mediomayoreo,ps.mayoreo
                FROM cotizacion_partidas as c
                INNER JOIN productos AS p ON p.productoid=c.productoid
                LEFT JOIN productos_sucursal_precios AS ps ON ps.productoid=c.productoid
                WHERE c.activo=1 AND ps.idsucursal=1 AND c.idcotizacion=$idcotizacion";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_partidas_all($idcotizacion){
        $strq = "SELECT c.*,p.nombre,ps.stock
                FROM cotizacion_partidas as c
                INNER JOIN productos AS p ON p.productoid=c.productoid
                LEFT JOIN productos_sucursal_precios AS ps ON ps.productoid=c.productoid
                WHERE c.activo=1 AND ps.idsucursal=1 AND c.idcotizacion=$idcotizacion";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_tras_detalles($idtras,$idsucursal){
        $strq = "SELECT t.*,p.nombre,ps.precioventa,ps.especial1_precio,ps.especial2_precio,ps.mediomayoreo,ps.mayoreo
                FROM traspasos_detalles as t
                INNER JOIN productos AS p ON p.productoid=t.productoid
                LEFT JOIN productos_sucursal_precios AS ps ON ps.productoid=t.productoid
                WHERE t.activo=1 AND ps.idsucursal=$idsucursal AND t.idtraspaso=$idtras";
        $query = $this->db->query($strq);
        return $query->result();
    }

    ///====== Traspasos =========!!
    function get_traspasos($params){
        $columns = array( 
            0=>'t.id',
            1=>'DATE_FORMAT(t.fecha,  "%d / %m / %Y" ) AS fecha',
            2=>'su.nombre',
            3=>'t.total',
            4=>'us.Usuario',
            5=>'t.estatus',
            6=>'t.hora'
        );
        $columns2 = array( 
            0=>'t.id',
            1=>'t.fecha',
            2=>'su.nombre',
            3=>'t.total',
            4=>'us.Usuario',
            5=>'t.estatus',
            6=>'t.hora'
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('traspasos t');
        $this->db->join('usuarios us','us.UsuarioID=t.usuario','left'); 
        $this->db->join('sucursales su','su.id=t.sucursal','left'); 
        $where = array('t.activo'=>1,'t.sucursal'=>$params['idsucursal']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    function total_traspasos($params){
        $columns = array( 
            0=>'t.id',
            1=>'t.fecha',
            2=>'su.nombre',
            3=>'t.total',
            4=>'us.Usuario',
            5=>'t.estatus',
            6=>'t.hora'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('traspasos t');
        $this->db->join('usuarios us','us.UsuarioID=t.usuario','left'); 
        $this->db->join('sucursales su','su.id=t.sucursal','left'); 
        $where = array('t.activo'=>1,'t.sucursal'=>$params['idsucursal']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
   
    function stock_descontar_producto($cantidad,$idsucursal,$id){
        $strq="UPDATE productos_sucursal_precios SET stock = stock-$cantidad WHERE idsucursal=$idsucursal AND productoid=$id";
        $this->db->query($strq);
    }
    function stock_descontar_producto_paq($cantidad,$idsucursal,$id){
        $strq="UPDATE productos_sucursal_precios SET stock = stock-(stock_paq*$cantidad) WHERE idsucursal=$idsucursal AND productoid=$id";
        $this->db->query($strq);
    }

    function stock_aumentar_producto($cantidad,$idsucursal,$id){
        $strq="UPDATE productos_sucursal_precios SET stock = stock+$cantidad WHERE idsucursal=$idsucursal AND productoid=$id";
        $this->db->query($strq);
    }
    function stock_aumentar_producto_paq($cantidad,$idsucursal,$id){
        $strq="UPDATE productos_sucursal_precios SET stock = stock+(stock_paq*$cantidad) WHERE idsucursal=$idsucursal AND productoid=$id";
        $this->db->query($strq);
    }

    ///====== Listado de ventas =========!!
    function get_ventas($params){
        $columns = array( 
            0=>'ven.id_venta', 
            1=>'DATE_FORMAT(ven.reg,  "%d / %m / %Y %r") AS reg',
            2=>'cli.Nom as cliente', 
            3=>'concat(per.nombre," ",per.apellidos) as vendedor',
            4=>'ven.metodo',
            5=>'ven.subtotal', 
            6=>'ven.monto_total',
            7=>'ven.cancelado',
            8=>'ven.tipo_venta',
            9=>'ven.descuentocant',
            10=>'ven.factura',
            11=>'ven.pagado_credito'
            
        );
        $columns2 = array( 
            0=>'ven.id_venta',
            1=>'ven.reg', 
            2=>'cli.Nom', 
            3=>'per.nombre',
            3=>'per.apellidos',
            4=>'ven.metodo',
            5=>'ven.subtotal', 
            6=>'ven.monto_total',
            7=>'ven.cancelado',
            8=>'ven.descuentocant'
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ventas ven');
        $this->db->join('personal per','per.personalId=ven.id_personal'); 
        $this->db->join('clientes cli','cli.ClientesId=ven.id_cliente'); 
        if($params['sucursal']!=0){
            $where = array('ven.sucursal'=>$params['sucursal']);
            $this->db->where($where);
        }
        if($params['tipo_venta']!=0){
            $wheret = array('ven.metodo'=>$params['tipo_venta']);
            $this->db->where($wheret);
        }
        if($params['tipo_factura']==1){
            $wheref = array('ven.factura'=>1);
            $this->db->where($wheref);
        }else if($params['tipo_factura']==2){
            $wheref = array('ven.factura'=>0);
            $this->db->where($wheref);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    function total_ventas($params){
        $columns = array( 
            0=>'ven.id_venta',
            1=>'ven.reg', 
            2=>'cli.Nom', 
            3=>'per.nombre',
            3=>'per.apellidos',
            4=>'ven.metodo',
            5=>'ven.subtotal', 
            6=>'ven.monto_total',
            7=>'ven.cancelado'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('ventas ven');
        $this->db->join('personal per','per.personalId=ven.id_personal'); 
        $this->db->join('clientes cli','cli.ClientesId=ven.id_cliente'); 
        if($params['sucursal']!=0){
            $where = array('ven.sucursal'=>$params['sucursal']);
            $this->db->where($where);
        }
        if($params['tipo_venta']!=0){
            $wheret = array('ven.metodo'=>$params['tipo_venta']);
            $this->db->where($wheret);
        }
        if($params['tipo_factura']==1){
            $wheref = array('ven.factura'=>1);
            $this->db->where($wheref);
        }else if($params['tipo_factura']==2){
            $wheref = array('ven.factura'=>0);
            $this->db->where($wheref);
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /////////////////////////////////////
    public function get_traspasos_pruductos($idtraspaso){
        $strq = "SELECT c.*,p.nombre
                FROM traspasos_detalles as c
                INNER JOIN productos AS p ON p.productoid=c.productoid
                WHERE c.activo=1 AND c.idtraspaso=$idtraspaso";
        $query = $this->db->query($strq);
        return $query->result();
    }
    // Listado de compras
    function get_compras($params){
        $columns = array( 
            0=>'comp.id_compra',
            1=>'DATE_FORMAT(comp.reg,  "%d / %m / %Y %r") AS reg',
            2=>'comp.monto_total',
            3=>'su.nombre AS sucursal'
        );
        $columns2 = array( 
            0=>'comp.id_compra',
            1=>'comp.reg',
            2=>'comp.monto_total',
            3=>'su.nombre'
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compras comp');
        $this->db->join('sucursales su','su.id=comp.idsucursal','left'); 
        if($params['fechain']!='' && $params['fechafin']!=''){
            $this->db->where('comp.reg>="'.$params['fechain'].' 00:00:00" AND comp.reg<="'.$params['fechafin'].' 23:59:59"');
        }
        $where = array('comp.idsucursal'=>$params['sucursal']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    function total_compras($params){
        $columns = array( 
            0=>'comp.id_compra',
            1=>'comp.reg',
            2=>'comp.monto_total',
            3=>'su.nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('compras comp');
        $this->db->join('sucursales su','su.id=comp.idsucursal','left'); 
        if($params['fechain']!='' && $params['fechafin']!=''){
            $this->db->where('comp.reg>="'.$params['fechain'].' 00:00:00" AND comp.reg<="'.$params['fechafin'].' 23:59:59"');
        }
        $where = array('comp.idsucursal'=>$params['sucursal']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    /*
    function get_compras($params){
        $columns = array( 
            0=>'compdll.id_detalle_compra',
            1=>'comp.reg',
            2=>'pro.nombre as producto',
            3=>'prov.razon_social',
            4=>'compdll.cantidad',
            5=>'compdll.precio_compra',
            6=>'su.nombre AS sucursal'
        );
        $columns2 = array( 
            0=>'compdll.id_detalle_compra',
            1=>'comp.reg',
            2=>'pro.nombre',
            3=>'prov.razon_social',
            4=>'compdll.cantidad',
            5=>'compdll.precio_compra',
            6=>'su.nombre'
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('compra_detalle compdll');
        $this->db->join('compras comp','comp.id_compra=compdll.id_compra'); 
        $this->db->join('productos pro','pro.productoid=compdll.id_producto'); 
        $this->db->join('proveedores prov','prov.id_proveedor=comp.id_proveedor');
        $this->db->join('sucursales su','su.id=comp.idsucursal','left'); 
        if($params['fechain']!='' && $params['fechafin']!=''){
            $this->db->where('comp.reg>="'.$params['fechain'].' 00:00:00" AND comp.reg<="'.$params['fechafin'].' 23:59:59"');
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    function total_compras($params){
        $columns = array( 
            0=>'compdll.id_detalle_compra',
            1=>'comp.reg',
            2=>'pro.nombre',
            3=>'prov.razon_social',
            4=>'compdll.cantidad',
            5=>'compdll.precio_compra',
            6=>'su.nombre'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('compra_detalle compdll');
        $this->db->join('compras comp','comp.id_compra=compdll.id_compra'); 
        $this->db->join('productos pro','pro.productoid=compdll.id_producto'); 
        $this->db->join('proveedores prov','prov.id_proveedor=comp.id_proveedor'); 
        $this->db->join('sucursales su','su.id=comp.idsucursal','left'); 
        if($params['fechain']!='' && $params['fechafin']!=''){
            $this->db->where('comp.reg>="'.$params['fechain'].' 00:00:00" AND comp.reg<="'.$params['fechafin'].' 23:59:59"');
        }
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }
    */
    /////////////////////////////////////
    public function get_traspasos_pruductos_sucursal($idtraspaso){
        $strq = "SELECT c.*,p.nombre
                FROM traspasos_detalles as c
                INNER JOIN productos AS p ON p.productoid=c.productoid
                WHERE c.activo=1 AND c.estatus>=1  AND c.idtraspaso=$idtraspaso";
        $query = $this->db->query($strq);
        return $query->result();
    }

    //////////////////////////////////
    ///====== Traspasos Sucursal=========!!
    function get_traspasos_sucursal($params){
        $columns = array( 
            0=>'t.id',
            1=>'DATE_FORMAT(t.fecha,  "%d / %m / %Y" ) AS fecha',
            2=>'su.nombre',
            3=>'t.total',
            4=>'us.Usuario',
            5=>'t.estatus',
            6=>'t.hora'
        );
        $columns2 = array( 
            0=>'t.id',
            1=>'t.fecha',
            2=>'su.nombre',
            3=>'t.total',
            4=>'us.Usuario',
            5=>'t.estatus',
            6=>'t.hora'
        );    
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('traspasos t');
        $this->db->join('usuarios us','us.UsuarioID=t.usuario','left'); 
        $this->db->join('sucursales su','su.id=t.sucursal','left'); 
        $where = array('t.estatus'=>1,'t.activo'=>1,'t.sucursal'=>$params['idsucursal']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    function total_traspasos_sucursal($params){
        $columns = array( 
            0=>'t.id',
            1=>'t.fecha',
            2=>'su.nombre',
            3=>'t.total',
            4=>'us.Usuario',
            5=>'t.estatus',
            6=>'t.hora'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('traspasos t');
        $this->db->join('usuarios us','us.UsuarioID=t.usuario','left'); 
        $this->db->join('sucursales su','su.id=t.sucursal','left'); 
        $where = array('t.estatus'=>1,'t.activo'=>1,'t.sucursal'=>$params['idsucursal']);
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function getproducto_sucursal($productoid,$idsucursal){
        $strq = "SELECT 
                    p.productoid,
                    p.nombre,
                    p.codigo,
                    p.codigo_pag,
                    ps.precioventa,
                    ps.especial1_precio,
                    ps.especial2_precio,
                    ps.mediomayoreo,
                    ps.mayoreo,
                    ps.canmediomayoreo,
                    ps.canmayoreo,
                    ps.especial1_cantidad,
                    ps.especial2_cantidad,
                    ps.precio_paq,
                    ps.stock
                FROM productos as p
                INNER JOIN productos_sucursal_precios AS ps ON ps.productoid=p.productoid
                WHERE p.productoid=$productoid AND ps.idsucursal=$idsucursal";
        $query = $this->db->query($strq);
        return $query;
    }

    public function get_total_abono($id){
        $sql = "SELECT SUM(cantidad) AS total FROM venta_abono WHERE id_venta = $id AND activo=1";
        $query = $this->db->query($sql);
        return $query->row();
    }
    public function getall_total_abono($id){
        $sql = "SELECT v.*,p.nombre,p.apellidos FROM venta_abono AS v 
                INNER JOIN personal AS p ON p.personalId = v.idpersona
                WHERE v.id_venta = $id AND v.activo=1 ORDER BY v.id DESC";
        $query = $this->db->query($sql);
        return $query->result();
    }

    function get_compras_detalles($id){
        $strq = "SELECT cd.id_detalle_compra,p.nombre AS producto,cd.cantidad,cd.precio_compra,pr.razon_social
                FROM compra_detalle AS cd
                INNER JOIN productos AS p ON p.productoid=cd.id_producto
                INNER JOIN proveedores AS pr ON pr.id_proveedor=cd.id_proveedor
                WHERE cd.id_compra=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    ///====== Pedidos =========!!
    function get_pedidos($params){
        $columns = array( 
            0=>'c.id',
            1=>'cl.Nom AS nombre',
            2=>'DATE_FORMAT(c.fechapedido,  "%d / %m / %Y" ) AS fechapedido',
            3=>'us.Usuario',
            4=>'c.estatus',
            5=>'c.total',
            6=>'c.anticipo',
            7=>'DATE_FORMAT(c.fechaentrega,  "%d / %m / %Y" ) AS fechaentrega',
            8=>'c.activo',
            9=>'c.cliente',
            10=>'c.personacontacto',
            11=>'c.cantdescuento'
        );
        $columns2 = array( 
            0=>'c.id',
            1=>'cl.Nom',
            2=>'c.fechapedido',
            3=>'us.Usuario',
            4=>'c.total',
            5=>'c.anticipo',
            6=>'c.estatus',
            7=>'c.fechaentrega',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('pedidos c');
        $this->db->join('usuarios us','us.UsuarioID=c.usuario','left'); 
        $this->db->join('clientes cl','cl.ClientesId=c.cliente','left'); 
        //$where = array('c.activo'=>1);
        //$this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns2[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query;
    }
    public function total_pedidos($params){
        $columns = array( 
            0=>'c.id',
            1=>'cl.Nom',
            2=>'c.fechapedido',
            3=>'us.Usuario',
            4=>'c.total',
            5=>'c.anticipo',
            6=>'c.estatus',
            7=>'c.fechaentrega',
            8=>'c.activo',
            9=>'c.cliente',
            10=>'c.personacontacto',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('pedidos c');
        $this->db->join('usuarios us','us.UsuarioID=c.usuario','left'); 
        $this->db->join('clientes cl','cl.ClientesId=c.cliente','left'); 
        //$where = array('c.activo'=>1);
        //$this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }  
        $query=$this->db->get();
        return $query->row()->total;
    }

    public function get_pedido_partidas($id){
        $strq = "SELECT c.*,p.nombre,ps.precioventa,ps.especial1_precio,ps.especial2_precio,ps.mediomayoreo,ps.mayoreo,c.tipo,c.nombre_producto
                FROM pedidos_partidas as c
                LEFT JOIN productos AS p ON p.productoid=c.productoid
                LEFT JOIN productos_sucursal_precios AS ps ON ps.productoid=c.productoid
                WHERE c.activo=1 AND c.idpedido=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_pedidos_partidas_all($id){
        $strq = "SELECT c.*,p.nombre,ps.stock
                FROM pedidos_partidas as c
                LEFT JOIN productos AS p ON p.productoid=c.productoid
                LEFT JOIN productos_sucursal_precios AS ps ON ps.productoid=c.productoid
                WHERE c.activo=1 AND c.idpedido=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function get_productos_validar($buscar,$idsucursal){
        $strq = "SELECT 
                        p.productoid,
                        p.nombre,
                        p.codigo,
                        p.codigo_pag,
                        ps.precioventa,
                        ps.especial1_precio,
                        ps.especial2_precio,
                        ps.mediomayoreo,
                        ps.mayoreo,
                        ps.precio_paq
                FROM productos as p
                LEFT JOIN productos_sucursal_precios AS ps ON ps.productoid=p.productoid
                WHERE 
                    p.activo = 1 AND 
                    ps.idsucursal=$idsucursal AND 
                    (
                        p.nombre = '$buscar' OR 
                        p.codigo = '$buscar' OR
                        p.codigo_pag = '$buscar'
                    )";
        $query = $this->db->query($strq);
        return $query->result();
    }
}