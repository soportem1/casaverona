<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,per.idsucursal 
                 FROM usuarios as usu,personal as per 
                 where 
                 per.personalId = usu.personalId and 
                 per.estatus=1 and 
                 usu.Usuario ='$usu';";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;  
            $idsucursal = $row->idsucursal;  
            
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                //$_SESSION['usuarioid_tz']=$id;
                //$_SESSION['usuario_tz']=$nom;
                //$_SESSION['perfilid_tz']=$perfil;
                //$_SESSION['idpersonal_tz']=$idpersonal;
                //$_SESSION['idsucursal_tz']=$idsucursal;

                $data = array(
                        'logeado' => true,
                        'usuarioid_tz' => $id,
                        'usuario_tz' => $nom,
                        'perfilid_tz'=>$perfil,
                        'idpersonal_tz'=>$idpersonal,
                        'idsucursal_tz'=>$idsucursal
                    );
                $this->session->set_userdata($data);
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        
        echo $count;
    }
    public function menus($perfil){
        //$strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' ORDER BY men.MenuId ASC";
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon 
                from menu as men, menu_sub as mens, personal_menu as perfd 
                where men.MenuId=mens.MenuId and perfd.MenuId=mens.MenusubId and perfd.personalId='$perfil' ORDER BY men.MenuId ASC";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }

    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon 
                from menu_sub as menus, personal_menu as perfd 
                WHERE perfd.MenuId=menus.MenusubId and perfd.personalId='$perfil' and menus.MenuId='$menu' ORDER BY menus.MenusubId ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;

    }

    function alertproductos(){
        $strq = "SELECT count(*) as total FROM productos where stock<=3 and activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    function alertproductosall(){
        $strq = "SELECT * FROM productos where stock<=3 and activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function notas(){
        $strq = "SELECT * FROM notas";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

    function get_all_sucursales($sucursal){
        if($sucursal==1){
           $where="";
        }else{
           $where="id=".$sucursal." AND ";
        }    
        $strq = "SELECT * FROM sucursales WHERE $where activo=1 ORDER BY id DESC";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function alertproductos_sucursal($id){
        $strq = "SELECT count(*) as total FROM productos AS p 
                 INNER JOIN productos_sucursal_precios AS ps ON ps.productoid=p.productoid WHERE ps.stock<=3 AND p.activo=1 AND ps.status=1 AND ps.idsucursal=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    function alertproductosall_sucursal($id){
        $strq = "SELECT * FROM productos AS p
                INNER JOIN productos_sucursal_precios AS ps ON ps.productoid=p.productoid WHERE ps.stock<=3 AND p.activo=1 AND ps.status=1 AND ps.idsucursal=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

}
