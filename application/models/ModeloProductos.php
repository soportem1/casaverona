<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProductos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM productos where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,pro.precioventa,pro.stock FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function productosall() {
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo,pro.descripcion,cat.categoria,ps.precioventa,ps.stock 
                FROM productos as pro
                inner join categoria as cat on cat.categoriaId=pro.categoria 
                left join productos_sucursal_precios as ps on ps.productoid=pro.productoid and ps.idsucursal=1
                where pro.activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function total_paginadosp($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
        return $strq;
    }
    function productoallsearch($pro){
        $strq = "SELECT * FROM productos where activo=1 and codigo like '%".$pro."%' or activo=1 and nombre like '%".$pro."%'";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function categorias() {
        $strq = "SELECT * FROM categoria where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    public function productosinsert($cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$especial1_precio,$especial1_cantidad,$especial2_precio,$especial2_cantidad){
            $strq = "INSERT INTO productos
                    (codigo, productofiscal, nombre, descripcion, categoria, stock, preciocompra, porcentaje, precioventa, mediomayoreo, canmediomayoreo, mayoreo, canmayoreo,especial1_precio,especial1_cantidad,especial2_precio,especial2_cantidad) 
                    VALUES ('$cod',$pfiscal,'$nom','$des','$catego','$stock','$preciocompra','$porcentaje','$precioventa','$pmmayoreo','$cpmmayoreo','$pmayoreo','$cpmayoreo','$especial1_precio','$especial1_cantidad','$especial2_precio','$especial2_cantidad')";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function productosupdate($id,$cod,$pfiscal,$nom,$des,$catego,$stock,$preciocompra,$porcentaje,$precioventa,$pmmayoreo,$cpmmayoreo,$pmayoreo,$cpmayoreo,$especial1_precio,$especial1_cantidad,$especial2_precio,$especial2_cantidad){
            $strq = "UPDATE productos 
                        SET 
                        codigo='$cod',
                        productofiscal=$pfiscal,
                        nombre='$nom',
                        descripcion='$des',
                        categoria='$catego',
                        stock='$stock',
                        preciocompra='$preciocompra',
                        porcentaje='$porcentaje',
                        precioventa='$precioventa',
                        mediomayoreo='$pmmayoreo',
                        canmediomayoreo='$cpmmayoreo',
                        mayoreo='$pmayoreo',
                        canmayoreo='$cpmayoreo',
                        especial1_precio='$especial1_precio',
                        especial1_cantidad='$especial1_cantidad',
                        especial2_precio='$especial2_precio',
                        especial2_cantidad='$especial2_cantidad'
                        WHERE productoid=$id";
            $this->db->query($strq);
    }
    public function imgpro($file,$pro){
        $strq = "UPDATE productos SET img='$file' WHERE productoid=$pro";
        $this->db->query($strq);
    }
    function totalproductosenexistencia() {
        $strq = "SELECT ROUND(sum(stock),2) as total FROM `productos_sucursal_precios` where status=1 ";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductopreciocompra() {
        $strq = "SELECT ROUND(sum(preciocompra),2) as total FROM `productos_sucursal_precios` where status=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function totalproductoporpreciocompra(){
        $strq = "SELECT ROUND(sum(preciocompra*stock),2) as total FROM `productos_sucursal_precios` where status=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function getproducto($id){
        $strq = "SELECT * FROM productos where productoid=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function productosdelete($id){
        $strq = "UPDATE productos SET activo=0 WHERE productoid=$id";
        $this->db->query($strq);
    }
}