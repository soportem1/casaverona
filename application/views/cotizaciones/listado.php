<style type="text/css">
  .border_eti{
    border-radius: 13px;
  }
</style>
<div class="row">
  <div class="col-md-10">
    <h2>Listado de Cotizaciones</h2>
  </div>
  <div class="col-md-2" align="center">
    <a href="<?php echo base_url() ?>Cotizaciones/registro" class="btn btn-round btn_orange" style="color:white !important"> Nueva cotización</a>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <br>
                <!--------//////////////-------->
                <table class="table table-striped" id="data_tables" style="width: 100%">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Cliente</th>
                      <th>Monto</th>
                      <th>Fecha</th>
                      <th>Usuario</th>
                      <th>Estatus</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>


<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> esta cotización?
                <input type="hidden" id="id_aux">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="delete_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="cancelar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>rechazar</b> esta cotización?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="rechazar_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!------------------------------------------------>
<div class="modal fade text-left" id="cancelar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>rechazar</b> esta cotización?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="rechazar_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!------------------------------------------------>
<div class="modal fade text-left" id="aceptar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Aceptación de cotización</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <fieldset class="form-group">
                      <label>Fecha:</label>
                      <input type="date" readonly class="form-control" id="fecha" value="<?php echo $fecha ?>">
                  </fieldset>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Método de pago:</label>
                        <select class="form-control" id="mpago">
                          <option value="1">Efectivo</option>
                          <option value="2">Tarjeta de crédito</option>
                          <option value="3">Tarjeta de débito</option>
                        </select>
                    </div>
                </div>
              </div>
              <br>
              <h4>Productos</h4>
              <div class="row">
                <div class="col-md-12">
                  <div class="tabla_productos"></div>
                </div>
              </div> 
              <div class="row">
                <div class="col-md-12 subtotal_p" align="right">
                  <h5><b>Subtotal: $<span class="subtotal_p_total"></span></b></h5>
                </div>
                <div class="col-md-12 iva_p" align="right">
                  <h5><b>Iva: $<span class="iva_p_total"></span></b></h5>
                </div>
                <div class="col-md-12 total_p" align="right">
                  <h5><b>Total: $<span class="total_p_total"></span></b></h5>
                  <span class="total_p_total2" style="display: none;">0</span>
                  <span class="cantdescuento" style="display: none;">0</span>
                </div>
              </div>  
              <div class="row">
                <div class="col-md-8" align="right">
                  <label>Descuento %:</label>
                </div>
                <div class="col-md-4">
                  <fieldset class="form-group">
                      <input type="number" class="form-control" id="descuento" oninput="descuento_cotizacion()">
                  </fieldset>
                </div>
              </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="aceptar_cotizacion()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
 
 <!------------------------------------------------>
<div class="modal fade text-left" id="aceptar_cotizacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>aceptar</b> la cotización?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="aceptar_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

