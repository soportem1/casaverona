<style type="text/css">
  .h-60 {
      height: 60px;
  }
  .ai-center {
      align-items: center;
  }
  .lg\:ph-32 {
      padding-left: 32px;
      padding-right: 32px;
  }
  .bgc-moon, .hover\:bgc-moon:focus, .hover\:bgc-moon:hover {
      background-color: #f5f5fa;
  }
  .cursor-pointer {
      cursor: pointer;
  }
  .bdw-0 {
      border-width: 0;
  }
  .fxg-1 {
      flex-grow: 1;
  }
  .bgc-transparent {
      background-color: transparent;
  }
  .letra_t{
    font-size: 20px;
  } 
  .color_amarillo{
    color: #f2b327;
  }
  .borde_div{
    border-radius: 34px;
  }
</style>
<div class="row">
  <div class="col-md-10">
    <h2><?php echo $tipo ?> traspaso</h2>
  </div>
  <div class="col-md-2" align="center">
    <a href="<?php echo base_url() ?>Traspasos" class="btn btn-round btn_amarillo" style="color:white !important"><i class="ft-arrow-left"></i> Regresar</a>
  </div>
</div>
<input type="hidden" readonly id="tras_detalle_aux" value="<?php echo $tras_detalle_aux ?>">
<input type="hidden" readonly id="idtraspaso" value="<?php echo $id ?>">
<input type="hidden" readonly id="idsucursal_aux" value="<?php echo $sucursal ?>">
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <div class="row">
                  <div class="col-md-4">
                    <fieldset class="form-group">
                        <label>Seleccione sucursal</label>
                        <select class="form-control" id="idsucursal">
                        <?php foreach ($get_sucursales as $item){ 
                          if($item->id!=1){
                        ?>
                          <option value="<?php echo $item->id ?>" <?php if($item->id==$sucursal) echo 'selected' ?>><?php echo $item->nombre ?></option>
                        <?php } 
                        }?>  
                        </select>
                    </fieldset>
                  </div>
                  <div class="col-md-3">
                    <fieldset class="form-group">
                        <label>Fecha</label>
                        <input type="date" readonly class="form-control" id="fecha" value="<?php echo $fecha ?>">
                    </fieldset>
                  </div>
                  <div class="col-md-3">
                    <fieldset class="form-group">
                        <label>Hora</label>
                        <input type="time" readonly class="form-control" id="hora" value="<?php echo $hora ?>">
                    </fieldset>
                  </div>
                </div>
                <hr>
                <div class="row" style="display: none">
                  <div class="col-md-12"> 
                     <canvas id="barcode"></canvas>
                  </div>
                </div> 

                <div class="row">
                  <div class="col-md-12">  
                    <div class="recordableHolder">
                    <div class="borde_div h-60 lg:ph-32 bdr-4 d-flex ai-center bgc-moon">
                      <i class="fa fa-search color_amarillo" style="font-size: 25px;"></i>
                      &nbsp;&nbsp;&nbsp;
                      
                        <input class="letra_t bgc-transparent fxg-1 bdw-0 recordable rinited" id="productos" placeholder="Buscar producto" type="text" >
                        <a class="rec"><i class="fa fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                      
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <img style="width: 30px;"  src="<?php echo base_url() ?>img/mango.png">
                    </div>
                    </div>  
                  </div>  
                </div>    
                <div class="row">
                  <div class="col-md-12">
                    <div style="position: relative;">
                        <div class="producto_buscar_t" style="position: absolute; top: 25px; z-index: 3;">  
                        </div>
                    </div>    
                  </div>
                </div>
                <br>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-striped" id="data_tables_productos" style="width: 100%">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Cantidad</th>
                          <th>Precio</th>
                          <th></th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>  
                <div class="row">
                  <div class="col-md-12" align="right">
                    <h4><b>Total de traspaso: $<span class="total_p_total"><?php echo $total ?></span></b></h4>
                  </div>
                </div>   
                <div class="row"> 
                  <div class="col-md-12" align="center">
                    <button class="btn btn-round btn_orange btn_registro" onclick="varlidar_existencia()" style="width: 170px;"><i class="ft-save"></i> Guardar</button>
                  </div>
                </div>   
              </div>
          </div>
      </div>
  </div>
</div>

<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_traspaso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Desea <b>Eliminar</b> este traspaso?
                <input type="hidden" id="id_traspaso_aux">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="delete_registro_transpaso()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>