<style type="text/css">
  .h-60 {
      height: 60px;
  }
  .ai-center {
      align-items: center;
  }
  .lg\:ph-32 {
      padding-left: 32px;
      padding-right: 32px;
  }
  .bgc-moon, .hover\:bgc-moon:focus, .hover\:bgc-moon:hover {
      background-color: #f5f5fa;
  }
  .cursor-pointer {
      cursor: pointer;
  }
  .bdw-0 {
      border-width: 0;
  }
  .fxg-1 {
      flex-grow: 1;
  }
  .bgc-transparent {
      background-color: transparent;
  }
  .letra_t{
    font-size: 20px;
  } 
  .color_amarillo{
    color: #f2b327;
  }
  .borde_div{
    border-radius: 34px;
  }
  .custom-control-input:checked ~ .custom-control-indicator {
    color: #FFFFFF;
    background-color: #f6a169;
  }
  .custom-control-input:active ~ .custom-control-indicator {
    background-color: #f6a169;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Registrar traspaso</h2>
  </div>
</div>
<input type="password"  class="form-control" disabled="" style="background-color: #ff000000; border: 1px solid rgb(0 0 0 / 0%); font-size: 0rem;">
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <br> 
                <div class="row">
                  <div class="col-md-12">  
                    <div class="recordableHolder">
                      <div class="borde_div h-60 lg:ph-32 bdr-4 d-flex ai-center bgc-moon">
                        <i class="fa fa-search color_amarillo" style="font-size: 25px;"></i>
                        &nbsp;&nbsp;&nbsp;
                        
                          <input class="letra_t bgc-transparent fxg-1 bdw-0 recordable rinited" id="productos" placeholder="Ingresar código producto" type="text" >
                          <a class="rec"><i class="fa fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <img style="width: 30px;"  src="<?php echo base_url() ?>img/mango.png">
                      </div>
                    </div>  
                  </div>  
                </div>    
                <hr>
                <div class="row btn_aceptar"> 
                  <div class="col-md-12" align="center">
                    <button class="btn btn-round btn_orange" style="width: 170px;" onclick="buscar_producto_tras()"><i class="ft-save"></i> Aceptar</button>
                  </div>
                </div> 
                <div class="info_traspaso"></div>  
              </div>
          </div>
      </div>
  </div>
</div>

<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_traspaso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Desea <b>cancelar</b> este traspaso?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="cancelar_traspaso()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="traspaso_cantidad_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Modificación de cantidad</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12" align="center">
                  <fieldset class="form-group">
                      <label for="basicInput">Contraseña</label>
                      <input type="password" class="form-control" id="passw" style="text-align: center">
                  </fieldset>
                </div>
              </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="verificar_pass()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="traspaso_partida_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Quitar partida de traspaso</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12" align="center">
                  <fieldset class="form-group">
                      <label for="basicInput">Contraseña</label>
                      <input type="password" class="form-control" id="passw2" style="text-align: center">
                  </fieldset>
                </div>
              </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="verificar_pass2()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>