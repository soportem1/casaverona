<div class="row">
  <div class="col-md-4">
    <h2>Listado de traspasos</h2>
  </div>
  <div class="col-md-3">
    <fieldset class="form-group">
      <select class="form-control" name="idsucursal" id="idsucursal" onchange="reload_registro()">
      <?php foreach ($get_sucursales as $item){ 
          if($item->id!=1){
        ?>
        <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
      <?php }
      } ?>  
      </select>
    </fieldset>
  </div>
  <div class="col-md-5" align="right">
    <a href="<?php echo base_url(); ?>Traspasos/registro" class="btn btn-round btn_orange"><i class="fa fa-plus"></i></span> Nuevo</a>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <br>
                <!--------//////////////-------->
                <table class="table table-striped" id="data_tables" style="width: 100%">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Fecha</th>
                      <th>Sucursal</th>
                      <th>Monto de traspaso</th>
                      <th>Resumen</th>
                      <th>Usuario</th>
                      <th>Estatus</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> este traspaso?
                <input type="hidden" id="id_aux">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="delete_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="ver_resumen_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Resumen</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <h4>Productos</h4>
              <div class="row">
                <div class="col-md-12">
                  <div class="tabla_productos"></div>
                </div>
              </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>