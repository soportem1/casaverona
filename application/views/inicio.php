<style type="text/css">
    .gradient_nepal2 {
    background-image: linear-gradient(177deg, #5f5d5d, #bdbcb9) !important;
    background-repeat: repeat-x !important;
    border-radius: 60px;
    color: white!important;
    border-color: #86817f  !important;
    font-weight: bold !important;
  }
</style>
<?php if($idsucursal==1) {?>
<div class="row">
    <div class="col-sm-6">
        <h5 class="card-title">Productos más vendidos</h5>   
    </div>
    <div class="col-sm-6">
        <h5 class="card-title" align="right">Fecha actual: <?php echo $fechaactual_normal ?></h5> 
    </div>
</div>       
<!-- Bar Chart -->

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-block" align="center">
                            <h5><b>Accesos directos</b></h5>
                            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Clientes">
                                <i class="fa fa-user"></i> Clientes
                            </a>
                            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Ventas">
                                <i class="fa fa-shopping-cart"></i> Ventas
                            </a>
                            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>ListaVentas">
                                <i class="fa fa-cogs"></i> Listado de ventas
                            </a>
                            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Cotizaciones">
                                <i class="fa fa-newspaper-o"></i> Cotizaciones
                            </a>
                            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Traspasos">
                                <i class="fa fa-exchange"></i> Traspasos
                            </a>
                            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Corte_caja">
                                <i class="fa fa-cogs"></i> Corte de caja
                            </a>
                            <a class="btn gradient_nepal2" href="<?php echo base_url(); ?>Pedidos">
                                <i class="fa fa-cogs"></i> Pedidos
                            </a>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>            
</div>                                
<div class="col_texto"></div>
<?php } ?>
