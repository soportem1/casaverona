<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
if (!isset($_SESSION['idpersonal_tz'])) {
    $perfil=0;
    $sucursal=0;
}else{
    $perfil=$_SESSION['idpersonal_tz'];  
    $sucursal=$_SESSION['idsucursal_tz'];  
}
if(!isset($_SESSION['usuario_tz'])){
    ?>
    <script>
        document.location="<?php echo base_url(); ?>index.php/Login"
    </script>
    <?php
}

$menu=$this->ModeloSession->menus($perfil);
?>
<style type="text/css">
  .logo {
    background-color: white;  !important;
  }
  .dsfsd{
    background-color: #fff6db;
  }
  .sidebar-content{
    background: #d6d5d3  !important;
    color: black !important;
  }
  .app-sidebar[data-background-color='warning'] .navigation i, .app-sidebar[data-background-color='warning'] .navigation li > a, .off-canvas-sidebar[data-background-color='warning'] .navigation i, .off-canvas-sidebar[data-background-color='warning'] .navigation li > a {
    color: #000000 !important;
  }
  .btn_sesion{
    color: #FFFFFF;
    background-color: #b3b3b3;
    border-color: #b3b3b3;
  }
  .btn_amarillo{
    color: #FFFFFF;
    background-color: #b3b3b3;
    border-color: #b3b3b3;
  }
  .btn_orange {
    color: #FFFFFF;
    background-color: #f2752c;
    border-color: #f2752c;
  }
  .btn_crema {
    color: black;
    background-color: #fff6db;
    border-color: #fff6db;
  }
  .card {
    border: 3px solid #92908c;
    border-radius: 15px;
  }
  .tx_orange{
    color: #f2752c; 
  }
  .form-control {
    border-radius: 19px;
  }
  .transparente{
    color: #ff000000;
  }
  .page-item.active .page-link {
    z-index: 2;
    color: #FFFFFF;
    background-color: #f2b327;
    border-color: #f2752c;
  }

  .page-link {
    color: black;
  }
  .btn[class*='btn-'], .fc button[class*='btn-'] {
    margin-bottom: 0rem;
  }
  .table th, .table td {
    padding: 0.50rem;
    vertical-align: revert;
    border-top: 1px solid #E9ECEF;
  }
  .form-group {
     margin-bottom: 0rem;
  }
  .vd_red{
    color: red;
  }
  .vd_green{
    color: #4caf50;
  }

  .btn_orange_borde {
    color: #FFFFFF !important;
    background-color: #f2752c;
    border-color: #f2b327;
  }
  .btn_sinfondo {
    color: #4caf50 !important;
    background-color: #f2b32700;
    border-color: #f2b32700;
  }
  .titulo_alert{
    position: absolute;
    top: 24px;
    font-size: 12px;
    left: 0px;
  }
</style>
<!-- main menu-->
<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div data-active-color="white" data-background-color="warning" data-image="<?php echo base_url(); ?>public/img/inicio_2a.JPG" style="background: url(<?php echo base_url(); ?>public/img/inicio_2a.JPG);" class="app-sidebar">
<!-- main menu header-->
<!-- Sidebar Header starts-->
<div class="sidebar-header">
  <div class="logo clearfix" style="background: white;padding-bottom: 5px;padding-top: 5px;">
      <a href="<?php echo base_url(); ?>" class="logo-text float-left">
        <div class="logo-img">
          
        </div>
        <span class="text pull-left">
          <img src="<?php echo base_url(); ?>img/pos.png" style="width: 40%; position: absolute; left: 66px; top: -6px;" alt="Logo" >
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </span>
      </a>
      <a id="sidebarToggle" href="javascript:;" class="nav-toggle d-none d-sm-none d-md-none d-lg-block" style="color: #d07731;"><i data-toggle="expanded" class="ft-toggle-right toggle-icon"></i></a><a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a></div>
</div>
<!-- Sidebar Header Ends-->
<!-- / main menu header-->
<!-- main menu content-->
<div class="sidebar-content ps-container ps-theme-default ps-active-y">
  <div class="nav-container">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
        <!--
          <li class="nav-item"><a href="#" data-target='#fnotas' data-toggle="modal"><i class="fa fa-pencil"></i><span data-i18n="" class="menu-title">Notas</span></li></a>
        -->
        <?php foreach ($menu->result() as $item){ ?>
          <li class="has-sub nav-item"><a href="#"><i class="<?php echo $item->Icon; ?>"></i><span data-i18n="" class="menu-title"><?php echo $item->Nombre; ?></span></a>
            <ul class="menu-content">
                <?php 
                    $perfil=$perfil;
                    $menu =  $item->MenuId;
                    $menusub = $this->ModeloSession->submenus($perfil,$menu);
                    foreach ($menusub->result() as $datos) { ?>
                        <li>
                            <a href="<?php echo base_url(); ?><?php echo $datos->Pagina; ?>" class="menu-item">
                                <i class="<?php echo $datos->Icon; ?>"></i>
                                <?php echo $datos->Nombre; ?>
                            </a>
                        </li>
                <?php } ?>
              
            </ul>
          </li>
        <?php } ?>
    </ul>
  </div>
</div>
<!-- main menu content-->
<div class="sidebar-background"></div>
<!-- main menu footer-->
<!-- include includes/menu-footer-->
<!-- main menu footer-->
<?php 
$notas = $this->ModeloSession->notas();
foreach ($notas->result() as $notasf) { 
  $nmensaje=$notasf->mensaje;
  $nuser=$notasf->usuario;
  $nactualizado=$notasf->reg;
  
}?>

</div>

<div class="modal fade text-left" id="fnotas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Notas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      <fieldset class="form-group">
                          <textarea class="form-control" id="notass" rows="5"><?php echo $nmensaje;?></textarea>
                      </fieldset>                                
                  </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <p><b>Actualizado: </b><?php echo $nuser;?> / <?php echo $nactualizado;?></p>
                </div>
              </div>
              <input type="type" id="notasedit2" readonly hidden value="<?php echo $_SESSION['usuario_tz']; ?>">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" data-dismiss="modal" id="updatenotas">Actualizar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!-- / main menu-->

<!-- Navbar (Header) Starts-->
      <nav class="navbar navbar-expand-lg navbar-light bg-faded" >
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" data-toggle="collapse" class="navbar-toggle d-lg-none float-left"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          </div>
          <div class="navbar-container">
            <div id="navbarSupportedContent" class="collapse navbar-collapse">
              <ul class="navbar-nav">
                <li class="nav-item mr-2">
                  <a class="btn white btn-round btn_sesion" href="<?php echo base_url(); ?>index.php/Login/exitlogin">Cerrar sesión</a>
                </li>
                <!-- 
                <li class="nav-item mr-2"><a id="navbar-fullscreen" href="javascript:;" class="nav-link apptogglefullscreen"><i class="ft-maximize font-medium-3 blue-grey darken-4"></i>
                    <p class="d-none">fullscreen</p></a></li>
                -->
                <!--<li class="dropdown nav-item"><a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-flag font-medium-3 blue-grey darken-4"></i><span class="selected-language d-none"></span></a>
                  <div class="dropdown-menu dropdown-menu-right"><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/us.png" class="langimg"/><span> English</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/es.png" class="langimg"/><span> Spanish</span></a><a href="javascript:;" class="dropdown-item py-1"><img src="../app-assets/img/flags/br.png" class="langimg"/><span> Portuguese</span></a><a href="javascript:;" class="dropdown-item"><img src="../app-assets/img/flags/de.png" class="langimg"/><span> French</span></a></div>
                </li>-->
                <?php 
                $get_sucusales = $this->ModeloSession->get_all_sucursales($sucursal); 
                foreach ($get_sucusales as $s) { ?>
                  <li class="dropdown nav-item">
                    <a id="dropdownBasic2" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle" style="width: 82px;">
                      <i class="ft-bell font-medium-3 blue-grey darken-4"></i>

                      <span class="notification badge badge-pill num_sucu_<?php echo $s->id ?> <?php if($this->ModeloSession->alertproductos_sucursal($s->id)>0){ echo 'badge-danger';} ?>"><?php echo $this->ModeloSession->alertproductos_sucursal($s->id);?>
                      </span>
                      <b  class="titulo_alert"><?php echo $s->nombre ?></b>
                    </a>
                    
                    <div class="notification-dropdown dropdown-menu dropdown-menu-right">
                      <div class="noti-list">
                        <br>
                        <div class="row">
                          <div class="col-sm-12">
                              <div class="card-block">
                                <div class="row">
                                  <div class="col-md-12">
                                    <h5 align="center">Productos con existencia baja</h5>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div> 
                        <hr style="border-color: #ff9800;">           
                        <?php 
                            $resultado = $this->ModeloSession->alertproductosall_sucursal($s->id);
                            foreach ($resultado->result() as $item){
                        ?>
                        <div class="row alert_prod_<?php echo $item->productoid; ?>_<?php echo $item->idsucursal; ?>">
                            <div class="col-sm-12">
                                <div class="card-block">
                                  <div class="row">
                                    <div class="col-md-1">
                                    </div>
                                    <div class="col-md-1">
                                      <i class="fa fa-shopping-cart tx_orange float-left d-block font-large-1 mt-1 mr-2"></i>
                                    </div>
                                    <div class="col-md-6">
                                      <span class="noti-title line-height-1 d-block text-bold-400 "><?php echo $item->nombre; ?> </span>
                                      <span class="noti-text danger">(<?php echo $item->stock; ?>)</span>
                                    </div>
                                    <div class="col-md-4">
                                      <button type="button" class="btn mr-1 mb-1 btn_orange btn-sm" onclick="href_traspaso()">Surtir</button>
                                      <button type="button" class="btn btn-raised btn-outline-danger round btn-min-width mr-1 mb-1 btn-sm"
                                      onclick="producto_status(<?php echo $item->productoid; ?>,<?php echo $item->idsucursal; ?>)">
                                        <i class="fa fa-times-circle-o"></i>
                                      </button>
                                    </div>  
                                  </div>
                                </div>
                            </div>
                        </div>
                        <hr style="border-color: #ff9800;" class="alert_prod_<?php echo $item->productoid; ?>_<?php echo $item->idsucursal; ?>">   
                        <!--
                        <a class="dropdown-item noti-container py-3 border-bottom border-bottom-blue-grey border-bottom-lighten-4">
                          <i class="fa fa-shopping-cart tx_orange float-left d-block font-large-1 mt-1 mr-2"></i>
                          <span class="noti-wrapper">
                            <span class="noti-title line-height-1 d-block text-bold-400 "><?php echo $item->nombre; ?> </span>
                            <span class="noti-text danger">(<?php echo $item->stock; ?>)</span>
                          </span>
                        </a>
                        -->
                        <?php } ?>
                      </div>
                      <a class="noti-footer primary text-center d-block border-top border-top-blue-grey border-top-lighten-4 text-bold-400 py-1"><!--Read All Notifications--></a>
                    </div>
                  </li>
                <?php } ?>

                <li class="dropdown nav-item">
                  <a id="dropdownBasic3" href="#" data-toggle="dropdown" class="nav-link position-relative dropdown-toggle"><i class="ft-user font-medium-3 blue-grey darken-4"> <?php echo $_SESSION['usuario_tz'];?></i>
                    <p class="d-none">User Settings</p></a>
                      <div ngbdropdownmenu="" aria-labelledby="dropdownBasic3" class="dropdown-menu dropdown-menu-right">
                        
                        <a href="<?php echo base_url(); ?>index.php/Login/exitlogin" class="dropdown-item"><i class="ft-power mr-2"></i><span>Cerrar</span></a>
                  </div>
                </li>
                
              </ul>
            </div>
          </div>
        </div>
      </nav>
      <!-- Navbar (Header) Ends-->
      <div class="main-panel">
        <div class="main-content">
          <div class="content-wrapper">