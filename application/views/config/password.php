<div class="row">
  <div class="col-md-10">
    <h2>Contraseña</h2>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <br>
                <!--------//////////////-------->
                <div class="row">
                  <div class="col-md-4">
                    <form class="form" method="post" role="form" id="form_registro_marca">
                      <fieldset class="form-group">
                          <label for="nombre_categoria">Contraseña</label>
                          <input type="text" id="pass" class="form-control round" value="<?php echo $get_pass->pass ?>">
                      </fieldset>
                    </form>                            
                  </div>
                  <div class="col-md-6">
                    <fieldset class="form-group">
                        <label style="color: #fff0 !important;">.</label><br>
                        <button type="button" class="btn btn-round btn_amarillo" id="generacodigo">
                             Generar
                        </button>
                    </fieldset>                          
                  </div>
                </div>  
                <div class="row">
                  <div class="col-md-12">
                    <button type="button" class="btn btn-round btn_orange" id="generacodigo" onclick="guardar()">
                      Guardar
                    </button>
                  </div>
                </div>    
                <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>