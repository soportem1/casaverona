<div class="row">
                <div class="col-md-12">
                  <h2>Lista Turnos</h2>
                </div>
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de Turnos</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                    <table class="table table-striped" id="data-tables-turnos">
                                      <thead>
                                        <tr>
                                          <th></th>
                                          <th>Sucursal</th>
                                          <th>Fecha</th>
                                          <th>Hora inicio</th>
                                          <th>Hora Cierre</th>
                                          <th>Nombre</th>
                                          <th>Estatus</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                        
                                            
                                      </tbody>
                                    </table>
                                    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
<!------------------------------------------------>


<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ventas del Turno</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Productos vendidos en el turno</p>
                <div class="row" align="center" >
                  <div class="col-md-12" id="tbCorte">
                  </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>