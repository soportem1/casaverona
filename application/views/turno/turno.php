<?php 
    $fechahoy =date('Y').'-'.date('m'). '-'.date('d'); 
?>
<style type="text/css">
    .card .card-block {
        padding: 0.5rem 0.5rem 0.5rem;
    }
    #sample_2 td{
        font-size: 13px;
    }
</style>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<input type="hidden" name="idturno" id="idturno" value="">
<input id="txtFin" name="txtFin" type="hidden" value="<?php echo $fechahoy; ?>" hidden/>
<input id="txtInicio" name="txtInicio" type="hidden" value="" hidden/>
<div class="row">
    <div class="col-md-12">
      <h2>Turno </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <div class="col-md-4">
                        <label>Sucursal</label>
                        <select class="form-control" id="idsucursal" onchange="consultarturno()">
                            <?php foreach ($get_sucursales as $item){ ?>
                              <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                            <?php } ?>  
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 inputbusquedas">
                        <div class="form-group">
                            
                            <div class="col-md-3">
                                <label class="control-label">Abrir Turno</label>
                                <input type="number" min="0" class="form-control" id="cantidadt" name="cantidadt" value="">
                            </div>
                            
                            <div class="col-md-3">
                                <label class="control-label " >Nombre del turno</label>
                                <input type="text" class="form-control" id="nombredelturno" name="nombredelturno" value="" >
                            </div>
                            <div class="col-md-6">
                                <button type="button" class="btn btn-round btn_orange" id="btnabrirt" style="margin-top: 29px;">Abrir Turno</button>
                                <button type="button" class="btn btn-round btn_orange" id="btncerrar" style="margin-top: 29px;">Inspeccionar</button>
                                <button type="button" class="btn btn-round btn_orange" id="cerrarturno" style="margin-top: 29px;">Cerrar Turno</button>
                                <a id="btnImprimir" onclick="imprimir();" >
                                    <button type="button" class="btn btn-round btn_orange"   style="margin-top: 29px;"><i class="fa fa-print"></i></button>
                                </a>
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-12 inputbusquedas">
                        <div class="form-group">
                            
                            <div class="col-md-3">
                                <label class="control-label">hora de inicio</label>
                                <input type="text" class="form-control"id="horainicial" name="horainicial" value="" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="imprimir">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">SUBTOTAL:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="dSubtotal">0</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">DESCUENTO:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="ddescuento">0</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">TOTAL:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="dTotal3">0.00</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">EN CAJA:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="val2">0.00</span>
                                    </span>
                                </p>
                                <p style="font-size: 20px">
                                    <span class="col-md-6 text-warning">TOTAL EN CAJA:</span>
                                    <span class="col-md-4" >
                                        <span class="text-warning">$</span>
                                        <span id="stotal2">0.00</span>
                                    </span>
                                </p>







                                
                                
                                
                                <!--
                                <label style="font-size: 20px" class="col-md-6 text-warning">Compras:</label> 
                                <label style="font-size: 20px" class="col-md-1 text-warning">$</label>
                                <label style="font-size: 20px" class="col-md-4" id="caja"><label id="comps">0.00</label> </label><br>-->

                                
                                <script src="http://code.jquery.com/jquery-latest.js"></script>
                            </div>
                        </div>
                        <div class="col-md-12" id="tbCorte" >
                          
                        </div>
                        
                    </div>
                    
                    
                </div>
               
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>