<div class="row">
  <div class="col-md-10">
    <h2>Proveedores</h2>
  </div>
  <div class="col-md-2" align="center">
    <a href="<?php echo base_url(); ?>Proveedores/proveedoradd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo</a>
  </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de Proveedores</h4>
          </div>
          <div class="card-body">
            <div class="card-block">
              <!--------//////////////-------->
              <table class="table table-striped" id="data_tables" style="width: 100%">
                <thead>
                  <tr>
                    <th>Razon social</th>
                    <th>Domicilio</th>
                    <th>Ciudad</th>
                    <th>Telefono local</th>
                    <th>Telefono celular</th>
                    <th>Email</th>
                    <th>RFC</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>

            </div>
          </div>
      </div>
  </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> este proveedor?
                <input type="hidden" id="id_aux">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieliminar" class="btn btn-round btn_orange" onclick="delete_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>