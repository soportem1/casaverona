<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>CASA VERONA</title>
    <!-- Favicon-->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>img/posicon.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>img/posicon.png">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">
    <script>
        var base_url="<?php echo base_url(); ?>";
    </script>
</head>
<style type="text/css">
    .btn_sesion{
        color: black !important;
        background-color: #a5a5a5  !important;
        border-radius: 17px !important;
    }
    .login-page {
        background-color: #e8e8e8 !important;
    }
    .cx3 {
        background-color: #c1c0bb !important;
    }
    .cx2 {
        background-color: #71706f !important;
    }
    .col_sesion {
        color: #585755  !important;
    }
    .card{
        background: white; !important;
        border-radius: 38px !important;
    }
    .input-group .input-group-addon .material-icons {
        font-size: -1px;
        color: #080808;
    }
    .form-control{
        border-radius: 17px !important;
        text-align: center !important;
    }
    .form-group .form-line {
        width: 100%;
        position: relative;
        border-bottom: 1px solid #ddd0;
    }
    .form-control{
        background-color: #eaeaea !important;
    }
</style>
<body class="login-page">
    <div class="sesion_texto" style="width: 400px;
        height: 400px;
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -200px;
        margin-top: -200px;
        z-index: 1;
        display: none;" align="center">
            <!-- style="background: #ffffffad; border-radius: 20px;" 
            
            -->   
        <img style="" src="<?php echo base_url();?>img/posicon.png" width="50%" class="m-t-90"  />
        
        <h2 style="color: white;">Iniciando Sesión</h2>
        <h3 style="color: white;">Conectado a la base de datos</h3>
    </div>
    <div class="cx3"></div>
    <div class="cx2"></div>
    <div class="cx1"></div>
    
    <div class="login-box">
        <div class="logo">
            <br>
        </div>
        <div class="card login_texto">
            <div class="body">
                <form id="login-form" method="POST">
                    <div class="row align-center">
                        <div class="col-md-6 " >
                           <img src="<?php echo base_url();?>img/pos.png" width="60%"  />
                        </div>
                        <div class="col-md-6" style="border-left: 1px solid #908f8a;">
                            <i class="material-icons font-40 m-t-20 col_sesion">lock</i>
                            <h2 class="col_sesion">Accede a tu cuenta</h2>
                            <h5 class="col_sesion">Estamos contentos de que regreses</h5>
                            <br>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="txtUsuario" id="txtUsuario" placeholder="Nombre de Usuario" required autofocus>
                                </div>
                            </div>
                            <div class="form-group input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="txtPass" id="txtPass" placeholder="Contraseña" required>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-xs-12">
                                    <button class="btn btn-block btn_sesion waves-effect" type="submit">INICIAR SESIÓN</button>
                                </div>
                            </div>
                            <br>
                            <div class="alert bg-pink" id="error" style="display:none">
                                <i class="material-icons ">error</i> <strong>Error!</strong> El nombre de usuario y/o contraseña son incorrectos 
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        <div>    
    </div>
    </div>
    <div class="text-center" id="loader">
        <div class="lds-ripple"><div></div><div></div></div>
    </div>
    <!--
    <div class="alert bg-light-green" id="success">
        <i class="material-icons ">done</i> <strong>Acceso Correcto!</strong> Será redirigido al sistema 
    </div>
    -->
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url();?>plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
</body>

</html>