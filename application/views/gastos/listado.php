<input type="hidden" id="sucursal_aux" value="<?php echo $idsucursal ?>">
<div class="row">
  <div class="col-md-10">
    <h2>Listado de gastos</h2>
  </div>
  <div class="col-md-2" align="center">
    <a onclick="registro_modal(0)" class="btn btn-round btn_orange" style="color:white !important"> Registrar gasto</a>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <input type="password"  class="form-control" disabled="" style="background-color: #ff000000; border: 1px solid rgb(0 0 0 / 0%); font-size: 0rem;">
                <!--------//////////////-------->
                <table class="table table-striped" id="data_tables" style="width: 100%">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                      <th>Monto</th>
                      <th>Fecha</th>
                      <th>Usuario</th>
                      <th>Sucursal</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="registro_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Registrar gastos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form method="post"  role="form" id="formgastos">
                <input type="hidden" name="idgasto" id="idgasto" value="0">
                <div class="row">
                  <div class="col-md-12">
                    <fieldset class="form-group">
                        <label for="basicInput">Nombre del gasto</label>
                        <input type="text" class="form-control" name="nombre" id="gasto">
                    </fieldset>
                  </div>
                </div>
                <hr style="border-color: #f2752c;">
                <div class="row">
                  <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="basicInput">Monto</label>
                        <input type="number" class="form-control" name="monto" id="monto">
                    </fieldset>
                  </div>
                  <div class="col-md-6">
                    <fieldset class="form-group">
                        <label for="basicInput">Fecha</label>
                        <input type="date" class="form-control" name="fecha" id="fecha">
                    </fieldset>
                  </div>
                </div>
                <?php if($idsucursal==1){ ?>
                <div class="row">
                  <div class="col-md-12">
                    <fieldset class="form-group">
                      <label>Seleccione sucursal</label>
                      <select class="form-control" name="idsucursal" id="idsucursal">
                      <?php foreach ($get_sucursales as $item){ ?>
                        <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                      <?php } ?>  
                      </select>
                    </fieldset>
                  </div>
                </div>    
                <?php } ?>
                
              </form>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange btn_registro" onclick="registrar()">Guardar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> este gasto?
                <input type="hidden" id="id_aux">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="delete_registro()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modal_verificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12" align="center">
                  <fieldset class="form-group">
                      <label for="basicInput">Contraseña</label>
                      <input type="password" class="form-control" id="passw" style="text-align: center">
                  </fieldset>
                </div>
              </div>  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="verificar_pass()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>