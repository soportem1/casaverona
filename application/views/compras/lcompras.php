<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Listado de Compras</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                <div class="row">
                  <div class="col-md-3">
                    <fieldset class="form-group">
                      <select class="form-control" id="idsucursal" onchange="reload_registro()">
                      <?php foreach ($get_sucursales as $item){ ?>
                        <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                      <?php } ?>  
                      </select>
                    </fieldset>
                  </div>
                  <div class="col-md-3">
                    <input type="date" id="productoscin" class="form-control" onchange="reload_registro()">
                  </div>
                  <div class="col-md-3">
                    <input type="date" id="productoscfin" class="form-control" onchange="reload_registro()">
                  </div>
                </div>  
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-striped" id="data_tables">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Fecha</th>
                          <th>Total</th>
                          <th>Sucursal</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="tbody_lcompras">
                      </tbody>
                    </table>
                  </div>
                </div>    
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade text-left" id="modal_detalle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Detalles de compra</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-striped" id="data_tables">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Precio</th>
                          <th>Total</th>
                          <th>Proveedor</th>
                        </tr>
                      </thead>
                      <tbody class="tbody_lcompras_detalle">
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!------------------------------------------------>

<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <!--<div id="iframereporte"></div>-->
            <!--</div>-->
            <div class="modal-body iframereporte">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>