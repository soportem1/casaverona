<style type="text/css">
  .h-60 {
      height: 60px;
  }
  .ai-center {
      align-items: center;
  }
  .lg\:ph-32 {
      padding-left: 32px;
      padding-right: 32px;
  }
  .bgc-moon, .hover\:bgc-moon:focus, .hover\:bgc-moon:hover {
      background-color: #f5f5fa;
  }
  .cursor-pointer {
      cursor: pointer;
  }
  .bdw-0 {
      border-width: 0;
  }
  .fxg-1 {
      flex-grow: 1;
  }
  .bgc-transparent {
      background-color: transparent;
  }
  .letra_t{
    font-size: 20px;
  } 
  .color_amarillo{
    color: #f2b327;
  }
  .borde_div{
    border-radius: 34px;
  }
</style>
<div class="row">
    <div class="col-md-12">
      <h2>Compras </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <!--
                    <div class="col-md-12">
                        <input type="checkbox" name="checkimprimir" id="checkimprimir" > <label for="checkimprimir"> Imprimir Ticket</label>
                    </div>
                    -->
                    <div class="col-sm-9">
                        <div class="form-group">
                            <label class="control-label">Proveedor:</label>
                            <select class="form-control" id="cproveedor" name="cproveedor"></select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label>Seleccionar sucursal</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <fieldset class="form-group">
                            <select class="form-control" id="idsucursal">
                                <?php foreach ($get_sucursales as $item){ ?>
                                  <option value="<?php echo $item->id ?>"><?php echo $item->nombre ?></option>
                                <?php } ?>  
                            </select>
                        </fieldset>
                    </div>
                </div>
                <hr>
                <div class="row">    
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Cantidad:</label>
                            <input type="number" name="vcantidad" id="vcantidad" class="form-control" min="0">
                        </div>
                    </div>  
                    <div class="col-md-2">
                        <div class="form-group">
                            <label class="control-label">Precio compra:</label>
                            <input type="number" name="cprecio" id="cprecio" class="form-control" min="0">
                        </div>
                    </div>   
                    <div class="col-md-8">
                        <!--
                        <div class="form-group">
                            <label class="control-label"><i class="fa fa-barcode"></i> Producto/Codigo:</label>
                            <select class="form-control" id="vproducto" name="vproducto"></select>
                        </div>
                        -->
                        <br>
                        <div class="recordableHolder">
                          <div class="borde_div h-60 lg:ph-32 bdr-4 d-flex ai-center bgc-moon">
                            <i class="fa fa-search color_amarillo" style="font-size: 25px;"></i>
                            &nbsp;&nbsp;&nbsp;
                            
                              <input class="letra_t bgc-transparent fxg-1 bdw-0 recordable rinited" id="productos" placeholder="Buscar producto" type="text" oninput="campo_vacio()">
                              <a class="rec"><i class="fa fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                            
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <img style="width: 30px;"  src="<?php echo base_url() ?>img/mango.png">
                          </div>
                        </div>  
                        <div style="position: relative;">
                          <div class="producto_buscar_t" style="position: absolute; top: 25px; z-index: 3;">  
                          </div>
                        </div>
                    </div>   
                </div>
                <hr>
                <div class="row">     
                    <div class="col-md-12">
                        <table class="table table-hover" id="productosv">
                            <thead>
                                <tr>
                                    <th>Clave</th>
                                    <th>Cantidad</th>
                                    <th>Proveedor</th> 
                                    <th>Producto</th>
                                    <th>Precio/U</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody id="class_productos"> 
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                      <a href="#" class="btn btn-round btn_amarillo" onclick="limpiar()">Limpiar</a>
                    </div>   
                    <div class="col-md-4">
                    </div>  
                    <div class="col-md-3 btn-group">
                          <h4><b>Total:</b></h4>&nbsp;&nbsp;
                          <div class="form-group">
                            <input type="texto" id="total_p_total_texto" class="form-control" value="$0.00" readonly  style="background-color: #f2b327;color: black; font-size: 22px; padding: 0.3rem 0.55rem;">
                          </div>  
                          <input type="hidden" name="vtotal" id="vtotal" class="form-control">
                          <input type="hidden" name="vsbtotal" id="vsbtotal" class="form-control" readonly>
                    </div>
                    <div class="col-md-2">
                      <a class="btn btn-round btn_orange white" id="ingresaventa">Ingresar Compra</a>
                    </div>  

                </div> 
        <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>