<style type="text/css">
  .h-60 {
      height: 60px;
  }
  .ai-center {
      align-items: center;
  }
  .lg\:ph-32 {
      padding-left: 32px;
      padding-right: 32px;
  }
  .bgc-moon, .hover\:bgc-moon:focus, .hover\:bgc-moon:hover {
      background-color: #f5f5fa;
  }
  .cursor-pointer {
      cursor: pointer;
  }
  .bdw-0 {
      border-width: 0;
  }
  .fxg-1 {
      flex-grow: 1;
  }
  .bgc-transparent {
      background-color: transparent;
  }
  .letra_t{
    font-size: 20px;
  } 
  .color_amarillo{
    color: #f2b327;
  }
  .borde_div{
    border-radius: 34px;
  }
  .custom-control-input:checked ~ .custom-control-indicator {
    color: #FFFFFF;
    background-color: #f6a169;
  }
  .custom-control-input:active ~ .custom-control-indicator {
    background-color: #f6a169;
  }

  :root {
      --color-green: #f2752c;
      --color-red: #b9b9b9;
      --color-button: #fdffff;
      --color-black: #000;
  }


  .switch-button .switch-button__checkbox {
      display: none;
  }
  .switch-button .switch-button__label {
      background-color: var(--color-red);
      width: 3rem;
      height: 1.5rem;
      border-radius: 1rem;
      display: inline-block;
      position: relative;
  }
  .switch-button .switch-button__label:before {
      transition: .2s;
      display: block;
      position: absolute;
      width: 1.5rem;
      height: 1.5rem;
      background-color: var(--color-button);
      content: '';
      border-radius: 50%;
      box-shadow: inset 0px 0px 0px 1px var(--color-black);
  }
  .switch-button .switch-button__checkbox:checked + .switch-button__label {
      background-color: var(--color-green);
  }
  .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
      transform: translateX(1.5rem);
  }
</style>
<div class="row">
  <div class="col-md-10">
    <h2><?php echo $titulo ?> Pedido</h2>
  </div>
</div>
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <br>
                <!--------//////////////-------->
                <div class="row">
                  <div class="col-md-6">
                    <h3>Datos del cliente</h3>
                  </div>
                  <div class="col-md-6" align="right">
                    <a href="<?php echo base_url() ?>Cotizaciones" class="btn btn-round btn_amarillo" style="color:white !important"> Regresar</a>
                  </div>
                </div>
                <hr>
                <input type="hidden" readonly id="partidas_aux" value="<?php echo $partidas_aux ?>">
                <input type="hidden" readonly id="terminos_aux" value="<?php echo $terminos_aux ?>">
                <form method="post"  role="form" id="form_regitro">
                  <input type="hidden" readonly name="id" id="idreg" value="<?php echo $id ?>">
                  <div class="row">
                    <div class="col-md-6">
                      <fieldset class="form-group">
                          <label>Cliente</label>
                          <input type="text" class="form-control" name="personacontacto" value="<?php echo $personacontacto ?>">
                      </fieldset>
                    </div>
                    <div class="col-md-6">
                      <fieldset class="form-group">
                          <label>Empresa</label>
                          <input type="text" class="form-control" name="empresa" value="<?php echo $empresa ?>">
                      </fieldset>
                    </div>
                    <div class="col-md-4">
                      <fieldset class="form-group">
                          <label>Teléfono</label>
                          <input type="text" class="form-control" name="telefono" value="<?php echo $telefono ?>">
                      </fieldset>
                    </div>
                    <div class="col-md-8">
                      <fieldset class="form-group">
                          <label>Correo</label>
                          <input type="text" class="form-control" name="correo" value="<?php echo $correo ?>">
                      </fieldset>
                    </div>
                    <div class="col-md-12">
                      <fieldset class="form-group">
                          <label>Dirección</label>
                          <input type="text" class="form-control" name="direccion" value="<?php echo $direccion ?>">
                      </fieldset>
                    </div>
                    <?php /*
                  	<div class="col-md-6">
                      <fieldset class="form-group">
                          <label>Nombre / Empresa</label>
                          <select class="form-control" id="vcliente" name="cliente">
                          <?php if($cliente!=0){ ?>
                             <?php echo '<option value="'.$cliente.'">'.$clientetxt.'</option>'; ?>
                          <?php } ?>  
                          </select>
                      </fieldset>
                    </div>
                    */?>  
                    <div class="col-md-3">
                      <fieldset class="form-group">
                          <label>Folio de pedido</label>
                          <input type="text" readonly class="form-control" name="folio" id="folio" value="<?php echo $folio ?>">
                      </fieldset>
                    </div>
                    <div class="col-md-3">
                      <fieldset class="form-group">
                          <label>Fecha alta de pedido</label>
                          <input type="date" readonly class="form-control" name="fechapedido" value="<?php echo $fechapedido ?>">
                      </fieldset>
                    </div>
                    <div class="col-md-3">
                      <fieldset class="form-group">
                          <label>Fecha de entrega</label>
                          <input type="date" class="form-control" name="fechaentrega" value="<?php echo $fechaentrega ?>">
                      </fieldset>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                          <label>por producto abierto</label>
                    </div>
                    <div class="col-md-5">
                      <div class="switch-button">
                          <!-- Checkbox -->
                          <input type="checkbox" id="check_busqueda" class="switch-button__checkbox" checked onclick="partidas()">
                          <!-- Botón -->
                          <label for="check_busqueda" class="switch-button__label"> </label>
                      </div>
                    </div>  
                  </div>  
                </form>
                <br>
                <div class="row">
                  <div class="col-md-2">
                    <h3>Partidas</h3>
                  </div>
                  <div class="col-md-5">
                    <!--
                    <div class="mt-1">
                      <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                        <input type="checkbox" class="custom-control-input" id="check_iva" onclick="incluir_iva()" <?php // if($uncluir=='on') echo 'checked' ?>>
                        <span class="custom-control-indicator"></span>
                        ¿Desea incluir IVA?
                      </label>
                    </div> 
                    -->
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-md-12">  
                    <div class="recordableHolder">
                    <div class="borde_div h-60 lg:ph-32 bdr-4 d-flex ai-center bgc-moon">
                      <i class="fa fa-search color_amarillo" style="font-size: 25px;"></i>
                      &nbsp;&nbsp;&nbsp;
                        <!-- autocapitalize="off" -->
                        <input class="letra_t bgc-transparent fxg-1 bdw-0 recordable rinited" id="productos" placeholder="Buscar producto" type="text" autocorrect="off" >
                        <a class="rec"><i class="fa fa-microphone" data-category="VoiceRecord" data-event="click" data-label="undefined"></i></a>
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      <span class="btn_buscar"><a type="button" onclick="validar_producto()"><img style="width: 30px;"  src="<?php echo base_url() ?>img/mango.png"></a></span>
                    </div>
                    </div>  
                  </div>  
                </div>    
                <!--------//////////////-------->
                <div class="row">
                  <div class="col-md-12">
                    <div style="position: relative;">
                        <div class="producto_buscar_t" style="position: absolute; top: 25px; z-index: 3;">  
                        </div>
                    </div>    
                  </div>
                </div>
                <br>
                <br>
                <div class="row">
                  <div class="col-md-12">
                    <table class="table table-striped" id="data_tables_productos" style="width: 100%">
                      <thead>
                        <tr>
                          <th>Nombre</th>
                          <th>Cantidad</th>
                          <th>Precio</th>
                          <th></th>
                          <th>Subtotal</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>  
                <div class="row">
                  <div class="col-md-7"></div>
                  <div class="col-md-5 sub_total_p" align="right">
                    <h4><b>Subtotal: $<span class="subtotal_p_subtotal">0.00</span></b></h4>
                  </div>
                  <div class="col-md-7"></div>
                  <div class="col-md-2" align="right"><h4><b>Descuento:</b></h4></div>
                  <div class="col-md-3">
                    <fieldset class="form-group">
                      <div class="form-group">
                          <b><h4><input style="text-align:right;" type="number" id="descuento" class="form-control" oninput="calculartotal()" value="<?php echo $cantdescuento ?>"></b></h4>
                          <!--calculartotal() -->
                      </div>   
                    </fieldset>
                  </div>  
                </div>
                <div class="row">

                  <!--
                  <div class="col-md-12 subtotal_p" align="right" style="display: none">
                    <h4><b>Subtotal: $<span class="subtotal_p_total"><?php // echo $subtotal ?></span></b></h4>
                  </div>
                  <div class="col-md-12 iva_p" align="right" style="display: none">
                    <h4><b>Iva: $<span class="iva_p_total"><?php // echo $iva ?></span></b></h4>
                  </div>
                  -->
                  
                  <div class="col-md-12 total_p" align="right">
                    <h4><b>Nuevo Subtotal: $<span class="total_p_total">0.00</span></b></h4>
                  </div>
                </div>  

                <div class="row">
                  <div class="col-md-7"></div>
                  <div class="col-md-2" align="right"><h4><b>Anticipo:</b></h4></div>
                  <div class="col-md-3">
                    <fieldset class="form-group">
                      <b><h4><input style="text-align:right;" type="number" class="form-control" name="anticipo" id="anticipo" oninput="anticipo()" value="<?php echo $anticipo ?>"></b></h4>
                    </fieldset>
                  </div>  
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12" align="right">
                    <h4 class="restatxt"></h4>
                  </div>  
                </div>  
                <br>
                <h3>Terminos y condiciones</h3>
                <hr> 
                <ul style="text-align: justify; font-size: 15px;">
                  <li>NO SE ACEPTAN CAMBIOS NI DEVOLUCIONES DE NINGUN TIPO UNA VEZ REALIZADO SU PEDIDO</li>
                  <li>TIEMPO DE ENTREGA SUJETA A PROGRAMACION </li>
                  <li>MATERIAL PUESTO EN PLANTA BAJA, NO HACEMOS ENTREGAS EN SEGUNDO PISO Y/O MAS</li>
                  <li>CLIENTE ACEPTA QUE EN MADERA, ONIX, MARMOL , GRANITOS POR SER PIEDRAS NATURALES TIENEN VARIACION EN TONO Y VETA Y PERMITEN HACER EL USO DE ESTUCO PARA CORREGIR DEFECTOS </li>
                  <li>NO CONTAMOS CON SERVICIO DE ALMACENAJE DESPUES DE LA FECHA DE ENTREGA DE SU PEDIDO POR LO CUAL NO NOS HACEMOS RESPONSABLES POR DAÑOS Y/O PERDIDAS</li>
                </ul>
                <!--
                <div class="tabla_terminos">
                  <div class="tabla_terminos2">
                    
                  </div>
                </div>
                -->
                  <hr>
                  <div class="row"> 
                    <div class="col-md-12" align="center">
                      <button class="btn btn-round btn_orange btn_registro" id="guardar_btn" ><i class="ft-save"></i> Guardar pedido</button>
                    </div>
                  </div>  
                <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_terminos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Desea <b>Eliminar</b> este termino y condición?
                <input type="hidden" id="id_termino_aux">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="delete_registro_termino()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="eliminar_partida" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Desea <b>Eliminar</b> esta partida?
                <input type="hidden" id="id_partida_aux">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="delete_registro_partidas()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>