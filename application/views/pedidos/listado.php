<style type="text/css">
  .border_eti{
    border-radius: 13px;
  }

  :root {
        --color-green: #f2752c;
        --color-red: #b9b9b9;
        --color-button: #fdffff;
        --color-black: #000;
    }
  .switch-button .switch-button__checkbox {
      display: none;
  }
  .switch-button .switch-button__label {
      background-color: var(--color-red);
      width: 3rem;
      height: 1.5rem;
      border-radius: 1rem;
      display: inline-block;
      position: relative;
  }
  .switch-button .switch-button__label:before {
      transition: .2s;
      display: block;
      position: absolute;
      width: 1.5rem;
      height: 1.5rem;
      background-color: var(--color-button);
      content: '';
      border-radius: 50%;
      box-shadow: inset 0px 0px 0px 1px var(--color-black);
  }
  .switch-button .switch-button__checkbox:checked + .switch-button__label {
      background-color: var(--color-green);
  }
  .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
      transform: translateX(1.5rem);
  }
</style>
<div class="row">
  <div class="col-md-10">
    <h2>Listado de Pedidos</h2>
  </div>
  <div class="col-md-2" align="center">
    <a href="<?php echo base_url() ?>Pedidos/formulario" class="btn btn-round btn_orange" style="color:white !important"> Nueva pedido</a>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-body">
              <div class="card-block">
                <br>
                <!--------//////////////-------->
                <table class="table table-striped" id="data_tables" style="width: 100%">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Cliente</th>
                      <th>Total</th>
                      <th>Anticipo</th>
                      <th>Fecha pedido</th>
                      <th>Fecha entrega</th>
                      <th>Usuario</th>
                      <th>Estatus</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <!--
                     <tr>
                      <td>Id</td>
                      <td>Cliente</td>
                      <td>Monto</td>
                      <td>Anticipo</td>
                      <td>Fecha</td>
                      <td>Usuario</td>
                      <td>Fecha</td>
                      <td><span class="badge btn_orange border_eti">Pendiente</span></td>
                      <td><div class="form-group">                        
                          <div class="btn-group" role="group" aria-label="Basic example">                            
                            <a class="btn btn-raised btn-icon btn_orange" href="http://localhost/casaverona/Cotizaciones/registro/20"><i class="ft-edit"></i></a>                              
                            <a class="btn btn-raised btn-icon btn_amarillo" target="block_" href="http://localhost/casaverona/Cotizaciones/documento/20"><i class="ft-printer"></i></a>
                            <a class="btn btn-raised btn-icon btn-danger white" title="Rechazar" onclick="modal_cancelar(20)"><i class="ft-x-square"></i></a>
                            <a class="btn btn-raised btn-icon btn_amarillo" target="block_" href="http://localhost/casaverona/Cotizaciones/documento/20"><b>$</b></a>
                          </div>                    
                        </div>
                      </td>
                    </tr>
                  -->
                  </tbody>
                </table>
                <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="editar_estatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Cambiar estatus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                    <h6>Proceso</h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        <div class="switch-button">
                            <input type="checkbox" name="switch-button" id="proceso" class="switch-button__checkbox" onclick="estatus_proceso()">
                            <label for="proceso" class="switch-button__label"> </label>
                        </div>
                    </div>    
                </div>
                <div class="col-md-6">
                    <div class="btn-group">
                    <h6>Entregado</h6>&nbsp;&nbsp;&nbsp;&nbsp; 
                        <div class="switch-button">
                            <input type="checkbox" name="switch-button" id="entrgado" class="switch-button__checkbox" onclick="estatus_entregada()">
                            <label for="entrgado" class="switch-button__label"> </label>
                        </div>
                    </div>    
                </div>
              </div> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="cancelar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>cancelar</b> este pedido?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" onclick="cancelar_pedido()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>
<div class="modal fade text-left" id="pagos_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Pagos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <span>Nombre del cliente: <span class="cliente_txt"></span></span><br>
                  <span>Fecha de pedido: <span class="fecha_pedido_txt"></span></span><br>
                  <span>Fecha de entrega: <span class="fecha_entrega_txt"></span></span><br>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <span><b>Total: <span class="total_t"></span></b></span><br>
                  <span><b>Anticipo: <span class="anticipo_t"></span></b></span><br>
                  <span><b>Descuento: <span class="descuento_t"></span></b></span><br>
                  <span><b>Restante: <span class="restante_t"></span></b></span><br>
                </div>
              </div>  
              <div class="row">
                <div class="col-md-12">
                  <table  class="table table-striped" style="width: 100%">
                    <thead>
                      <tr>
                        <th>Pago</th>
                        <th>Fecha</th>
                        <th></th>
                      </tr> 
                      <tr class="txt_abono">
                        <td><input type="number" class="form-control" id="pago"></td>
                        <td><input type="date" class="form-control" id="fecha" readonly value="<?php date_default_timezone_set('America/Mexico_City'); echo date('Y-m-d') ?>"></td>
                        <td><a class="btn btn-raised btn-icon btn_orange btn_pago" onclick="guardar_pago()"><b style="font-size: 13px;">+</b></a></td>
                      </tr>                  
                    </thead>
                    <tbody id="tbody_pago">
                      
                    </tbody>

                  </table>
                </div>
              </div>   
              <div class="row">
                <div class="col-md-12">
                  <div class="total_abono"></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
