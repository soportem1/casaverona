<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
    $GLOBALS['get_result_terminos']=$get_result_terminos;
    $GLOBALS['total']=$total;
    $GLOBALS['cantdescuento']=$cantdescuento;
    $GLOBALS['subtotal']=$total+$cantdescuento;
    $GLOBALS['estatus']=$estatus;
    $GLOBALS['anticipo']=$anticipo;
    $GLOBALS["resta_total"]=$resta_total;
    
//=======================================================================================
class MYPDF extends TCPDF {
    //===========================================================
      var $Void = ""; 
      var $SP = " "; 
      var $Dot = "."; 
      var $Zero = "0"; 
      var $Neg = "Menos";
      function ValorEnLetras($x, $Moneda ){ 
        $s=""; 
        $Ent=""; 
        $Frc=""; 
        $Signo=""; 
             
        if(floatVal($x) < 0) 
         $Signo = $this->Neg . " "; 
        else 
         $Signo = ""; 
         
        if(intval(number_format($x,2,'.','') )!=$x) //<- averiguar si tiene decimales 
          $s = number_format($x,2,'.',''); 
        else 
          $s = number_format($x,2,'.',''); 
            
        $Pto = strpos($s, $this->Dot); 
             
        if ($Pto === false) 
        { 
          $Ent = $s; 
          $Frc = $this->Void; 
        } 
        else 
        { 
          $Ent = substr($s, 0, $Pto ); 
          $Frc =  substr($s, $Pto+1); 
        } 

        if($Ent == $this->Zero || $Ent == $this->Void) 
           $s = "Cero "; 
        elseif( strlen($Ent) > 7) 
        { 
           $s = $this->SubValLetra(intval( substr($Ent, 0,  strlen($Ent) - 6))) .  
                 "Millones " . $this->SubValLetra(intval(substr($Ent,-6, 6))); 
        } 
        else 
        { 
          $s = $this->SubValLetra(intval($Ent)); 
        } 

        if (substr($s,-9, 9) == "Millones " || substr($s,-7, 7) == "Millón ") 
           $s = $s . "de "; 

        $s = $s . $Moneda; 
        if ($Moneda=='pesos') {
          $abreviaturamoneda='M.N.';
        }else{
          $abreviaturamoneda='U.S.D';
        }

        if($Frc != $this->Void) 
        { 
           $s = $s . " " . $Frc. "/100"; 
           //$s = $s . " " . $Frc . "/100"; 
        } 
        $letrass=$Signo . $s . " ".$abreviaturamoneda; 
        return ($Signo . $s . " ".$abreviaturamoneda);    
      } 
      function SubValLetra($numero) { 
          $Ptr=""; 
          $n=0; 
          $i=0; 
          $x =""; 
          $Rtn =""; 
          $Tem =""; 

          $x = trim("$numero"); 
          $n = strlen($x); 

          $Tem = $this->Void; 
          $i = $n; 
           
          while( $i > 0) 
          { 
             $Tem = $this->Parte(intval(substr($x, $n - $i, 1).  
                                 str_repeat($this->Zero, $i - 1 ))); 
             If( $Tem != "Cero" ) 
                $Rtn .= $Tem . $this->SP; 
             $i = $i - 1; 
          } 

           
          //--------------------- GoSub FiltroMil ------------------------------ 
          $Rtn=str_replace(" Mil Mil", " Un Mil", $Rtn ); 
          while(1) 
          { 
             $Ptr = strpos($Rtn, "Mil ");        
             If(!($Ptr===false)) 
             { 
                If(! (strpos($Rtn, "Mil ",$Ptr + 1) === false )) 
                  $this->ReplaceStringFrom($Rtn, "Mil ", "", $Ptr); 
                Else 
                 break; 
             } 
             else break; 
          } 

          //--------------------- GoSub FiltroCiento ------------------------------ 
          $Ptr = -1; 
          do{ 
             $Ptr = strpos($Rtn, "Cien ", $Ptr+1); 
             if(!($Ptr===false)) 
             { 
                $Tem = substr($Rtn, $Ptr + 5 ,1); 
                if( $Tem == "M" || $Tem == $this->Void) 
                   ; 
                else           
                   $this->ReplaceStringFrom($Rtn, "Cien", "Ciento", $Ptr); 
             } 
          }while(!($Ptr === false)); 

          //--------------------- FiltroEspeciales ------------------------------ 
          $Rtn=str_replace("Diez Un", "Once", $Rtn ); 
          $Rtn=str_replace("Diez Dos", "Doce", $Rtn ); 
          $Rtn=str_replace("Diez Tres", "Trece", $Rtn ); 
          $Rtn=str_replace("Diez Cuatro", "Catorce", $Rtn ); 
          $Rtn=str_replace("Diez Cinco", "Quince", $Rtn ); 
          $Rtn=str_replace("Diez Seis", "Dieciseis", $Rtn ); 
          $Rtn=str_replace("Diez Siete", "Diecisiete", $Rtn ); 
          $Rtn=str_replace("Diez Ocho", "Dieciocho", $Rtn ); 
          $Rtn=str_replace("Diez Nueve", "Diecinueve", $Rtn ); 
          $Rtn=str_replace("Veinte Un", "Veintiun", $Rtn ); 
          $Rtn=str_replace("Veinte Dos", "Veintidos", $Rtn ); 
          $Rtn=str_replace("Veinte Tres", "Veintitres", $Rtn ); 
          $Rtn=str_replace("Veinte Cuatro", "Veinticuatro", $Rtn ); 
          $Rtn=str_replace("Veinte Cinco", "Veinticinco", $Rtn ); 
          $Rtn=str_replace("Veinte Seis", "Veintiseís", $Rtn ); 
          $Rtn=str_replace("Veinte Siete", "Veintisiete", $Rtn ); 
          $Rtn=str_replace("Veinte Ocho", "Veintiocho", $Rtn ); 
          $Rtn=str_replace("Veinte Nueve", "Veintinueve", $Rtn ); 

          //--------------------- FiltroUn ------------------------------ 
          If(substr($Rtn,0,1) == "M") $Rtn = "Un " . $Rtn; 
          //--------------------- Adicionar Y ------------------------------ 
          for($i=65; $i<=88; $i++) 
          { 
            If($i != 77) 
               $Rtn=str_replace("a " . Chr($i), "* y " . Chr($i), $Rtn); 
          } 
          $Rtn=str_replace("*", "a" , $Rtn); 
          return($Rtn); 
      } 
      function ReplaceStringFrom(&$x, $OldWrd, $NewWrd, $Ptr) { 
        $x = substr($x, 0, $Ptr)  . $NewWrd . substr($x, strlen($OldWrd) + $Ptr); 
      } 
      function Parte($x) { 
          $Rtn=''; 
          $t=''; 
          $i=''; 
          Do 
          { 
            switch($x) 
            { 
               Case 0:  $t = "Cero";break; 
               Case 1:  $t = "Un";break; 
               Case 2:  $t = "Dos";break; 
               Case 3:  $t = "Tres";break; 
               Case 4:  $t = "Cuatro";break; 
               Case 5:  $t = "Cinco";break; 
               Case 6:  $t = "Seis";break; 
               Case 7:  $t = "Siete";break; 
               Case 8:  $t = "Ocho";break; 
               Case 9:  $t = "Nueve";break; 
               Case 10: $t = "Diez";break; 
               Case 20: $t = "Veinte";break; 
               Case 30: $t = "Treinta";break; 
               Case 40: $t = "Cuarenta";break; 
               Case 50: $t = "Cincuenta";break; 
               Case 60: $t = "Sesenta";break; 
               Case 70: $t = "Setenta";break; 
               Case 80: $t = "Ochenta";break; 
               Case 90: $t = "Noventa";break; 
               Case 100: $t = "Cien";break; 
               Case 200: $t = "Doscientos";break; 
               Case 300: $t = "Trescientos";break; 
               Case 400: $t = "Cuatrocientos";break; 
               Case 500: $t = "Quinientos";break; 
               Case 600: $t = "Seiscientos";break; 
               Case 700: $t = "Setecientos";break; 
               Case 800: $t = "Ochocientos";break; 
               Case 900: $t = "Novecientos";break; 
               Case 1000: $t = "Mil";break; 
               Case 1000000: $t = "Millón";break; 
            } 

            if($t == $this->Void) 
            { 
              $i = floatval($i) + 1; 
              $x = $x / 1000; 
              if($x== 0) $i = 0; 
            } 
            else 
               break; 
                  
          }while($i != 0); 
          
          $Rtn = $t; 
          switch($i) 
          { 
             Case 0: $t = $this->Void;break; 
             Case 1: $t = " Mil";break; 
             Case 2: $t = " Millones";break; 
             Case 3: $t = " Billones";break; 
          } 
          return($Rtn . $t); 
      }  
    //===========================================================
    //Page header
    public function Header() {
        $img_file = base_url().'img/san_header1.png';  
        //$this->Image($img_file, 0, 0, 210, 15, '', '', '', false, 330, '', false, false, 0); 
    }
    // Page footer
    public function Footer() {
        $html='';
        $html.='<table border="0">
                    <tr>
                        <td>
                            <table border="0">
                                <tr>
                                    <td colspan="2" width="70%">'.($this->ValorEnLetras($GLOBALS["subtotal"],'pesos')).'</td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>Subtotal</b></td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>$'.number_format(($GLOBALS["subtotal"]),2,'.',',').'</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" width="70%">'.($this->ValorEnLetras($GLOBALS["cantdescuento"],'pesos')).'</td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>Descuento</b></td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>$'.number_format(($GLOBALS["cantdescuento"]),2,'.',',').'</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" width="70%">'.($this->ValorEnLetras($GLOBALS["total"],'pesos')).'</td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>Nvo. Total</b></td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>$'.number_format(($GLOBALS["total"]),2,'.',',').'</b></td>
                                </tr>
                                <tr>
                                    <td colspan="2" width="70%">'.($this->ValorEnLetras($GLOBALS["anticipo"],'pesos')).'</td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>Anticipo</b></td>
                                    <td width="15%" style="text-align:right; font-size: 13px;"><b>$'.number_format(($GLOBALS["anticipo"]),2,'.',',').'</b></td>
                                </tr>
                                <!--<tr>
                                    <td colspan="2" width="70%">'.($this->ValorEnLetras($GLOBALS["total"],'pesos')).'</td>
                                    <td width="15%"><b>Total</b></td>
                                    <td width="15%"><b>$'.number_format(($GLOBALS["total"]),2,'.',',').'</b></td>
                                </tr>-->
                                <tr>
                                    <td colspan="2" width="70%"><i>'.($this->ValorEnLetras($GLOBALS["resta_total"],'pesos')).'</i></td>
                                    <td width="15%" style="text-align:right;"><i><b>Restan</b></i></td>
                                    <td width="15%" style="text-align:right;"><i><b>$'.number_format(($GLOBALS["resta_total"]),2,'.',',').'</b></i></td>
                                </tr>
                                <tr>
                                    <td width="55%" style="font-size: 11px;"><b>TIEMPO DE ENTREGA:</b> DESPUES DE SU CONFIRMACION Y PAGO</td>
                                    <td width="25%" style="font-size: 11px;"><b>CONDICION DE PAGO:</b></td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;"><b>CASA VERONA</b></td>
                                    <td style="font-size: 11px;">CONTADO</td>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;"></td>
                                    <td style="font-size: 11px;"></td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>NOTAS Y COMENTARIOS:</td>
                    </tr>
                    <tr>
                        <td>
                            <ul style="text-align: justify">';
                              $html.='<li style="font-size: 12px;">NO SE ACEPTAN CAMBIOS NI DEVOLUCIONES DE NINGUN TIPO UNA VEZ REALIZADO SU PEDIDO.</li>';
                              $html.='<li style="font-size: 12px;">TIEMPO DE ENTREGA SUJETA A PROGRAMACION.</li>';
                              $html.='<li style="font-size: 12px;">MATERIAL PUESTO EN PLANTA BAJA, NO HACEMOS ENTREGAS EN SEGUNDO PISO Y/O MAS.</li>';
                              $html.='<li style="font-size: 12px;">CLIENTE ACEPTA QUE EN MADERA, ONIX, MARMOL , GRANITOS POR SER PIEDRAS NATURALES TIENEN VARIACION EN TONO Y VETA Y PERMITEN HACER EL USO DE ESTUCO PARA CORREGIR DEFECTOS.</li>';
                              $html.='<li style="font-size: 12px;">NO CONTAMOS CON SERVICIO DE ALMACENAJE DESPUES DE LA FECHA DE ENTREGA DE SU PEDIDO POR LO CUAL NO NOS HACEMOS RESPONSABLES POR DAÑOS Y/O PERDIDAS.</li>';
                            $html.='</ul>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"></td>
                    </tr>
                </table>';
        $this->writeHTML($html, true, false, true, false, '');
        if($GLOBALS['estatus']==2){
          $this->SetXY(80, 100);
          $this->StartTransform();
          $this->Rotate(45);
          $this->SetFont('dejavusans', '', 30);
          $this->SetTextColor(255,0,0);
          $this->Cell(50,0,'E N T R E G A D O',0,1,'C',0,'');
          $this->StopTransform();
        }  
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Pedido');
$pdf->SetTitle('Pedido');
$pdf->SetSubject('Pedido');
$pdf->SetKeywords('Pedido');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', '10', '10'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('80'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
    $html='';
    $html.='<table border="0">
                <tr>
                    <td width="38%">
                        <img style="width:140px;" src="'.base_url().'img/pos.png">
                    </td>
                    <td width="31%" style="font-size:11px" align="center">
Av. Rafael Cortes #11,<br>
Esquina 25 Agosto.<br>
Tecali de Herrera, Pue.<br>
Telefono: 224 115 4010</td>
                    <td width="31%"  align="center">
<b style="font-size:17px">Pedido<br>';
if($folio!=''){
    $html.=$folio;
}
            $html.='</b><br><br>
            <span style="font-size:14px">
                    <b>Fecha alta de pedido:</b> '.date('d/m/Y',strtotime($fechapedido)).'
            </span>


            </td>
                </tr>
            </table>
            <table border="0">
                <tr>
                    <td>
                        <table border="1">
                            <tr>
                                <td>
                                    <table border="0">
                                        <tr>
                                            <td colspan="2" style="font-size:10px"><b>Cliente:</b> '.$clientetxt.'
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="60%" style="font-size:9px">
                                            </td>
                                            <td width="40%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:9px"><b>Dirección:</b> '.$clientedirecciom.' '.$clientedirecciom2.'
                                            </td>
                                            <td style="font-size:9px"><b>Número de teléfono:</b> '.$telefono.'
                                            </td>
                                        </tr>';
                                        /*
                                        <tr>
                                            <td style="font-size:9px"><b>RFC:</b> '.$extencionc.'
                                            </td>
                                        </tr>
                                        */
                                    $html.='</table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td></td>
                </tr>
            </table>
            ';


    //*****************************************************************************************//
    
    $html.='<table width="100%" border="0"> 
                <tbody> 
                    <tr> 
                        <td width="100%">
                            <h5 style="border-bottom-color: #f2752c;">Lista de productos / Servicios</h5>
                        </td> 
                    </tr> 
                </tbody>';
    $html.='</table>';
    $html.='<table width="100%" border="0"> 
                <thead>
                    <tr style="font-size: 12px;">
                        <th width="20%"><b>Cantidad</b></th>
                        <th><b>Producto</b></th>
                        <th><b>Precio</b></th>
                        <th><b>Subtotal</b></th>
                    </tr>
                </thead>
                <tbody>';
                foreach ($get_result_partidas as $item){
                  $pro='';
                  if($item->productoid==0){
                    $pro=$item->nombre_producto;
                  }else{
                    $pro=$item->nombre;
                  }
                  $html.='<tr style="font-size: 12px;"> 
                      <td width="20%">'.$item->cantidad.'</td>
                      <td>'.$pro.'</td>
                      <td>$'.number_format($item->precio, 2).'</td>
                      <td>$'.number_format($item->cantidad*$item->precio, 2).'</td>
                  </tr>';
                }
                $html.='</tbody>';
    $html.='</table>';
    
    


$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('cotizacion.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>