<?php

    require_once('TCPDF/examples/tcpdf_include.php');
    require_once('TCPDF/tcpdf.php');
    $this->load->helper('url');
    //var_dump($GLOBALS['folio']);die;
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $img_file = base_url().'img/san_header2.png';  
        $this->Image($img_file, 0, 0, 210, 15, '', '', '', false, 330, '', false, false, 0); 
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'img/san_footer.png'; 
        //$pdf->Image($img_file, 0, 0, 0, 500, '', '', '', false, 500, '', false, false, 0); 
        $this->Image($img_file, 0, 280, 210, 18, '', '', '', false, 330, '', false, false, 0); 
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Traspaso');
$pdf->SetTitle('Traspaso');
$pdf->SetSubject('Traspaso');
$pdf->SetKeywords('Traspaso');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('12', '20', '12'); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 13);
// add a page
$pdf->AddPage('P', 'A4');
    $html='<table width="100%" border="0"> 
                <tbody> 
                    <tr style="font-size: 15px;"> 
                        <td width="50%" align="center"> 
                            <img style="width:240px;" src="'.base_url().'img/logo_1.png">
                        </td>
                        <td width="50%" align="right">
                            <span style="font-size: 25px;">Traspasos</span><br>
                            <span>Sucursal:'.$sucursal.'</span><br>
                            <span>Fecha de creación: '.date('d/m/Y',strtotime($fecha)).'</span><br>
                            <span>Hora de creación: '.date('g:i:a',strtotime($hora)).'</span>   
                        </td> 
                    </tr> 
                </tbody>';
    $html.='</table>';
    $html.='<table width="100%" border="0"> 
                <tbody> 
                    <tr> 
                        <td width="100%">
                            <h5 style="border-bottom-color: #f2752c; border-bottom: 1px">Listado de productos</h5>
                        </td> 
                    </tr> 
                </tbody>';
    $html.='</table>';
    $html.='<table width="100%" border="0"> 
                <thead>
                    <tr style="font-size: 12px;">
                        <th width="20%"><b>Cantidad</b></th>
                        <th><b>Producto</b></th>
                    </tr>
                </thead>
                <tbody>';
                //<th>P. Unitario</th>
                foreach ($get_result_productos as $item){
                    $html.='<tr style="font-size: 12px;"> 
                        <td width="20%">'.$item->cantidad.'</td>
                        <td>'.$item->nombre.'</td> 
                    </tr>';
                }
                //<td>$'.$item->precio.'</td>
                $html.='</tbody>';
    $html.='</table>';
    /*
    $html.='<br>
            <br>
            <table width="100%" border="0"> 
                <tbody>';
                    $html.='<tr>
                        <td align="right">Total: $'.$total.'</td>
                    </tr>
                </tbody>';
    $html.='</table>'; 
    */
    if($codigo_barras!=''){ 
    $html.='<br>
            <br>
            <table>
                <table width="100%" border="0"> 
                <tbody>';
                    $html.='<tr>
                        <td align="center"><img style="width: 200px;" src="'.base_url().'uploads/codigo_barras/'.$codigo_barras.'"></td>
                    </tr>
                </tbody>';
    $html.='</table>';   
    }

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Traspaso.pdf', 'I');
//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
?>