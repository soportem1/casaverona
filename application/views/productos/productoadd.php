<?php 
if (isset($_GET['id'])) {
    //$id=$_GET['id'];
    $idaux=explode("-",$_GET['id']);
    $id=$idaux[0];
    $tipo=$idaux[1];
    $personalv=$this->ModeloProductos->getproducto($id);
    foreach ($personalv->result() as $item){
      $label='Editar Producto';
      $id = $item->productoid;
      $codigo = $item->codigo;
      $pag=$item->pag;
      $codigo_pag=$item->codigo_pag;
      $productofiscal = $item->productofiscal;
      $nombre = $item->nombre;
      $descripcion = $item->descripcion;
      $categoria = $item->categoria;
      $stock = $item->stock;
      $preciocompra = $item->preciocompra;
      $porcentaje = $item->porcentaje;
      $precioventa = $item->precioventa;
      $mediomayoreo = $item->mediomayoreo;
      $canmediomayoreo = $item->canmediomayoreo;
      $mayoreo = $item->mayoreo;
      $canmayoreo = $item->canmayoreo;
      $img_aux=$item->img;
      $img = '<img src="'.base_url().''.$item->img.'">' ;
      $especial1_precio = $item->especial1_precio;
      $especial1_cantidad = $item->especial1_cantidad;
      $especial2_precio = $item->especial2_precio;
      $especial2_cantidad = $item->especial2_cantidad;
  }
}else{
  $tipo=0;
  $label='Nuevo producto';
  $id=0;
  $productoid='';
  $codigo='';
  $pag=0;
  $codigo_pag='';
  $productofiscal='';
  $nombre='';
  $descripcion='';
  $categoria='';
  $stock='';
  $preciocompra='';
  $porcentaje=0;
  $precioventa='';
  $mediomayoreo='';
  $canmediomayoreo='';
  $mayoreo='';
  $canmayoreo='';
  $img_aux='';
  $img='';  
  $especial1_precio ='';
  $especial1_cantidad ='';
  $especial2_precio ='';
  $especial2_cantidad  ='';  
} 
if($pag==1){
  $pag_checked='checked';
}else{
  $pag_checked='';
}
$btn_ocultar='';
if($tipo==1){
  $btn_ocultar='style="display:none"';
}else{
  $btn_ocultar='style="display:block"';
}
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
    margin-bottom: 5px;
  }
  .vd_green{
    color: #009688; 
  }
  input,select,textarea{
    margin-bottom: 15px;
  }
  .nav-link{
    color: black;
  }
  .nav-link:hover{
     color:#f2752c
  }
  .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #f2752c;
    background-color: #F5F7FA;
    border-color: #DDDDDD #DDDDDD #F5F7FA;
  }
  .displaynone{
    display: none;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2><b><?php echo $label;?><b></h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <br>
          <div class="card-body">
              <div class="card-block form-horizontal">
                  <!--------//////////////-------->
                  <div class="row">
                    <form method="post"  role="form" id="formproductos">
                      <input type="hidden" name="productoid" id="productoid" value="<?php echo $id;?>">
                      <div class="col-md-9">
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-3 control-label">Código:</label>
                              <div class=" col-md-4">
                                <input type="text" class="form-control" name="codigo" id="pcodigo" value="<?php echo $codigo;?>" aria-describedby="button-addon2">
                              </div>
                              <div class="col-md-2" <?php echo $btn_ocultar ?>>
                                <a class="btn btn-round btn_amarillo" id="generacodigo" ><i class="fa fa-barcode"></i> Generar código de barras</a>
                              </div>
                              <!--
                              <div class="col-md-4">
                                <input type="checkbox" name="productofiscal" id="productofiscal" <?php  if($productofiscal==1){ echo 'checked';}?> >
                                <label for="productofiscal">Producto fiscal</label>
                                
                              </div>
                              -->
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                              <label class="col-md-2 control-label" style="display:none;">Paguete:</label>
                              <div class=" col-md-1" style="display:none;">
                                <input type="checkbox" class="form-control" name="pag" id="pag" value="1" aria-describedby="button-addon2" onchange="paq()" <?php echo $pag_checked;?> >
                              </div>
                              <div class="col-md-4 input_pag" style="display: none;">
                                <input type="text" class="form-control" name="codigo_pag" id="codigo_pag" value="<?php echo $codigo_pag;?>" aria-describedby="button-addon2">
                              </div>
                              <!--
                              <div class="col-md-4">
                                <input type="checkbox" name="productofiscal" id="productofiscal" <?php  if($productofiscal==1){ echo 'checked';}?> >
                                <label for="productofiscal">Producto fiscal</label>
                                
                              </div>
                              -->
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-3 control-label">Nombre:</label>
                            <div class=" col-md-9 ">
                              <input type="text" class="form-control" name="nombre" value="<?php echo $nombre;?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-3 control-label">Descripción:</label>
                            <div class=" col-md-9 ">
                              <textarea class="form-control" name="descripcion" ><?php echo $descripcion;?></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-3 control-label">Categoría:</label>
                            <div class=" col-md-9">
                              <div class="input-group">
                                  <select class="form-control" id="pcategoria" name="categoria">
                                <?php foreach ($categorias->result() as $item){ ?>      
                                  <option value="<?php echo $item->categoriaId; ?>" <?php if ($item->categoriaId==$categoria) { echo 'selected';} ?>    ><?php echo $item->categoria; ?></option>
                                <?php } ?>
                              </select>
                                  <button <?php echo $btn_ocultar ?> type="button" class="btn btn-raised btn-icon btn_orange" onclick="modal_categoria()">
                                    <i class="ft-plus-square"></i>
                                  </button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-3 control-label">Marca:</label>
                            <div class=" col-md-9">
                              <div class="input-group">
                                  <select name="marca" id="marca" class="form-control">
                                      <?php foreach ($get_marca as $x){ ?>
                                          <option value="<?php echo $x->idmarca ?>"><?php echo $x->nombre ?></option>
                                      <?php } ?>
                                  </select>
                                  <button <?php echo $btn_ocultar ?> type="button" class="btn btn-raised btn-icon btn_orange" onclick="modal_marcas()">
                                    <i class="ft-plus-square"></i>
                                  </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3">
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px; border: solid #a6a9ae 1px; border-radius: 12px;">
                                <?php
                                  if($img_aux!=''){
                                    echo $img;
                                  }else{
                                    echo '<img style="width: 144px;" src="'.base_url().'img/posicon.png">';
                                  }?>
                              </div>
                              <div>
                                  <span class="btn btn-round btn_amarillo btn-file" <?php echo $btn_ocultar ?>><i class="ft-camera"></i>
                                      <span class="fileinput-new" <?php echo $btn_ocultar ?>>Foto</span>
                                      <span class="fileinput-exists">Cambiar</span>
                                      <input type="file" name="img" id="imgpro" data-allowed-file-extensions='["jpg", "png"]'>
                                  </span>
                                  <a href="#" class="btn btn-round btn_amarillo fileinput-exists" data-dismiss="fileinput">Quitar</a>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-12">
                        <h3>Configuración de precios y sucursales</h3>
                        <hr>
                        <div class="row match-height">
                          <div class="col-xl-12">
                            <div class="card">
                              <div class="card-body">
                                <div class="card-block">
                                  <ul class="nav nav-tabs">
                                  <!-- for --> 
                                  <?php foreach ($get_sucursales as $i){
                                      if($i->id==1){ ?>
                                        <li class="nav-item">
                                          <a class="nav-link active" data-toggle="tab" href="#secu_<?php echo $i->id ?>"><i class="<?php echo $i->icon ?>"></i> <?php echo $i->nombre ?> </a>
                                        </li>
                                      <?php }else{ ?>
                                        <li class="nav-item">
                                          <a class="nav-link" data-toggle="tab" href="#secu_<?php echo $i->id ?>"><i class="<?php echo $i->icon ?>"></i> <?php echo $i->nombre ?> </a>
                                        </li>
                                  <?php }
                                      } ?>
                                  <!-- -->  
                                  </ul>
                                  <div class="row_detalles">
                                    <div class="tab-content px-1 pt-1">
                                    <!-- Contenido -->  
                                    <?php foreach ($get_sucursales as $i){
                                      
                                      $iddetalles=0;
                                      $stock=0;
                                      $stock_paq=0;
                                      $preciocompra=0;
                                      $porcentaje=0;
                                      $precioventa=0;
                                      $especial1_precio=0;
                                      $especial1_cantidad=0;
                                      $especial2_precio=0;
                                      $especial2_cantidad=0;
                                      $mediomayoreo=0;
                                      $canmediomayoreo=0;
                                      $mayoreo=0;
                                      $canmayoreo=0;
                                      $precio_paq=0;
                                      $id_sucu=intval($i->id);
                                      $id_pro=intval($id);
                                      $get_precios=$this->General_model->get_tabla('productos_sucursal_precios',array('idsucursal'=>$id_sucu,'productoid'=>$id_pro));
                                      
                                      foreach ($get_precios as $x) {
                                        $iddetalles=$x->id;
                                        $stock=$x->stock;
                                        $stock_paq=$x->stock_paq;
                                        $preciocompra=$x->preciocompra;
                                        $porcentaje=$x->porcentaje;
                                        $precioventa=$x->precioventa;
                                        /*
                                        $especial1_precio=$x->especial1_precio;
                                        $especial1_cantidad=$x->especial1_cantidad;
                                        $especial2_precio=$x->especial2_precio;
                                        $especial2_cantidad=$x->especial2_cantidad;
                                        $mediomayoreo=$x->mediomayoreo;
                                        $canmediomayoreo=$x->canmediomayoreo;
                                        $mayoreo=$x->mayoreo;
                                        $canmayoreo=$x->canmayoreo;
                                        */
                                        $especial1_precio=$x->precioventa;
                                        $especial1_cantidad=1;
                                        $especial2_precio=$x->precioventa;
                                        $especial2_cantidad=1;
                                        $mediomayoreo=$x->precioventa;
                                        $canmediomayoreo=1;
                                        $mayoreo=$x->precioventa;
                                        $canmayoreo=1;

                                        $precio_paq=$x->precio_paq;
                                      }
                                      if($i->id==1){ ?>
                                        <div role="tabpanel" class="tab-pane active" id="secu_<?php echo $i->id ?>">
                                          <div class="row">
                                            <div class="col-md-1">
                                              <p class="tx_orange"><?php echo $i->nombre ?></p>
                                            </div>
                                            <div class="col-md-11">
                                              <b>¿Desea incluir el mismo precio en todas las sucursales?</b>
                                              <input type="checkbox" class="incluir" id="incluir" onclick="incluir_tx()">
                                            </div>
                                          </div>
                                          <div class="row">
                                            <input type="hidden" id="idsucursal" value="<?php echo $i->id ?>">
                                            <input type="hidden" id="iddetalles" value="<?php echo $iddetalles ?>">
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Stock</label>
                                                  <input type="number" class="form-control" name="pstock" id="pstock" placeholder="Inventario" value="<?php echo $stock ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio Compra</label>
                                                  <input type="number" name="preciocompra" id="preciocompra" class="form-control preciocompra1 preciocompra_c_<?php echo $i->id ?>" value="<?php echo $preciocompra;?>" value="<?php echo $preciocompra ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">% de ganancia</label>
                                                  <input type="number" class="form-control porcentaje1 porcentaje_c_<?php echo $i->id ?>" name="porcentaje" id="porcentaje"  aria-describedby="button-addon2" oninput="calcular(<?php echo $i->id ?>)" value="<?php echo $porcentaje ?>">
                                              </fieldset>
                                            </div>
                                          </div>
                                          <div class="row input_pag" style="display: none;">
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Contenido Paquete</label>
                                                  <input type="number" class="form-control"  id="stock_paq" placeholder="Inventario" value="<?php echo $stock_paq ?>">
                                              </fieldset>
                                            </div>
                                            
                                          </div>
                                          

                                          <div class="row">
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio venta:</label>
                                                  <input type="number" class="form-control precioventa1 precioventa_c_<?php echo $i->id ?>" name="precioventa" id="precioventa" value="<?php echo $precioventa ?>">
                                              </fieldset>
                                            </div>
                                          </div>
                                          <div class="row displaynone">
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio especial 1:</label>
                                                  <input type="number" class="form-control especial1_precio1 especial1_precio_c_<?php echo $i->id ?>" id="especial1_precio" value="<?php echo $especial1_precio ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control especial1_cantidad1 especial1_cantidad_c_<?php echo $i->id ?>" id="especial1_cantidad" placeholder="a partir de # de unidades" value="<?php echo $especial1_cantidad ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio especial 2:</label>
                                                  <input type="number" class="form-control especial2_precio1 especial2_precio_c_<?php echo $i->id ?>" id="especial2_precio" value="<?php echo $especial2_precio ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control especial2_cantidad1 especial2_cantidad_c_<?php echo $i->id ?>" id="especial2_cantidad" placeholder="a partir de # de unidades" value="<?php echo $especial2_cantidad ?>">
                                              </fieldset>
                                            </div>
                                          </div>
                                          <div class="row displaynone">
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio medio mayoreo:</label>
                                                  <input type="number" class="form-control preciommayoreo1 preciommayoreo_c_<?php echo $i->id ?>" id="preciommayoreo" value="<?php echo $mediomayoreo ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control cpmmayoreo1 cpmmayoreo_c_<?php echo $i->id ?>" id="cpmmayoreo" placeholder="a partir de # de unidades" value="<?php echo $canmediomayoreo ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio mayoreo:</label>
                                                  <input type="number" class="form-control preciomayoreo1 preciomayoreo_c_<?php echo $i->id ?>" id="preciomayoreo" value="<?php echo $mayoreo ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control cpmayoreo1 cpmayoreo_c_<?php echo $i->id ?>" id="cpmayoreo" placeholder="a partir de # de unidades" value="<?php echo $canmayoreo ?>"> 
                                              </fieldset>
                                            </div>

                                            <div class="col-md-3 input_pag" style="display:none;">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio paquete:</label>
                                                  <input type="number" class="form-control precio_paquete precio_paquete_c_<?php echo $i->id ?>" id="precio_paquete" value="<?php echo $precio_paq ?>">
                                              </fieldset>
                                            </div>

                                          </div>
                                        </div>
                                      <?php }else{ ?>
                                        <div role="tabpanel" class="tab-pane" id="secu_<?php echo $i->id ?>">
                                          <p class="tx_orange"><?php echo $i->nombre ?></p>
                                          <div class="row">
                                            <input type="hidden" id="idsucursal" value="<?php echo $i->id ?>">
                                            <input type="hidden" id="iddetalles" value="<?php echo $iddetalles ?>">
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Stock</label>
                                                  <input type="number" class="form-control" name="pstock" id="pstock" placeholder="Inventario" value="<?php echo $stock ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio Compra</label>
                                                  <input type="number" name="preciocompra" id="preciocompra" class="form-control preciocompra  preciocompra_c_<?php echo $i->id ?>" value="<?php echo $preciocompra;?>" value="<?php echo $preciocompra ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">% de ganancia</label>
                                                  <input type="number" class="form-control porcentaje porcentaje_c_<?php echo $i->id ?>" name="porcentaje" id="porcentaje"  aria-describedby="button-addon2" onchange="calcular(<?php echo $i->id ?>)" value="<?php echo $porcentaje ?>">
                                              </fieldset>
                                            </div>
                                          </div>
                                          <div class="row input_pag" style="display: none;">
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Contenido Paquete</label>
                                                  <input type="number" class="form-control"  id="stock_paq" placeholder="Inventario" value="<?php echo $stock_paq ?>">
                                              </fieldset>
                                            </div>
                                            
                                          </div>
                                          <div class="row">
                                            <div class="col-md-4">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio venta:</label>
                                                  <input type="number" class="form-control precioventa precioventa_c_<?php echo $i->id ?>" name="precioventa" id="precioventa" value="<?php echo $precioventa ?>">
                                              </fieldset>
                                            </div>
                                          </div>
                                          <div class="row displaynone">
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio especial 1:</label>
                                                  <input type="number" class="form-control especial1_precio especial1_precio_c_<?php echo $i->id ?>" id="especial1_precio" value="<?php echo $especial1_precio ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control especial1_cantidad especial1_cantidad_c_<?php echo $i->id ?>" id="especial1_cantidad" placeholder="a partir de # de unidades" value="<?php echo $especial1_cantidad ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio especial 2:</label>
                                                  <input type="number" class="form-control especial2_precio especial2_precio_c_<?php echo $i->id ?>" id="especial2_precio" value="<?php echo $especial2_precio ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control especial2_cantidad especial2_cantidad_c_<?php echo $i->id ?>" id="especial2_cantidad" placeholder="a partir de # de unidades" value="<?php echo $especial2_cantidad ?>">
                                              </fieldset>
                                            </div>
                                          </div>
                                          <div class="row displaynone">
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio medio mayoreo:</label>
                                                  <input type="number" class="form-control preciommayoreo preciommayoreo_c_<?php echo $i->id ?>" id="preciommayoreo" value="<?php echo $mediomayoreo ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control cpmmayoreo cpmmayoreo_c_<?php echo $i->id ?>" id="cpmmayoreo" placeholder="a partir de # de unidades" value="<?php echo $canmediomayoreo ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio mayoreo:</label>
                                                  <input type="number" class="form-control preciomayoreo preciomayoreo_c_<?php echo $i->id ?>" id="preciomayoreo" value="<?php echo $mayoreo ?>">
                                              </fieldset>
                                            </div>
                                            <div class="col-md-3">
                                              <fieldset class="form-group">
                                                  <label for="basicInput"><span class="transparente">____</span></label>
                                                  <input type="number" class="form-control cpmayoreo cpmayoreo_c_<?php echo $i->id ?>" id="cpmayoreo" placeholder="a partir de # de unidades" value="<?php echo $canmayoreo ?>"> 
                                              </fieldset>
                                            </div>

                                            <div class="col-md-3 input_pag" style="display:none;">
                                              <fieldset class="form-group">
                                                  <label for="basicInput">Precio paquete:</label>
                                                  <input type="number" class="form-control precio_paquete precio_paquete_c_<?php echo $i->id ?>" id="precio_paquete" value="<?php echo $precio_paq ?>">
                                              </fieldset>
                                            </div>
                                          </div>
                                        </div>
                                    <?php }
                                      } ?> 
                                      
                                    <!-- -->  
                                    </div>
                                  </div> 
                                </div>
                              </div>
                            </div>
                          </div>
                        </div> 
                      </div>  
                    </form>
                    <div class="col-md-12" align="center">
                      <button <?php echo $btn_ocultar ?> style="width: 170px;" class="btn btn-round btn_orange btn_registro"  type="button"  id="savepr"><i class="ft-save"></i> Guardar</button>
                    </div>
                  </div>
                  <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>


<div class="modal fade text-left" id="modalcategoria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Categorías</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-10">
                  <form class="form" method="post" role="form" id="form_registro_categoria">
                    <fieldset class="form-group">
                        <label for="nombre_categoria">Nombre</label>
                        <input type="hidden" name="categoriaId" id="idcategoria_categoria" value="0">
                        <input type="text" name="categoria" id="nombre_categoria" class="form-control round" placeholder="Nombre de categoría">
                    </fieldset>
                  </form>                            
                </div>
                <div class="col-md-2">
                  <label style="color: #21252900">_</label><br>
                  <button class="btn btn-round btn_orange"  type="submit" id="savepr" onclick="guarda_registro_categoria()"><i class="ft-save"></i> </button>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-striped" id="table_datos_categoria">
                    <thead>
                      <tr>
                        <th>Categoría</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>    
                  </table>  
                </div>  
              </div>
            </div>  
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="elimina_registro_modal_cate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar ésta categoría? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_cate">
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-round btn_amarillo" onclick="delete_registro_cate()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>
<!--  Marca  -->

<div class="modal fade text-left" id="modal_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Marcas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-10">
                  <form class="form" method="post" role="form" id="form_registro_marca">
                    <fieldset class="form-group">
                        <label for="nombre_categoria">Nombre</label>
                        <input type="hidden" name="idmarca" id="idmarca_marca" value="0">
                        <input type="text" name="nombre" id="nombre_marca" class="form-control round" placeholder="Nombre de la marca">
                    </fieldset>
                  </form>                            
                </div>
                <div class="col-md-2">
                  <label style="color: #21252900">_</label><br>
                  <button class="btn btn-round btn_orange"  type="submit" id="savepr" onclick="guarda_registro_marca()"><i class="ft-save"></i> </button>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-striped" id="table_datos_marcas">
                    <thead>
                      <tr>
                        <th>Marca</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>    
                  </table>  
                </div>  
              </div>
            </div>  
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="elimina_registro_modal_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div align="center">
                    <h4 class="modal-title" id="myLargeModalLabel">Confirmación</h4>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h5>¿Está seguro que desea eliminar ésta marca? Esta operación no se puede deshacer.</h5>
            </div>
            <input type="hidden" id="id_marc">
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_orange" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-round btn_amarillo" onclick="delete_registro_marca()">Aceptar</button>
            </div>
        </div>   
    </div>
</div>

