<style type="text/css">
  .imgpro{
    width: 80px;
  }
  .imgpro:hover {
    transform: scale(2.6); 
    filter: drop-shadow(5px 9px 12px #444);
}
</style>
<input type="hidden" id="idsucursal" value="<?php echo $idsucursal ?>">

<div class="row">
  <div class="col-md-4">
    <h2><b> Productos </b></h2>
  </div>                        
</div>
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <br>
          <div class="tabla_productos"></div>
          <!--------//////////////-------->
        </div>
      </div>
    </div>
  </div>
</div>  

<!------------------------------------------------>
<style type="text/css">
  iframe{
        width: 100%;
        height: 300px;
        border: 0;
  }
</style>
<div class="modal fade text-left" id="modaletiquetas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Etiquetas</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="col-md-6">
                <p>Código de producto: <b><span id="ecodigo"></span></b> </p>
                <p>Producto: <b><span id="eproducto"></span></b></p>
                <p>Categoría: <b><span id="ecategoria"></span></b></p>
                <p>Precio: <b>$ <span id="eprecio"></span></b></p>
                <input type="hidden" name="idproetiqueta" id="idproetiqueta" readonly>
                <div class="col-md-12">
                  <div class="form-group">
                    <div class=" col-md-6">
                        <input type="number" name="numprint" id="numprint" class="form-control" min="1" value="1">
                    </div>
                    <div class=" col-md-5">
                      <a href="#" class="btn btn-round btn_orange" id="imprimiretiqueta">Imprimir</a>
                    </div>
                  </div>
                </div>

              </div>
              <div class="col-md-6" id="iframeetiqueta">
                
              </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="modalproductos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Productos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
              <div class="col-md-12" id="iframeproductos">
                
              </div>

           <!-- </div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade text-left" id="precio_sucu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Precios</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div class="modal-body">
              <div class="col-md-12">
                <div class="tabla_precios"></div>
              </div>  
            </div>
            <!-- </div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-left" id="existencia_sucu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Existencias</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div class="modal-body">
              <div class="col-md-12">
                <div class="tabla_existencias"></div>
              </div>  
            </div>
            <!-- </div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
