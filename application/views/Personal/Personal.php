<div class="row">
  <div class="col-md-10">
    <h2>Personal</h2>
  </div>
  <div class="col-md-2" align="center">
    <a href="Personal/Personaladd" class="btn btn-round btn_orange"><i class="fa fa-plus"></i> Nuevo</a>
  </div>
</div>

<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
          <h4 class="card-title">Listado de personal</h4>
      </div>
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <table class="table table-striped" id="data_tables">
            <thead>
              <tr>
                <th></th>
                <th>Nombre</th>
                <th>Usuario</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
           <!--------//////////////-------->
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade text-left" id="eliminar_registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmación</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> esta persona?
                <input type="hidden" id="id_per">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieliminar" class="btn btn-round btn_orange" onclick="personaldelete()">Aceptar</button>
                <button type="button" class="btn btn-round btn_amarillo" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<!------------------------------------------------>