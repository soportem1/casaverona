<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Traspasos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        

        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->hora = date('G:i');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
            if($this->idsucursal!=1){
                header('Location: '.base_url().'Inicio');
            } 
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('traspasos/listado',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/listadojs');
	}
    public function registro($id=0){
        $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
        $tras_detalle_aux=0;
        if($id==0){
            $data['id']=0;
            $data['fecha']=$this->fechaactual;
            $data['hora']=$this->hora;
            $data['sucursal']=0;
            $data['total']=0;
            $data['tras_detalle_aux']=$tras_detalle_aux;
            $data['tipo']='Nuevo';
        }else{
            $get_result_detalles=$this->General_model->get_records_condition('activo=1 AND idtraspaso='.$id,'traspasos_detalles');
            foreach ($get_result_detalles as $x){
                $tras_detalle_aux=1;
            }
            $get_result=$this->General_model->get_records_condition('id='.$id,'traspasos');
            foreach ($get_result as $item) {
                $data['id']=$item->id;
                $data['fecha']=$item->fecha;
                $data['sucursal']=$item->sucursal;
                $data['total']=$item->total;
                $data['tras_detalle_aux']=$tras_detalle_aux;
                $data['hora']=$item->hora;
            }
            $data['tipo']='Editar';
        }    
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('traspasos/traspaso',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/traspasojs');
    }
    public function get_productos(){
        $search = $this->input->get('search');
        $idsucursal = $this->input->get('idsucursal');
        $results=$this->ModelCatalogos->get_productos_traspasos($search,$idsucursal);
        echo json_encode($results);    
    }


    public function buscar_producto(){
        $nom=$this->input->post('producto');
        $idsucursal=$this->input->post('idsucursal');
        $html='';
        $result=$this->ModelCatalogos->get_productos_traspasos($nom,$idsucursal);
        echo json_encode($result);
    }

    public function deleteregistro_traspaso(){
        $id = $this->input->post('id');
        $cantidad = $this->input->post('cantidad');
        $productoid = $this->input->post('productoid');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'traspasos_detalles');
        $this->ModelCatalogos->stock_aumentar_producto($cantidad,1,$productoid);
    }

    // Validar existencia
    public function validar_exisntecias(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $validar=0;
        for ($i=0;$i<count($DATA);$i++) { 
            $productoid=$DATA[$i]->productoid;
            $cantidad=$DATA[$i]->cantidad;
            $get_result_stock=$this->General_model->get_records_condition('idsucursal=1 AND productoid='.$productoid,'productos_sucursal_precios');
            $stock=0;
            foreach ($get_result_stock as $y){
                $stock=$y->stock;
            }
            if($cantidad>=$stock){
                $validar=1;
            }
        }
        echo $validar;
    }
     
    public function registrar(){
        $data=$this->input->post();
        $idtras=$data['id'];
        unset($data['id']);
        if($idtras==0){
            $data['usuario']=$this->usuarioid;
            $data['reg']=$this->fechahoy;
            $id=$this->General_model->add_record('traspasos',$data);
        }else{
            $this->General_model->edit_record('id',$idtras,$data,'traspasos');
            $id=$idtras;
        }
        echo $id;
    }

    // Traspasos detalles
    public function insert_trapasos(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->idtrasdetalle;
            $data['productoid']=$DATA[$i]->productoid;
            $data['idtraspaso']=$DATA[$i]->idtraspaso;
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['tipo_precio']=$DATA[$i]->tipo_precio;
            $data['precio']=$DATA[$i]->precio;
            $data['subtotal']=$DATA[$i]->subtotal;
            if($id==0){
                $this->General_model->add_record('traspasos_detalles',$data);
                $this->ModelCatalogos->stock_descontar_producto($DATA[$i]->cantidad,1,$DATA[$i]->productoid);
            }else{
                $this->General_model->edit_record('id',$id,$data,'traspasos_detalles');
            }   
        }
    }
    

    public function get_traspasos_detalles(){
        $id = $this->input->post('id');
        $idsucursal = $this->input->post('idsucursal');
        $get_result=$this->ModelCatalogos->get_tras_detalles($id,$idsucursal);
        echo json_encode($get_result);
    }
    
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_traspasos($params);
        $totaldata= $this->ModelCatalogos->total_traspasos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  
    
    public function deleteregistro(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'traspasos');
    }


    public function tabla_produtos(){
        $id = $this->input->post('id');
        $get_result=$this->ModelCatalogos->get_traspasos_pruductos($id);
        $html='';
        foreach ($get_result as $x){
            $html.='<tr>
                    <td>'.$x->nombre.'</td>
                    <td>'.$x->cantidad.'</td>
                    <td>$'.$x->precio.'</td>
                    <td>$'.$x->subtotal.'</td>
                   </tr>';
        }
        echo $html;
    }

    public function documento($id){
        $data['get_result_productos']=$this->ModelCatalogos->get_traspasos_pruductos($id);
        $get_result=$this->General_model->get_records_condition('id='.$id,'traspasos');
        foreach ($get_result as $item) {
            $data['id']=$item->id;
            $data['total']=$item->total;
            $data['codigo_barras']=$item->codigo_barras;
            $data['fecha']=$item->fecha;
            $data['hora']=$item->hora;
            $get_result_sucu=$this->General_model->get_records_condition('id='.$item->sucursal,'sucursales');
            foreach ($get_result_sucu as $s){
                $data['sucursal']=$s->nombre;
            }    

        } 
        $this->load->view('Reportes/traspaso',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/listadojs');
    }

    function add_img_codigo_barras(){
      $id = $this->input->post('id');
      $datax = $this->input->post();
      $codigo = $this->input->post('codigo');
      $base64Image=$datax['codigo_barras'];
      $namefile=date('Y_m_d-h_i_s').'.png';
      $imagenBinaria = base64_decode(str_replace('data:image/png;base64,', '', $base64Image));
      file_put_contents('uploads/codigo_barras/'.$namefile, $imagenBinaria);
      $datos = array('codigo_barras'=>$namefile,'codigo'=>$codigo);
      $this->General_model->edit_record('id',$id,$datos,'traspasos');
    }
    
}