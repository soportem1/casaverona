<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('General_model');
        $this->load->model('ModeloProductos');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->fechaactual_normal = date('d/m/Y');
        $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        if (isset($_SESSION['idpersonal_tz'])){
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['total_rows'] = $this->ModeloProductos->filas();
        $data['totalexistencia'] = $this->ModeloProductos->totalproductosenexistencia();
        $data['totalproductopreciocompra'] = $this->ModeloProductos->totalproductopreciocompra();
        $data['totalproductoporpreciocompra'] = $this->ModeloProductos->totalproductoporpreciocompra();  
        $data['fechaactual_normal'] = $this->fechaactual_normal;
        $data['idsucursal'] = $this->idsucursal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('inicio',$data);
        $this->load->view('templates/footer');
        $this->load->view('iniciojs');
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
	}
    function notas(){
        $user = $this->input->post('user');
        $nota = $this->input->post('nota');
        $this->ModeloCatalogos->updatenota($user,$nota);
    }

    public function get_sucursales_all(){
        if($this->idsucursal==1){
            $wharesu='';
        }else{
            $wharesu=' AND id='.$this->idsucursal;
        }
        $result=$this->General_model->get_records_condition('activo=1'.$wharesu,'sucursales');

        echo json_encode($result);
    }
    
    public function sucursal_all(){
        $id = $this->input->post('id');
        $inicio = $this->fechaactual;
        $fin = $this->fechaactual;
        $resulta_ven_pro=$this->General_model->pro_venta($inicio,$fin,$id);
        echo json_encode($resulta_ven_pro);
    }

    public function get_sucursales_all_texto(){
        if($this->idsucursal==1){
            $wharesu='';
        }else{
            $wharesu=' AND id='.$this->idsucursal;
        }
        $result=$this->General_model->get_records_condition('activo=1'.$wharesu,'sucursales');
        $html='<div class="row">';
        foreach ($result as $x) {
           $html.='<div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-block chartjs">
                                            <canvas id="chart_'.$x->id.'" height="400"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-block">';
                                            $id = $x->id;
                                            $inicio = $this->fechaactual;
                                            $fin = $this->fechaactual;
                                            $resulta_ven_pro=$this->General_model->pro_venta($inicio,$fin,$id);
                                            $cont=1;
                                            foreach ($resulta_ven_pro as $r){
                                                $html.='<h6>('.$cont.') '.$r->producto.'<h6>';
                                                $cont++;
                                            } 
                                        $html.='</div>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
        }
        
        $html.='</div>';
        echo $html;
    }
    
    public function update_alerta_pro(){
        $pro = $this->input->post('pro');
        $sucu = $this->input->post('sucu');
        $this->General_model->editar_status_alert($pro,$sucu);
    }

}