<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['categoriasll']=$this->ModeloCatalogos->categorias_all();
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/categorias',$data);
        $this->load->view('templates/footer');
	}
    function categoriadell(){
        $id = $this->input->post('id');
        $this->ModeloCatalogos->categoriadell($id);
    }
    function categoriaadd(){
        $nom = $this->input->post('nom');
        $this->ModeloCatalogos->categoriaadd($nom);
    }
    
    
    

}