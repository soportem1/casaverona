<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Corte_caja extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        //$this->load->model('Personal/ModeloPersonal');
        $this->load->model('General_model');
        $this->load->model('ModeloVentas');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
        $data['idsucursal'] = $this->idsucursal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('corte/corte',$data);
        $this->load->view('templates/footer');
        $this->load->view('corte/jscorte');

	}

    function corte(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        if($this->idsucursal==1){
            $idsucursal = $this->input->post('idsucursal');
        }else{
            $idsucursal = $this->idsucursal;
        }
        $tipo_venta = $this->input->post('tipo_venta');
        $pro_ven = $this->input->post('pro_ven');

        $resultadoc=$this->ModeloVentas->corte($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven);
        $resultadocs=$this->ModeloVentas->cortesum($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven);

        $table="<table class='tabla table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>No. venta</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Subtotal</th>
                            <th>Total</th>
                            <th>Descuento</th>
                            <th>Tipo de venta</th>
                            <th>Efectivo</th>
                            <th>Sucursal</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        $obedt=0;
        foreach ($resultadoc->result() as $fila) {
            $totalventas=$fila->monto_total; 
            $totalpreciocompra=$this->ModeloVentas->consultartotal_venta2($fila->id_venta,$fila->sucursal);
            $obed =$totalventas-$totalpreciocompra; 
            $tipo_venta_text='';
            if($fila->metodo==1){
                $tipo_venta_text='Efectivo';
            }else if($fila->metodo==2){
                $tipo_venta_text='Tarjeta de crédito';
            }else if($fila->metodo==3){
                $tipo_venta_text='Tarjeta de débito';
            }else if($fila->metodo==4){
                $tipo_venta_text='Crédito';
            }
            $table .= "<tr>
                            <td>".$fila->id_venta."</td>
                            <td>".$fila->vendedor."</td>
                            <td>".$fila->Nom."</td>
                            <td>".$fila->reg."</td>
                            <td>$".number_format($fila->subtotal,2,'.',',')."</td>
                            <td>$".number_format($fila->monto_total,2,'.',',')."</td>
                            <td>$".number_format($fila->descuentocant,2,'.',',')."</td>
                            <td>".$tipo_venta_text."</td>
                            <td>$".number_format($fila->efectivo,2,'.',',')."</td>
                            <td>".$fila->sucursal_txt."</td>
                        </tr>";
            $obedt=$obedt+$obed;
            $rowventas++;
            
        }
        $table.="</tbody> </table>";
        
        if($idsucursal==0){
            $wheresuc=" ";
        }else{
            $wheresuc=" AND id=".$idsucursal;
        }
        $get_sucursales=$this->General_model->get_records_condition('activo=1 '.$wheresuc,'sucursales');
        $mas_vendindos='<br><h5><b>Productos mas vendidos</b><h5><hr>';
        foreach ($get_sucursales as $y){
            $mas_vendindos.='<h5><b>Sucursal:'.$y->nombre.'</b><h5>
                        <table class="tabla table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="font-size: 16px;">Número</th>
                                    <th style="font-size: 16px;">Producto</th>
                                    <th style="font-size: 16px;">Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>';
                        $resulta_ven_pro=$this->ModeloVentas->pro_venta($inicio,$fin,$y->id);
                        $contador=1;
                        foreach ($resulta_ven_pro->result() as $x){
                                $mas_vendindos .= "<tr>
                                    <td>".$contador."</td>
                                    <td>".$x->producto."</td>
                                    <td>".$x->total_cantidad."</td>
                                </tr>";
                            $contador++;         
                        }
            $mas_vendindos.='</tbody>
                        </table>';
        }

        $total=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
        }

        $total = round($total,2);
        $subtotal = round($subtotal,2);

        
        $subtotal=number_format($subtotal,2,'.',',');

        $obedt=number_format($obedt,2,'.',',');

        $ttotal=0;
        $gasto=0;
        $resultgastos=$this->ModeloVentas->get_gastos($inicio,$fin,$idsucursal);
        foreach ($resultgastos->result() as $x){
            $gasto=$x->gasto;
        }

        $ttotal=$total-$gasto;


        $array = array("tabla"=>$table,
                        "tabla2"=>'',
                        "dgastos"=>number_format($gasto,2,'.',','),
                        "dTotal"=>number_format($ttotal,2,'.',','),
                        "totalventas"=>$rowventas,
                        "dImpuestos"=>0,
                        "totalutilidad"=>$obedt,
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                        "mas_vendindos"=>$mas_vendindos 
                    );
            echo json_encode($array);
    }

    function corte_credito(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        if($this->idsucursal==1){
            $idsucursal = $this->input->post('idsucursal');
        }else{
            $idsucursal = $this->idsucursal;
        }
        $tipo_venta = $this->input->post('tipo_venta');
        $pro_ven = $this->input->post('pro_ven');

        $resultadoc=$this->ModeloVentas->corte_credito($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven);
        $resultadocs=$this->ModeloVentas->cortesum_credito($inicio,$fin,$idsucursal,$tipo_venta,$pro_ven);

        $table="<table class='tabla table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>No. venta</th>
                            <th>Cajero</th>
                            <th>Cliente</th>
                            <th>Fecha</th>
                            <th>Subtotal</th>
                            <th>Total</th>
                            <th>Descuento</th>
                            <th>Tipo de venta</th>
                            <th>Efectivo</th>
                            <th>Sucursal</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        $obedt=0;
        foreach ($resultadoc->result() as $fila) {
            $totalventas=$fila->monto_total; 
            $totalpreciocompra=$this->ModeloVentas->consultartotal_venta2($fila->id_venta,$fila->sucursal);
            $obed =$totalventas-$totalpreciocompra; 
            $tipo_venta_text='';
            if($fila->metodo==1){
                $tipo_venta_text='Efectivo';
            }else if($fila->metodo==2){
                $tipo_venta_text='Tarjeta de crédito';
            }else if($fila->metodo==3){
                $tipo_venta_text='Tarjeta de débito';
            }else if($fila->metodo==4){
                $tipo_venta_text='Crédito';
            }
            $abono = $this->ModeloVentas->getall_total_abono($fila->id_venta);
            $restar=$fila->monto_total-$abono;
            $table .= "<tr>
                            <td rowspan='2'>".$fila->id_venta."</td>
                            <td>".$fila->vendedor."</td>
                            <td>".$fila->Nom."</td>
                            <td>".$fila->reg."</td>
                            <td>$".number_format($fila->subtotal,2,'.',',')."</td>
                            <td>$".number_format($fila->monto_total,2,'.',',')."</td>
                            <td>$".number_format($fila->descuentocant,2,'.',',')."</td>
                            <td>".$tipo_venta_text."</td>
                            <td>$".number_format($fila->efectivo,2,'.',',')."</td>
                            <td>".$fila->sucursal_txt."</td>
                        </tr>
                        <tr>
                            <td colspan='3' align='center'><h4><b> Abono </b></h4></td>
                            <td>$".number_format($abono,2,'.',',')."</td>
                            <td colspan='3' align='center'><h4><b> Restante </b> </h4></td>
                            <td colspan='2'>$".number_format($restar,2,'.',',')."</td>
                        </tr>";
            $obedt=$obedt+$obed;
            $rowventas++;
            
        }
        $table.="</tbody> </table>";
        
        if($idsucursal==0){
            $wheresuc=" ";
        }else{
            $wheresuc=" AND id=".$idsucursal;
        }
        $get_sucursales=$this->General_model->get_records_condition('activo=1 '.$wheresuc,'sucursales');
        $mas_vendindos='<br><h5><b>Productos mas vendidos</b><h5><hr>';
        foreach ($get_sucursales as $y){
            $mas_vendindos.='<h5><b>Sucursal:'.$y->nombre.'</b><h5>
                        <table class="tabla table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="font-size: 16px;">Producto</th>
                                    <th style="font-size: 16px;">Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>';
                        $resulta_ven_pro=$this->ModeloVentas->pro_venta($inicio,$fin,$y->id);
                        foreach ($resulta_ven_pro->result() as $x){
                                $mas_vendindos .= "<tr>
                                    <td>".$x->producto."</td>
                                    <td>".$x->total_cantidad."</td>
                                </tr>";         
                        }
            $mas_vendindos.='</tbody>
                        </table>';
        }

        $total=0;
        $subtotal=0;
        foreach ($resultadocs->result() as $fila) {
            $total = $fila->total;
            $subtotal = $fila->subtotal;
            $descuento = $fila->descuento;
        }

        $total = round($total,2);
        $subtotal = round($subtotal,2);

        
        $subtotal=number_format($subtotal,2,'.',',');

        $obedt=number_format($obedt,2,'.',',');
        
        $ttotal=0;
        $gasto=0;
        $resultgastos=$this->ModeloVentas->get_gastos($inicio,$fin,$idsucursal);
        foreach ($resultgastos->result() as $x){
            $gasto=$x->gasto;
        }

        $ttotal=$total-$gasto;

        $array = array("tabla"=>$table,
                        "tabla2"=>'',
                        "dgastos"=>number_format($gasto,2,'.',','),
                        "dTotal"=>number_format($ttotal,2,'.',','),
                        "totalventas"=>$rowventas,
                        "dImpuestos"=>0,
                        "totalutilidad"=>$obedt,
                        "descuento"=>$descuento,
                        "dSubtotal"=>"".$subtotal."",
                        "mas_vendindos"=>$mas_vendindos 
                    );
            echo json_encode($array);
    }
    /// Pedidos 
    function corte_pedidos(){
        $inicio = $this->input->post('fecha1');
        $fin = $this->input->post('fecha2');
        $resultadocp=$this->ModeloVentas->corte_pedidos($inicio,$fin);
        $table="<table class='tabla table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>No. Pedido</th>
                            <th>Cliente</th>
                            <th>Total</th>
                            <th>Anticipo</th>
                            <th>Fecha pedido</th>
                            <th>Fecha entrega</th>                            
                            <th>Estatus</th>
                        </tr>
                    </thead>
                    <tbody>";
        $rowventas=0;
        $total=0;
        $anticipo=0;
        foreach ($resultadocp->result() as $fila) {
            $total+=$fila->total;
            $anticipo+=$fila->anticipo;
            if($fila->estatus==0){
                $estatus_x='Pendiente';
            }else if($fila->estatus==1){
                $estatus_x='Proceso';
            }else if($fila->estatus==2){
                $estatus_x='Entregada';
            }else if($fila->estatus==3){
                $estatus_x='Saldado';
            }

            $table .= "<tr>
                            <td>".$fila->id."</td>
                            <td>".$fila->nombre."</td>
                            <td>$".number_format($fila->total,2,'.',',')."</td>
                            <td>$".number_format($fila->anticipo,2,'.',',')."</td>
                            <td>".$fila->fechapedido."</td>
                            <td>".$fila->fechaentrega."</td>
                            <td>".$estatus_x."</td>
                        </tr>";
            
        }
        $table.="</tbody> </table>";
 
        $resultadocp=$this->ModeloVentas->get_pedido_abono($inicio,$fin);
        $abono=0;
        foreach ($resultadocp->result() as $c){
            $abono=$c->pago;
        }  
        $array = array("tabla"=>$table,"total"=>number_format($total,2,'.',','),"anticipo"=>number_format($anticipo,2,'.',','),'abono'=>$abono);

        echo json_encode($array);
    }

    
}


        /*
        $table2="<table class='table table-striped table-bordered table-hover' id='sample_2'>
                    <thead>
                        <tr>
                            <th>Turno</th>
                            <th>Apertura</th>
                            <th>Cierre</th>
                            <th>Utilidad</th>
                        </tr>
                    </thead>
                    <tbody>";
        $respuestaturno=$this->ModeloVentas->consultarturnoname($inicio,$fin);
        $obedt=0;
        foreach ($respuestaturno->result() as $fila) { 
            if ($fila->fechacierre=='0000-00-00') {
                $horac =date('H:i:s');
                $fechac =date('Y-m-d');
            }else{
                $horac =$fila->horac;
                $fechac =$fila->fechacierre;
            }
            $fecha =$fila->fecha;
            $horaa =$fila->horaa; 
            $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac);
            $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac);
            $obed =$totalventas-$totalpreciocompra;
            $table2 .= "<tr>
                            <td>".$fila->nombre."</td>
                            <td>".$fila->horaa."</td>
                            <td>".$fila->horac."</td>
                            <td>".number_format($obed,2,'.',',')."</td>
                        </tr>";
            $obedt=$obedt+$obed;
        }
        $table2.="</tbody> </table>";

        */
