<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Etiquetas extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModeloProductos');
        $this->load->model('General_model');
    }
	public function index(){
		$id=$this->input->get('id');
		$data['getventasd']=$this->ModeloProductos->getproducto($id);

		$datarow=$this->General_model->get_tabla('productos_sucursal_precios',array('idsucursal'=>1,'productoid'=>$id));
		$precio=0;
		foreach ($datarow as $item) {
			$precio=$item->precioventa;
		}
		$data['preciov']=$precio;
		$this->load->view('Reportes/etiqueta',$data);
	        
	}
}