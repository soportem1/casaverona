<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
            if($this->idsucursal!=1){
                header('Location: '.base_url().'Inicio');
            } 
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        //$data['personal']=$this->ModeloPersonal->getpersonal();
        //carga de vistas
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        //$this->load->view('Personal/Personal',$data);
        //$data['personal']=$this->ModeloPersonal->getpersonal();
        $this->load->view('Personal/Personal');
        $this->load->view('templates/footer');
        $this->load->view('Personal/jspersonal');
	}
    /**
     * Retorna vista para agregar personal 
     */

    public function Personaladd(){
            //carga de vistas
            $data['estadosp']=$this->ModeloPersonal->estados();
            $data['menuse']=$this->ModeloPersonal->getAllmenu();
            $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('Personal/Personaladd',$data);
            $this->load->view('templates/footer');
            $this->load->view('Personal/jspersona');
    }  
     
    public function addpersonal(){
        $id = $this->input->post('id');
        $nom = $this->input->post('nom');
        $ape = $this->input->post('ape');
        $fnaci = $this->input->post('fnaci');
        $sexo = $this->input->post('sex');
        $domic = $this->input->post('domic');
        $ciudad = $this->input->post('ciudad');
        $estado = $this->input->post('estado');
        $copo = $this->input->post('copo');
        $telcasa = $this->input->post('telcasa');
        $telcel = $this->input->post('telcel');
        $email = $this->input->post('email');
        $turno = $this->input->post('turno');
        $fechain = $this->input->post('fechain');
        $fechaba = $this->input->post('fechaba');
        $sueldo = $this->input->post('sueldo');
        $idsucursal = $this->input->post('idsucursal');
        $usuario = $this->input->post('usuario');
        $pass = $this->input->post('pass');
        if ($id>0) {
            $this->ModeloPersonal->personalupdate($id,$nom,$ape,$fnaci,$sexo,$domic,$ciudad,$estado,$copo,$telcasa,$telcel,$email,$turno,$fechain,$fechaba,$sueldo,$idsucursal); 
            $this->ModeloUsuarios->usuariosupdate($usuario,$pass,$id);
            echo $id;

        }else{
            $idd=$this->ModeloPersonal->personalnuevo($nom,$ape,$fnaci,$sexo,$domic,$ciudad,$estado,$copo,$telcasa,$telcel,$email,$turno,$fechain,$fechaba,$sueldo,$idsucursal);
            $this->ModeloUsuarios->usuariosinsert($usuario,$pass,$idd);
            echo $idd;
        } 
    }
    public function deletepersonal(){
        $id = $this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('personalId',$id,$data,'personal');
    }
    public function addmenuss(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {
            $usu =  $DATA[$i]->usu;
            $menu= $DATA[$i]->menu;
            echo $this->ModeloUsuarios->addmenus($usu,$menu);
        }
    } 

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_personal($params);
        $totaldata= $this->ModelCatalogos->total_personal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  
    
}
