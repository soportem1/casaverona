<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cotizaciones extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloClientes');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        $this->load->model('ModeloVentas');
        
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->anio = date('y');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
            if($this->idsucursal!=1){
                header('Location: '.base_url().'Inicio');
            } 
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['fecha']=$this->fechaactual;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('cotizaciones/listado',$data);
        $this->load->view('templates/footer');
        $this->load->view('cotizaciones/listadojs');
	}

    public function registro($id=0){
        $terminos_aux=0;
        $partidas_aux=0;
        $cliente_num=0;
        $cliente_txt='';
        if($id==0){
            $folio_aux='SP-00-0';
            $get_result_folio=$this->General_model->get_where_orden_asc('cotizacion','id');
            foreach ($get_result_folio as $i){
                $folio_aux=$i->folio;
            }
            $folio_dibi=$folio_aux;
            $array = explode("-",$folio_dibi);
            $suma_folio=intval($array[2])+1;
            $data['num_folio']=$suma_folio;
            $data['id']=0;

            $data['cliente_num']=$cliente_num;
            $data['cliente_txt']=$cliente_txt;

            $data['folio']='';
            $data['fecha']='';
            $data['correo']='';
            $data['telefono']='';
            $data['contacto']='';
            $data['personacontacto']='';
            $data['tipo']='';
            $data['uncluir']='';
            $data['terminos_aux']=0;
            $data['partidas_aux']=0;
            $data['subtotal']=0;
            $data['iva']=0;
            $data['total']=0;
            $data['empresa']='';
            $data['direccion']='';
        }else{
            $get_result_terminos=$this->General_model->get_records_condition('activo=1 AND idcotizacion='.$id,'cotizacion_terminos');
            foreach ($get_result_terminos as $x){
               $terminos_aux=1;
            }
            $get_result_partidas=$this->General_model->get_records_condition('activo=1 AND idcotizacion='.$id,'cotizacion_partidas');
            foreach ($get_result_partidas as $x){
                $partidas_aux=1;
            }
            ////

            $get_result=$this->General_model->get_records_condition('id='.$id,'cotizacion');
            foreach ($get_result as $item) {
            
                $folio_dibi=$item->folio;
                $array = explode("-",$folio_dibi);

                $get_clientes=$this->General_model->get_records_condition('ClientesId='.$item->cliente,'clientes');
                foreach ($get_clientes as $x){
                    $cliente_num=$x->ClientesId;
                    $cliente_txt=$x->Nom;
                }
                $data['num_folio']=intval($array[2]);
                $data['id']=$item->id;

                $data['cliente_num']=$cliente_num;
                $data['cliente_txt']=$cliente_txt;
                
                $data['folio']=$item->folio;
                $data['fecha']=$item->fecha;
                $data['correo']=$item->correo;
                $data['telefono']=$item->telefono;
                $data['contacto']=$item->contacto;
                $data['uncluir']=$item->uncluir;
                $data['subtotal']=$item->subtotal;
                $data['iva']=$item->iva;
                $data['total']=$item->total;
                $data['empresa']=$item->empresa;
                $data['direccion']=$item->direccion;
                $data['personacontacto']=$item->personacontacto;
            } 
            $data['terminos_aux']=$terminos_aux;
            $data['partidas_aux']=$partidas_aux;
            $data['tipo']='Editar';
        }
      
        $data['fecha']=$this->fechaactual;
        $data['anio']=$this->anio;

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('cotizaciones/registro',$data);
        $this->load->view('templates/footer');
        $this->load->view('cotizaciones/registrojs');
    }
   
    public function registrar(){
        $data=$this->input->post();
        $idcoti=$data['id'];
        unset($data['id']);
        if($idcoti==0){
            $data['usuario']=$this->usuarioid;
            $data['reg']=$this->fechahoy;
            $id=$this->General_model->add_record('cotizacion',$data);
        }else{
            $this->General_model->edit_record('id',$idcoti,$data,'cotizacion');
            $id=$idcoti;
        }
        echo $id;
    }
    
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_cotizaciones($params);
        $totaldata= $this->ModelCatalogos->total_cotizaciones($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  

    public function deleteregistro(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'cotizacion');
    }

    public function deleteregistro_termino(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'cotizacion_terminos');
    }

    public function insert_terminos(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->idtermino;
            $data['idcotizacion']=$DATA[$i]->idcotizacion;
            $data['nombre']=$DATA[$i]->termino;
            if($id==0){
                $this->General_model->add_record('cotizacion_terminos',$data);
            }else{
                $this->General_model->edit_record('id',$id,$data,'cotizacion_terminos');
            }   
        }
    }

    public function get_terminos_condiciones(){
        $id = $this->input->post('id');
        $get_result=$this->General_model->get_records_condition('idcotizacion='.$id.' AND activo=1','cotizacion_terminos');
        echo json_encode($get_result);
    }

    public function buscar_producto(){
        $nom=$this->input->post('producto');
        $idsucursal=$this->input->post('idsucursal');
        $html='';
        $result=$this->ModelCatalogos->get_productos_traspasos($nom,$idsucursal);

        echo json_encode($result);
    }
    // Partidas
    public function insert_partidas(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->idpartida;
            $data['productoid']=$DATA[$i]->productoid;
            $data['idcotizacion']=$DATA[$i]->idcotizacion;
            $data['cantidad']=$DATA[$i]->cantidad;
            $data['tipo_precio']=$DATA[$i]->tipo_precio;
            $data['precio']=$DATA[$i]->precio;
            $data['subtotal']=$DATA[$i]->subtotal;
            if($id==0){
                $this->General_model->add_record('cotizacion_partidas',$data);
            }else{
                $this->General_model->edit_record('id',$id,$data,'cotizacion_partidas');
            }   
        }
    }

    public function get_partidas(){
        $id = $this->input->post('id');
        $get_result=$this->ModelCatalogos->get_partidas($id);
        echo json_encode($get_result);
    }

    public function deleteregistro_partida(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'cotizacion_partidas');
    }

    public function documento($id){
        $data['get_result_partidas']=$this->ModelCatalogos->get_partidas_all($id);
        $data['get_result_terminos']=$this->General_model->get_records_condition('activo=1 AND idcotizacion='.$id,'cotizacion_terminos');
        $get_result=$this->General_model->get_records_condition('id='.$id,'cotizacion');
        $cliente=0;
        foreach ($get_result as $item) {
            $data['id']=$item->id;
            //$data['nombre']=$item->nombre;
            $data['folio']=$item->folio_aux;
            $data['fecha']=$item->fecha;
            $data['correo']=$item->correo;
            $data['telefono']=$item->telefono;
            $data['personacontacto']=$item->personacontacto;
            $data['uncluir']=$item->uncluir;
            $data['subtotal']=$item->subtotal;
            $data['iva']=$item->iva;
            $data['total']=$item->total;
            $cliente=$item->cliente;
            $contacto=$item->contacto;
            $direccion=$item->direccion;

        } 
        $data['clientenom']='';
        $data['clientedirecciom']='';
        $data['clientedirecciom2']='';
        $get_result_cliente=$this->General_model->get_tabla('clientes',array('ClientesId'=>$cliente));
        foreach ($get_result_cliente as $item) {
            $data['clientenom']=$item->Nom;
            $data['clientedirecciom']=$item->Calle.' '.$item->noExterior.' '.$item->Colonia;
            $data['clientedirecciom2']=$item->Municipio.','.$item->Municipio;
        }
        if($cliente==0){
            $data['clientenom']=$contacto;
            $data['clientedirecciom']=$direccion;
        }
        $this->load->view('Reportes/cotizacion',$data);
    }

    public function rechazarregistro(){
        $id = $this->input->post('id');
        $data = array('estatus'=>2);
        $this->General_model->edit_record('id',$id,$data,'cotizacion');
    }
    public function tabla_produtos(){
        $id = $this->input->post('id');
        // La relacion es con la sucursal matriz 
        $get_result=$this->ModelCatalogos->get_partidas_all($id);
        $html='';
        foreach ($get_result as $x){
            $html.='<tr>
                    <td>'.$x->nombre.'</td>
                    <td>'.$x->cantidad.'</td>
                    <td style="color:red">Existencia: '.$x->stock.'</td>
                    <td>$'.$x->precio.'</td>
                    <td>$'.$x->subtotal.'</td>
                   </tr>';
        }
        echo $html;
    }

    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }
    
    public function registrar_cotizacion(){
        $id = $this->input->post('id');
        $metopago = $this->input->post('mpago');
        $total_p_total = $this->input->post('total_p_total');
        $descuento = $this->input->post('descuento');
        $cantdescuento = $this->input->post('cantdescuento');
        $get_result_cotiza=$this->General_model->get_records_condition('id='.$id,'cotizacion');
        foreach ($get_result_cotiza as $z){
            $cliente=$z->cliente;
            $uncluir=$z->uncluir;
            $subtotal_p=$z->subtotal;
        }
        $get_result=$this->General_model->get_records_condition('idcotizacion='.$id,'cotizacion_partidas');
        $validar=0;
        foreach ($get_result as $x){
            $idproducto=$x->productoid;
            $cantidad=$x->cantidad;
            $get_result_stock=$this->General_model->get_records_condition('idsucursal=1 AND productoid='.$idproducto,'productos_sucursal_precios');
            $stock=0;
            foreach ($get_result_stock as $y){
                $stock=$y->stock;
            }
            if($cantidad>=$stock){
                $validar=1;
            }
        }
        $id_venta=0;
        if($validar==0){
            $data = array('estatus'=>1);
            $this->General_model->edit_record('id',$id,$data,'cotizacion');
            $uss = $this->personalId;
            $cli = $cliente;
            $mpago = $metopago;
            $desc = $descuento;
            $descu = $cantdescuento;
            if($uncluir=='on'){
                $sbtotal = $subtotal_p;
                $total = $total_p_total;
            }else{
                $sbtotal = $total_p_total;
                $total = $total_p_total;
            }
            $efectivo = 0;
            $tarjeta = 0;
            $data_info = array('id_personal'=>$uss,
                               'id_cliente'=>$cli,
                               'metodo'=>$mpago,
                               'subtotal'=>$sbtotal,
                               'descuento'=>$desc,
                               'descuentocant'=>$descu,
                               'monto_total'=>$total,
                               'pagotarjeta'=>$efectivo,
                               'efectivo'=>$tarjeta,
                               'tipo_venta'=>1,
                               'sucursal'=>1);
            //var_dump($data_info);die;
            $id_venta=$this->General_model->add_record('ventas',$data_info);
        }
        $array_venta = array('validar'=>$validar,'id_venta'=>$id_venta);
        echo json_encode($array_venta);
    }
    function ingresarventapro(){
        $id = $this->input->post('id');
        $idcot = $this->input->post('idcot');
        $get_result_cotiza=$this->General_model->get_records_condition('idcotizacion='.$idcot,'cotizacion_partidas');
        foreach ($get_result_cotiza as $z){
            $idventa=$id;
            $producto=$z->productoid;
            $cantidad=$z->cantidad;
            $precio=$z->precio;
            $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$precio);
            $this->ModelCatalogos->stock_descontar_producto($cantidad,1,$producto);
        } 
    }
}