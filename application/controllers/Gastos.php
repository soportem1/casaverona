<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gastos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
       
        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
            if($this->idsucursal!=1){
                header('Location: '.base_url().'Inicio');
            } 
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
        $data['idsucursal']=$this->idsucursal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/listado',$data);
        $this->load->view('templates/footer');
        $this->load->view('gastos/jslistado');
	}

    public function getlistado(){
        $params = $this->input->post();
        $params['idsucursal'] = $this->idsucursal;
        $getdata = $this->ModelCatalogos->get_gastos($params);
        $totaldata= $this->ModelCatalogos->total_gastos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  

    public function deleteregistro(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'gastos');
    }

    public function registro_gasto(){
        $data=$this->input->post();
        $idgasto=$data['idgasto'];
        unset($data['idgasto']);
        if($idgasto==0){
            $data['usuario']=$this->usuarioid;
            if($this->idsucursal==1){
                $data['sucursal']=$data['idsucursal'];    
            }else{
                $data['sucursal']=$this->idsucursal;
            }
            unset($data['idsucursal']);
            $id=$this->General_model->add_record('gastos',$data);
        }else{
            $data['usuario']=$this->usuarioid;
            if($this->idsucursal==1){
                $data['sucursal']=$data['idsucursal'];    
            }else{
                $data['sucursal']=$this->idsucursal;
            }
            unset($data['idsucursal']);
            $this->General_model->edit_record('id',$idgasto,$data,'gastos');
            $id=$idgasto;
        }
        echo $id;
    }

    public function verificar(){
        $pass = $this->input->post('pass');
        $get_pass=$this->General_model->get_record('idpass',1,'password');
        $bandera=0;
        if($get_pass->pass==$pass){
            $bandera=1;
        }else{
            $bandera=0;
        } 
        echo $bandera;
    }
}