<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
            if($this->idsucursal!=1){
                header('Location: '.base_url().'Inicio');
            } 
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['get_pass']=$this->General_model->get_record('idpass',1,'password');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('config/password',$data);
        $this->load->view('templates/footer');
        $this->load->view('config/passwordjs');
	}

    public function guardar(){
        $pass = $this->input->post('pass');
        $data = array('pass'=>$pass);
        $this->General_model->edit_record('idpass',1,$data,'password');
    }

}