<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listado_traspasos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('traspasos/listado_traspasos');
        $this->load->view('templates/footer');
        $this->load->view('traspasos/listado_traspasosjs');
	}

    public function getlistado(){
        $params = $this->input->post();
        $params['idsucursal'] = $this->idsucursal;
        $getdata = $this->ModelCatalogos->get_traspasos_sucursal($params);
        $totaldata= $this->ModelCatalogos->total_traspasos_sucursal($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  

    public function tabla_produtos(){
        $id = $this->input->post('id');
        $get_result=$this->ModelCatalogos->get_traspasos_pruductos_sucursal($id);
        $html='';
        foreach ($get_result as $x){
            $html.='<tr>
                    <td>'.$x->nombre.'</td>
                    <td>'.$x->cantidad_recibe.'</td>
                   </tr>';
        }
        echo $html;
    }
}