<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Personal/ModeloPersonal');
        $this->load->model('Usuarios/ModeloUsuarios');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        $this->load->model('ModeloClientes');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
            if($this->idsucursal!=1){
                header('Location: '.base_url().'Inicio');
            } 
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('pedidos/listado');
        $this->load->view('templates/footer');
        $this->load->view('pedidos/listadojs');
	}

    public function formulario($id=0){
        $get_folio=$this->General_model->get_folio();
        if($id==0){
            $data['id']=0;
            $data['cliente']='';
            $data['clientetxt']='';
            $data['empresa']='';
            $data['direccion']='';
            $data['folio']=$get_folio+1;
            $data['fechapedido']=$this->fechaactual;
            $data['correo']='';
            $data['telefono']='';
            $data['personacontacto']='';
            $data['fechaentrega']='';
            $data['total']='';
            $data['anticipo']='';
            $data['titulo']='Nuevo';
            $data['partidas_aux']=0;
            $data['terminos_aux']=0;
            $data['cantdescuento']=0;
        }else{
            $x=$this->General_model->getselectwhererow('pedidos','id',$id);
            $data['id']=$x->id;
            $data['cliente']=$x->cliente;
            $c=$this->General_model->getselectwhererow('clientes','ClientesId',$x->cliente);
            if($c!=null){
                $data['clientetxt']=$c->Nom;
            }else{
                $data['clientetxt']='';
            }
            $data['empresa']=$x->empresa;
            $data['direccion']=$x->direccion;
            $data['folio']=$x->folio;
            $data['fechapedido']=$x->fechapedido;
            $data['correo']=$x->correo;
            $data['telefono']=$x->telefono;
            $data['personacontacto']=$x->personacontacto;
            $data['fechaentrega']=$x->fechaentrega;
            $data['total']=$x->total;
            $data['anticipo']=$x->anticipo;
            $data['cantdescuento']=$x->cantdescuento;
            $data['titulo']='Editar';
            $data['partidas_aux']=0;
            $get_result_partidas=$this->General_model->get_records_condition('activo=1 AND idpedido='.$x->id,'pedidos_partidas');
            foreach ($get_result_partidas as $i){
                $data['partidas_aux']=1;
            }
            $data['terminos_aux']=0; 
            $get_result_partidas=$this->General_model->get_records_condition('activo=1 AND idpedido='.$x->id,'pedidos_terminos');
            foreach ($get_result_partidas as $y){
                $data['terminos_aux']=1;
            }
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('pedidos/form',$data);
        $this->load->view('templates/footer');
        $this->load->view('pedidos/formjs');
    }

    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }

    public function registrar(){
        $data=$this->input->post();
        $idreg=$data['id'];
        unset($data['id']);
        if($idreg==0){
            $data['usuario']=$this->usuarioid;
            if($data['anticipo']==$data['total']){
                $data['estatus']=3;
            }
            $id=$this->General_model->add_record('pedidos',$data);
        }else{
            $this->General_model->edit_record('id',$idreg,$data,'pedidos');
            $id=$idreg;
        }
        echo $id;
    }

    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_pedidos($params);
        $totaldata= $this->ModelCatalogos->total_pedidos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  

    public function buscar_producto(){
        $nom=$this->input->post('producto');
        $idsucursal=$this->input->post('idsucursal');
        $html='';
        $result=$this->ModelCatalogos->get_productos_traspasos($nom,$idsucursal);

        echo json_encode($result);
    }

    public function deleteregistro_partida(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'pedidos_partidas');
    }

    // Partidas
    public function insert_partidas(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->idpartida;
            $data['productoid']=$DATA[$i]->productoid;
            $data['idpedido']=$DATA[$i]->idpedido;
            $data['cantidad']=$DATA[$i]->cantidad;
            if(!isset($DATA[$i]->tipo_precio)){
                if(!is_null($DATA[$i]->tipo_precio)){
                    $data['tipo_precio']=$DATA[$i]->tipo_precio;
                }
            }
            $data['precio']=$DATA[$i]->precio;
            $data['subtotal']=$DATA[$i]->subtotal;
            $data['nombre_producto']=$DATA[$i]->nombre_producto;
            $data['tipo']=$DATA[$i]->tipo;
            if($id==0){
                $this->General_model->add_record('pedidos_partidas',$data);
            }else{
                $this->General_model->edit_record('id',$id,$data,'pedidos_partidas');
            }   
        }
    }

    public function get_partidas(){
        $id = $this->input->post('id');
        $get_result=$this->ModelCatalogos->get_pedido_partidas($id);
        echo json_encode($get_result);
    }
    
    public function insert_terminos(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->idtermino;
            $data['idpedido']=$DATA[$i]->idpedido;
            $data['nombre']=$DATA[$i]->termino;
            if($id==0){
                $this->General_model->add_record('pedidos_terminos',$data);
            }else{
                $this->General_model->edit_record('id',$id,$data,'pedidos_terminos');
            }   
        }
    }

    public function get_terminos_condiciones(){
        $id = $this->input->post('id');
        $get_result=$this->General_model->get_records_condition('idpedido='.$id.' AND activo=1','pedidos_terminos');
        echo json_encode($get_result);
    }

    public function deleteregistro_termino(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id',$id,$data,'pedidos_terminos');
    }
    
    public function pagos_pedido()
    {    
        $id=$this->input->post('id');
        $ped=$this->General_model->getselectwhererow('pedidos','id',$id);
        $result=$this->General_model->get_tabla('pedidos_pagos',array('idpedido'=>$id,'activo'=>1));
        $html='';
        $suma=0;
        foreach ($result as $x){
            $html.='<tr>
                <td>$'.number_format($x->pago, 2).'</td>
                <td>'.date('d/m/Y',strtotime($x->fecha)).'</td>
            </tr>';
            $suma+=$x->pago;
        }
        $total=$ped->total;
        $anticipo=$ped->anticipo;
        $suma_anticipo_pago=$ped->anticipo+$suma;
        $resta_total=$ped->total-$suma_anticipo_pago;
        $suma_pagos=$suma;
        $info = array('tabla'=>$html,'total'=>$total,'anticipo'=>$anticipo,'resta_total'=>$resta_total,'suma_pagos'=>$suma_pagos);
        echo json_encode($info);
    }

    public function registrar_pago(){
        $data=$this->input->post();
        $this->General_model->add_record('pedidos_pagos',$data);
    }
    
    public function update_estatus(){
        $data=$this->input->post();
        $idreg=$data['idpedido'];
        unset($data['idpedido']);
        $idsucursal=$this->idsucursal;
        $this->General_model->edit_record('id',$idreg,$data,'pedidos');
        if($data['estatus']==2){
            $get_result=$this->ModelCatalogos->get_pedido_partidas($idreg);
            foreach ($get_result as $x){
                if($x->productoid!=0){
                    $this->ModelCatalogos->stock_descontar_producto(floatval($x->cantidad),intval($idsucursal),intval($x->productoid));
                }
            }    
        }
    }

    public function update_registro(){
        $data=$this->input->post();
        $idreg=$data['idpedido'];
        $this->General_model->edit_record('id',$idreg,array('activo'=>0),'pedidos');
    }

    public function documento($id){
        $x=$this->General_model->getselectwhererow('pedidos','id',$id);
        $data['id']=$x->id;
        $data['cliente']=$x->cliente;
        $c=$this->General_model->getselectwhererow('clientes','ClientesId',$x->cliente);
        if($c!=null){
            $data['clientetxt']=$c->Nom;
            $data['clientedirecciom']=$c->Calle.' '.$c->noExterior.' '.$c->Colonia;
            $data['clientedirecciom2']=$c->Municipio.','.$c->estadocli;
            $data['extencionc']=$c->extencionc;
        }else{
            $data['clientetxt']='';
            $data['clientedirecciom']='';
            $data['clientedirecciom2']='';
            $data['extencionc']='';
        }
        if($x->cliente==0){
            $data['clientetxt']=$x->personacontacto;
            $data['clientedirecciom']=$x->direccion;
        }
        $data['folio']=$x->folio;
        $data['fechapedido']=$x->fechapedido;
        $data['correo']=$x->correo;
        $data['telefono']=$x->telefono;
        
        $data['fechaentrega']=$x->fechaentrega;
        $data['total']=$x->total;
        $data['cantdescuento']=$x->cantdescuento;
        $data['anticipo']=$x->anticipo;
        $data['empresa']=$x->empresa;
        $data['titulo']='Editar';
        $data['partidas_aux']=0;
        $data['estatus']=$x->estatus;

        $result=$this->General_model->get_tabla('pedidos_pagos',array('idpedido'=>$id,'activo'=>1));
        $suma=0;
        foreach ($result as $y){
            $suma+=$y->pago;
        }
        $suma_anticipo_pago=$x->anticipo+$suma;
        $data['resta_total']=$x->total-$suma_anticipo_pago;
        $data['get_result_partidas']=$this->ModelCatalogos->get_pedidos_partidas_all($id);
        $data['get_result_terminos']=$this->General_model->get_records_condition('activo=1 AND idpedido='.$id,'pedidos_terminos');
        
        $this->load->view('Reportes/pedido',$data);
    }

    public function update_saldado(){
        $id = $this->input->post('id');
        $ped=$this->General_model->getselectwhererow('pedidos','id',$id);
        $tipo=0;
        if($ped->estatus==0){
            $tipo=1;
            $data = array('estatus'=>3);
            $this->General_model->edit_record('id',$id,$data,'pedidos');
        }
        echo $tipo;
    }

    public function buscar_producto_validar(){
        $nom=$this->input->post('producto');
        $idsucursal=$this->input->post('idsucursal');
        $result=$this->ModelCatalogos->get_productos_validar($nom,$idsucursal);
        echo json_encode($result);
    }
    
}