<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrar_Traspasos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloClientes');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        $this->load->model('ModeloVentas');
        
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
        $this->anio = date('y');

        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['fecha']=$this->fechaactual;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('traspasos/registrar_traspaso',$data);
        $this->load->view('templates/footer');
        $this->load->view('traspasos/registrar_traspasojs');
	}

    function get_producto_traspaso(){
        $codigo=$this->input->post('codigo');
        $get_result=$this->General_model->get_records_condition('estatus=0 AND codigo="'.$codigo.'"','traspasos');
        $validar=0;
        $idsucursal_aux=$this->idsucursal;
        $idsucursal=0;
        $html='';
        foreach ($get_result as $x){
            $validar=1;
            //////////////////////////////////////////////
            $idsucursal = $x->sucursal;
            /////////////////////////////////////////////
            $get_result_sucu=$this->General_model->get_records_condition('id='.$idsucursal,'sucursales');
            $sucuc_nombre='';
            foreach ($get_result_sucu as $s){
                $sucuc_nombre=$s->nombre;
            }
            $idtraspaso=$x->id;
            $get_result_productos=$this->ModelCatalogos->get_traspasos_pruductos_sucursal($x->id);
            $html='<div class="row"> 
                        <div class="col-md-12">
                            <span style="font-size: 20px;">Sucursal:'.$sucuc_nombre.'</span><br>
                            <span>Fecha de creación: '.date('d/m/Y',strtotime($x->fecha)).'</span><br>
                            <span>Hora de creación: '.date('g:i:a',strtotime($x->hora)).'</span>   
                        </div>
                    </div>
                    <br>
                    <div class="row"> 
                        <div class="col-md-12">
                            <h4>Listado de productos</h4>
                            <hr>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-md-12">
                            <table width="100%" border="0" id="tabla_productos"> 
                                <thead>
                                    <tr>
                                        <th style="width: 240px;"><b>Cantidad</b><br></th>
                                        <th><b>Producto</b></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>';
                                //<th>P. Unitario</th>
                                foreach ($get_result_productos as $item){
                                    $checked_aux='';
                                    if($item->estatus==2){
                                        $checked_aux='checked';
                                    }else{
                                        $checked_aux='';
                                    }
                                    $html.='<tr class="row_tras_'.$item->id.'"> 
                                        <td>
                                            <input type="hidden" readonly class="form-control'.$item->id.'" id="idtraspasodx" value="'.$item->id.'">
                                            <input type="hidden" readonly class="form-control'.$item->id.'" id="productoidx" value="'.$item->productoid.'">
                                            <input type="number" style="width: 140px;" class="form-control cantidad_tras_'.$item->id.'" id="cantidad_trasx" value="'.$item->cantidad.'" onclick="modificar_cantidad('.$item->id.','.$item->cantidad.')">
                                            <input type="hidden" disabled class="form-control cantidad_tras_aux_'.$item->id.'" value="1">
                                        </td>
                                        <td>'.$item->nombre.'</td> 
                                        <td>
                                            <div class="btn-group">
                                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                                    <input type="checkbox" class="custom-control-input check_tras_'.$item->id.'" id="check_tras" onclick="aceptar_traspaso('.$item->id.')" '.$checked_aux.'>
                                                    <span class="custom-control-indicator"></span>
                                                </label>
                                                <button type="button" class="btn mr-1 mb-1 btn-danger btn-sm" onclick="cancelar_traspaso_modal('.$item->id.')"><i class="ft-trash-2"></i></button>
                                            </div> 
                                        </td>
                                    </tr>';
                                }
                                //<td>$'.$item->precio.'</td>
                                $html.='</tbody>';
                    $html.='</table>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-md-12" align="center">
                            <hr>
                            <button class="btn btn-round btn_orange btn_registro" style="width: 170px;" onclick="guardar_traspaso('.$idtraspaso.')"><i class="ft-save"></i> Registrar ingreso</button>
                        </div>
                    </div>
                    ';
        }
        if($idsucursal_aux==$idsucursal){
            $validar=1;
        }else{
            $validar=0;
        }
        $arraytraspaso = array('validar' => $validar,'traspaso'=>$html);
        echo json_encode($arraytraspaso);
    }

    public function deleteregistro_traspaso(){
        $id = $this->input->post('id');
        $data = array('estatus'=>0);
        $this->General_model->edit_record('id',$id,$data,'traspasos_detalles');
    }

    public function aceptar_registro_traspaso(){
        $id = $this->input->post('id');
        $checked = $this->input->post('checked');
        $data = array('estatus'=>$checked);
        $this->General_model->edit_record('id',$id,$data,'traspasos_detalles');
    }

    public function verificar(){
        $pass = $this->input->post('pass');
        $get_pass=$this->General_model->get_record('idpass',1,'password');
        $bandera=0;
        if($get_pass->pass==$pass){
            $bandera=1;
        }else{
            $bandera=0;
        } 
        echo $bandera;
    }
    public function update_partidas(){
        $datos = $this->input->post('data');
        $idtraspaso = $this->input->post('idtraspaso');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idtraspaso_detalle=$DATA[$i]->idtraspaso_detalle;
            $productoid=$DATA[$i]->productoidx;
            $data['cantidad_recibe']=$DATA[$i]->cantidad;
            $data['usuario_recibe']=$this->usuarioid;
            $this->General_model->edit_record('id',$idtraspaso_detalle,$data,'traspasos_detalles');
            $idsucursal=$this->idsucursal;
            $this->ModelCatalogos->stock_aumentar_producto($DATA[$i]->cantidad,$idsucursal,$productoid);
        }
        $arraytras = array('estatus'=>1);
        $this->General_model->edit_record('id',$idtraspaso,$arraytras,'traspasos');   
    }
    
}