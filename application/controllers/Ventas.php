<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        date_default_timezone_set('America/Mexico_City');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->usuarioid=$usuarioid_tz=$_SESSION['usuarioid_tz'];
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        }else{
            redirect('/Login');
        }
    }
    public function index(){
        $sturno=$this->ModeloVentas->turnoss($this->idsucursal);
        $bloqueporturno=0;
        foreach ($sturno->result() as $item) {
            if($item->status_var==1){
                $bloqueporturno=1;
            }else{
                $bloqueporturno=0;
            }
        }
        $data['bloqueporturno']=$bloqueporturno;
        $data['clientedefault']=$this->ModeloVentas->clientepordefecto();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/ventas2',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/ventas2js');
    }
	public function index2(){
        //$data['sturno']=$this->ModeloVentas->turnos();
        $data['clientedefault']=$this->ModeloVentas->clientepordefecto();
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('ventas/ventas',$data);
        $this->load->view('templates/footer');
        $this->load->view('ventas/jsventas');
	}
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }
    public function searchproducto(){
        $pro = $this->input->get('search');
        $results=$this->ModeloProductos->productoallsearch($pro);
        echo json_encode($results->result());
    }
    public function searchproductopost(){
        $pro = $this->input->post('search');
        $results=$this->ModeloProductos->productoallsearch($pro);
        echo json_encode($results->result());
    }
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $personalv=$this->ModeloProductos->getproducto($prod);
        //$oProducto = new Producto();
        foreach ($personalv->result() as $item){
              $id = $item->productoid;
              $codigo = $item->codigo;
              $nombre = $item->nombre;
              $precioventa = $item->precioventa;
              $mediomayoreo = $item->mediomayoreo;
              $canmediomayoreo = $item->canmediomayoreo;
              $mayoreo = $item->mayoreo;
              $canmayoreo = $item->canmayoreo;
                $especial1_precio = $item->especial1_precio;
                $especial1_cantidad = $item->especial1_cantidad;
                $especial2_precio = $item->especial2_precio;
                $especial2_cantidad = $item->especial2_cantidad;

                $oProducto=array(
                    "id"=>$id,
                    "codigo"=>$codigo,
                    "nombre"=>$nombre,
                    'precioventa'=>$precioventa,
                    'mediomayoreo'=>$mediomayoreo,
                    'canmediomayoreo'=>$canmediomayoreo,
                    'mayoreo'=>$mayoreo,
                    'canmayoreo'=>$canmayoreo,
                    'especial1_precio'=>$especial1_precio,
                    'especial1_cantidad'=>$especial1_cantidad,
                    'especial2_precio'=>$especial2_precio,
                    'especial2_cantidad'=>$especial2_cantidad
                );

        }
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
        }
        //======================================================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
            if ($Cantidad<=$fila['especial1_cantidad']) {
                $precio=$fila['especial1_precio'];
            }elseif ($Cantidad > $fila['especial1_cantidad'] && $Cantidad<=$fila['especial2_cantidad']) {
                $precio=$fila['especial2_precio'];
            }elseif ($Cantidad>=$fila['canmayoreo']) {
                $precio=$fila['mayoreo'];
            }elseif ($Cantidad>=$fila['canmediomayoreo'] && $Cantidad<$fila['canmayoreo']) {
                $precio=$fila['mediomayoreo'];
            }else{
                $precio=$fila['precioventa'];
            }
            $cantotal=$Cantidad*$precio;
        ?>
            <tr class="producto_<?php echo $count;?>">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <?php echo $fila['codigo'];?>
                </td>                                        
                <td>
                    <input type="number" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                <td><?php echo $fila['nombre'];?></td>                                        
                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="<?php echo $precio;?>" readonly style="background: transparent;border: 0px;width: 100px;"></td>                                        
                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px;    width: 100px;"></td>                                        
                <td>                                            
                    <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>)">
                        <i class="ft-trash font-medium-3"></i>
                    </a>
                </td>
            </tr>
        <?php
        $count++;
        }
    }
    function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
    }

    function ingresarventa(){
        //$uss = $this->input->post('uss');
        $uss = $this->personalId;
        $cli = $this->input->post('cli');
        $mpago = $this->input->post('mpago');
        $desc = $this->input->post('desc');
        $descu = $this->input->post('descu');
        $sbtotal = $this->input->post('sbtotal');
        $total = $this->input->post('total');
        $efectivo = $this->input->post('efectivo');
        $cambio = $this->input->post('cambio');
        $factura = $this->input->post('factura');
        $idsucursal=$this->idsucursal;
        //$tarjeta = $this->input->post('tarjeta');
        $id=$this->ModeloVentas->ingresarventa($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$idsucursal,$factura);
        echo $id;
    }
    function ingresarventapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $idsucursal=$this->idsucursal;
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $tipo = $DATA[$i]->tipo;
            $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$precio,$tipo);
            if($tipo==0){
                $this->ModelCatalogos->stock_descontar_producto(floatval($cantidad),intval($idsucursal),intval($producto));
            }else{
                $this->ModelCatalogos->stock_descontar_producto_paq(floatval($cantidad),intval($idsucursal),intval($producto));
            }
            
        }
    }

    function ingresarventa_temporal(){
        //$uss = $this->input->post('uss');
        $uss = $this->personalId;
        $cli = $this->input->post('cli');
        $mpago = $this->input->post('mpago');
        $desc = $this->input->post('desc');
        $descu = $this->input->post('descu');
        $sbtotal = $this->input->post('sbtotal');
        $total = $this->input->post('total');
        $efectivo = $this->input->post('efectivo');
        $cambio = $this->input->post('cambio');
        $idsucursal=$this->idsucursal;
        $id=$this->ModeloVentas->ingresarventa_temporal($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$cambio,$idsucursal);
        echo $id;
    }

    function ingresarventapro_temporal(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        $idsucursal=$this->idsucursal;
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $tipo = $DATA[$i]->tipo;
            $this->ModeloVentas->ingresarventad_temporal($idventa,$producto,$cantidad,$precio,$tipo);
        }
    }

    function abrirturno(){
        $cantidad = $this->input->post('cantidad');
        $nombre = $this->input->post('nombre');
        $suc = $this->input->post('suc');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->abrirturno($cantidad,$nombre,$fecha,$horaa,$suc);
    }
    function cerrarturno(){
        $id = $this->input->post('id');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->cerrarturno($id,$horaa);
    }
    function consultarturno(){
        $id = $this->input->post('id');
        $sucursal = $this->input->post('suc');
        $vturno=$this->ModeloVentas->consultarturno($id);
        foreach ($vturno->result() as $item){
            $fecha= $item->fecha;
            $fechac= $item->fechacierre;
            $horaa= $item->horaa;
            $horac= $item->horac;
        }
        if ($horac=='00:00:00') {
            $horac =date('H:i:s');
            $fechac =date('Y-m-d');
        }
        $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac,$sucursal);
        echo "<p><b>Total: ".number_format($totalventas,2,'.',',')."</b></p>";
        $totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac,$sucursal);
        $obed =$totalventas-$totalpreciocompra;
        echo "<p><b>Utilidad: ".number_format($obed,2,'.',',')."</b></p>";
        $productos =$this->ModeloVentas->consultartotalturnopro($fecha,$horaa,$horac,$fechac,$sucursal);
        echo "<div class='panel-body table-responsive' id='table'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables2'>
                            <thead class='vd_bg-green vd_white'>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                    
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($productos->result() as $item){
            echo "<tr><td>".$item->producto."</td><td>".$item->cantidad."</td><td> $".$item->precio."</td></tr>";
        }
        echo "</tbody> </table></div>";
        echo "<div style='    position: absolute; float: left; top: 0; left: 800px; background: white; width: 250px; z-index: 20;'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables'>
                            <thead>
                                <tr>
                                    <td colspan='4' style='text-align: center;'>Productos mas vendido</td>
                                </tr>
                            </thead>
                            <tbody>";
        $productosmas =$this->ModeloVentas->consultartotalturnopromas($fecha,$horaa,$horac,$fechac,$sucursal);
        foreach ($productosmas->result() as $item){
            echo "<tr><td colspan='2' style='text-align: center;'>".$item->total." </td><td colspan='2' style='text-align: center;'>".$item->producto." </td></tr>";
        }
        echo "</tbody> </table></div>";

    }

    ////////////////////////// Ventas Nuevas //////////////////////////////////
    public function buscar_producto(){
        $nom=$this->input->post('producto');
        $idsucursal=$this->idsucursal;
        $html='';
        $result=$this->ModelCatalogos->get_productos_traspasos($nom,$idsucursal);

        echo json_encode($result);
    }

    public function addproducto_tabla(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $prod_paquetes = $this->input->post('productos_barras');
        $idsucursal=$this->idsucursal;
        
        $personalv=$this->ModelCatalogos->getproducto_sucursal($prod,$idsucursal);
        //$oProducto = new Producto();
        if($prod!=0){
            foreach ($personalv->result() as $item){
                $id = $item->productoid;
                $codigo = $item->codigo;
                $nombre = $item->nombre;
                $precioventa = $item->precioventa;
                $especial1_precio = $item->especial1_precio;
                $especial2_precio = $item->especial2_precio;
                $mediomayoreo = $item->mediomayoreo;
                $mayoreo = $item->mayoreo;
                $canmediomayoreo = $item->canmediomayoreo;
                $canmayoreo = $item->canmayoreo;
                $especial1_cantidad = $item->especial1_cantidad;
                $especial2_cantidad = $item->especial2_cantidad;
                $precio_paq = $item->precio_paq;
                $stock = $item->stock;
                $tipo=0;
                if($prod_paquetes==$item->codigo_pag){
                    $codigo = $item->codigo_pag;
                    $tipo=1;
                    $nombre=$nombre.' (Paquete)';
                }
                $oProducto=array(
                    "id"=>$id,
                    "codigo"=>$codigo,
                    "nombre"=>$nombre,
                    'precioventa'=>$precioventa,
                    'mediomayoreo'=>$mediomayoreo,
                    'canmediomayoreo'=>$canmediomayoreo,
                    'mayoreo'=>$mayoreo,
                    'canmayoreo'=>$canmayoreo,
                    'especial1_precio'=>$especial1_precio,
                    'especial1_cantidad'=>$especial1_cantidad,
                    'especial2_precio'=>$especial2_precio,
                    'especial2_cantidad'=>$especial2_cantidad,
                    'precio_paq'=>$precio_paq,
                    'tipo'=>$tipo,
                    'stock'=>$stock
                );

            }

            if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                $idx = array_search($oProducto, $_SESSION['pro']);
                $_SESSION['can'][$idx]+=$cant;
            }
            else{ //sino lo agrega
                array_push($_SESSION['pro'],$oProducto);
                array_push($_SESSION['can'],$cant);
            }
        }

        //var_dump($_SESSION['can']);
        //======================================================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 
            if ($Cantidad<=$fila['especial1_cantidad']) {
                $precio=$fila['especial1_precio'];
            }elseif ($Cantidad > $fila['especial1_cantidad'] && $Cantidad<=$fila['especial2_cantidad']) {
                $precio=$fila['especial2_precio'];
            }elseif ($Cantidad>=$fila['canmayoreo']) {
                $precio=$fila['mayoreo'];
            }elseif ($Cantidad>=$fila['canmediomayoreo'] && $Cantidad<$fila['canmayoreo']) {
                $precio=$fila['mediomayoreo'];
            }else{
                $precio=$fila['precioventa'];
            }
            if(isset($fila['tipo'])){
                $tipo=$fila['tipo'];
            }else{
                $tipo=0;
            }
            if($tipo==1){
                $precio=$fila['precio_paq'];
            }
            $cantotal=$Cantidad*$precio;

            //var_dump($Cantidad);
        ?>
            <tr class="producto_<?php echo $count;?>">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <input type="hidden" name="vstipo" id="vstipo" value="<?php echo $tipo;?>">
                    <?php echo $fila['codigo'];?>
                </td>                                        
                <td>
                    <input type="number" name="vscanti" id="vscanti" class="vscanti_<?php echo $count; ?>" oninput="calcular_cantidad_total(<?php echo $count; ?>)" value="<?php echo $Cantidad;?>" style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                <td><?php echo $fila['nombre'];?></td> 
                <td class="stock_col"><?php echo $fila['stock'];?></td>                                        
                <td>
                    <div class="btn-group">
                        $<input type="text" class="precio_<?php echo $count; ?>"
                        data-idproducto="<?php echo $fila['id']; ?>" 
                        data-cantidad="<?php echo $_SESSION['can'][$count]; ?>" 
                        data-precioventa="<?php echo $fila['precioventa']; ?>" 
                        data-especial1_cantidad="<?php echo $fila['especial1_cantidad']; ?>" 
                        data-especial1_precio="<?php echo $fila['especial1_precio']; ?>" 
                        data-especial2_cantidad="<?php echo $fila['especial2_cantidad']; ?>" 
                        data-especial2_precio="<?php echo $fila['especial2_precio']; ?>"
                        data-mayoreo="<?php echo $fila['mayoreo']; ?>"
                        data-canmayoreo="<?php echo $fila['canmayoreo']; ?>" 
                        data-canmediomayoreo="<?php echo $fila['canmediomayoreo']; ?>" 
                        data-mediomayoreo="<?php echo $fila['mediomayoreo']; ?>" 
                        name="vsprecio" id="vsprecio" value="<?php echo $precio ?>" readonly style="background: transparent;border: 0px;width: 100px;">
                    </div>
                </td>                                        
                <td><div class="btn-group">
                        $<input type="text" class="vstotal vstotal_<?php echo $count; ?>" name="vstotal" id="vstotal" value="<?php echo $cantotal ?>" readonly style="background: transparent;border: 0px;    width: 100px;">
                    </div>    
                </td>                                        
                <td>                                            
                    <a data-original-title="Eliminar" title="Eliminar" style="font-size: 20px; color: #f2b327;" onclick="deletepro(<?php echo $count;?>)">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php
        $count++;
        }
        $contadormenos=$count-1;
        echo '<input type="hidden" id="borrar_producto" value="'.$contadormenos.'">';
    }

    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
    }
    
    function temporal_get_sucursal(){
        $idsucursal=$this->idsucursal;
        $get_result_ventas=$this->General_model->get_records_condition('activo=1 AND sucursal='.$idsucursal,'ventas_temporal');
        $venta_aux=0;
        foreach ($get_result_ventas as $x){
            $venta_aux=1;
        }
        if($venta_aux==1){
            $html='<div class="form-group">
                <input type="hidden" id="idtemporal_aux" value="1" readonly>
                <select class="form-control" id="idtemporal" onchange="ventas_temporal_get()"">
                    <option value="0">Seleccione venta temporal</option>';
                    foreach ($get_result_ventas as $x){
                        $html.='<option value="'.$x->id_venta.'">'.$x->id_venta.' / '.date('d/m/Y g:i:a',strtotime($x->reg)).'</option>';
                    }
                $html.='</select>
            </div>';
        }else{
            $html='<input type="hidden" id="idtemporal_aux" value="0" readonly>';
        }
        echo $html;    
    }

    public function get_venta_temporal(){
        $id = $this->input->post('id');
        $get_result_ventas=$this->General_model->get_records_condition('id_venta='.$id,'ventas_temporal');
        $id_cliente=0;
        $cliente='';
        foreach ($get_result_ventas as $item){
            $id_cliente=$item->id_cliente;
        }
        $get_result_cliente=$this->General_model->get_records_condition('ClientesId='.$id_cliente,'clientes');
        foreach ($get_result_cliente as $y){
            $cliente=$y->Nom;
        }
        $get_result_ventas_detalles=$this->General_model->get_records_condition('id_venta='.$id,'venta_detalle_temporal');
        $arrayventa = array('venta' =>$get_result_ventas,'venta_detalle'=>$get_result_ventas_detalles,'id_cliente'=>$id_cliente,'cliente'=>$cliente);
        echo json_encode($arrayventa);
    }

    public function cambio_activo_temporal(){
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('id_venta',$id,$data,'ventas_temporal');
    }


    public function actulizar_producto_tabla(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $idsucursal=$this->idsucursal;
        $personalv=$this->ModelCatalogos->getproducto_sucursal($prod,$idsucursal);
        //$oProducto = new Producto();
        if($prod!=0){
            foreach ($personalv->result() as $item){
                $id = $item->productoid;
                $codigo = $item->codigo;
                $nombre = $item->nombre;
                $precioventa = $item->precioventa;
                $especial1_precio = $item->especial1_precio;
                $especial2_precio = $item->especial2_precio;
                $mediomayoreo = $item->mediomayoreo;
                $mayoreo = $item->mayoreo;
                $canmediomayoreo = $item->canmediomayoreo;
                $canmayoreo = $item->canmayoreo;
                $especial1_cantidad = $item->especial1_cantidad;
                $especial2_cantidad = $item->especial2_cantidad;
                $oProducto=array(
                    "id"=>$id,
                    "codigo"=>$codigo,
                    "nombre"=>$nombre,
                    'precioventa'=>$precioventa,
                    'mediomayoreo'=>$mediomayoreo,
                    'canmediomayoreo'=>$canmediomayoreo,
                    'mayoreo'=>$mayoreo,
                    'canmayoreo'=>$canmayoreo,
                    'especial1_precio'=>$especial1_precio,
                    'especial1_cantidad'=>$especial1_cantidad,
                    'especial2_precio'=>$especial2_precio,
                    'especial2_cantidad'=>$especial2_cantidad
                );

            }

            if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
                $idx = array_search($oProducto, $_SESSION['pro']);
                $_SESSION['can'][$idx]=$cant;
            }
            else{ //sino lo agrega
                array_push($_SESSION['pro'],$oProducto);
                array_push($_SESSION['can'],$cant);
            }
        }
    }
}