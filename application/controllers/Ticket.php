<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ticket extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->library('Pdf');
        //$this->load->model('Solicitud');
        $this->load->model('ModeloVentas');
        $this->load->model('General_model');
        $this->load->helper('url');

    }
	public function index(){
		$id=$this->input->get('id');
		
		$data['configticket']=$this->ModeloVentas->configticket();
		$data['getventas']=$this->ModeloVentas->getventas($id);
		$data['getventasd']=$this->ModeloVentas->getventasd($id);
		$this->load->view('Reportes/ticket',$data);
	        
	}

	/// ticket venta
    function ticket_venta($id){
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mangoo');
        $pdf->SetTitle('Ticket');
        $pdf->SetSubject('Ticket por venta');
        $pdf->SetKeywords('Ticket, Pago, Comprobante');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins('5','9','5');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFont('courier', '', '10');

        $pdf->AddPage();
        
        $imglogo=base_url().'img/pos.png';
        $getventas=$this->ModeloVentas->getventas($id);
        foreach ($getventas->result() as $item){
          $idticket = $item->id_venta;
          $id_personal = $item->id_personal;
          $id_cliente = $item->id_cliente;
          $idsucursal = $item->sucursal;
          $personalId = $item->id_personal;
          $cliente=$this->General_model->get_record('ClientesId',$id_cliente,'clientes');
          $sucursal=$this->General_model->get_record('id',$idsucursal,'sucursales');
          $personal=$this->General_model->get_record('personalId',$personalId,'personal');
          $metodo = $item->metodo;
          $subtotal = $item->subtotal;
          $descuento = $item->descuentocant;
          $monto_total = $item->monto_total;
          $cambio = $item->cambio;
          $fecha = date("d/m/Y",strtotime($item->reg));
          $hora = date("g:i:a",strtotime($item->reg));
        }
        $getventasd=$this->ModeloVentas->getventasd($id);
        $html = '';
        /*
                           <tr>
                        <td align="center">Rejuvenecimiento Integral</td>
                    </tr>
                    <tr>
                        <td align="center">27 A Norte 1019 Colonia San Alejandro CP.72090
                           <hr>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-style: italic;">"La pasión cuando llega, nos hace más humildes y nos da un sentimiento de éxito diario"<br></td>
                        <hr>
                    </tr>
        */
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" width="100px">
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="center">Casa Verona</td>
                    </tr>
                    <tr>
                        <td align="center">Sucursal: '.$sucursal->nombre.'</td>
                    </tr>
                    <tr>
                        <td align="center">Av. Rafael Cortés # 11 Esquina 25 Agosto. Tecali de Herrera, Pue.</td>
                    </tr>
                    <tr>
                        <td align="center">Teléfono: 224 115 4010</td>
                    </tr>
                    <tr>
                        <td align="center">Información de venta</td>
                    </tr>
                    <tr>
                        <td align="center">Fecha de venta: '.$fecha.'</td>
                    </tr>
                    <tr>
                        <td align="center">Hora de venta: '.$hora.'</td>
                    </tr>
                    <tr> 
                        <td align="center">Cliente: '.$cliente->Nom.'</td>
                    </tr>
                    <tr>
                        <td align="center">Cajero: '.$personal->nombre.' '.$personal->apellidos.'</td>
                    </tr>
                    <tr>
                        <hr> 
                        <td align="center">Productos</td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td width="16%" style="font-size:9px">Cant</td>
                        <td width="35%" style="font-size:9px">Articulo</td>
                        <td width="26%" style="font-size:9px">Precio U.</td>
                        <td width="23%" style="font-size:9px">Subtotal</td>
                    </tr>';
                    foreach ($getventasd->result() as $rowEmp){
                        if($rowEmp->tipo==1){
                            $productoe=' (Paquete)';
                        }else{
                            $productoe='';
                        }
          $html .= '<tr width="100%">
                        <td width="16%" style="font-size:9px">'.$rowEmp->cantidad.'</td>
                        <td width="35%" style="font-size:9px">'.$rowEmp->nombre.''.$productoe.'</td>
                        <td width="26%" style="font-size:9px">$'.number_format($rowEmp->precio, 2).'</td>
                        <td width="23%" style="font-size:9px">$'.number_format($rowEmp->cantidad*$rowEmp->precio, 2).'</td>
                    </tr>';
                    }
          $html .= '<tr width="100%">
                        <hr>
                        <td width="30%"></td>
                        <td width="40%">Subtotal</td>
                        <td width="30%">$'.number_format($subtotal, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="30%"></td>
                        <td width="40%">Descuento</td>
                        <td width="30%">$'.number_format($descuento, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="30%"></td>
                        <td width="40%">Total</td>
                        <td width="30%">$'.number_format($monto_total, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <td width="30%"></td>
                        <td width="40%">Cambio</td>
                        <td width="30%">$'.number_format($cambio, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <hr>
                        <td width="100%" align="center">
                            ¡GRACIAS POR TU PREFERENCIA!
                        </td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td align="center">
                            Este ticket no tiene ninguna validéz o representación fiscal
                        </td>
                    </tr>

                    ';
  
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('ticket.pdf', 'I');

    }

    /// ticket Compras
    function ticket_compras($id){
        
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(80, 250), true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Mangoo');
        $pdf->SetTitle('Ticket');
        $pdf->SetSubject('Ticket por venta');
        $pdf->SetKeywords('Ticket, Pago, Comprobante');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins('8','9','8');
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetFont('courier', '', '10');

        $pdf->AddPage();
        
        $imglogo=base_url().'img/vpc2.png';
        $getventas=$this->ModeloVentas->getcompras($id);
        foreach ($getventas->result() as $item){
          $idticket = $item->id_compra;
          $monto_total = $item->monto_total;
          $idsucursal = $item->idsucursal;
          $personalId = $item->personalId;
          $sucursal=$this->General_model->get_record('id',$idsucursal,'sucursales');
          $personal=$this->General_model->get_record('personalId',$personalId,'personal');
          $fecha = date("d/m/Y",strtotime($item->reg));
          $hora = date("g:i:a",strtotime($item->reg));
        }
        $get_compras=$this->ModeloVentas->getcomprasd($id);
        $html = '';
        $html .= '<table border="0">
                    <tr>
                        <td align="center">
                            <img src="'.$imglogo.'" width="100px">
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">San Pablo Dulces y botanas</td>
                    </tr>
                    <tr>
                        <td align="center">Sucursal: '.$sucursal->nombre.'</td>
                    </tr>
                    <tr>
                        <td align="center">Información de compra</td>
                    </tr>
                    <tr>
                        <td align="center">Fecha de compra: '.$fecha.'</td>
                    </tr>
                    <tr>
                        <td align="center">Hora de compra: '.$hora.'</td>
                    </tr>
                    <tr>
                        <td align="center">Cajero: '.$personal->nombre.' '.$personal->apellidos.'</td>
                    </tr>
                    <tr>
                        <hr> 
                        <td align="center">Productos</td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td width="30%">Cant</td>
                        <td width="40%">Articulo</td>
                        <td width="30%">Precio</td>
                    </tr>';
                    foreach ($get_compras->result() as $rowEmp){
          $html .= '<tr width="100%">
                        <td width="30%">'.$rowEmp->cantidad.'</td>
                        <td width="40%">'.$rowEmp->nombre.'</td>
                        <td width="30%">$'.number_format($rowEmp->precio_compra, 2).'</td>
                    </tr>';
                    }
          $html .= '<tr width="100%">
                        <td width="30%"></td>
                        <td width="40%">Total</td>
                        <td width="30%">$'.number_format($monto_total, 2).'</td>
                    </tr>
                    <tr width="100%">
                        <hr>
                        <td width="100%" align="center">
                            ¡GRACIAS POR TU PREFERENCIA!
                        </td>
                        <hr>
                    </tr>
                    <tr width="100%">
                        <td align="center">
                            Este ticket no tiene ninguna validéz o representación fiscal
                        </td>
                    </tr>

                    ';
  
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->IncludeJS('print(true);');
        $pdf->Output('ticket.pdf', 'I');

    }
}