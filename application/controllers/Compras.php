<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
        $this->load->model('General_model');
        $this->load->model('ModelCatalogos');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
            if($this->idsucursal!=1){
                header('Location: '.base_url().'Inicio');
            } 
        }else{
            redirect('/Login');
        }
    }
	public function index(){
        $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compras2',$data);
        $this->load->view('templates/footer');
        $this->load->view('compras/jscompras');
	}
    public function searchpro(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProveedor->proveedorallsearch($usu);
        echo json_encode($results->result());
    }
    
    
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $prec = $this->input->post('prec');
        $prov = $this->input->post('prov');
        $personalv=$this->ModeloProductos->getproducto($prod);
        foreach ($personalv->result() as $item){
              $id = $item->productoid;
              $codigo = $item->codigo;
              $nombre = $item->nombre;
              $precio = $item->preciocompra;
        }
        $get_proveedor=$this->General_model->get_records_condition('id_proveedor='.$prov,'proveedores');
        foreach ($get_proveedor as $x) {
            $proveedor=$x->razon_social;
            $provedor_id=$x->id_proveedor;
        }
        $array = array("id"=>$id,
                        "codigo"=>$codigo,
                        "cant"=>$cant,
                        "producto"=>$nombre,
                        "precio"=>$prec,
                        "proveedor"=>$proveedor,
                        "provedor_id"=>$provedor_id
                    );
            echo json_encode($array);
    }
    function ingresarcompra(){
        $uss = $this->input->post('uss');
        $prov = $this->input->post('prov');
        $total = $this->input->post('total');
        $idsucursal = $this->input->post('idsucursal');
        $personalId = $this->personalId;
        $id=$this->ModeloVentas->ingresarcompra($uss,$prov,$total,$idsucursal,$personalId);
        echo $id;
    }
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idcompra = $DATA[$i]->idcompra;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $provedor_id = $DATA[$i]->provedor_id;
            $this->ModeloVentas->ingresarcomprad($idcompra,$producto,$cantidad,$precio,$provedor_id);
            $this->ModelCatalogos->stock_aumentar_producto(floatval($cantidad),intval($DATA[$i]->idsucursal),intval($producto));
            $this->General_model->updateCatalogo('productos_sucursal_precios',array('preciocompra'=>$precio),array('productoid'=>$producto,'idsucursal'=>$DATA[$i]->idsucursal));
        }
    }

    public function buscar_producto(){
        $nom=$this->input->post('producto');
        $idsucursal=$this->input->post('idsucursal');
        $html='';
        $result=$this->ModelCatalogos->get_productos_traspasos($nom,$idsucursal);
        echo json_encode($result);
    }


}