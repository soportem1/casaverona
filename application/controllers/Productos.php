<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        }else{
            redirect('/Login');
        }
        
    }
    public function index(){ 
        $data['idsucursal']=$this->idsucursal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productos',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/productosjs');
    }
    public function productosadd(){
        $data["categorias"] = $this->ModeloProductos->categorias();
        $data['get_marca']=$this->General_model->get_records_condition('activo=1','marca');
        $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
        if($this->idsucursal==1){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('productos/productoadd',$data);
            $this->load->view('templates/footer');
            $this->load->view('productos/jsproducto');
        }else{
            header('Location: '.base_url().'Productos');
        } 
    }

    public function registra_productos(){
        $data=$this->input->post();
        $productoid=$data['productoid'];
        unset($data['img']);
        unset($data['pstock']);
        unset($data['preciocompra']);
        unset($data['porcentaje']);
        unset($data['precioventa']);
        if(!isset($data['pag'])){
            $data['pag']=0;
        }
        if($productoid==0){
            $data['precioventa']=$this->personalId;
            $id=$this->General_model->add_record('productos',$data);
        }else{
            $id=$this->General_model->edit_record('productoid',$productoid,$data,'productos');
            $id=$productoid;
        }
        echo $id;
    }

    function imgpro(){
        $idpro = $this->input->post('idpro');
        

        $upload_folder ='public/img/productos';
        $nombre_archivo = $_FILES['img']['name'];
        $tipo_archivo = $_FILES['img']['type'];
        $tamano_archivo = $_FILES['img']['size'];
        $tmp_archivo = $_FILES['img']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $fecha=date('ymd-His');
        $tipo_archivo1 = explode('/', $tipo_archivo); 
        $tipoactivo=$tipo_archivo1[1];
        if ($tipoactivo!='png') {
            $tipo_archivo1 = explode('+', $tipoactivo); 
            $tipoactivo=$tipo_archivo1[0];
        }
        $archivador = $upload_folder . '/'.$fecha.'pro.'.$tipoactivo;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $resimg=$this->ModeloProductos->imgpro($archivador,$idpro);
            $return = Array('ok'=>TRUE,'img'=>$tipoactivo);
        }
        echo json_encode($return);
    }
    public function deleteproductos(){
        $id = $this->input->post('id');
        $this->ModeloProductos->productosdelete($id); 
    }
    function buscarpro(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloProductos->productoallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trpro_<?php echo $item->productoid; ?>">
                <td>
                    <?php 
                    if ($item->img=='') {
                        $img='public/img/ops.png';
                    }else{
                        $img=$item->img;
                    }?>
                <img src="<?php echo base_url(); ?><?php echo $img; ?>" class="imgpro" >
                </td>
              <td><?php echo $item->nombre; ?></td>
              <td><?php echo $item->descripcion; ?></td>
              <td><?php echo $item->categoria; ?></td>
              <td><?php echo $item->precioventa; ?></td>
              <td><?php echo $item->stock; ?></td>
              <td>
                  <div class="btn-group mr-1 mb-1">
                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                      <a class="dropdown-item" href="<?php echo base_url();?>Productos/productosadd?id=<?php echo $item->productoid; ?>">Editar</a>
                      <a class="dropdown-item" onclick="etiquetas(<?php echo $item->productoid; ?>,'<?php echo $item->codigo; ?>','<?php echo $item->nombre; ?>','<?php echo $item->categoria; ?>',<?php echo $item->precioventa; ?>);"href="#">Etiquetas</a>
                      <a class="dropdown-item" onclick="productodelete(<?php echo $item->productoid; ?>);"href="#">Eliminar</a>
                  </div>
              </div>
              </td>
            </tr>
        <?php }
    }
    //// 25 - 03 - 2021
    public function getlistado(){
        $params = $this->input->post();
        $params['idsucursal'] = $this->idsucursal;
        $getdata = $this->ModelCatalogos->get_productos($params);
        $totaldata= $this->ModelCatalogos->total_productos($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function getlistado_categoria(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_categoria($params);
        $totaldata= $this->ModelCatalogos->total_categoria($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registro_categoria(){
        $data=$this->input->post();
        $idcategoria=$data['categoriaId'];
        unset($data['categoriaId']);

        if($idcategoria==0){
            $id=$this->General_model->add_record('categoria',$data);
        }else{
            $this->General_model->edit_record('categoriaId',$idcategoria,$data,'categoria');
            $id=$idcategoria;
        }
        echo $id;
    }

    public function deleteregistro_cate(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('categoriaId',$id,$data,'categoria');
    }

    public function getlistado_marca(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_marcas($params);
        $totaldata= $this->ModelCatalogos->total_marcas($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registro_marca(){
        $data=$this->input->post();
        $idmarca=$data['idmarca'];
        if($idmarca==0){
            $id=$this->General_model->add_record('marca',$data);
        }else{
            $this->General_model->edit_record('idmarca',$idmarca,$data,'marca');
            $id=$idmarca;
        }
        echo $id;
    }

    public function deleteregistro_marca(){
        $id=$this->input->post('id');
        $data = array('activo'=>0);
        $this->General_model->edit_record('idmarca',$id,$data,'marca');
    }

    public function insert_productos_sucursal_precios(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $id = $DATA[$i]->iddetalles;
            $data['productoid']=$DATA[$i]->productoid;
            $data['idsucursal']=$DATA[$i]->idsucursal;
            $data['stock']=$DATA[$i]->stock;
            $data['stock_paq']=$DATA[$i]->stock_paq;
            $data['preciocompra']=$DATA[$i]->preciocompra;
            $data['porcentaje']=$DATA[$i]->porcentaje;
            $data['precioventa']=$DATA[$i]->precioventa;
            //$data['especial1_precio']=$DATA[$i]->especial1_precio;
            $data['especial1_precio']=$DATA[$i]->precioventa;
            //$data['especial1_cantidad']=$DATA[$i]->especial1_cantidad;
            $data['especial1_cantidad']=1;
            //$data['especial2_precio']=$DATA[$i]->especial2_precio;
            $data['especial2_precio']=$DATA[$i]->precioventa;
            //$data['especial2_cantidad']=$DATA[$i]->especial2_cantidad;
            $data['especial2_cantidad']=1;
            //$data['mediomayoreo']=$DATA[$i]->mediomayoreo;
            $data['mediomayoreo']=$DATA[$i]->precioventa;
            //$data['canmediomayoreo']=$DATA[$i]->canmediomayoreo;
            $data['canmediomayoreo']=1;
            //$data['mayoreo']=$DATA[$i]->mayoreo;
            $data['mayoreo']=$DATA[$i]->precioventa;
            //$data['canmayoreo']=$DATA[$i]->canmayoreo;
            $data['canmayoreo']=1;
            //$data['precio_paq']=$DATA[$i]->precio_paq;
            $data['precio_paq']=$DATA[$i]->precioventa;
            if($id==0){
                $this->General_model->add_record('productos_sucursal_precios',$data);
            }else{
                $this->General_model->edit_record('id',$id,$data,'productos_sucursal_precios');
            }   
        }
    }
    public function get_precios_sucu(){
        $id=$this->input->post('id');
        $html='<table class="table table-striped">
        <thead>
          <tr>
            <th>Sucursal</th>
            <th>Precio venta</th>
          </tr>
        </thead>
        <tbody>';
        $get_precios=$this->ModelCatalogos->get_precios_sucur($id,$this->idsucursal);
        foreach ($get_precios as $item){
            $html.='<tr>
                <td>'.$item->nombre.'</td>
                <td>'.$item->precioventa.'</td>
               </tr>';
        }
        $html.='</tbody>
        </table>';
        echo $html;
    }

    public function get_existencias_sucu(){
        $id=$this->input->post('id');
        $html='<table class="table table-striped">
        <thead>
          <tr>
            <th>Sucursal</th>
            <th>Total KG </th>
            <th>Contenido paquete</th>
            <th>Paqs</th>
            
          </tr>
        </thead>
        <tbody>';
        $get_precios=$this->ModelCatalogos->get_existencias_sucur($id,$this->idsucursal);
        foreach ($get_precios as $item){
            if($item->stock_paq>0){
                $paquete=round($item->stock/$item->stock_paq, 2);
            }else{
                $paquete=round($item->stock, 2);
            }
            $paquetel='';
            if($paquete>0){
                $paquetel=$paquete.' Paquetes';
            }
            $html.='<tr>
                <td>'.$item->nombre.'</td>
                <td>'.$item->stock.'</td>
                <td>'.$item->stock_paq.'</td>
                <td>'.$paquetel.'</td>
                
               </tr>';
        }
        $html.='</tbody>
        </table>';
        echo $html;
    }

}