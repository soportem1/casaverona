<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModelCatalogos');
        if (isset($_SESSION['idpersonal_tz'])){
        }else{
            redirect('/Login');
        }
    }
	public function index(){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('proveedores/proveedores');
            $this->load->view('templates/footer');
            $this->load->view('proveedores/jslistado');
	}
    public function proveedoradd(){
        $data["Estados"] = $this->ModeloProveedor->estados();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedores/proveedoradd',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedores/jsproveedores');
    }
    function preoveedoraddd(){
        $id = $this->input->post('id');
        $razonSocial = $this->input->post('razonSocial');
        $domicilio = $this->input->post('domicilio');
        $ciudad = $this->input->post('ciudad');
        $cp = $this->input->post('cp');
        $estado = $this->input->post('estado');
        $contacto = $this->input->post('contacto');
        $email = $this->input->post('email');
        $rfc = $this->input->post('rfc');
        $telefonoLocal = $this->input->post('telefonoLocal');
        $telefonoCelular = $this->input->post('telefonoCelular');
        $fax = $this->input->post('fax');
        $obser = $this->input->post('obser');



        if ($id>0) {
            $this->ModeloProveedor->proveedorupdate($id,$razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$telefonoLocal,$telefonoCelular,$fax,$obser);
        }else{
            $this->ModeloProveedor->proveedorinsert($razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$telefonoLocal,$telefonoCelular,$fax,$obser);
        }
        redirect('/Proveedores');
    }
    
    public function deleteproveedor(){
        $id = $this->input->post('id');
        $this->ModeloProveedor->deleteproveedors($id); 
    }
    /*
    function buscarprov(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloProveedor->proveedorallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trcli_<?php echo $item->id_proveedor; ?>">
                <td><?php echo $item->razon_social; ?></td>
                <td><?php echo $item->domicilio; ?></td>
                <td><?php echo $item->ciudad; ?></td>
    
                <td><?php echo $item->telefono_local; ?></td>
                <td><?php echo $item->telefono_celular; ?></td>
                <td><?php echo $item->email_contacto; ?></td>
                <td><?php echo $item->rfc; ?></td>
                <td>
                    <div class="btn-group mr-1 mb-1">
                        <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                          <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                              <a class="dropdown-item" href="<?php echo base_url();?>Proveedores/proveedoradd?id=<?php echo $item->id_proveedor; ?>">Editar</a>
                              <a class="dropdown-item" onclick="proveedordelete(<?php echo $item->id_proveedor; ?>);"href="#">Eliminar</a>
                          </div>
                    </div>
                </td>
            </tr>
        <?php }
    }
    */
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_proveedores($params);
        $totaldata= $this->ModelCatalogos->total_proveedores($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    

       
    
}
