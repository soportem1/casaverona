<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductosEmpleados extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        if (isset($_SESSION['idpersonal_tz'])){
            $this->personalId=$idpersonal_tz=$_SESSION['idpersonal_tz'];
            $this->idsucursal=$idsucursal_tz=$_SESSION['idsucursal_tz'];
        }else{
            redirect('/Login');
        }
        
    }
    public function index(){ 
        $data['idsucursal']=$this->idsucursal;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/producto_empleado_list',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/producto_empleado_listjs');
    }

    public function productosadd(){
        $data["categorias"] = $this->ModeloProductos->categorias();
        $data['get_marca']=$this->General_model->get_records_condition('activo=1','marca');
        $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
        if($this->idsucursal==1){
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            $this->load->view('productos/producto_empleado',$data);
            $this->load->view('templates/footer');
        }else{
            header('Location: '.base_url().'Productos');
        } 
    }
}
