<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaTurnos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloTurno');
    }
    public function index(){
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('turno/listaturnos');
            $this->load->view('templates/footer');
            $this->load->view('turno/listaturnosjs');
    }
       
    public function getlistturnos() {
        $params = $this->input->post();
        $getdata = $this->ModeloTurno->getlistturnos($params);
        $totaldata= $this->ModeloTurno->getlistturnost($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
}