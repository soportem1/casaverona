<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listacompras extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
        $this->load->model('ModelCatalogos');
        $this->load->model('General_model');
        if (isset($_SESSION['idpersonal_tz'])){

        }else{
            redirect('/Login');
        }
    }
	public function index(){
            /*
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Clientes/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filaslcompras();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 20; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["lcompras"] = $this->ModeloVentas->total_paginadoslcompras($pagex,$config['per_page']);
            */
            $data['get_sucursales']=$this->General_model->get_records_condition('activo=1','sucursales');
            $this->load->view('templates/header');
            $this->load->view('templates/navbar',$data);
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('compras/lcompras');
            $this->load->view('templates/footer');
            $this->load->view('compras/listadojs');
	}

    /*
    function consultar(){
        $inicio = $this->input->post('fechain');
        $fin = $this->input->post('fechafin');
        $clcompras = $this->ModeloVentas->lcomprasconsultar($inicio,$fin);
        foreach ($clcompras->result() as $item) {
            echo '<tr id="trcli_'.$item->id_detalle_compra.'">
                          <td>'.$item->id_detalle_compra.'</td>
                          <td>'.$item->reg.'</td>
                          <td>'.$item->producto.'</td>
                          <td>'.$item->razon_social.'</td>
                          <td>'.$item->cantidad.'</td>
                          <td>'.$item->precio_compra.'</td>
                        </tr>';
        }
    }
    */
    
    public function getlistado(){
        $params = $this->input->post();
        $getdata = $this->ModelCatalogos->get_compras($params);
        $totaldata= $this->ModelCatalogos->total_compras($params); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totaldata),  
            "recordsFiltered" => intval($totaldata),
            "data"            => $getdata->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }  

    public function get_tabla_detalles(){
        $id = $this->input->post('id');
        $html='';
        $result=$this->ModelCatalogos->get_compras_detalles($id);
        foreach ($result as $c){
            $mult=$c->cantidad*$c->precio_compra;
            $html.='<tr>
                    <td>'.$c->id_detalle_compra.'</td>
                    <td>'.$c->producto.'</td>
                    <td>'.$c->cantidad.'</td>
                    <td>$'.number_format($c->precio_compra,2,'.',',').'</td>
                    <td>$'.number_format($mult,2,'.',',').'</td>
                    <td>'.$c->razon_social.'</td>
                   </tr>';
        }
        echo $html;
    }
}
