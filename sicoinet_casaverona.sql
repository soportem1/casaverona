-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql1002.mochahost.com:3306
-- Tiempo de generación: 19-10-2021 a las 09:44:56
-- Versión del servidor: 10.3.31-MariaDB
-- Versión de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sicoinet_casaverona`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `SP_ADD_PERSONAL`$$
CREATE  PROCEDURE `SP_ADD_PERSONAL` (IN `_NOM` VARCHAR(120), IN `_SEX` INT(1))  NO SQL
INSERT INTO personal (nombre,sexo)VALUES (_NOM,_SEX)$$

DROP PROCEDURE IF EXISTS `SP_DEL_PERSONAL`$$
CREATE  PROCEDURE `SP_DEL_PERSONAL` (IN `_ID` INT)  NO SQL
UPDATE personal SET estatus=0 where personalId=_ID$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_ESTADOS`$$
CREATE  PROCEDURE `SP_GET_ALL_ESTADOS` ()  NO SQL
SELECT * FROM Estado$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_PERFILES`$$
CREATE  PROCEDURE `SP_GET_ALL_PERFILES` ()  NO SQL
SELECT *  FROM Perfiles$$

DROP PROCEDURE IF EXISTS `SP_GET_ALL_PERSONAL`$$
CREATE  PROCEDURE `SP_GET_ALL_PERSONAL` ()  NO SQL
SELECT per.personalId,per.nombre,per.apellidos,usu.Usuario 
FROM personal as per 
LEFT JOIN usuarios as usu on usu.personalId=per.personalId
WHERE per.tipo=1 AND per.estatus=1$$

DROP PROCEDURE IF EXISTS `SP_GET_MENUS`$$
CREATE  PROCEDURE `SP_GET_MENUS` (IN `PERFIL` INT)  NO SQL
SELECT distinct men.MenuId,men.Nombre,men.Icon from menu as men, menu_sub as mens, Perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId=PERFIL$$

DROP PROCEDURE IF EXISTS `SP_GET_PERSONAL`$$
CREATE  PROCEDURE `SP_GET_PERSONAL` (IN `_ID` INT)  NO SQL
SELECT *  FROM personal where personalId=_ID$$

DROP PROCEDURE IF EXISTS `SP_GET_SESSION`$$
CREATE  PROCEDURE `SP_GET_SESSION` (IN `USUA` VARCHAR(10))  NO SQL
SELECT usu.UsuarioID,per.personalId,per.nombre, usu.perfilId, usu.contrasena,per.idsucursal FROM usuarios as usu,personal as per where per.personalId = usu.personalId and per.estatus=1 and usu.Usuario = USUA$$

DROP PROCEDURE IF EXISTS `SP_GET_USUARIOS`$$
CREATE  PROCEDURE `SP_GET_USUARIOS` ()  NO SQL
SELECT usu.UsuarioID,usu.Usuario,per.nombre as perfil FROM usuarios as usu, Perfiles as per WHERE usu.perfilId=per.perfilId$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `categoriaId` int(11) NOT NULL,
  `categoria` varchar(200) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`categoriaId`, `categoria`, `activo`) VALUES
(1, 'Dulces de frutas', 0),
(2, 'frutas', 0),
(3, 'Frutas', 0),
(4, 'Adornos', 1),
(5, 'Sala', 1),
(6, 'Pared', 1),
(7, 'Escutura ', 1),
(8, 'Pedestales ', 1),
(9, 'Laterales', 1),
(10, 'Mesas Centro ', 1),
(11, 'Bowls ', 1),
(12, 'Accesorios ', 1),
(13, 'Jarrones ', 1),
(14, 'Espejos ', 1),
(15, 'Tapetes ', 1),
(16, 'Percheros ', 1),
(17, 'Alhajeros ', 1),
(18, 'Bufeteros', 1),
(19, 'Maceteros', 1),
(20, 'Follajes ', 1),
(21, 'Comedores ', 1),
(22, 'Sillas', 1),
(23, 'Platos base ', 1),
(24, 'Individuales', 1),
(25, 'Cuadros ', 1),
(26, 'Cojines', 1),
(27, 'Riñoneras', 1),
(28, 'Sillones', 1),
(29, 'Placas', 1),
(30, 'Muebles ', 1),
(31, 'Recamaras', 1),
(32, 'Lamparas ', 1),
(33, 'Escritorios ', 1),
(34, ' baño ', 1),
(35, 'Juegos de mesa', 1),
(36, 'SECUNDARIO', 1),
(37, 'Collares', 1),
(38, 'Charolas', 1),
(39, 'Portavelas ', 1),
(40, 'despachadores ', 1),
(41, 'Cubiertas', 1),
(42, 'Base para Comedor', 1),
(43, 'Placas', 1),
(44, 'Vasijas ', 1),
(45, 'FABRICACION', 1),
(46, 'Arbotantes ', 1),
(47, 'herreria', 1),
(48, 'Arbotantes', 1),
(49, 'plato giratorio', 1),
(50, 'Guamuchil', 1),
(51, 'floreros', 1),
(52, 'Ovalines', 1),
(53, 'Credenza ', 1),
(54, 'Flete', 1),
(55, 'BISUTERIA', 1),
(56, 'Anticipos ', 1),
(57, 'portaretrato', 1),
(58, 'bowls', 1),
(59, 'jabonera', 1),
(60, 'Bisutería Onix', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClientesId` int(11) NOT NULL,
  `Nom` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `noExterior` varchar(45) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `Localidad` varchar(45) DEFAULT NULL,
  `Municipio` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Pais` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Correo` varchar(100) NOT NULL,
  `noInterior` varchar(45) NOT NULL,
  `nombrec` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `correoc` varchar(45) NOT NULL,
  `telefonoc` varchar(30) NOT NULL,
  `extencionc` varchar(20) NOT NULL,
  `nextelc` varchar(30) NOT NULL,
  `descripcionc` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1 COMMENT '1 activo 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClientesId`, `Nom`, `Calle`, `noExterior`, `Colonia`, `Localidad`, `Municipio`, `Estado`, `Pais`, `CodigoPostal`, `Correo`, `noInterior`, `nombrec`, `correoc`, `telefonoc`, `extencionc`, `nextelc`, `descripcionc`, `activo`) VALUES
(1, 'Público en general', '', '', '', '', '', '', 'México', '', 'demo@cliente.com', '', '', '', '', '', '', '', 1),
(45, 'PUBLICO EN GENERAL', '', '', '', '', '', '', 'Mexico', '', '', '', '', '', '', '', '', '', 0),
(46, 'DULCIBOTANASsx', 'GUADALAJARA', '', 'INDEPENDENCIA', 'PUEBLA', 'PUEBLA', 'PUEBLA', 'Mexico', '72150', 'edreimagdiel@gmail.com', '', 'EDREI', 'edreimagdiel@gmail.com', '2225464434', '', '', '', 0),
(47, 'xxxxxxxx11', 'calle', '1', 'colonia', 'localidad', 'municipio', 'estado', 'mexico', '94140', 'ddd@h.com', '1', 'contacto', 'ccc', '111111111', '11', '222222', 'des', 0),
(48, 'ñkñkñk', 'k', '0', 'k', 'k', 'k', 'k', 'México', '94140', 'cas@hot.com', '9', 'n', 'cas@h.com', '123456789', '123', '123456', 'fghjk', 0),
(49, 'hectoe lagunes loyo', 'conocida', '9', 'conocida', 'conocida', 'conocido', 'veracruz', 'México', '94140', 'lagunes@hotmal.com', '9', 'yo', 'lagunes@hotmal.com', '12356789', '34', '2345678', 'sdñokfsñldkfsd', 0),
(50, 'Patito', 'avr 2', '32', 'compania', 'puebla', 'quecho', 'COAHUILA', 'México', '342434', 'pa@gmail.com', '32', '', '', '234567', '', '', '', 0),
(51, 'JESUS GUTIERREZ BOCANEGRA', '', '', '', '', '', '', 'México', '', 'jgutierrezboca@gmail.com', '', '', '', '', '', '', '', 1),
(52, 'MARIA ELENA SUSANA REGALADO JUAREZ', '', '', '', '', '', '', 'México', '', 'conta_susy@hotmail.com', '', 'MARIA ELENA SUSANA REGALADO JUAREZ', 'conta_susy@hotmail.com', '2211392859', '5576658680', '', '', 1),
(53, 'MARIA ELENA JUAREZ MENDOZA', '', '', 'BOSQUES DE SAN SEBASTIAN', '', '', 'PUEBLA', 'México', '', 'conta_susy@hotmail.com', '', 'MARIA ELENA JUAREZ MENDOZA', 'conta_susy@hotmail.com', '5586160976', '', '', '', 1),
(54, 'TELLEZ ONIX MARMOL DISEÑO SA DE CV', '', '', '', '', '', '', 'México', '', 'tellezonix@prodigy.net.mx', '', 'felipe', '', '', '', '', '', 0),
(55, 'ARMANDO PEREZ SANCHEZ', 'AV. ZARAGOZA', '', 'BOSQUES DE SAN SEBASTIAN', '', '', 'PUEBLA', 'México', '', 'jgutierrezboca@gmail.com', '', 'ARMANDO PEREZ SANCHEZ', '', '5586160976', '', '', '', 1),
(56, 'CRISTOFER FERNANDEZ', 'CALLE DURANGO NO. 77', '', 'COLONIA SAN RAFAEL PONIENTE', '', '', 'PUEBLA', 'México', '72090', '', '', 'CRISTOFER FERNANDEZ', '', '2228674180', '', '', '', 1),
(57, 'LUZ GUADALUPE MATA LARA ', 'PROVADA BENITO JUAREZ No. 1', '', 'BARRIO DE ATLATZINCO', '', 'TIZATLAN', 'TLAXCALA', 'México', '90100', '', '', 'LUZ GUADALUPE MATA LARA ', '', '2462167675', '', '', '', 1),
(58, 'Gerardo Carrasco', '', '', '', '', '', '', 'México', '', '', '', '', '', '', '', '', '', 1),
(59, 'ANUAR CARREON SANCHEZ', '', '', '', '', '', '', 'México', '', '', '', 'ANUAR CARREON SANCHEZ', '', '5587795913', '', '', '', 1),
(60, 'edward cullen', '', '', '', '', '', '', 'México', '', '', '', '', '', '', '', '', '', 1),
(61, 'beatriz rios ', 'cascada azul poniente bloque 3 manzana 14', '6', '', 'cocoyoc', '', 'mexico', 'México', '', '', '6', '', '', '', '', '', '', 1),
(62, 'Paola Méndez ', '', '', '', '', '', '', 'México', '', '', '', '', '', '', '', '', '', 1),
(63, 'Fabiola rojas ', '', '', '', '', '', '', 'México', '', '', '', '', '', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compra` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `monto_total` double NOT NULL,
  `idsucursal` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compras`
--

INSERT INTO `compras` (`id_compra`, `id_proveedor`, `monto_total`, `idsucursal`, `personalId`, `reg`) VALUES
(1, 0, 200, 1, 1, '2021-07-23 15:16:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra_detalle`
--

CREATE TABLE `compra_detalle` (
  `id_detalle_compra` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio_compra` float NOT NULL,
  `id_proveedor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compra_detalle`
--

INSERT INTO `compra_detalle` (`id_detalle_compra`, `id_compra`, `id_producto`, `cantidad`, `precio_compra`, `id_proveedor`) VALUES
(1, 1, 1, 4, 50, 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion`
--

CREATE TABLE `cotizacion` (
  `id` int(11) NOT NULL,
  `cliente` int(11) NOT NULL,
  `folio` varchar(255) NOT NULL,
  `folio_aux` varchar(20) NOT NULL,
  `fecha` date NOT NULL,
  `correo` varchar(255) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `contacto` varchar(255) NOT NULL,
  `usuario` int(11) NOT NULL,
  `uncluir` varchar(2) NOT NULL,
  `subtotal` float NOT NULL,
  `iva` float NOT NULL,
  `total` float NOT NULL,
  `estatus` int(1) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cotizacion`
--

INSERT INTO `cotizacion` (`id`, `cliente`, `folio`, `folio_aux`, `fecha`, `correo`, `telefono`, `contacto`, `usuario`, `uncluir`, `subtotal`, `iva`, `total`, `estatus`, `activo`, `reg`) VALUES
(1, 1, 'SP-21-1', 'SP210001', '2021-07-23', 'eduardo_tellez11@hotmail.com', '2223589617', 'eduardo', 1, '', 0, 0, 0, 2, 0, '2021-07-23 10:14:17'),
(2, 1, 'SP-21-2', 'SP210002', '2021-07-23', '', '', '', 6, '', 0, 0, 0, 0, 0, '2021-07-23 15:03:17'),
(3, 1, 'SP-21-3', 'SP210003', '2021-07-23', '', '', '', 6, '', 0, 0, 31990, 0, 0, '2021-07-23 15:04:50'),
(4, 1, 'SP-21-4', 'SP210004', '2021-08-11', '', '', '', 6, '', 0, 0, 0, 1, 0, '2021-07-23 19:03:18'),
(5, 51, 'SP-21-5', 'SP210005', '2021-08-27', 'jgutierrezboca@gmail.com', '5586160976', '', 6, '', 0, 0, 5490, 0, 1, '2021-08-15 11:52:12'),
(6, 52, 'SP-21-6', 'SP210006', '2021-08-27', '', '5586160976', '', 6, '', 0, 0, 6900, 0, 1, '2021-08-27 15:44:33'),
(7, 53, 'SP-21-7', 'SP210007', '2021-08-27', '', '5586160976', '', 6, '', 0, 0, 2300, 0, 1, '2021-08-27 15:51:46'),
(8, 54, 'SP-21-8', 'SP210008', '2021-09-03', '', '', '', 6, '', 0, 0, 30380, 0, 1, '2021-09-03 12:01:19'),
(9, 55, 'SP-21-9', 'SP210009', '2021-09-03', 'jgutierrezboca@gmail.com', '5586160976', '', 6, '', 0, 0, 4470, 0, 1, '2021-09-03 12:27:05'),
(10, 57, 'SP-21-10', 'SP210010', '2021-09-14', '', '2462167675', 'LUZ GUADALUPE MATA LARA ', 8, '', 0, 0, 40300, 1, 1, '2021-09-12 15:34:52'),
(11, 57, 'SP-21-11', 'SP210011', '2021-09-13', '', '', '', 6, '', 0, 0, 34495, 0, 0, '2021-09-13 17:27:09'),
(12, 58, 'SP-21-12', 'SP210012', '2021-10-03', '', '5549174655', 'Gerardo Carrasco Vega ', 1, '', 0, 0, 6500, 1, 1, '2021-09-16 15:54:04'),
(13, 59, 'SP-21-13', 'SP210013', '2021-09-23', '', '5587795913', '', 6, '', 0, 0, 1500, 0, 1, '2021-09-23 16:25:42'),
(14, 59, 'SP-21-14', 'SP210014', '2021-09-24', '', '5587795913', '', 6, '', 0, 0, 5000, 0, 0, '2021-09-24 11:26:31'),
(15, 59, 'SP-21-15', 'SP210015', '2021-09-24', '', '5587795913', '', 6, '', 0, 0, 4400, 0, 1, '2021-09-24 11:30:39'),
(16, 60, 'SP-21-16', 'SP210016', '2021-09-27', '', '', '', 1, '', 0, 0, 18800, 0, 1, '2021-09-27 17:13:27'),
(17, 61, 'SP-21-17', 'SP210017', '2021-10-02', '', '', '', 1, '', 0, 0, 18350, 0, 1, '2021-10-02 15:20:13'),
(18, 62, 'SP-21-18', 'SP210018', '2021-10-03', '', '8182015335', '', 1, '', 0, 0, 3880, 0, 1, '2021-10-03 14:33:10'),
(19, 51, 'SP-21-19', 'SP210019', '2021-10-09', '', '', '', 9, '', 0, 0, 18000, 0, 1, '2021-10-09 12:45:34'),
(20, 63, 'SP-21-20', 'SP210020', '2021-10-16', '', '', '', 1, '', 0, 0, 1460, 0, 1, '2021-10-16 13:04:45');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion_partidas`
--

CREATE TABLE `cotizacion_partidas` (
  `id` int(11) NOT NULL,
  `productoid` int(11) NOT NULL,
  `idcotizacion` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `tipo_precio` varchar(20) NOT NULL,
  `precio` float NOT NULL,
  `subtotal` float NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cotizacion_partidas`
--

INSERT INTO `cotizacion_partidas` (`id`, `productoid`, `idcotizacion`, `cantidad`, `tipo_precio`, `precio`, `subtotal`, `activo`) VALUES
(1, 1, 1, 1, '1-100.00', 100, 100, 0),
(2, 7, 1, 0, '2-6500', 6500, 0, 1),
(3, 10, 2, 0, '1-890.00', 890, 0, 1),
(4, 25, 2, 0, '1-1485.00', 1485, 0, 1),
(5, 10, 3, 1, '1-890', 890, 890, 1),
(6, 34, 3, 1, '1-24200', 24200, 24200, 1),
(7, 4, 3, 1, '1-6900.00', 6900, 6900, 1),
(8, 10, 4, 0, '1-890', 890, 0, 1),
(9, 26, 4, 0, '1-4900', 4900, 0, 1),
(10, 42, 4, 0, '1-890', 890, 0, 1),
(11, 91, 5, 2, '1-990', 990, 1980, 1),
(12, 18, 5, 2, '1-155', 155, 310, 1),
(13, 78, 5, 1, '1-1320', 1320, 1320, 1),
(14, 92, 5, 2, '1-495', 495, 990, 1),
(15, 322, 5, 1, '1-890', 890, 890, 1),
(16, 84, 6, 1, '1-6900.00', 6900, 6900, 1),
(17, 235, 7, 1, '1-2300', 2300, 2300, 1),
(18, 3, 8, 1, '1-29600.00', 29600, 29600, 1),
(19, 20, 8, 2, '1-390.00', 390, 780, 1),
(20, 72, 9, 1, '1-4200.00', 4200, 4200, 1),
(21, 217, 9, 3, '1-90.00', 90, 270, 1),
(22, 266, 10, 1, '1-26550', 26550, 29500, 1),
(23, 267, 10, 1, '1-8450', 8450, 9800, 1),
(24, 364, 10, 1, '1-1000', 1000, 1000, 1),
(25, 266, 11, 1, '1-26550', 26550, 29500, 1),
(26, 12, 11, 1, '1-400', 400, 495, 1),
(27, 68, 11, 1, '1-4800.00', 4500, 4500, 1),
(28, 83, 12, 1, '2-6500', 6500, 6500, 1),
(29, 369, 13, 2, '1-675', 675, 1500, 1),
(30, 264, 14, 2, '1-2500.00', 2000, 5000, 1),
(31, 264, 15, 2, '1-2500.00', 2200, 4400, 1),
(32, 58, 16, 2, '1-9400.00', 9400, 18800, 1),
(33, 501, 17, 1, '1-11250', 11250, 12500, 1),
(34, 512, 17, 1, '1-2400', 2400, 2400, 1),
(35, 511, 17, 1, '1-3105', 3105, 3450, 1),
(36, 514, 18, 1, '1-2378', 2378, 2378, 1),
(37, 515, 18, 1, '1-1502', 1502, 1502, 1),
(38, 29, 19, 5, '1-3600.00', 3600, 18000, 1),
(39, 149, 20, 20, '1-30', 30, 600, 1),
(40, 194, 20, 20, '1-28', 28, 560, 1),
(41, 315, 20, 30, '1-10', 10, 300, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cotizacion_terminos`
--

CREATE TABLE `cotizacion_terminos` (
  `id` int(11) NOT NULL,
  `idcotizacion` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cotizacion_terminos`
--

INSERT INTO `cotizacion_terminos` (`id`, `idcotizacion`, `nombre`, `activo`) VALUES
(1, 1, '', 1),
(2, 2, 'cotizacion valido por 7 dias ', 1),
(3, 2, '', 1),
(4, 3, 'COTIZACION VALIDA 7 DIAS ', 1),
(5, 4, 'Cotización válida por 7 días ', 1),
(6, 4, 'No se aceptan cambios ni devoluciones', 1),
(7, 4, 'Apartado válido por 30 días ', 1),
(8, 5, 'Piezas entregadas en TELLEZ ONIX ', 1),
(9, 5, '', 1),
(10, 5, '', 1),
(11, 6, 'QUEDA PENDIENTE EL FLETE E INSTALACION A LA CIUDAD DE PUEBLA $800.00 EN BOSQUES DE SAN SEBASTIAN', 1),
(12, 6, 'CLIENTE DEJA SOLO LA CANTIDAD DE $1000.00 PESOS A CUENTA', 1),
(13, 6, 'CADA QUINCENA DEPOSITARA Y DAREMOS NUMERO DE CUENTA, APROXIMADAMENTE  EN 3 MESES TERMINA DE PAGARLO', 1),
(14, 7, 'QUEDA PENDIENTE EL FLETE E INSTALACION A LA CIUDAD DE PUEBLA $800.00 EN BOSQUES DE SAN SEBASTIAN', 1),
(15, 7, 'CLIENTE DEJA SOLO LA CANTIDAD DE $500.00 PESOS A CUENTA ', 1),
(16, 7, 'CLIENTE DEPOSITARA Y DAREMOS NUMERO DE CUENTA PARA QUE CLIENTE RECOJA O SE ENVIE JUNTO CON LA NOTA SP210006 CON ESTA MISMA FECHA', 1),
(17, 8, 'NO SE ACEPTAN CAMBIOS, DEVOLUCIONES NI CANCELACIONES ', 1),
(18, 8, 'QUEDA PENDIENTE CONFIRMACION DE ENTREGA A DOMICILIO EN LA CIUDAD DE PUEBLA CUAUTLANCINGO POR LA CANTIDAD DE $1000.00 PESOS', 1),
(19, 8, 'PUESTO EN PLANTA BAJA ', 1),
(20, 8, 'CLIENTE APOYARA CON 5 PERSONAS PARA LA DESCARGA ', 1),
(21, 8, 'CLIENTE DEJA A CUENTA $15000.00 EN EFECTIVO ', 1),
(22, 8, 'SALDO A PAGAR EN EFECTIVO ', 1),
(23, 9, 'NO SE ACEPTAN CAMBIOS DEVOLUCIONES NI CANCELACIONES', 1),
(24, 9, 'CLIENTE DEJA ACUENTA LA CANTIDAD DE $ 500', 1),
(25, 9, 'SALDO A PAGAR EN ENFECTIVO 3970', 1),
(26, 9, 'CLIENTE CUENTA CON 2 MESES PARA RECOGER SU PEDIDO DE LO CONTRARIO SE PERDERA SU ANTICIPO.', 1),
(27, 10, 'PUESTO EN PLANTA BAJA, CLIENTE APOYARA CON 4 PERSONAS PARA LA DESCARGA', 1),
(28, 10, 'NO SE ACEPTAN CAMBIOS, DEVOLUCIONES NI CANCELACIONES', 1),
(29, 10, 'CLIENTE DEJA DE ANTICIPO $18,000.00 PESOS CON TARJETA DE DEBITO, Y EL SALDO DE $18,000.00 LO PAGARA EN EFECTIVO A LA HORA DE LA ENTREGA', 1),
(30, 10, '', 1),
(31, 11, '', 1),
(32, 12, 'No se aceptan cambios , ni devoluciones ', 1),
(33, 12, 'Cliente recoge mercancía en tienda 2 octubre ', 1),
(34, 12, 'Saldo a pagar $5500', 1),
(35, 12, 'Cliente deja apartado de $1000 ', 1),
(36, 13, 'NO SE ACEPTAN CAMBIOS, DEVOLUCIONES NI CANCELACIONES', 1),
(37, 13, 'RECOGE EN TIENDA APROX EN UNA SEMANA', 1),
(38, 13, 'CLIENTE DEJA EN EFECTIVO $500.00 PESOS Y EL SALDO EN EFECTIVO ', 1),
(39, 14, 'PUESTO EN NUESTRA PLANTA ', 1),
(40, 14, 'NO SE ACEPTAN CAMBIOS, DEVOLUCIONES NI CANCELACIONES', 1),
(41, 14, 'CLIENTE DEJA EN EFECTIVO LA CANTIDAD DE $1500.00 EL SALDO ES DE $2,900.00 POR TRANSFERENCIA', 1),
(42, 15, 'NO SE ACEPTAN DEVOLUCIONES, CANCELACIONES, NI CAMBIOS ', 1),
(43, 15, 'RECOGE EN TIENDA', 1),
(44, 15, 'CLIENTE DEJA EN EFECTIVO $1500.00, EL SALDO ES DE $2,900.00 POR TRANSFERENCIA', 1),
(45, 16, 'precio incluye empaque para exportación y puesto en alguna dirección de tecali  ', 1),
(46, 16, 'no se aceptan cambios ni devoluciones ', 1),
(47, 16, 'cliente pagara por transferencia ', 1),
(48, 17, 'puesto en planta baja ', 1),
(49, 17, 'anticipo 4500', 1),
(50, 17, 'entrega 29 de octubre ', 1),
(51, 17, '12255', 1),
(52, 18, 'precio incluye IVA ', 1),
(53, 18, 'piedra por ser natural presentan tonos y texturas naturales ', 1),
(54, 18, 'no se aceptan cambios ni devoluciones ', 1),
(55, 18, 'tiempo de entrega 1 semana despues de su anticpo ', 1),
(56, 18, 'compra facturada ', 1),
(57, 18, 'anticipo $1940.00 saldo a pagar en efectivo $1940', 1),
(58, 19, 'no se ', 1),
(59, 20, 'no se aceptan cambios ni devoluciones ', 1),
(60, 20, 'piezas llevaran grabado ', 1),
(61, 20, 'PAGO EN EFECTIVO ', 1),
(62, 20, 'PEDIDO PAGADO ', 1),
(63, 20, 'RECOJE EN TECALI ', 1),
(64, 20, 'ENTREGA JUEVES 21 DE OCTUBRE ', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `EstadoId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Alias` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`EstadoId`, `Nombre`, `Alias`) VALUES
(1, 'AGUASCALIENTES', 'AS'),
(2, 'BAJA CALIFORNIA', 'BC'),
(3, 'BAJA CALIFORNIA SUR', 'BS'),
(4, 'CAMPECHE', 'CC'),
(5, 'COAHUILA', 'CL'),
(6, 'COLIMA', 'CM'),
(7, 'CHIAPAS', 'CS'),
(8, 'CHIHUAHUA', 'CH'),
(9, 'DISTRITO FEDERAL', 'DF'),
(10, 'DURANGO', 'DG'),
(11, 'GUANAJUATO', 'GT'),
(12, 'GUERRERO', 'GR'),
(13, 'HIDALGO', 'HG'),
(14, 'JALISCO', 'JC'),
(15, 'ESTADO DE MEXICO', 'MC'),
(16, 'MICHOACAN', 'MN'),
(17, 'MORELOS', ''),
(18, 'NAYARIT', 'NT'),
(19, 'NUEVO LEON', 'NL'),
(20, 'OAXACA', 'OC'),
(21, 'PUEBLA', 'PL'),
(22, 'QUERETARO', 'QT'),
(23, 'QUINTANA ROO', 'QR'),
(24, 'SAN LUIS POTOSI', 'SP'),
(25, 'SINALOA', 'SL'),
(26, 'SONORA', 'SR'),
(27, 'TABASCO', 'TC'),
(28, 'TAMAULIPAS', 'TS'),
(29, 'TLAXCALA', 'TL'),
(30, 'VERACRUZ', 'VZ'),
(31, 'YUCATAN', 'YN'),
(32, 'ZACATECAS', 'ZS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gastos`
--

CREATE TABLE `gastos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `monto` float NOT NULL,
  `fecha` date NOT NULL,
  `usuario` int(11) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gastos`
--

INSERT INTO `gastos` (`id`, `nombre`, `monto`, `fecha`, `usuario`, `sucursal`, `activo`) VALUES
(1, 'Gas', 200, '2021-04-16', 1, 2, 0),
(2, '', 0, '0000-00-00', 2, 2, 0),
(3, 'prueba 2', 333, '2021-03-29', 2, 2, 0),
(4, 'prueba 3', 2345, '2021-03-29', 2, 2, 0),
(5, 'prueba 4', 34567, '2021-03-29', 2, 2, 0),
(6, 'prueba 2', 34234, '2021-03-29', 1, 5, 0),
(7, 'sersdf', 444, '2021-03-29', 1, 4, 0),
(8, 'vzxvzxc', 43, '2021-04-01', 2, 2, 0),
(9, 'Casetas 2', 500, '2021-03-31', 2, 2, 0),
(10, 'xxxx', 300, '2021-03-30', 1, 1, 0),
(11, 'gasolina', 500, '2021-03-30', 2, 2, 0),
(12, 'flores', 20, '2021-07-23', 6, 1, 0),
(13, 'Extintor', 220, '2021-09-04', 8, 1, 1),
(14, 'basura ', 15, '2021-09-28', 8, 1, 1),
(15, 'limpiador mop', 60, '2021-10-06', 8, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idmarca`, `nombre`, `activo`) VALUES
(1, 'Patito', 0),
(2, 'Prueba2 ', 0),
(3, 'Delmon', 0),
(4, 'Verona', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(45) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`MenuId`, `Nombre`, `Icon`) VALUES
(1, 'Catálogos', 'fa fa-book'),
(2, 'Operaciones', 'fa fa-folder-open'),
(3, 'Configuración\n', 'fa fa fa-cogs');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu_sub`
--

CREATE TABLE `menu_sub` (
  `MenusubId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `Pagina` varchar(120) DEFAULT NULL,
  `Icon` varchar(45) DEFAULT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu_sub`
--

INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `activo`) VALUES
(1, 1, 'Productos', 'Productos', 'fa fa-barcode', 1),
(2, 3, 'Categoria', 'Categoria', 'fa fa-cogs', 0),
(3, 1, 'Personal', 'Personal', 'fa fa-group', 1),
(4, 2, 'Ventas', 'Ventas', 'fa fa-shopping-cart', 1),
(5, 2, 'Compras', 'Compras', 'fa fa-cogs', 1),
(6, 1, 'Clientes', 'Clientes', 'fa fa-user', 1),
(7, 1, 'Proveedores', 'Proveedores', 'fa fa-truck', 1),
(8, 2, 'Corte de caja', 'Corte_caja', 'fa fa-cogs', 1),
(9, 2, 'Lista de ventas', 'ListaVentas', 'fa fa-cogs', 1),
(10, 2, 'Turno', 'Turno', 'fa fa-cogs', 0),
(11, 2, 'Lista de turnos', 'ListaTurnos', 'fa fa-cogs', 0),
(12, 2, 'Lista de compras', 'Listacompras', 'fa fa-shopping-cart', 1),
(13, 3, 'Config. de ticket', 'Config_ticket', 'fa fa-cogs', 0),
(14, 2, 'Gastos', 'Gastos', 'fa fa-money', 1),
(15, 3, 'Contraseña', 'Password', 'fa fa-unlock-alt', 1),
(16, 2, 'Traspasos', 'Traspasos', 'fa fa-exchange', 0),
(17, 2, 'Cotizaciones', 'Cotizaciones', 'fa fa-newspaper-o', 1),
(18, 2, 'Registrar traspasos ', 'Registrar_Traspasos', 'fa fa-exchange', 1),
(19, 2, 'Listado de traspasos', 'Listado_traspasos', 'fa fa-exchange', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id_nota` int(1) NOT NULL,
  `mensaje` text NOT NULL,
  `usuario` varchar(120) NOT NULL,
  `reg` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id_nota`, `mensaje`, `usuario`, `reg`) VALUES
(1, '<p>notass</p>\n', 'Administrador', '2018-05-10 19:01:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password`
--

CREATE TABLE `password` (
  `idpass` int(11) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `password`
--

INSERT INTO `password` (`idpass`, `pass`) VALUES
(1, '551121892302');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `perfilId` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`perfilId`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Personal'),
(3, 'Residentes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles_detalles`
--

CREATE TABLE `perfiles_detalles` (
  `Perfil_detalleId` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `MenusubId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles_detalles`
--

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES
(1, 1, 1),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 4),
(7, 2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `personalId` int(11) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `fechanacimiento` date NOT NULL,
  `sexo` int(1) NOT NULL,
  `domicilio` varchar(500) NOT NULL,
  `ciudad` varchar(120) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigopostal` int(5) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `turno` int(1) NOT NULL,
  `fechaingreso` date NOT NULL,
  `fechabaja` date NOT NULL,
  `sueldo` decimal(10,2) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT 1 COMMENT '0 administrador 1 normal',
  `idsucursal` int(11) NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT 1 COMMENT '1 visible 0 eliminado'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`personalId`, `nombre`, `apellidos`, `fechanacimiento`, `sexo`, `domicilio`, `ciudad`, `estado`, `codigopostal`, `telefono`, `celular`, `correo`, `turno`, `fechaingreso`, `fechabaja`, `sueldo`, `tipo`, `idsucursal`, `estatus`) VALUES
(1, 'Administrador', '', '2021-03-11', 0, '', '', 0, 0, '', '', '', 0, '2021-03-10', '2021-03-16', 0.00, 0, 1, 1),
(2, 'Demo', 'Empleado', '1990-03-23', 1, '', '', 1, 0, '', '', 'admin', 1, '0000-00-00', '0000-00-00', 0.00, 1, 1, 0),
(3, 'Patito', 'Rodriguez', '2000-02-24', 1, 'av sur 111', 'puebla', 5, 342434, '456765432456', '34567865432', 'admin', 1, '2021-03-29', '0000-00-00', 2134567.00, 1, 5, 0),
(4, 'Felipe', 'Gonzales', '1994-03-31', 1, '', '', 1, 0, '', '', 'admin', 3, '0000-00-00', '0000-00-00', 0.00, 1, 3, 0),
(5, 'Carlos', ' Jimenez', '2000-03-04', 1, '', '', 1, 0, '', '', 'admin', 3, '0000-00-00', '0000-00-00', 0.00, 1, 4, 0),
(6, 'patricia', 'gonzalez', '1985-02-01', 2, '', '', 1, 0, '', '', '', 1, '0000-00-00', '0000-00-00', 0.00, 1, 1, 0),
(7, 'Meli', 'Martinez', '1996-12-12', 1, '', '', 21, 0, '', '2241047882', '', 1, '2021-08-23', '0000-00-00', 950.00, 1, 1, 0),
(8, 'Miriam', 'Meza', '0000-00-00', 2, '', '', 21, 0, '', '', 'patricia', 1, '2021-09-03', '0000-00-00', 1100.00, 1, 1, 1),
(9, 'Dalia ', 'Hernández', '0000-00-00', 2, '', '', 1, 0, '', '', 'miriam', 1, '2021-10-09', '0000-00-00', 0.00, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_menu`
--

CREATE TABLE `personal_menu` (
  `personalmenuId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `MenuId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal_menu`
--

INSERT INTO `personal_menu` (`personalmenuId`, `personalId`, `MenuId`) VALUES
(1, 1, 1),
(19, 1, 3),
(20, 1, 4),
(21, 1, 5),
(22, 1, 6),
(23, 1, 7),
(24, 1, 8),
(25, 1, 9),
(28, 1, 12),
(38, 1, 14),
(44, 1, 15),
(46, 1, 17),
(69, 4, 4),
(71, 5, 4),
(72, 3, 1),
(73, 3, 4),
(74, 1, 10),
(75, 1, 11),
(84, 2, 3),
(85, 2, 6),
(86, 2, 1),
(87, 2, 4),
(88, 2, 14),
(89, 2, 18),
(90, 2, 19),
(91, 2, 8),
(123, 7, 4),
(124, 7, 5),
(125, 7, 8),
(126, 7, 14),
(127, 7, 12),
(128, 7, 17),
(162, 8, 4),
(163, 8, 5),
(164, 8, 8),
(165, 8, 9),
(166, 8, 14),
(167, 8, 17),
(168, 6, 6),
(169, 6, 4),
(170, 6, 9),
(171, 6, 14),
(172, 6, 17),
(173, 6, 8),
(188, 9, 8),
(189, 9, 4),
(190, 9, 14),
(191, 9, 9),
(192, 9, 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `productoid` int(11) NOT NULL,
  `codigo` varchar(20) NOT NULL,
  `pag` tinyint(1) NOT NULL DEFAULT 0,
  `codigo_pag` varchar(20) DEFAULT NULL,
  `productofiscal` int(1) NOT NULL COMMENT '0 no 1 si',
  `nombre` varchar(120) NOT NULL,
  `descripcion` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `marca` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `preciocompra` float NOT NULL,
  `porcentaje` int(11) NOT NULL,
  `especial1_precio` float NOT NULL,
  `especial1_cantidad` float NOT NULL,
  `especial2_precio` float NOT NULL,
  `especial2_cantidad` float NOT NULL,
  `precioventa` float NOT NULL,
  `mediomayoreo` float NOT NULL,
  `canmediomayoreo` float NOT NULL,
  `mayoreo` float NOT NULL,
  `canmayoreo` float NOT NULL,
  `img` varchar(120) NOT NULL,
  `personalId` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1 COMMENT '1 actual 0 eliminado',
  `reg` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`productoid`, `codigo`, `pag`, `codigo_pag`, `productofiscal`, `nombre`, `descripcion`, `categoria`, `marca`, `stock`, `preciocompra`, `porcentaje`, `especial1_precio`, `especial1_cantidad`, `especial2_precio`, `especial2_cantidad`, `precioventa`, `mediomayoreo`, `canmediomayoreo`, `mayoreo`, `canmayoreo`, `img`, `personalId`, `activo`, `reg`) VALUES
(1, '123456789', 0, '', 0, 'Demo de producto', 'Demo de producto descripción', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 0, '2021-07-22 23:02:24'),
(2, '29341018236235', 0, '', 0, 'sala rey azul ', 'sala 3-2-1', 5, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, '', 0, 0, '2021-07-23 15:35:08'),
(3, '1535101836235', 0, '', 0, 'Sala Rey Azul ', 'Sala 3pza', 5, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-124736pro.jpeg', 0, 1, '2021-07-23 15:35:56'),
(4, '2636101886235', 0, '', 0, 'Juego de pared dorado ', 'Juego redondo 6 pza', 6, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-124802pro.jpeg', 0, 1, '2021-07-23 15:38:07'),
(5, '17381018716235', 0, '', 0, 'Escultura pareja ', 'Escutura de 60 x26x20 cms. ', 7, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-124924pro.jpeg', 0, 1, '2021-07-23 15:40:04'),
(6, '56391018916235', 0, '', 0, 'Pedestal Travertino Rojo 100x25x25 cms. ', 'Pedestal 100x25x25 cms. ', 8, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-124943pro.jpeg', 0, 1, '2021-07-23 15:41:10'),
(7, '56401018946235', 0, '', 0, 'Juego 2 Mesas Laterales Cruzadas Parota ', 'Juego de 2 pza 50x50x60 cms. alto ', 9, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125003pro.jpeg', 0, 1, '2021-07-23 15:42:00'),
(8, '48411018706235', 0, '', 0, 'Mesa de Centro Cruzada Parota ', 'Mesa rect. 120x80x48 cms. alto ', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125023pro.jpeg', 0, 1, '2021-07-23 15:43:16'),
(9, '32431018126235', 0, '', 0, 'Bowl irreg ónix limón 38x22x8 cms. ', 'Bowl irreg. 38x22x8 cms. ', 11, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125106pro.jpeg', 0, 1, '2021-07-23 15:44:58'),
(10, '51441018906235', 0, '', 0, 'Set 3 piezas Changos Dorados Emoji', 'Set de 3 piezas ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125215pro.jpeg', 0, 1, '2021-07-23 15:46:10'),
(11, '58451018706235', 0, '', 0, 'Set 2 piezas pajaritos con corona plateados', 'Set 2 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125233pro.jpeg', 0, 1, '2021-07-23 15:47:45'),
(12, '54471018486235', 0, '', 0, 'Cotorros con base dorados ', 'Cotorros ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125305pro.jpeg', 0, 1, '2021-07-23 15:48:59'),
(13, '45481018766235', 0, '', 0, 'Set 3 piezas de jarrones botella dorados ', 'Set de 3 pza ', 13, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125337pro.jpeg', 0, 1, '2021-07-23 15:50:04'),
(14, '59491018946235', 0, '', 0, 'Espejo Tulum parota ', 'Espejo rect. 180x60x10 cms. ', 14, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125355pro.jpeg', 0, 1, '2021-07-23 15:51:30'),
(15, '1521018816235', 0, '', 0, 'Tapete ', '180x120 cms. ', 15, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125431pro.jpeg', 0, 1, '2021-07-23 15:53:43'),
(16, '10541018846235', 0, '', 0, 'Perchero fino parota ', 'Perchero 190 ms. alto ', 16, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125459pro.jpeg', 0, 1, '2021-07-23 15:55:34'),
(17, '24551018876235', 0, '', 0, 'Set 3 piezas de jarrones plateados ', 'Set de 3 pza ', 13, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125601pro.jpeg', 0, 1, '2021-07-23 15:56:31'),
(18, '20561018696235', 0, '', 0, 'Alhajero cuadrado mármol negro ', 'Alhajero 14x14x 6 cms. ', 17, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130016pro.jpeg', 0, 1, '2021-07-23 15:59:39'),
(19, '27591018766235', 0, '', 0, 'Alhajero cuadrado mármol blanco ', 'Alhajero 14x14x6 cms. ', 17, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-125910pro.jpeg', 0, 1, '2021-07-23 16:00:27'),
(20, '2101118676235', 0, '', 0, 'Alhajero rect mármol blanco  ', 'Alhajero 23x10x8 cms. ', 17, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130039pro.jpeg', 0, 1, '2021-07-23 16:01:36'),
(21, '831118496235', 0, '', 0, 'Alhajero rect mármol negro', 'Alhajero 23x10x8 cms. ', 17, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130059pro.jpeg', 0, 1, '2021-07-23 16:04:09'),
(22, '374111816235', 0, '', 0, 'Bufetero rect. parota 140x40x85', 'Bufetero 140x40x85 cms. alto ', 18, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130237pro.jpeg', 0, 1, '2021-07-23 16:06:12'),
(23, '461118356235', 0, '', 0, 'Espejo cuadrado parota 100x100x7', 'Espejo 100x100x7 cms. ', 14, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130213pro.jpeg', 0, 1, '2021-07-23 16:07:12'),
(24, '471118806235', 0, '', 0, 'Tortuga dorada ', 'Tortuga 30 cms. ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130303pro.jpeg', 0, 1, '2021-07-23 16:08:17'),
(25, '881118916235', 0, '', 0, 'Set perro globo dorado ', 'Set 2 pza', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130322pro.jpeg', 0, 1, '2021-07-23 16:09:28'),
(26, '309111896235', 0, '', 0, 'Set macetero con base ', 'Set 3 pza color negro con base cafè ', 19, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130341pro.jpeg', 0, 1, '2021-07-23 16:10:52'),
(27, '42101118636235', 0, '', 0, 'Follajes No. 1 verde', 'Follajes largos ', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130400pro.jpeg', 0, 1, '2021-07-23 16:12:24'),
(28, '14121118336235', 0, '', 0, 'Mesa Uxmal pino ', 'Mesa 110x110x77 cms. alto ', 21, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-130604pro.jpeg', 0, 1, '2021-07-23 16:14:50'),
(29, '54141118536235', 0, '', 0, 'Silla lluvia ', 'Silla Laca nogal michelle gris ', 22, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131032pro.jpeg', 0, 1, '2021-07-23 16:16:29'),
(30, '19161118226235', 0, '', 0, 'Bowl irreg ónix limón ', 'Bowl irreg 35x20x10 cms. ', 11, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-153633pro.jpeg', 0, 1, '2021-07-23 16:18:03'),
(31, '5317111856235', 0, '', 0, 'Bowl irreg ónix limón', 'Bowl 30x26x6 cms. ', 11, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-153609pro.jpeg', 0, 1, '2021-07-23 16:19:05'),
(32, '20191118396235', 0, '', 0, 'Bowl irreg ónix limón', 'Bowl 30x18x8 cms. ', 11, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-153550pro.jpeg', 0, 1, '2021-07-23 16:20:06'),
(33, '22231118136235', 0, '', 0, 'Bowl irreg ónix limón', 'Bowl 45x35x10 cms. ', 11, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-153528pro.jpeg', 0, 1, '2021-07-23 16:24:27'),
(34, '18241118456235', 0, '', 0, 'Mesa Tulum parota ', 'Mesa 220x110', 21, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131052pro.jpeg', 0, 1, '2021-07-23 16:25:25'),
(35, '14251118886235', 0, '', 0, 'Plato base plateados ', 'Plato redondo rayado 33 cms.', 23, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131201pro.jpeg', 0, 1, '2021-07-23 16:27:08'),
(36, '4271118756235', 0, '', 0, 'Silla Imperial ', 'Silla lisa laca parota infinity alabastro ', 22, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131129pro.jpeg', 0, 1, '2021-07-23 16:28:10'),
(37, '028111856235', 0, '', 0, 'Perchero picos parota ', 'Perchero nogal 188 cms.', 16, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131300pro.jpeg', 0, 1, '2021-07-23 16:29:16'),
(38, '4291118966235', 0, '', 0, 'Mantel Individual', 'Color plata ', 24, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131319pro.jpeg', 0, 1, '2021-07-23 16:30:25'),
(39, '1430111856235', 0, '', 0, 'Cuadro Última Cena', 'Marco color dorado 152x100x5 cms. ', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131425pro.jpeg', 0, 1, '2021-07-23 16:31:33'),
(40, '25311118336235', 0, '', 0, 'Manada elefantes ', 'Set 3 pza color blanco ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131340pro.jpeg', 0, 1, '2021-07-23 16:32:54'),
(41, '56321118536235', 0, '', 0, 'Set cotorros dorados', 'Set 2 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131358pro.jpeg', 0, 1, '2021-07-23 16:33:51'),
(42, '40331118426235', 0, '', 0, 'Cojin Nudo', '', 26, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131443pro.jpeg', 0, 1, '2021-07-23 16:35:05'),
(43, '55341118956235', 0, '', 0, 'Riñonera', '', 27, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131654pro.jpeg', 0, 1, '2021-07-23 16:36:18'),
(44, '6361118416235', 0, '', 0, 'Cojin Estampado ', ' ', 26, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131538pro.jpeg', 0, 1, '2021-07-23 16:37:03'),
(45, '52361118546235', 0, '', 0, 'Cojin ', '', 26, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131556pro.jpeg', 0, 1, '2021-07-23 16:37:46'),
(46, '44381118266235', 0, '', 0, 'Cojín ', 'Rosa ', 26, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, '', 0, 0, '2021-07-23 16:39:39'),
(47, '59431118706235', 0, '', 0, 'Follaje No. 2 ', 'Verde olivo', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131839pro.jpeg', 0, 1, '2021-07-23 16:45:20'),
(48, '9451118236235', 0, '', 0, 'Follaje Hoja Gde', '', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131907pro.jpeg', 0, 1, '2021-07-23 16:47:23'),
(49, '10471118136235', 0, '', 0, 'Follaje Hoja Med', '', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-131922pro.jpeg', 0, 1, '2021-07-23 16:47:56'),
(50, '48481118136235', 0, '', 0, 'Juego de pared rueda ', '12 pza color dorado, café, negro ', 6, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132035pro.jpeg', 0, 1, '2021-07-23 16:51:00'),
(51, '48501118816235', 0, '', 0, 'Sillón Toronto ', 'Verona Mint', 28, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132116pro.jpeg', 0, 1, '2021-07-23 16:52:18'),
(52, '2553111826235', 0, '', 0, 'Mesa centro parota ', 'Base de herreria 80x42 cms. ', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132233pro.jpeg', 0, 1, '2021-07-23 16:54:53'),
(53, '41541118436235', 0, '', 0, 'Set pescados ', 'Set 2 pza color plata y blanco ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132252pro.jpeg', 0, 1, '2021-07-23 16:55:42'),
(54, '29551118116235', 0, '', 0, 'Set Sillón Cosmo  ', 'Set 2 pza Capitonado Laca Parota Jenry Ceniza', 28, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132146pro.jpeg', 0, 1, '2021-07-23 16:57:14'),
(55, '9591118606235', 0, '', 0, 'Cuadro Oro Azul ', 'Marco plata patinado 135x85x5 cms. ', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132207pro.jpeg', 0, 1, '2021-07-23 17:00:20'),
(56, '1101218926235', 0, '', 0, 'Silla Francia Cap', 'Capitonada Laca Nogal Michelle Capuzzino ', 22, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132430pro.jpeg', 0, 1, '2021-07-23 17:02:49'),
(57, '3921218306235', 0, '', 0, 'Plato base dorados ', 'Plato redondo 33 cms. ', 23, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132511pro.jpeg', 0, 1, '2021-07-23 17:03:59'),
(58, '4731218876235', 0, '', 0, 'Placas Ónix Multi', '200x130x2 cms. ', 29, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132527pro.jpeg', 0, 1, '2021-07-23 17:05:11'),
(59, '351218126235', 0, '', 0, 'Mesa Uxmal Parota', '150x150x78 cms. alto ', 21, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132727pro.jpeg', 0, 1, '2021-07-23 17:06:25'),
(60, '1461218216235', 0, '', 0, 'Set jarrones dorados ', 'Set 3 pza ', 13, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132746pro.jpeg', 0, 1, '2021-07-23 17:07:15'),
(61, '771218176235', 0, '', 0, 'Charola Maple ', 'Color dorado 25x20 cms. ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132603pro.jpeg', 0, 1, '2021-07-23 17:08:15'),
(62, '681218626235', 0, '', 0, 'Set Mesa Cristal ', 'Blanco/negro 3 pza', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-132622pro.jpeg', 0, 1, '2021-07-23 17:09:49'),
(63, '3991218186235', 0, '', 0, 'Mesa Grapa Encino ', '120x80x48 cms. ', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-133043pro.jpeg', 0, 1, '2021-07-23 17:10:49'),
(64, '40101218206235', 0, '', 0, 'Sala Monterrey', 'Tipo \"L\" Azul Marino ', 5, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-151449pro.jpeg', 0, 1, '2021-07-23 17:12:02'),
(65, '49111218606235', 0, '', 0, 'Lentes Corazón ', 'Plateados ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-133110pro.jpeg', 0, 1, '2021-07-23 17:12:37'),
(66, '25121218156235', 0, '', 0, 'Set pajaritos bola ', '3 pza', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210724-142629pro.jpeg', 0, 1, '2021-07-23 17:13:13'),
(67, '4131218326235', 0, '', 0, 'Follaje Hoja Plateados ', '', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-133212pro.jpeg', 0, 1, '2021-07-23 17:14:14'),
(68, '1141218786235', 0, '', 0, 'Set Jarrones Platinados ', '3 pza', 13, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-133232pro.jpeg', 0, 1, '2021-07-23 17:14:55'),
(69, '44141218176235', 0, '', 0, 'Gorila ', 'Dorado', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-133258pro.jpeg', 0, 1, '2021-07-23 17:15:26'),
(70, '13151218916235', 0, '', 0, 'Consola T.V. Rect. Encino ', '180x45x56 cms. ', 30, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-143038pro.jpeg', 0, 1, '2021-07-23 17:17:18'),
(71, '8171218946235', 0, '', 0, 'Set 2 Jarrones Vintage', '2 pza dorado platinado ', 13, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-133532pro.jpeg', 0, 1, '2021-07-23 17:18:07'),
(72, '55171218406235', 0, '', 0, 'Set espejos redondos ', '5 pza', 14, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142942pro.jpeg', 0, 1, '2021-07-23 17:19:28'),
(73, '12201218566235', 0, '', 0, 'Perchero Plano Nogal ', '188 cms.', 16, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142903pro.jpeg', 0, 1, '2021-07-23 17:21:07'),
(74, '55201218656235', 0, '', 0, 'Mesa Oporto ', '140x82 cms. ', 21, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142710pro.jpeg', 0, 1, '2021-07-23 17:22:06'),
(75, '58211218226235', 0, '', 0, 'Bowl irreg ónix limón', '50x33x17 cms. ', 11, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142511pro.jpeg', 0, 1, '2021-07-23 17:22:42'),
(76, '29221218746235', 0, '', 0, 'Recamara Francia 4 piezas', 'consta de 4 pza', 31, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142304pro.jpeg', 0, 1, '2021-07-23 17:23:58'),
(77, '46231218106235', 0, '', 0, 'Lampara Cilindro Ónix Multi', '30x15 cms. ', 32, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142230pro.jpeg', 0, 1, '2021-07-23 17:24:50'),
(78, '41241218396235', 0, '', 0, 'toro Dorado ', '', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142157pro.jpeg', 0, 1, '2021-07-23 17:25:59'),
(79, '47251218906235', 0, '', 0, 'Set Pavorreales', '2 pza', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142118pro.jpeg', 0, 1, '2021-07-23 17:26:33'),
(80, '22261218206235', 0, '', 0, 'Escritorio España Parota', 'Tipo \"L\" 210x160x60x76 cms. alto ', 33, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142044pro.jpeg', 0, 1, '2021-07-23 17:28:14'),
(81, '3291218456235', 0, '', 0, 'Bowl irreg 40x20x13 ónix limón', '40x20x13 cms. ', 11, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-142010pro.jpeg', 0, 1, '2021-07-23 17:29:52'),
(82, '51291218216235', 0, '', 0, 'Cuadro Árboles Invernales', '145x95x5 cms. Marco Dorado ', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-141852pro.jpeg', 0, 1, '2021-07-23 17:31:18'),
(83, '12311218286235', 0, '', 0, 'Cuadro Elefantes ', '145x95x5 cms. Marco Dorado\r\n', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-141806pro.jpeg', 0, 1, '2021-07-23 17:32:34'),
(84, '2132121836235', 0, '', 0, 'Cuadro Raíz', '144x95x2 cms. Marco Parota ', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-141637pro.jpeg', 0, 1, '2021-07-23 17:33:26'),
(85, '16331218406235', 0, '', 0, 'Cuadro Fusión ', '150x100x6 cms. Marco Dorado ', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-141602pro.jpeg', 0, 1, '2021-07-23 17:34:15'),
(86, '6341218596235', 0, '', 0, 'Follaje No. 3', '', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-151009pro.jpeg', 0, 1, '2021-07-23 17:34:53'),
(87, '41341218266235', 0, '', 0, 'Follaje No. 4', '', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-150941pro.jpeg', 0, 1, '2021-07-23 17:35:21'),
(88, '8351218726235', 0, '', 0, 'Cocodrilo ', 'Dorado ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210723-141513pro.jpeg', 0, 1, '2021-07-23 17:35:48'),
(89, '7451418156235', 0, '', 0, 'demo producto', 'demo ', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 0, '2021-07-23 19:45:30'),
(90, '2131218176246', 0, '', 0, 'Lampara cubo ónix zebra', '160x30x30', 32, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210724-121925pro.jpeg', 0, 1, '2021-07-24 17:19:24'),
(91, '48221218426246', 0, '', 0, 'Set 5 piezas de baño redondo mármol negro ', '5 pza. ', 34, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210724-122629pro.jpeg', 0, 1, '2021-07-24 17:26:28'),
(92, '3561418296246', 0, '', 0, 'Alhajero redondo marmol negro ', '12x10 cms. alto ', 17, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210724-145828pro.jpeg', 0, 1, '2021-07-24 19:58:28'),
(93, '15581418246246', 0, '', 0, 'Juego de gato con tablero de madera', 'Madera con fichas mármol negro ', 35, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210724-145933pro.jpeg', 0, 1, '2021-07-24 19:59:33'),
(94, '5381418956250', 0, '', 0, 'Corazón Onix 8cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210725-155822pro.jpeg', 0, 1, '2021-07-25 19:12:53'),
(95, '37131418636250', 0, '', 0, 'Huevo Onix Mini', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210725-155756pro.jpeg', 0, 1, '2021-07-25 19:15:27'),
(96, '37151418666250', 0, '', 0, 'Figura de Mar Onix', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210725-155732pro.jpeg', 0, 1, '2021-07-25 19:18:09'),
(97, '3518141846250', 0, '', 0, 'Tortuga Chica Onix', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210725-155713pro.jpeg', 0, 1, '2021-07-25 19:23:50'),
(98, '59231418366250', 0, '', 0, 'Tortuga Mediana Onix', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210725-155657pro.jpeg', 0, 1, '2021-07-25 19:26:07'),
(99, '40281418486250', 0, '', 0, 'Virgen 15 cm Onix', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210725-155637pro.jpeg', 0, 1, '2021-07-25 19:39:57'),
(100, '27401418836250', 0, '', 0, 'Virgen 15 cm Marmol', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210725-155606pro.jpeg', 0, 1, '2021-07-25 19:42:45'),
(101, '15191518136250', 0, '', 0, 'Virgen 12 cm Ònix ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125356pro.jpeg', 0, 1, '2021-07-25 20:20:04'),
(102, '14201518756250', 0, '', 0, 'Virgen 12 cm Màrmol ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125412pro.jpeg', 0, 1, '2021-07-25 20:20:52'),
(103, '27451518506250', 0, '', 0, 'Dije Grande', '', 37, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210725-155552pro.jpeg', 0, 1, '2021-07-25 20:46:38'),
(104, '15471518846250', 0, '', 0, 'Dije chico ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210725-155552pro.jpeg', 0, 1, '2021-07-25 20:47:18'),
(105, '46591518296250', 0, '', 0, 'Charola 3 Divisiones Ònix', '45X15 cms', 38, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125300pro.jpeg', 0, 1, '2021-07-25 21:02:34'),
(106, '5221618446250', 0, '', 0, 'Charola 3 Divisiones Màrmol', '45x15 cms.', 38, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125316pro.jpeg', 0, 1, '2021-07-25 21:03:38'),
(107, '4271618726250', 0, '', 0, 'Anillos Ònix', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125332pro.jpeg', 0, 1, '2021-07-25 21:08:19'),
(108, '53251218236261', 0, '', 0, 'Portavela Eliptica Ònix', '9x6x6 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123027pro.jpeg', 0, 1, '2021-07-26 17:30:26'),
(109, '33301218626261', 0, '', 0, 'Portavela Eliptica Màrmol ', '9x6x6 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123110pro.jpeg', 0, 1, '2021-07-26 17:31:09'),
(110, '19321218706261', 0, '', 0, 'Portavela Eliptica Bicolor', '9x6x6 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123255pro.jpeg', 0, 1, '2021-07-26 17:32:54'),
(111, '3331218926261', 0, '', 0, 'Portavela 1/2 Esfera Ónix ', '6x4 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123408pro.jpeg', 0, 1, '2021-07-26 17:34:07'),
(112, '19341218986261', 0, '', 0, 'Portavela Rustica Màrmol ', '6 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123500pro.jpeg', 0, 0, '2021-07-26 17:34:59'),
(113, '45351218396261', 0, '', 0, 'Portavela Rustica Ónix 7cms', '6 y 7 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123617pro.jpeg', 0, 1, '2021-07-26 17:36:16'),
(114, '24361218496261', 0, '', 0, 'Portavela Rustica Mármol 7cms', '7 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123721pro.jpeg', 0, 0, '2021-07-26 17:37:20'),
(115, '27371218296261', 0, '', 0, 'Portavela Rustica Ònix', '7 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-123757pro.jpeg', 0, 0, '2021-07-26 17:37:55'),
(116, '340121836261', 0, '', 0, 'Portavela Rustica Màrmol 9 cms. ', '', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-124037pro.jpeg', 0, 1, '2021-07-26 17:40:36'),
(117, '52411218386261', 0, '', 0, 'Portavela Estrella Ònix', '', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-124223pro.jpeg', 0, 1, '2021-07-26 17:42:22'),
(118, '29431218696261', 0, '', 0, 'Portavela Cilindro Bicolor ', '6x8 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-124414pro.jpeg', 0, 1, '2021-07-26 17:44:13'),
(119, '26441218156261', 0, '', 0, 'Portavela Cubo 6x6x6 Mármol ', '6x6x6 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-124506pro.jpeg', 0, 1, '2021-07-26 17:45:04'),
(120, '30451218666261', 0, '', 0, 'Portavela Cubo Rect 6x6x8 Ónix', '6x6x8 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-124721pro.jpeg', 0, 1, '2021-07-26 17:47:20'),
(121, '28471218206261', 0, '', 0, 'Portavela Cubo Rect 6x6x8 Mármol ', '6x6x8 cms.', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-124813pro.jpeg', 0, 1, '2021-07-26 17:48:11'),
(122, '18481218966261', 0, '', 0, 'Portavela Cubo 8x8x8 Bicolor ', '8x8x8 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125005pro.jpeg', 0, 1, '2021-07-26 17:50:04'),
(123, '15501218846261', 0, '', 0, 'Portavela Cubo Rect 6x6x13 Ónix', '6x6x13 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125053pro.jpeg', 0, 1, '2021-07-26 17:50:52'),
(124, '59501218156261', 0, '', 0, 'Portavela Rectangular 8x6x18 Ónix ', '8x6x18 cms. ', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125200pro.jpeg', 0, 1, '2021-07-26 17:51:59'),
(125, '57541218876261', 0, '', 0, 'Juego de 3 Portavela Cubo Mármol ', '3 pza', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125540pro.jpeg', 0, 1, '2021-07-26 17:55:39'),
(126, '45551218866261', 0, '', 0, 'Juego de 3 Portavela \"S\" Bicolor', '3 pza', 39, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210726-125630pro.jpeg', 0, 1, '2021-07-26 17:56:29'),
(127, '49161718826261', 0, '', 0, 'Cruz plana con base ', 'onix blanco 18x12 ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-172126pro.jpeg', 0, 1, '2021-07-26 22:25:11'),
(128, '47251718116261', 0, '', 0, 'Mariposa con base ', 'ónix surtido 7 cm ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-172056pro.jpeg', 0, 1, '2021-07-26 22:26:06'),
(129, '13261718336261', 0, '', 0, 'Set Pera ', '3 pza ónix/mármol surtido ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171743pro.jpeg', 0, 1, '2021-07-26 22:28:05'),
(130, '3291718656261', 0, '', 0, 'Set de Manzanas 3 pza ónix/mármol ', '3 pza ónix/mármol ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171719pro.jpeg', 0, 1, '2021-07-26 22:29:08'),
(131, '14291718316261', 0, '', 0, 'Set de Manzana Plana', '3 pza. ónix/mármol ', 4, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171631pro.jpeg', 0, 1, '2021-07-26 22:30:07'),
(132, '14301718476261', 0, '', 0, 'Set de Peras Planas ', '3pza. onix/marmol', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171606pro.jpeg', 0, 1, '2021-07-26 22:30:45'),
(133, '52301718626261', 0, '', 0, 'vaso onix ', 'onix blanco 8x5 cm', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145353pro.jpeg', 0, 1, '2021-07-26 22:31:25'),
(134, '32311718956261', 0, '', 0, 'Jabonera rectangular ', '15x8 cm ', 38, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171509pro.jpeg', 0, 1, '2021-07-26 22:32:31'),
(135, '38321718746261', 0, '', 0, 'Despachador Redondo ', 'mármol/ónix 6x5 ', 40, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171408pro.jpeg', 0, 1, '2021-07-26 22:34:00'),
(136, '8341718256261', 0, '', 0, 'Despachador Cuadrado ', 'mármol/ónix 6x6x5', 40, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171346pro.jpeg', 0, 1, '2021-07-26 22:34:45'),
(137, '51341718916261', 0, '', 0, 'Despachador 1/2 esfera', 'mármol/ónix 9x5 cm', 40, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-171329pro.jpeg', 0, 1, '2021-07-26 22:35:38'),
(138, '0361718106261', 0, '', 0, 'Ángel Pera con Base', 'onix 10 cm ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170909pro.jpeg', 0, 1, '2021-07-26 22:36:34'),
(139, '40361718306261', 0, '', 0, 'Angel Cono ', 'ónix 10 cm ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170850pro.jpeg', 0, 1, '2021-07-26 22:37:05'),
(140, '11371718586261', 0, '', 0, 'Libro ', 'ónix 7x5', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170804pro.jpeg', 0, 1, '2021-07-26 22:37:46'),
(141, '52371718956261', 0, '', 0, 'Alhajero Margarita Mini', 'ónix 6x3 ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170730pro.jpeg', 0, 1, '2021-07-26 22:38:24'),
(142, '30381718556261', 0, '', 0, 'Imagen de Perfil 9 cms.', 'mármol/ónix', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170617pro.jpeg', 0, 1, '2021-07-26 22:39:19'),
(143, '25391718246261', 0, '', 0, 'Candelero Mini con Flor', 'mármol/ónix', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170823pro.jpeg', 0, 1, '2021-07-26 22:40:24'),
(144, '30401718276261', 0, '', 0, 'Rostro 16 cm ', 'onix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170316pro.jpeg', 0, 1, '2021-07-26 22:40:55'),
(145, '1411718396261', 0, '', 0, 'Rostro 20 cm ', 'mármol/ónix', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170248pro.jpeg', 0, 1, '2021-07-26 22:41:29'),
(146, '34411718426261', 0, '', 0, 'Rostro 30 cm ', 'mármol/ónix', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170217pro.jpeg', 0, 1, '2021-07-26 22:42:02'),
(147, '842171876261', 0, '', 0, 'Virgen Ónix 27 cm ', '27 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170138pro.jpeg', 0, 1, '2021-07-26 22:42:54'),
(148, '1431718676261', 0, '', 0, 'Virgen Mármol 27 cm', '27 cms.', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-170102pro.jpeg', 0, 1, '2021-07-26 22:43:27'),
(149, '32431718266261', 0, '', 0, 'Rostro 10 cm ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-165923pro.jpeg', 0, 1, '2021-07-26 22:43:55'),
(150, '2481718506261', 0, '', 0, 'Cruz Trébol 10 cm', 'ónix', 4, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-165538pro.jpeg', 0, 1, '2021-07-26 22:49:08'),
(151, '19491718246261', 0, '', 0, 'Imagen de perfil 10 cms.', 'ónix', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-165514pro.jpeg', 0, 1, '2021-07-26 22:50:17'),
(152, '37501718386261', 0, '', 0, 'Cruz ónix Latón 10 cm', 'ónix', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-165210pro.jpeg', 0, 1, '2021-07-26 22:51:11'),
(153, '3521718116261', 0, '', 0, 'Alhajero cuadrado 5x5x3 ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-165051pro.jpeg', 0, 1, '2021-07-26 22:52:12'),
(154, '17281618506272', 0, '', 0, 'Charola 2 Divisiones Ònix', '30X15 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-163327pro.jpeg', 0, 1, '2021-07-27 21:33:26'),
(155, '53331618736272', 0, '', 0, 'Charola Pepita Ònix', '24X12X3 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-163541pro.jpeg', 0, 1, '2021-07-27 21:35:40'),
(156, '51351618706272', 0, '', 0, 'Charola 1 Divisiòn Ònix', '15x15x5 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-163659pro.jpeg', 0, 1, '2021-07-27 21:36:58'),
(157, '14371618726272', 0, '', 0, 'Charola Cuadrada Màrmol ', '25x25x4 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-163807pro.jpeg', 0, 1, '2021-07-27 21:38:06'),
(158, '14381618726272', 0, '', 0, 'Toallero Curvo con Moldura Ònix', '18x10 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-164153pro.jpeg', 0, 1, '2021-07-27 21:41:52'),
(159, '542161826272', 0, '', 0, 'Charola Ovalada con Bisel ', '20x10 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-164249pro.jpeg', 0, 1, '2021-07-27 21:42:48'),
(160, '59421618566272', 0, '', 0, 'Charola Cuadrada Ònix', '14x14 cms., ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-164338pro.jpeg', 0, 1, '2021-07-27 21:43:36'),
(161, '44431618866272', 0, '', 0, 'Charola Cuadrada Ondeada Ònix', '15x15 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-164423pro.jpeg', 0, 1, '2021-07-27 21:44:22'),
(162, '32441618676272', 0, '', 0, 'Charola Cuadrada con Bisel Ònix', '20x20 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210727-164510pro.jpeg', 0, 1, '2021-07-27 21:45:09'),
(163, '1771618636305', 0, '', 0, 'Estrella Pico 10 cm ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120127pro.jpeg', 0, 1, '2021-07-30 21:08:01'),
(164, '2781618416305', 0, '', 0, 'Estrella picos 7 cm ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120153pro.jpeg', 0, 1, '2021-07-30 21:08:52'),
(165, '2391618976305', 0, '', 0, 'Estrella fina 10 cm ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120215pro.jpeg', 0, 1, '2021-07-30 21:09:25'),
(166, '3291618586305', 0, '', 0, 'Estrella fina 7 cm ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120246pro.jpeg', 0, 1, '2021-07-30 21:09:56'),
(167, '4101618986305', 0, '', 0, 'Esfera 4 cm ', 'ónix/mármol  ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120351pro.jpeg', 0, 1, '2021-07-30 21:10:43'),
(168, '0111618536305', 0, '', 0, 'Esfera 6 cm ', 'ónix/mármol ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120550pro.jpeg', 0, 1, '2021-07-30 21:11:11'),
(169, '14121618856305', 0, '', 0, 'Esfera 8 cm ', 'ónix/mármol', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120622pro.jpeg', 0, 1, '2021-07-30 21:12:50'),
(170, '58121618336305', 0, '', 0, 'Almeja 10 cm ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120652pro.jpeg', 0, 1, '2021-07-30 21:14:04'),
(171, '11141618826305', 0, '', 0, 'Almeja 8 cm  ', 'ónix ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120807pro.jpeg', 0, 1, '2021-07-30 21:14:43'),
(172, '50141618216305', 0, '', 0, 'Caracol 9 cm ', 'ónix ', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120751pro.jpeg', 0, 1, '2021-07-30 21:15:46'),
(173, '52151618836305', 0, '', 0, 'Set pajaritos dorados ', '2 pza', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120919pro.jpeg', 0, 1, '2021-07-30 21:16:37'),
(174, '017161836305', 0, '', 0, 'Set Jaguar Dorado ', '2 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-120941pro.jpeg', 0, 1, '2021-07-30 21:17:28'),
(175, '46171618186305', 0, '', 0, 'Jirafa Globo Dorado ', '1 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121128pro.jpeg', 0, 1, '2021-07-30 21:18:19'),
(176, '26181618526305', 0, '', 0, 'Set de Piñas doradas ', '3 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121241pro.jpeg', 0, 1, '2021-07-30 21:18:55'),
(177, '2191618306305', 0, '', 0, 'León con Corona ', '1 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121258pro.jpeg', 0, 1, '2021-07-30 21:19:36'),
(178, '41191618266305', 0, '', 0, 'Set de Jirafas Doradas ', '2 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121316pro.jpeg', 0, 1, '2021-07-30 21:20:10'),
(179, '1720161816305', 0, '', 0, 'Charola Mariposa Dorada ', '1 pza', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121428pro.jpeg', 0, 1, '2021-07-30 21:20:45'),
(180, '52201618536305', 0, '', 0, 'Set Manzanas Doradas ', '3 pza ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121451pro.jpeg', 0, 1, '2021-07-30 21:21:17'),
(181, '4221161866305', 0, '', 0, 'Set Conejos Gris', '3 pza. ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121551pro.jpeg', 0, 1, '2021-07-30 21:22:10'),
(182, '17221618876305', 0, '', 0, 'Set Jarrones con Tapa ', '2 pza. ', 13, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121609pro.jpeg', 0, 1, '2021-07-30 21:23:03'),
(183, '1023161856305', 0, '', 0, 'Elefante Corazón Dorado ', '1 pza. ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121632pro.jpeg', 0, 1, '2021-07-30 21:23:51'),
(184, '10241618846305', 0, '', 0, 'Elefante Corazón Blanco ', '1 pza', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121645pro.jpeg', 0, 1, '2021-07-30 21:24:37'),
(185, '43241618336305', 0, '', 0, 'Elefante Corazón Plata', '1 pza. ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121658pro.jpeg', 0, 1, '2021-07-30 21:25:16'),
(186, '22251618816305', 0, '', 0, 'Florero Hoja Plata ', 'plata 1 pza. ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121712pro.jpeg', 0, 1, '2021-07-30 21:26:44'),
(187, '50261618106305', 0, '', 0, 'Escultura Hoja Dorada ', 'con base madera ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121726pro.jpeg', 0, 1, '2021-07-30 21:27:31'),
(188, '37271618436305', 0, '', 0, 'Rinoceronte Gris ', '1 pza. ', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121740pro.jpeg', 0, 1, '2021-07-30 21:28:16'),
(189, '38281618526305', 0, '', 0, 'Buda Sentado ', '1 pza.', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121753pro.jpeg', 0, 1, '2021-07-30 21:29:24'),
(190, '4829161896305', 0, '', 0, 'Budas Sentados ', '', 12, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-121807pro.jpeg', 0, 1, '2021-07-30 21:31:51'),
(191, '50501618516305', 0, '', 0, 'Follaje raíz ', 'café claro / oscuro ', 20, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-122052pro.jpeg', 0, 1, '2021-07-30 21:52:21'),
(192, '1221718316305', 0, '', 0, 'Set Macetero Negro Herrería ', '2 pza. base dorada ', 19, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210731-122115pro.jpeg', 0, 1, '2021-07-30 22:03:31'),
(193, '16201318866316', 0, '', 0, 'Follaje Pluma ', '', 20, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210731-132336pro.jpeg', 0, 1, '2021-07-31 18:23:35'),
(194, '101131865732', 0, '', 0, 'Alhajero Rectangular Onix con Perilla', '7x5x4 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210803-130327pro.jpeg', 0, 1, '2021-08-03 18:03:25'),
(195, '323131882732', 0, '', 0, 'Alhajero Rectangular Onix', '7X5X4 CMS.', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210803-130403pro.jpeg', 0, 1, '2021-08-03 18:04:02'),
(196, '534111866765', 0, '', 0, 'Candelabro Grande 3 Rosas Màrmol 27 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145424pro.jpeg', 0, 1, '2021-08-06 16:06:16'),
(197, '266111868765', 0, '', 0, 'Candelabro Mediano 3 Rosas Màrmol 23 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145523pro.jpeg', 0, 1, '2021-08-06 16:07:07'),
(198, '167111845765', 0, '', 0, 'Candelabro Mediano 3 Rosas Ònix 23 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145541pro.jpeg', 0, 1, '2021-08-06 16:07:42'),
(199, '88111884765', 0, '', 0, 'Candelabro Mediano 2 Rosas Ònix 22 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145614pro.jpeg', 0, 1, '2021-08-06 16:09:03'),
(200, '109111810765', 0, '', 0, 'Candelabro Chico 2 Rosas Ònix 17 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145637pro.jpeg', 0, 1, '2021-08-06 16:09:49'),
(201, '56911188765', 0, '', 0, 'Candelabro Chico 2 Rosas Màrmol 17 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145658pro.jpeg', 0, 1, '2021-08-06 16:10:27'),
(202, '3610111898765', 0, '', 0, 'Candelabro Grande 1 Rosa Ònix 20 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-145734pro.jpeg', 0, 1, '2021-08-06 16:11:21'),
(203, '2811111883765', 0, '', 0, 'Candelabro Grande 1 Rosa Màrmol 20 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-173837pro.jpeg', 0, 1, '2021-08-06 16:11:58'),
(204, '512111816765', 0, '', 0, 'Candelabro Mediano 1 Rosa Ònix 16 cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-173923pro.jpeg', 0, 1, '2021-08-06 16:12:46'),
(205, '5312111827765', 0, '', 0, 'Candelabro 1 Rosa Ònix 6cm', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174103pro.jpeg', 0, 1, '2021-08-06 16:13:37'),
(206, '3114111878765', 0, '', 0, 'Servilletero Hongo Ònix ', '10 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174142pro.jpeg', 0, 1, '2021-08-06 16:15:02'),
(207, '1015111866765', 0, '', 0, 'Servilletero Manzana Ònix ', '10 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174203pro.jpeg', 0, 1, '2021-08-06 16:15:35'),
(208, '4215111825765', 0, '', 0, 'Florero Fino Bicolor ', '17 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174232pro.jpeg', 0, 1, '2021-08-06 16:17:39'),
(209, '4917111820765', 0, '', 0, 'Florero Flor Màrmol ', '18 cms. alto ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174309pro.jpeg', 0, 1, '2021-08-06 16:18:18'),
(210, '2518111875765', 0, '', 0, 'Florero Boca Zig-zac Màrmol ', '18 cms. alto ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174330pro.jpeg', 0, 1, '2021-08-06 16:18:57'),
(211, '1218121853765', 0, '', 0, 'Florero Semi Onix', '28 cms. alto ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174551pro.jpeg', 0, 1, '2021-08-06 17:20:10'),
(212, '1820121856765', 0, '', 0, 'Florero Cuadrado Cònico Ònix', '25x15x15 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174606pro.jpeg', 0, 1, '2021-08-06 17:20:54'),
(213, '121121898765', 0, '', 0, 'Florero Cuadrado Cònico Ònix', '30x15x15 cms. ', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174625pro.jpeg', 0, 1, '2021-08-06 17:21:25'),
(214, '4921121899765', 0, '', 0, 'Klienera Cuadrada Màrmol ', '15x15x15 cms', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174644pro.jpeg', 0, 1, '2021-08-06 17:22:59'),
(215, '62312187765', 0, '', 0, 'Klienera Cuadrada Ònix', '15x15x15', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174701pro.jpeg', 0, 1, '2021-08-06 17:23:24'),
(216, '183912183776', 0, '', 0, 'Maceta Chica con Planta ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174722pro.jpeg', 0, 1, '2021-08-07 17:42:10'),
(217, '1842121840776', 0, '', 0, 'Maceta Grande con Planta', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174736pro.jpeg', 0, 1, '2021-08-07 17:42:43'),
(218, '5042121883776', 0, '', 0, 'Maceta chica sin Planta', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174751pro.jpeg', 0, 1, '2021-08-07 17:43:15'),
(219, '2243121810776', 0, '', 0, 'Maceta Grande sin Planta', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174804pro.jpeg', 0, 1, '2021-08-07 17:43:49'),
(220, '5743121829776', 0, '', 0, 'Pipa Grande Màrmol', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174826pro.jpeg', 0, 1, '2021-08-07 17:44:25'),
(221, '3244121816776', 0, '', 0, 'Pipa Chica Màrmol', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-174837pro.jpeg', 0, 1, '2021-08-07 17:45:01'),
(222, '4748171890776', 0, '', 0, 'Ratón Mini Màrmol', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-175004pro.jpeg', 0, 1, '2021-08-07 22:50:03'),
(223, '451171848776', 0, '', 0, 'Gato Mini Màrmol', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-175123pro.jpeg', 0, 1, '2021-08-07 22:51:22'),
(224, '3551171893776', 0, '', 0, 'Lobo Mini Màrmol', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-175152pro.jpeg', 0, 1, '2021-08-07 22:51:52'),
(225, '3253171840776', 0, '', 0, 'Burrito Mini Màrmol', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-175358pro.jpeg', 0, 1, '2021-08-07 22:53:57'),
(226, '654171842776', 0, '', 0, 'Unicornio Mini Mármol ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-175514pro.jpeg', 0, 1, '2021-08-07 22:55:14'),
(227, '15717186776', 0, '', 0, 'Silla Dinamarca Nogal ', 'Tapiz Modena Granite', 22, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-175754pro.jpeg', 0, 1, '2021-08-07 22:57:53'),
(228, '258171817776', 0, '', 0, 'Mueble de T.V.', '200X40X177 cms. alto ', 30, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210807-175929pro.jpeg', 0, 1, '2021-08-07 22:59:28'),
(229, '2017131880780', 0, '', 0, 'Sillon Azul Monclova ', '', 28, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-132529pro.jpeg', 0, 1, '2021-08-08 18:25:29'),
(230, '582513185780', 0, '', 0, 'Cuadro Mariposa ', '60x60 cms. ', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-132702pro.jpeg', 0, 1, '2021-08-08 18:27:01'),
(231, '1827131880780', 0, '', 0, 'Set de Mesas de Centro Redondas ', '2 PZA', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-132816pro.jpeg', 0, 1, '2021-08-08 18:28:16'),
(232, '4528131828780', 0, '', 0, 'Set de Maceteros Base Herreria', '3 pzas', 19, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-132946pro.jpeg', 0, 1, '2021-08-08 18:29:45'),
(233, '830131880780', 0, '', 0, 'Lampara de Buro Base Vidrio ', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-133052pro.jpeg', 0, 1, '2021-08-08 18:30:52'),
(234, '2131131866780', 0, '', 0, 'Set Bowl para Pared', '5 pzas ', 6, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-133209pro.jpeg', 0, 1, '2021-08-08 18:32:06'),
(235, '2532131855780', 0, '', 0, 'Set de Botones para Pared ', '5 pzas ', 6, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-133328pro.jpeg', 0, 1, '2021-08-08 18:33:27'),
(236, '2234131882780', 0, '', 0, 'Pirinola ', '', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-133452pro.jpeg', 0, 1, '2021-08-08 18:34:51'),
(237, '3135131894780', 0, '', 0, 'Cuadro de Hojas ', '134x94 cms. ', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-133730pro.jpeg', 0, 1, '2021-08-08 18:37:29'),
(238, '503713181780', 0, '', 0, 'Mesa de Centro con Cubierta Quartzita ', '120x60x44 cms. alto ', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-133830pro.jpeg', 0, 1, '2021-08-08 18:38:29'),
(239, '3638131816780', 0, '', 0, 'Mesa de Centro Forma Huevo Quartzita ', '116x90x40 cms. alto', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-133930pro.jpeg', 0, 1, '2021-08-08 18:39:29'),
(240, '4839131844780', 0, '', 0, 'Sala Beige 2 pzas ', '', 5, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134047pro.jpeg', 0, 1, '2021-08-08 18:40:45'),
(241, '5640131828780', 0, '', 0, 'Mesa de Centro Cruzada 50x50x60 alto', '', 9, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134154pro.jpeg', 0, 1, '2021-08-08 18:41:53'),
(242, '2843131818780', 0, '', 0, 'Arreglo Blanco con Follaje ', '', 13, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134420pro.jpeg', 0, 1, '2021-08-08 18:44:19'),
(243, '2944131840780', 0, '', 0, 'Cuadro de Herreria Cubos 3D', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134515pro.jpeg', 0, 1, '2021-08-08 18:45:14'),
(244, '4645131821780', 0, '', 0, 'Cabeza de Buda', '', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134621pro.jpeg', 0, 1, '2021-08-08 18:46:20'),
(245, '324613189780', 0, '', 0, 'Set de Lamparas de Madera ', '2 pzas ', 32, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134733pro.jpeg', 0, 1, '2021-08-08 18:47:32'),
(246, '4147131828780', 0, '', 0, 'Set de Espejos Concavo-Convexo ', '9 pzas ', 14, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134817pro.jpeg', 0, 1, '2021-08-08 18:48:16'),
(247, '3448131891780', 0, '', 0, 'Set de Mesas Triangulares de Centro ', '2 pzas ', 10, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-134912pro.jpeg', 0, 1, '2021-08-08 18:49:11'),
(248, '2649131811780', 0, '', 0, 'Lampara de Pie Base Parota ', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-135011pro.jpeg', 0, 1, '2021-08-08 18:50:10'),
(249, '1950131831780', 0, '', 0, 'Cubierta Maskaratus', '256x160x12 cms. ', 41, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-135158pro.jpeg', 0, 1, '2021-08-08 18:51:57'),
(250, '1752131861780', 0, '', 0, 'Base Herreria para Comedor Y 176x94x78 alto ', '176x94x78 alto ', 42, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-135302pro.jpeg', 0, 1, '2021-08-08 18:53:02'),
(251, '1253131835780', 0, '', 0, 'Placa Blue Mare ', '315x189', 43, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-135632pro.jpeg', 0, 1, '2021-08-08 18:56:31'),
(252, '4056131838780', 0, '', 0, 'Arreglo Centro de Mesa ', '', 12, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-135720pro.jpeg', 0, 1, '2021-08-08 18:57:20'),
(253, '385713181780', 0, '', 0, 'Set de Maceteros Base Parota', '2 pzas', 19, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-135906pro.jpeg', 0, 1, '2021-08-08 18:59:06'),
(254, '2259131847780', 0, '', 0, 'Cuadro Colibrí', '60*60', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-140006pro.jpeg', 0, 1, '2021-08-08 19:00:03'),
(255, '11214185780', 0, '', 0, 'Cuadro Perro ', '60*60', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-140305pro.jpeg', 0, 1, '2021-08-08 19:03:05'),
(256, '145141868780', 0, '', 0, 'Set Mariposas Plata', '3 pza', 25, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-140556pro.jpeg', 0, 1, '2021-08-08 19:05:56'),
(257, '56141848780', 0, '', 0, 'Set Jarrones Barro Blanco', '2 pzas', 4, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-140638pro.jpeg', 0, 1, '2021-08-08 19:06:37'),
(258, '497141873780', 0, '', 0, 'Sillas Negras ', '', 22, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-140827pro.jpeg', 0, 1, '2021-08-08 19:08:27'),
(259, '191171820780', 0, '', 0, 'Vasija Irreg- 52x49x12 cms. Zebra', '', 44, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-171047pro.jpeg', 0, 1, '2021-08-08 22:10:47'),
(260, '512171894780', 0, '', 0, 'Fabricaciòn No. 1 ', 'SOLO CUBIERTA', 45, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, '', 0, 1, '2021-08-08 22:14:22'),
(261, '2914171852780', 0, '', 0, 'Fabricación No. 2 ', 'VARIOS CORTES DE PLACA ', 45, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, '', 0, 1, '2021-08-08 22:15:49'),
(262, '5715171822780', 0, '', 0, 'Ancla Màrmol', '', 6, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, '', 0, 1, '2021-08-08 22:16:15');
INSERT INTO `productos` (`productoid`, `codigo`, `pag`, `codigo_pag`, `productofiscal`, `nombre`, `descripcion`, `categoria`, `marca`, `stock`, `preciocompra`, `porcentaje`, `especial1_precio`, `especial1_cantidad`, `especial2_precio`, `especial2_cantidad`, `precioventa`, `mediomayoreo`, `canmediomayoreo`, `mayoreo`, `canmayoreo`, `img`, `personalId`, `activo`, `reg`) VALUES
(263, '5916171878780', 0, '', 0, 'Set de Cojines Estampados', '2 pza', 26, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-172016pro.jpeg', 0, 1, '2021-08-08 22:17:52'),
(264, '1318171888780', 0, '', 0, 'Arbotantes Rect. 140x30x10 cms. Zebra', '', 46, 4, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 'public/img/productos/210808-171953pro.jpeg', 0, 1, '2021-08-08 22:19:52'),
(265, '558131897113', 0, '', 0, 'Portavela Rustica Mármol 7cms', '', 39, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-11 18:59:28'),
(266, '39161718937113', 0, '', 0, 'Cubierta Verona 260x140x2', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-11 22:18:53'),
(267, '35201718317113', 0, '', 0, 'Base Herrería Doble C 170x60x79.5', '170x60x79.5', 42, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-11 22:20:36'),
(268, '38381218437135', 0, '', 0, 'base herreria cruzada', 'dorada ( base hexagonal )', 47, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210813-124334pro.jpeg', 0, 1, '2021-08-13 17:43:33'),
(269, '41431218747135', 0, '', 0, 'Base herrería lineal', 'dorada 3 soportes ', 47, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210813-124551pro.jpeg', 0, 1, '2021-08-13 17:45:50'),
(270, '23141718397135', 0, '', 0, 'Arbotante caja 140x100x10', 'ónix blanco / ónix vino ', 48, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210813-171645pro.jpeg', 0, 1, '2021-08-13 22:16:44'),
(271, '2330171867135', 0, '', 0, 'Plato giratorio mármol blanco veneciano 35 cm', 'mármol blanco veneciano ', 49, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210813-173237pro.jpeg', 0, 1, '2021-08-13 22:32:36'),
(272, '46321118437146', 0, '', 0, 'Base redonda herrería 100 cm ', 'color bronce 38x100 cm ', 47, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210814-113826pro.jpeg', 0, 1, '2021-08-14 16:38:25'),
(273, '35381118417146', 0, '', 0, 'Base redonda herrería 50 cm ', 'color bronce 50x50', 47, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210814-113939pro.jpeg', 0, 1, '2021-08-14 16:39:38'),
(274, '6491118607146', 0, '', 0, 'Cubierta redonda Negro 45 cm ', 'Negro Querétaro ', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210814-115946pro.jpeg', 0, 1, '2021-08-14 16:51:37'),
(275, '4541118207146', 0, '', 0, 'Cubierta redonda blanco 60 cm ', 'Mármol Blanco Veneciano ', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210814-115711pro.jpeg', 0, 1, '2021-08-14 16:57:10'),
(276, '10261218787150', 0, '', 0, 'Porta Cuchara Chica', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-122818pro.jpeg', 0, 1, '2021-08-15 17:28:17'),
(277, '25281218797150', 0, '', 0, 'Porta Cuchara Grande', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-123001pro.jpeg', 0, 1, '2021-08-15 17:29:59'),
(278, '51311218317150', 0, '', 0, 'Tabla con división Mediana', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-123300pro.jpeg', 0, 1, '2021-08-15 17:32:58'),
(279, '50331218347150', 0, '', 0, 'Tabla Cuadrada Lisa', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-123431pro.jpeg', 0, 1, '2021-08-15 17:34:29'),
(280, '7351218117150', 0, '', 0, 'Tabla Cuadrada con Marco', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-123538pro.jpeg', 0, 1, '2021-08-15 17:35:37'),
(281, '5391218117150', 0, '', 0, 'Par Cuchara Mini', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-124022pro.jpeg', 0, 1, '2021-08-15 17:40:21'),
(282, '40411218547150', 0, '', 0, 'Juego Cuchara + Pala', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-124225pro.jpeg', 0, 1, '2021-08-15 17:42:24'),
(283, '56441218117150', 0, '', 0, 'Molcajete Chico de Guamuchil', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-124620pro.jpeg', 0, 1, '2021-08-15 17:46:18'),
(284, '5146121847150', 0, '', 0, 'Molcajete Mediano de Guamuchil', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-124826pro.jpeg', 0, 1, '2021-08-15 17:48:24'),
(285, '55481218227150', 0, '', 0, 'Molcajete Grande de Guamuchil', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-125017pro.jpeg', 0, 1, '2021-08-15 17:50:15'),
(286, '46511218197150', 0, '', 0, 'Juego Cuchara + Tenedor Grande', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-125240pro.jpeg', 0, 1, '2021-08-15 17:52:37'),
(287, '14531218577150', 0, '', 0, 'Juego Cuchara + Tenedor Chico', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-125353pro.jpeg', 0, 1, '2021-08-15 17:53:52'),
(288, '39551218287150', 0, '', 0, 'Tabla Con Mango Grande', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-125619pro.jpeg', 0, 1, '2021-08-15 17:56:17'),
(289, '16571218997150', 0, '', 0, 'Tazón mini guamuchil', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-125755pro.jpeg', 0, 1, '2021-08-15 17:57:54'),
(290, '61131817150', 0, '', 0, 'Olla chica con martillo guamuchil', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-130329pro.jpeg', 0, 1, '2021-08-15 18:03:28'),
(291, '1441318647150', 0, '', 0, 'Ensaladera Llanta', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-130450pro.jpeg', 0, 1, '2021-08-15 18:04:49'),
(292, '2551318207150', 0, '', 0, 'Ensaldera 35cm', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-130656pro.jpeg', 0, 1, '2021-08-15 18:06:55'),
(293, '4871318527150', 0, '', 0, 'Ensaldera 30 cm', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-130903pro.jpeg', 0, 1, '2021-08-15 18:09:01'),
(294, '4691318687150', 0, '', 0, 'Tabla Lisa Grande', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-131105pro.jpeg', 0, 1, '2021-08-15 18:11:03'),
(295, '28111318757150', 0, '', 0, 'Tabla Semi Ovalada con marco', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210815-131212pro.jpeg', 0, 1, '2021-08-15 18:12:10'),
(296, '37511518967150', 0, '', 0, 'Tabla Grande con división ', '', 50, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-15 20:52:16'),
(297, '3311118197172', 0, '', 0, 'Alhajero rectangular con piedra ', 'con fluorita ', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210817-110349pro.jpeg', 0, 1, '2021-08-17 16:03:48'),
(298, '2161118787172', 0, '', 0, 'Alhajero cuadrado con piedra ', 'fluorita ', 17, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210817-110755pro.jpeg', 0, 1, '2021-08-17 16:07:54'),
(299, '44331318937172', 0, '', 0, 'Cubierta Redonda Beige Maya 100cm', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-17 18:34:42'),
(300, '318141867172', 0, '', 0, 'Arbotante Caja Ónix Gris 80x40x10', '', 46, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-17 19:19:04'),
(301, '091218317205', 0, '', 0, 'Cubierta Redonda Ónix Multicolor 40cms', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-20 17:09:48'),
(302, '2811418217216', 0, '', 0, 'piedra fluorita ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-21 19:02:02'),
(303, '0461418537216', 0, '', 0, 'Cubierta Rectangular amatista', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-21 19:46:28'),
(304, '3546141827216', 0, '', 0, 'Base de madera H', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-21 19:47:41'),
(305, '33391118767220', 0, '', 0, 'florero cónico ónix verde ', '', 51, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-22 16:40:10'),
(306, '15351718667220', 0, '', 0, 'Lampara cubo ónix verde 80x20x20', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-174315pro.jpeg', 0, 1, '2021-08-22 22:43:13'),
(307, '21431718687220', 0, '', 0, 'Lampara cubo ónix verde 100x20x20', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-174508pro.jpeg', 0, 1, '2021-08-22 22:45:07'),
(308, '24451718907220', 0, '', 0, 'Porta cuchara recinto ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-174945pro.jpeg', 0, 1, '2021-08-22 22:46:13'),
(309, '2246171867220', 0, '', 0, 'charola recinto con perforacion', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-174929pro.jpeg', 0, 1, '2021-08-22 22:47:10'),
(310, '25471718177220', 0, '', 0, 'Plato recinto con bisel ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-174914pro.jpeg', 0, 1, '2021-08-22 22:47:51'),
(311, '58471718657220', 0, '', 0, 'Plato recinto', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-174852pro.jpeg', 0, 1, '2021-08-22 22:48:14'),
(312, '28501718797220', 0, '', 0, 'Lampara cubo ónix verde 60x20x20', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-175152pro.jpeg', 0, 1, '2021-08-22 22:51:51'),
(313, '352171867220', 0, '', 0, 'Lampara rectangular ónix miel 60x30x20', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210822-175331pro.jpeg', 0, 1, '2021-08-22 22:53:30'),
(314, '36231718997231', 0, '', 0, 'Juego 3 Manzanas Rojas', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 0, '2021-08-23 22:24:49'),
(315, '16531318687242', 0, '', 0, 'Misterio ónix ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-24 18:55:24'),
(316, '48551318797242', 0, '', 0, 'Rosario ónix ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-24 18:56:07'),
(317, '57561318867242', 0, '', 0, 'Pulsera Gajo ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-24 18:57:26'),
(318, '3771718537264', 0, '', 0, 'Piedra fluorita chica ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-26 22:08:09'),
(319, '1581718327264', 0, '', 0, 'Piedra fluorita mediana', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-26 22:08:58'),
(320, '2291718407264', 0, '', 0, 'Piedra fluorita grande', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-26 22:09:56'),
(321, '4101718247264', 0, '', 0, 'Piedra fluorita EX', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-26 22:10:58'),
(322, '1251718147264', 0, '', 0, 'set de baño 4 pza mármol bellagio', '', 34, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210826-172716pro.jpeg', 0, 1, '2021-08-26 22:27:15'),
(323, '25271718437264', 0, '', 0, 'set de baño 5 pza mármol bellagio', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-26 22:29:27'),
(324, '56131218587275', 0, '', 0, 'Cubierta rectangular amatista', '240x140x10', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-27 17:15:29'),
(325, '29181218407275', 0, '', 0, 'Base Placa herrería ', '', 42, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-27 17:19:21'),
(326, '001ANTICIPO1', 0, '', 0, 'ANTICIPO ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-28 16:53:31'),
(327, '001ANTICIPO1', 0, '', 0, 'ANTICIPO ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-28 16:54:04'),
(328, '366121897286', 0, '', 0, 'Bowl Irregular Cebra 75x35x15', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-28 17:07:20'),
(329, '457121837286', 0, '', 0, 'Bowl Irregular Cebra 40x33x12', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-28 17:08:14'),
(330, '2581218417286', 0, '', 0, 'Bowl Irregular Cebra 45x29x10', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-28 17:08:56'),
(331, '2291218137286', 0, '', 0, 'Bowl Irregular Cebra 62x33x16', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-28 17:09:56'),
(332, '52511218357290', 0, '', 0, 'Espejo Iluminado Onix 70x55x10 ', '', 14, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-29 17:53:02'),
(333, '10531218537290', 0, '', 0, 'Espejo Iluminado Onix 95x63x10', '', 14, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-29 17:53:50'),
(334, '58531218797290', 0, '', 0, 'Espejo Iluminado Onix 100x65x10', '', 14, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-29 17:54:43'),
(335, '50541218317290', 0, '', 0, 'Espejo Iluminado Onix 115x65x10 ', '', 14, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-29 17:55:21'),
(336, '30561318407312', 0, '', 0, ' aretes onix', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210831-135835pro.jpeg', 0, 1, '2021-08-31 18:58:35'),
(337, '59351618557312', 0, '', 0, 'Ovalin Pañuelo 40x40 Belagio', '', 52, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-31 21:36:56'),
(338, '7371618457312', 0, '', 0, 'Ovalin 1/2 Esfera 40cm Belagio', '', 52, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-31 21:37:38'),
(339, '5437161817312', 0, '', 0, 'Ovalin Pañuelo 35x35 Sto Tomas ', '', 52, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-31 21:38:34'),
(340, '5839161867312', 0, '', 0, 'Ovalin Pañuelo Rect. 40x30 Sto Tomas', '', 52, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-31 21:40:30'),
(341, '37401618677312', 0, '', 0, 'Ovalin 1/2 Esfera 35cm Mazahua', '', 52, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-31 21:41:00'),
(342, '7411618697312', 0, '', 0, 'Ovalin 1/2 Esfera 35cm Onix Blanco', '', 52, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-08-31 21:41:30'),
(343, '938131836835', 0, '', 0, 'Tapete Amatista 3 piezas', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-03 18:38:54'),
(344, '5551131852835', 0, '', 0, 'Credenza círculos ', '', 53, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-03 18:54:51'),
(345, '3811171890835', 0, '', 0, 'Domino mini ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-171254pro.jpeg', 0, 1, '2021-09-03 22:12:53'),
(346, '113171829835', 0, '', 0, 'Domino mediano ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-171418pro.jpeg', 0, 1, '2021-09-03 22:14:18'),
(347, '5114171888835', 0, '', 0, 'Toro mármol #10', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-171632pro.jpeg', 0, 1, '2021-09-03 22:16:31'),
(348, '1118171849835', 0, '', 0, 'Toro mármol #12', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-171906pro.jpeg', 0, 1, '2021-09-03 22:19:05'),
(349, '3419171818835', 0, '', 0, 'Toro mármol #15', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-172058pro.jpeg', 0, 1, '2021-09-03 22:20:58'),
(350, '1021171812835', 0, '', 0, 'Pantera mármol #20', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-172239pro.jpeg', 0, 1, '2021-09-03 22:22:39'),
(351, '1523171899835', 0, '', 0, 'Pantera mármol #25', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-172445pro.jpeg', 0, 1, '2021-09-03 22:24:44'),
(352, '2327171864835', 0, '', 0, 'Caballo elegante mármol  #25', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-172758pro.jpeg', 0, 1, '2021-09-03 22:27:57'),
(353, '1328171823835', 0, '', 0, 'Caballo mármol #20', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-172932pro.jpeg', 0, 1, '2021-09-03 22:29:31'),
(354, '2032171831835', 0, '', 0, 'Elefante Cirquero #1', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-173500pro.jpeg', 0, 1, '2021-09-03 22:34:59'),
(355, '835171880835', 0, '', 0, 'Elefante cirquero #2', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-173533pro.jpeg', 0, 1, '2021-09-03 22:35:31'),
(356, '3835171859835', 0, '', 0, 'Elefante cirquero #3', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-173603pro.jpeg', 0, 1, '2021-09-03 22:36:03'),
(357, '3436171834835', 0, '', 0, 'Elefante cirquero #4', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210903-173654pro.jpeg', 0, 1, '2021-09-03 22:36:53'),
(358, '1828141886846', 0, '', 0, 'Flete 5 ', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:29:52'),
(359, '630141878846', 0, '', 0, 'Flete 6', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:30:53'),
(360, '03114182846', 0, '', 0, 'Flete 7', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:31:58'),
(361, '932141892846', 0, '', 0, 'Flete 8', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:32:26'),
(362, '4532141892846', 0, '', 0, 'Flete 4', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:33:03'),
(363, '334141861846', 0, '', 0, 'Flete 9', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:34:11'),
(364, '4039141854846', 0, '', 0, 'Flete 11', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:40:02'),
(365, '1040141822846', 0, '', 0, 'Flete 12', '', 54, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-04 19:40:33'),
(366, '3257161826846', 0, '', 0, 'Madona 9 cm ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210904-165914pro.jpeg', 0, 1, '2021-09-04 21:59:13'),
(367, '2059161855846', 0, '', 0, 'Madona 11 cm ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210904-170011pro.jpeg', 0, 1, '2021-09-04 22:00:10'),
(368, '1220121874850', 0, '', 0, 'Lampara cubo 30x15x15', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-05 17:20:58'),
(369, '821121820850', 0, '', 0, 'Lampara cubo 35x15x15', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-05 17:21:29'),
(370, '3621121826850', 0, '', 0, 'Lampara cubo 40x15x15', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-05 17:22:00'),
(371, '1022121831850', 0, '', 0, 'Lampara cubo 45x15x15', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-05 17:23:03'),
(372, '1523121866850', 0, '', 0, 'Lampara cubo 50x15x15', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-05 17:23:50'),
(373, '582151896850', 0, '', 0, 'Par jarrones madera con follaje', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-05 20:03:21'),
(374, 'ALV1', 0, '', 0, 'ARETES DE OPALO CON DIAMANTES ROSAS', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-223250pro.jpeg', 0, 1, '2021-09-06 03:32:50'),
(375, 'ACF1', 0, '', 0, 'ARETES DE CIRCULO CON FLOR Y DIAMANTES', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-223626pro.jpeg', 0, 1, '2021-09-06 03:36:25'),
(376, 'ACN001', 0, '', 0, 'ARETES DE CONCHA GEOMETRICA SIMPLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-224028pro.jpeg', 0, 1, '2021-09-06 03:40:27'),
(377, 'ATP001', 0, '', 0, 'ARRACADA TRIPLE EN C PLATA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-224702pro.jpeg', 0, 1, '2021-09-06 03:47:01'),
(378, 'CMC4', 0, '', 0, 'COLLAR 4 CAPAS CON PERLA Y MEDALLA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-225129pro.jpeg', 0, 1, '2021-09-06 03:51:28'),
(379, 'AFPM001', 0, '', 0, 'ARETES GALA CON DIAMANTE MORADO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-230504pro.jpeg', 0, 1, '2021-09-06 04:05:03'),
(380, 'AHCN001', 0, '', 0, 'ARETES FORMA HOJA ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-230936pro.jpeg', 0, 1, '2021-09-06 04:09:36'),
(381, 'ADAA001', 0, '', 0, 'ARETES GEOMETRICOS MULTICOLOR', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-231224pro.jpeg', 0, 1, '2021-09-06 04:12:24'),
(382, 'AHV001', 0, '', 0, 'ARETES HOJA EN CASCADA VERDE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-231536pro.jpeg', 0, 1, '2021-09-06 04:15:35'),
(383, 'AAFD001', 0, '', 0, 'ARETES EN C FLORES CON PERLA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-231826pro.jpeg', 0, 1, '2021-09-06 04:18:25'),
(384, 'AFP001', 0, '', 0, 'ARETE CIRCULAR CON FLORES Y DIAMANTES', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-232121pro.jpeg', 0, 1, '2021-09-06 04:21:20'),
(385, 'AMCM001', 0, '', 0, 'ARETE EN C CON MARIPOSAS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-232527pro.jpeg', 0, 1, '2021-09-06 04:25:25'),
(386, 'PTOT', 0, '', 0, 'PULSERA TEJIDA OJO TURCO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-232724pro.jpeg', 0, 1, '2021-09-06 04:27:23'),
(387, 'PPAM', 0, '', 0, 'PULSERA DELICADA AMATISTA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-234237pro.jpeg', 0, 1, '2021-09-06 04:42:37'),
(388, 'AFB001', 0, '', 0, 'ARETE BLANCO GEOMETRICO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-234820pro.jpeg', 0, 1, '2021-09-06 04:48:20'),
(389, 'PPCN001', 0, '', 0, 'PASADOR TERCIOPELO 1', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-234906pro.jpeg', 0, 1, '2021-09-06 04:49:05'),
(390, 'C2C1', 0, '', 0, 'COLLAR DOBLE CAPA CON PERLA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-235204pro.jpeg', 0, 1, '2021-09-06 04:52:03'),
(391, 'PHP1', 0, '', 0, 'PULSERA DE HILO CON PERLA DE RIO', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-235454pro.jpeg', 0, 1, '2021-09-06 04:54:54'),
(392, 'LCPN', 0, '', 0, 'LIGA CABELLO NEGRA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-235642pro.jpeg', 0, 1, '2021-09-06 04:56:42'),
(393, 'LCPC', 0, '', 0, 'LIGA CABELLO CAFE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210905-235926pro.jpeg', 0, 1, '2021-09-06 04:59:25'),
(394, 'CPFB2', 0, '', 0, 'COLLAR DE PIEDRA BLANCA IMITACION ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-000150pro.jpeg', 0, 1, '2021-09-06 05:01:49'),
(395, 'APAM001', 0, '', 0, 'ARETE COLGANTE DE AMATISTA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-000357pro.jpeg', 0, 1, '2021-09-06 05:03:56'),
(396, 'CDPD1', 0, '', 0, 'COLLAR DOBLE CON BORLA DE PERLA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-000556pro.jpeg', 0, 1, '2021-09-06 05:05:56'),
(397, 'PID1', 0, '', 0, 'PULSERA 5 TIRAS DORADA IMAN ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-000852pro.jpeg', 0, 1, '2021-09-06 05:08:51'),
(398, 'G4CP', 0, '', 0, 'COLLAR 4 CAPAS DORADO CON MEDALLAS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-001137pro.jpeg', 0, 1, '2021-09-06 05:11:36'),
(399, 'PTND', 0, '', 0, 'PULSERA TEJIDA DE CHAQUIRA NEGRA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-001453pro.jpeg', 0, 1, '2021-09-06 05:14:52'),
(400, 'APD001', 0, '', 0, 'ARETES DE PERLA CONCHA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-002007pro.jpeg', 0, 1, '2021-09-06 05:20:06'),
(401, 'AAD001', 0, '', 0, 'ARRACADA METALICA ASIMETRICA', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-002237pro.jpeg', 0, 1, '2021-09-06 05:22:36'),
(402, 'APBF001', 0, '', 0, 'ARETES INVERSA FLORES BLANCAS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-002655pro.jpeg', 0, 1, '2021-09-06 05:26:54'),
(403, 'CPFR', 0, '', 0, 'COLLAR DE PIEDRA ROSA IMITACION ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-003155pro.jpeg', 0, 1, '2021-09-06 05:31:55'),
(404, 'ACPAM002', 0, '', 0, 'ARETE COLGANTE DE AMATISTA CON MONEDA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-003442pro.jpeg', 0, 1, '2021-09-06 05:34:42'),
(405, 'CPFM', 0, '', 0, 'COLLAR DE PIEDRA MORADA IMITACION ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-003636pro.jpeg', 0, 1, '2021-09-06 05:36:36'),
(406, 'C2CCD', 0, '', 0, 'COLLAR CADENA DOBLE CON CUBO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-004004pro.jpeg', 0, 1, '2021-09-06 05:40:03'),
(407, 'PDOT', 0, '', 0, 'PULSERA DE COBRE OJO TURCO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-004251pro.jpeg', 0, 1, '2021-09-06 05:42:50'),
(408, 'CPFA', 0, '', 0, 'COLLAR DE PIEDRA AZUL IMITACION ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-004441pro.jpeg', 0, 1, '2021-09-06 05:44:41'),
(409, 'C2CMM', 0, '', 0, 'COLLAR ACERO INOXIDABLE DOS CADENAS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-004709pro.jpeg', 0, 1, '2021-09-06 05:47:08'),
(410, 'CDVGO', 0, '', 0, 'MEDALLA OVALADA Virgen de Guadalupe', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-005006pro.jpeg', 0, 1, '2021-09-06 05:50:05'),
(411, 'ACDP001', 0, '', 0, 'ARETES DOBLE CIRCULO DIAMANTE Y PERLA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-140101pro.jpeg', 0, 1, '2021-09-06 19:01:00'),
(412, 'CCDP', 0, '', 0, 'CADENA EN CHAPA 14K Y PERLA DE RIO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-140403pro.jpeg', 0, 1, '2021-09-06 19:04:03'),
(413, 'PP3P001', 0, '', 0, 'SET 3 PASADORES MOÑO CORAZON PERLA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-140641pro.jpeg', 0, 1, '2021-09-06 19:06:40'),
(414, 'AHD', 0, '', 0, 'ANILLO DE HOJA ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-140753pro.jpeg', 0, 1, '2021-09-06 19:07:53'),
(415, 'PP3PV001', 0, '', 0, 'SET 3 PASADORES PIEDRA VERDE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-141206pro.jpeg', 0, 1, '2021-09-06 19:12:06'),
(416, 'LCPT', 0, '', 0, 'LIGA CABELLO TRANSPARENTE', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-141339pro.jpeg', 0, 1, '2021-09-06 19:13:39'),
(417, 'AAM', 0, '', 0, 'ARETE PIEDRA AMATISTA MORADA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-141630pro.jpeg', 0, 1, '2021-09-06 19:16:30'),
(418, 'AHB001', 0, '', 0, 'ARETES HOJA EN CASCADA BLANCOS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-144027pro.jpeg', 0, 1, '2021-09-06 19:40:26'),
(419, 'AHCD002', 0, '', 0, 'ARETES DE FLOR DORADOS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-144518pro.jpeg', 0, 1, '2021-09-06 19:45:17'),
(420, 'AAG', 0, '', 0, 'ARETES DE AMATISTA GRIS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-144909pro.jpeg', 0, 1, '2021-09-06 19:49:08'),
(421, 'ACCD001', 0, '', 0, 'ARETES ESMERILADOS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-145145pro.jpeg', 0, 1, '2021-09-06 19:51:42'),
(422, 'GPAM', 0, '', 0, 'GARGANTILLA DELICADA AMATISTA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-164129pro.jpeg', 0, 1, '2021-09-06 21:41:29'),
(423, 'C2CMR', 0, '', 0, 'COLLAR DE ACERO INOXIDABLE DOBLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-164323pro.jpeg', 0, 1, '2021-09-06 21:43:23'),
(424, 'CC2D', 0, '', 0, 'COLLAR SIMPLE DOBLE CAPA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-164839pro.jpeg', 0, 1, '2021-09-06 21:48:38'),
(425, 'PATP', 0, '', 0, 'PULSERA DE ACERO DE TITANIO TIPO TIFFANY', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-165114pro.jpeg', 0, 1, '2021-09-06 21:51:14'),
(426, 'CGD', 0, '', 0, 'COLLAR CON HEBILLA DE COBRE Y ZIRCONIA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-165400pro.jpeg', 0, 1, '2021-09-06 21:54:00'),
(427, 'AHCD001', 0, '', 0, 'ARETES HOJA LEON ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-170116pro.jpeg', 0, 1, '2021-09-06 22:01:15'),
(428, 'AFT001', 0, '', 0, 'ARETES VINTAGE TEJIDO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-170313pro.jpeg', 0, 1, '2021-09-06 22:03:13'),
(429, 'LCP6M01', 0, '', 0, 'SET 6 LIGAS DEGRADADO MORADO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-170556pro.jpeg', 0, 1, '2021-09-06 22:05:55'),
(430, 'ACPCD001', 0, '', 0, 'ARETES GEOMETRICOS CUARZO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-170936pro.jpeg', 0, 1, '2021-09-06 22:09:35'),
(431, 'LCP6N001', 0, '', 0, 'SET 6 LIGAS DEGRADADO NEGRO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-171413pro.jpeg', 0, 1, '2021-09-06 22:14:12'),
(432, 'ACAM', 0, '', 0, 'ANILLO CLASICO AMATISTA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-171703pro.jpeg', 0, 1, '2021-09-06 22:17:03'),
(433, 'LCPR', 0, '', 0, 'LIGA CABELLO ROSA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-171756pro.jpeg', 0, 1, '2021-09-06 22:17:55'),
(434, 'ACA001', 0, '', 0, 'ARETES DE CUARZO AVENTURINA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-172155pro.jpeg', 0, 1, '2021-09-06 22:21:55'),
(435, 'CPFV', 0, '', 0, 'COLLAR DE PIEDRA VERDE IMITACION ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-172740pro.jpeg', 0, 1, '2021-09-06 22:27:39'),
(436, 'CDVGC', 0, '', 0, 'MEDALLA CIRCULAR Virgen de Guadalupe', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-173117pro.jpeg', 0, 1, '2021-09-06 22:31:17'),
(437, 'ACB001', 0, '', 0, 'ARETES CORAZON BLANCOS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-173521pro.jpeg', 0, 1, '2021-09-06 22:35:21'),
(438, 'ABCH001', 0, '', 0, 'ARETES CASCADA BLANCA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-173720pro.jpeg', 0, 1, '2021-09-06 22:37:20'),
(439, 'PPCS', 0, '', 0, 'PULSERA SIRENA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-174010pro.jpeg', 0, 1, '2021-09-06 22:40:09'),
(440, 'CPDC', 0, '', 0, 'COLLAR DE PERLAS DOBLE CAPA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-175119pro.jpeg', 0, 1, '2021-09-06 22:51:18'),
(441, 'LCP6R001', 0, '', 0, 'SET 6 LIGAS DEGRADADO ROSA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-175937pro.jpeg', 0, 1, '2021-09-06 22:59:37'),
(442, 'PP3PM001', 0, '', 0, 'SET 3 PASADORES PIEDRA MOSTAZA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-180415pro.jpeg', 0, 1, '2021-09-06 23:04:15'),
(443, 'PPCN002', 0, '', 0, 'PASADOR TERCIOPELO 2', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-181031pro.jpeg', 0, 1, '2021-09-06 23:10:30'),
(444, 'PPCN003', 0, '', 0, 'PASADOR TERCIOPELO 3', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-181058pro.jpeg', 0, 1, '2021-09-06 23:10:58'),
(445, 'ASD', 0, '', 0, 'ANILLO SERPIENTE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-181309pro.jpeg', 0, 1, '2021-09-06 23:13:08'),
(446, 'AAD003', 0, '', 0, 'ARRACADA IRREGULAR', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-181656pro.jpeg', 0, 1, '2021-09-06 23:16:55'),
(447, 'ADFD001', 0, '', 0, 'ARETES DOBLE FLOR', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-182046pro.jpeg', 0, 1, '2021-09-06 23:20:45'),
(448, 'ACD001', 0, '', 0, 'ARETES BOLA CON PERLA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-182213pro.jpeg', 0, 1, '2021-09-06 23:22:12'),
(449, 'C2DC', 0, '', 0, 'COLLAR DE ANILLO DE ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-183544pro.jpeg', 0, 1, '2021-09-06 23:35:44'),
(450, 'C2CD', 0, '', 0, 'COLLAR CANDADO DE ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-183817pro.jpeg', 0, 1, '2021-09-06 23:38:16'),
(451, 'BCAI', 0, '', 0, 'BRAZALETE CORAZON ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-184947pro.jpeg', 0, 1, '2021-09-06 23:49:46'),
(452, 'BTAI', 0, '', 0, 'BRAZALETE TRENZA ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-185306pro.jpeg', 0, 1, '2021-09-06 23:53:05'),
(453, 'BPAI', 0, '', 0, 'BRAZALETE PERLA ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-185618pro.jpeg', 0, 1, '2021-09-06 23:56:17'),
(454, 'BZAI', 0, '', 0, 'BRAZALETE ZIRCONIA ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-190323pro.jpeg', 0, 1, '2021-09-07 00:03:23'),
(455, 'BNAI', 0, '', 0, 'BRAZALETE NUDO ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-190408pro.jpeg', 0, 1, '2021-09-07 00:04:07'),
(456, 'BCAIP', 0, '', 0, 'BRAZALETE CANDADO ACERO INOXIDABLE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-190850pro.jpeg', 0, 1, '2021-09-07 00:08:49'),
(457, 'STV', 0, '', 0, 'SCRUNCHIE TERCIOPELO VERDE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-221511pro.jpeg', 0, 1, '2021-09-07 03:15:10'),
(458, 'STA', 0, '', 0, 'SCRUNCHIE TERCIOPELO AZUL', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-221538pro.jpeg', 0, 1, '2021-09-07 03:15:37'),
(459, 'SMC', 0, '', 0, 'SCRUNCHIE CON MOÑO DE CUERO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-221721pro.jpeg', 0, 1, '2021-09-07 03:17:20'),
(460, 'SSR', 0, '', 0, 'SCRUNCHIE SATIN ROSA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-221952pro.jpeg', 0, 1, '2021-09-07 03:19:52'),
(461, 'SCC', 0, '', 0, 'SCRUNCHIE CUERO CAFÉ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-222134pro.jpeg', 0, 1, '2021-09-07 03:21:33'),
(462, 'SCA', 0, '', 0, 'SCRUNCHIE CUERO AZUL', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-222159pro.jpeg', 0, 1, '2021-09-07 03:21:58'),
(463, 'SCR', 0, '', 0, 'SCRUNCHIE CUERO CAFÉ', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-222220pro.jpeg', 0, 1, '2021-09-07 03:22:20'),
(464, 'STMM', 0, '', 0, 'SCRUNCHIE TERCIOPELO MORADO CON MOÑO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-222328pro.jpeg', 0, 1, '2021-09-07 03:23:28'),
(465, 'PCTB', 0, '', 0, 'PAZADOR TERCIOPELO BEIGE', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-222616pro.jpeg', 0, 1, '2021-09-07 03:26:14'),
(466, 'PCTN', 0, '', 0, 'PAZADOR TERCIOPELO NEGRO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-222643pro.jpeg', 0, 1, '2021-09-07 03:26:43'),
(467, 'ATORY', 0, '', 0, 'ARETE ZIRCONIA TORY', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-222757pro.jpeg', 0, 1, '2021-09-07 03:27:57'),
(468, 'ACIR2', 0, '', 0, 'ARETE CRISTAL SMALL', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-223046pro.jpeg', 0, 1, '2021-09-07 03:30:45'),
(469, 'AAAR01', 0, '', 0, 'ARETES ALAS DE ANGEL', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-223204pro.jpeg', 0, 1, '2021-09-07 03:32:04'),
(470, 'AMB1', 0, '', 0, 'ARETES MARMOL BLANCO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-223339pro.jpeg', 0, 1, '2021-09-07 03:33:38'),
(471, 'AMN1', 0, '', 0, 'ARETES MARMOL NEGRO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-223406pro.jpeg', 0, 1, '2021-09-07 03:34:05'),
(472, 'AFPV01', 0, '', 0, 'ARETE FLORAL CON PERLA', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-223705pro.jpeg', 0, 1, '2021-09-07 03:37:05'),
(473, 'ACB01', 0, '', 0, 'ARRACADA EN C BLANCA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-223858pro.jpeg', 0, 1, '2021-09-07 03:38:57'),
(474, 'ACG01', 0, '', 0, 'ARRACADA EN C GRIS', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-223955pro.jpeg', 0, 1, '2021-09-07 03:39:55'),
(475, 'ACR001', 0, '', 0, 'ARETES EN C RETRO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-224117pro.jpeg', 0, 1, '2021-09-07 03:41:17'),
(476, 'ATD001', 0, '', 0, 'ARRACADA TRIPLE EN C DORADO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-224330pro.jpeg', 0, 1, '2021-09-07 03:43:30'),
(477, 'AMPAM', 0, '', 0, 'ARETE MINI AMATISTA MORADA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-224522pro.jpeg', 0, 1, '2021-09-07 03:45:21'),
(478, 'AIN001', 0, '', 0, 'ARETE INVERSO NUDO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-224709pro.jpeg', 0, 1, '2021-09-07 03:47:09'),
(479, 'ACT001', 0, '', 0, 'ARETE DE CRISTAL TEJIDO', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-225212pro.jpeg', 0, 1, '2021-09-07 03:52:11'),
(480, 'DPAC', 0, '', 0, 'DIJE AMATISTA CON CORREA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-225854pro.jpeg', 0, 1, '2021-09-07 03:58:53'),
(481, 'DPACD', 0, '', 0, 'DIJE AMATISTA CON CADENA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210906-230120pro.jpeg', 0, 1, '2021-09-07 04:01:19'),
(482, '5435131825872', 0, '', 0, 'Tabla con perforación mármol negro ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210907-133815pro.jpeg', 0, 1, '2021-09-07 18:38:14'),
(483, '17361318498120', 0, '', 0, 'Anticipó ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-12 20:36:39'),
(484, '41551518518164', 0, '', 0, 'Anticipó 1', '', 56, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-16 20:56:29'),
(485, '50291218738186', 0, '', 0, 'charola rectangular onix blanco', '', 38, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-18 17:30:06'),
(486, '5551418118186', 0, '', 0, 'follaje completo ', '', 20, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210918-140933pro.jpeg', 0, 1, '2021-09-18 19:09:32'),
(487, '13381418368186', 0, '', 0, 'lampara lobo', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-18 19:38:33'),
(488, '43381418198186', 0, '', 0, 'lampara elefante', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-18 19:39:06'),
(489, '17381618208190', 0, '', 0, 'Despachador bellagio', '', 34, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-19 21:38:57'),
(490, 'CPFNC', 0, '', 0, 'COLLAR DE PIEDRA NEGRA IMITACION CH', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-19 22:30:20'),
(491, 'AVP01', 0, '', 0, 'ARRACADA DE PERLA CON ESMERALDA', '', 55, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-20 18:40:33'),
(492, 'AVP02', 0, '', 0, 'ANILLO DE PERLA CON ESMERALDA', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-20 18:50:18'),
(493, '2581618718234', 0, '', 0, 'portarretrato ónix ', '', 57, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-23 21:59:19'),
(494, '361171838234', 0, '', 0, 'plato giratorio cuarcita 35 cm', '', 49, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-23 22:02:44'),
(495, '1371718608234', 0, '', 0, 'plato giratorio cuarcita 40 cm', '', 49, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-23 22:08:28'),
(496, '42281118128245', 0, '', 0, 'Anticipó 15', '', 56, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-24 16:29:06'),
(497, '3871518478245', 0, '', 0, 'cubierta vecchia 260x140x2', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-24 20:09:32'),
(498, '5221518928245', 0, '', 0, 'base comedor Y5', '', 42, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/210924-152541pro.jpeg', 0, 1, '2021-09-24 20:25:40'),
(499, '42331718388245', 0, '', 0, 'Comedor emperador con base de 1.20x1.20 ', '', 21, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-24 22:35:45'),
(500, '48141218818256', 0, '', 0, 'lampara virgen', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-25 17:15:25'),
(501, '5991418528260', 0, '', 0, 'barra tzalam', '', 30, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 19:10:31'),
(502, '22471518158260', 0, '', 0, 'Cuadro hoja dorada ', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 20:48:21'),
(503, '16581518328260', 0, '', 0, 'Cuadro Estambul ', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 20:58:50'),
(504, '5958151828260', 0, '', 0, 'Cuadro pintura ', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 20:59:43'),
(505, '58221618388260', 0, '', 0, 'Cuadro ballet ', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 21:23:46'),
(506, '59231618118260', 0, '', 0, 'Cuadro avioneta ', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 21:24:17'),
(507, '53241618428260', 0, '', 0, 'Cuadro hojas verde', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 21:25:51'),
(508, '49311618818260', 0, '', 0, 'Cuadro mapa ', '', 25, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-26 21:32:16'),
(509, '38241018188282', 0, '', 0, 'base herrería lamina', '', 42, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-28 15:25:19'),
(510, '419111858282', 0, '', 0, 'bulto de piedra ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-09-28 16:19:41'),
(511, '1449101869915', 0, '', 0, 'cubierta vecchia 150x50x2', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-01 15:50:09'),
(512, '31915188926', 0, '', 0, 'flete 240', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-02 20:19:14'),
(513, '3316141882930', 0, '', 0, 'anticipo cubiertas ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-03 19:17:00'),
(514, '2923141899930', 0, '', 0, 'cubierta granito negro 120x46x2', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-03 19:24:21'),
(515, '2824141843930', 0, '', 0, 'cubierta granito negro 50x70x2', '', 41, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-03 19:24:56'),
(516, '2547161898930', 0, '', 0, 'cubierta redonda 40 cm ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-03 21:47:36'),
(517, '1947111848941', 0, '', 0, 'esfera marq 50 cm ', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-04 16:47:57'),
(518, '4948111843941', 0, '', 0, 'esfera marq 40 cm ', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-04 16:49:13'),
(519, '3452111865941', 0, '', 0, 'lampara cilindro marq 120x25', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-04 16:55:16'),
(520, '4357111857941', 0, '', 0, 'arbotante 1/2 caña 140x30 zebra', '', 32, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-04 16:58:08'),
(521, '103012186952', 0, '', 0, 'bowl irregular espuma 62x50x15', '', 58, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-123217pro.jpeg', 0, 1, '2021-10-05 17:31:10'),
(522, '5832121884952', 0, '', 0, 'bowl irregular espuma 70x55x16', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-123427pro.jpeg', 0, 1, '2021-10-05 17:34:26'),
(523, '5634121859952', 0, '', 0, 'bowl irregular onix verde 80x50x20', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-123658pro.jpeg', 0, 1, '2021-10-05 17:36:57'),
(524, '238121834952', 0, '', 0, 'Bowl irregular espuma 66x55x25', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-124006pro.jpeg', 0, 1, '2021-10-05 17:40:04'),
(525, '374112185952', 0, '', 0, 'Bowl irregular onix verde 50x34x15', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-124453pro.jpeg', 0, 1, '2021-10-05 17:44:52'),
(526, '4845121822952', 0, '', 0, 'Bowl irregular onix verde 40x40x12 ', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-124644pro.jpeg', 0, 1, '2021-10-05 17:46:43'),
(527, '747121883952', 0, '', 0, 'Bowl irregular espuma 50x40x12', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-124755pro.jpeg', 0, 1, '2021-10-05 17:47:54'),
(528, '1748121842952', 0, '', 0, 'Bowl irregular onix verde 54x40x20', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-124904pro.jpeg', 0, 1, '2021-10-05 17:49:03'),
(529, '1949121825952', 0, '', 0, 'Bowl irregular espuma 52x40x20', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-125101pro.jpeg', 0, 1, '2021-10-05 17:51:00'),
(530, '2851121851952', 0, '', 0, 'Bowl irregular onix verde 47x40x13', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-125242pro.jpeg', 0, 1, '2021-10-05 17:52:41'),
(531, '5452121854952', 0, '', 0, 'Bowl irregular café 40x35x13', '', 11, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211005-125331pro.jpeg', 0, 1, '2021-10-05 17:53:30'),
(532, '352710186974', 0, '', 0, 'jabonera ovalada mármol 12x9', '', 59, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211007-103128pro.jpeg', 0, 1, '2021-10-07 15:31:27'),
(533, '4936101841974', 0, '', 0, 'jabonera hoja mármol 17x11', '', 59, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211007-103845pro.jpeg', 0, 1, '2021-10-07 15:38:44'),
(534, '214121884974', 0, '', 0, 'plato mármol negro 22 cm ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211007-121551pro.jpeg', 0, 1, '2021-10-07 17:15:50'),
(535, '5815121892974', 0, '', 0, 'plato Sto. tomas 22 cm ', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211007-121740pro.jpeg', 0, 1, '2021-10-07 17:17:39'),
(536, '5218121890974', 0, '', 0, 'Caja Mármol Negro Madera 20x10x5', '', 17, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211007-122022pro.jpeg', 0, 1, '2021-10-07 17:20:21'),
(537, '2121121830974', 0, '', 0, 'Caja Sto. Tomas Madera 20x10x5', '', 17, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211007-122354pro.jpeg', 0, 1, '2021-10-07 17:23:53'),
(538, '215121837985', 0, '', 0, 'collar ónix ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211008-121747pro.jpeg', 0, 1, '2021-10-08 17:17:46'),
(539, 'ALA12', 0, '', 0, 'Arete Largo Artesanal', '', 60, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 'public/img/productos/211014-135814pro.jpeg', 0, 1, '2021-10-08 17:21:05'),
(540, '5528121855985', 0, '', 0, 'pulsera ónix ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-08 17:29:04'),
(541, '1331121857985', 0, '', 0, 'pulsera fantasía ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-08 17:31:55'),
(542, '4532121856985', 0, '', 0, 'llavero ónix ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-08 17:34:12'),
(543, '2848151814985', 0, '', 0, 'columna mármol blanco', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-08 20:49:10'),
(544, '5949151828985', 0, '', 0, 'columna marmol negro ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-08 20:50:16'),
(545, '4718151850996', 0, '', 0, 'ajedrez mármol ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-09 20:19:13'),
(546, '58221718469122', 0, '', 0, 'Ancla Mármol con piedra ', '', 7, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-12 22:23:32'),
(547, '51481518959133', 0, '', 0, 'Mesa de centro blanca ', '', 10, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-13 20:49:12'),
(548, '7231218319144', 0, '', 0, 'Tequilero de onix', '', 4, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-14 17:24:52'),
(549, '11581218469166', 0, '', 0, 'cráneo malaquita', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 17:58:41'),
(550, '47581218289166', 0, '', 0, 'jaguar malaquita ', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 17:59:02'),
(551, '20161818859166', 0, '', 0, 'charola ovalo forest 35x26x5', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:17:05'),
(552, '40171818939166', 0, '', 0, 'charola ovalo forest 35x26x2', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:18:03'),
(553, '38181818239166', 0, '', 0, 'charola rectangular forest 35x15x5', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:19:02'),
(554, '29191818959166', 0, '', 0, 'charola rectangular forest 35x18x5', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:19:53'),
(555, '16201818749166', 0, '', 0, 'charola rectangular forest 30x15x2', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:20:34'),
(556, '7211818179166', 0, '', 0, 'charola ovalo forest 25x18x2', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:21:29'),
(557, '45211818589166', 0, '', 0, 'charola hexagonal forest 26x20x2', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:22:08'),
(558, '37221818369166', 0, '', 0, 'charola hexagonal forest 26x20x5', '', 36, 4, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, '', 0, 1, '2021-10-16 23:22:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_sucursal_precios`
--

CREATE TABLE `productos_sucursal_precios` (
  `id` int(11) NOT NULL,
  `idsucursal` int(11) NOT NULL,
  `productoid` int(11) NOT NULL,
  `stock` double NOT NULL,
  `stock_paq` float NOT NULL DEFAULT 0,
  `preciocompra` decimal(10,2) NOT NULL,
  `porcentaje` int(11) NOT NULL,
  `precioventa` decimal(10,2) NOT NULL,
  `especial1_precio` float NOT NULL,
  `especial1_cantidad` float NOT NULL,
  `especial2_precio` decimal(10,2) NOT NULL,
  `especial2_cantidad` float NOT NULL,
  `mediomayoreo` float NOT NULL,
  `canmediomayoreo` float NOT NULL,
  `mayoreo` float NOT NULL,
  `canmayoreo` float NOT NULL,
  `precio_paq` decimal(10,2) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productos_sucursal_precios`
--

INSERT INTO `productos_sucursal_precios` (`id`, `idsucursal`, `productoid`, `stock`, `stock_paq`, `preciocompra`, `porcentaje`, `precioventa`, `especial1_precio`, `especial1_cantidad`, `especial2_precio`, `especial2_cantidad`, `mediomayoreo`, `canmediomayoreo`, `mayoreo`, `canmayoreo`, `precio_paq`, `status`) VALUES
(51, 1, 1, 100, 0, 50.00, 100, 100.00, 100, 1, 100.00, 1, 100, 1, 100, 1, 100.00, 1),
(52, 1, 2, 1, 0, 0.00, 0, 0.00, 0, 1, 0.00, 1, 0, 1, 0, 1, 0.00, 1),
(53, 1, 3, 1, 0, 0.00, 0, 29600.00, 29600, 1, 29600.00, 1, 29600, 1, 29600, 1, 29600.00, 0),
(54, 1, 4, 1, 0, 0.00, 0, 6900.00, 6900, 1, 6900.00, 1, 6900, 1, 6900, 1, 6900.00, 0),
(55, 1, 5, 1, 0, 0.00, 0, 12200.00, 12200, 1, 12200.00, 1, 12200, 1, 12200, 1, 12200.00, 0),
(56, 1, 6, 1, 0, 0.00, 0, 2800.00, 2800, 1, 2800.00, 1, 2800, 1, 2800, 1, 2800.00, 0),
(57, 1, 7, 4, 0, 0.00, 0, 6500.00, 6500, 1, 6500.00, 1, 6500, 1, 6500, 1, 6500.00, 0),
(58, 1, 8, 2, 0, 0.00, 0, 8200.00, 8200, 1, 8200.00, 1, 8200, 1, 8200, 1, 8200.00, 0),
(59, 1, 9, 1, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(60, 1, 10, 1, 0, 0.00, 0, 890.00, 890, 1, 890.00, 1, 890, 1, 890, 1, 890.00, 0),
(61, 1, 11, 1, 0, 0.00, 0, 670.00, 670, 1, 670.00, 1, 670, 1, 670, 1, 670.00, 0),
(62, 1, 12, 2, 0, 0.00, 0, 495.00, 495, 1, 495.00, 1, 495, 1, 495, 1, 495.00, 0),
(63, 1, 13, 1, 0, 0.00, 0, 7600.00, 7600, 1, 7600.00, 1, 7600, 1, 7600, 1, 7600.00, 0),
(64, 1, 14, 1, 0, 0.00, 0, 5800.00, 5800, 1, 5800.00, 1, 5800, 1, 5800, 1, 5800.00, 0),
(65, 1, 15, 3, 0, 0.00, 0, 3900.00, 3900, 1, 3900.00, 1, 3900, 1, 3900, 1, 3900.00, 0),
(66, 1, 16, 1, 0, 0.00, 0, 5200.00, 5200, 1, 5200.00, 1, 5200, 1, 5200, 1, 5200.00, 0),
(67, 1, 17, 2, 0, 0.00, 0, 7600.00, 7600, 1, 7600.00, 1, 7600, 1, 7600, 1, 7600.00, 0),
(68, 1, 18, 6, 0, 0.00, 0, 155.00, 155, 1, 155.00, 1, 155, 1, 155, 1, 155.00, 1),
(69, 1, 19, 2, 0, 0.00, 0, 155.00, 155, 1, 155.00, 1, 155, 1, 155, 1, 155.00, 0),
(70, 1, 20, 10, 0, 0.00, 0, 390.00, 390, 1, 390.00, 1, 390, 1, 390, 1, 390.00, 1),
(71, 1, 21, 12, 0, 0.00, 0, 390.00, 390, 1, 390.00, 1, 390, 1, 390, 1, 390.00, 1),
(72, 1, 22, 1, 0, 0.00, 0, 12900.00, 12900, 1, 12900.00, 1, 12900, 1, 12900, 1, 12900.00, 0),
(73, 1, 23, 1, 0, 0.00, 0, 5400.00, 5400, 1, 5400.00, 1, 5400, 1, 5400, 1, 5400.00, 0),
(74, 1, 24, 1, 0, 0.00, 0, 850.00, 850, 1, 850.00, 1, 850, 1, 850, 1, 850.00, 0),
(75, 1, 25, 1, 0, 0.00, 0, 1485.00, 1485, 1, 1485.00, 1, 1485, 1, 1485, 1, 1485.00, 0),
(76, 1, 26, 1, 0, 0.00, 0, 4900.00, 4900, 1, 4900.00, 1, 4900, 1, 4900, 1, 4900.00, 0),
(77, 1, 27, 6, 0, 0.00, 0, 380.00, 380, 1, 380.00, 1, 380, 1, 380, 1, 380.00, 0),
(78, 1, 28, 1, 0, 0.00, 0, 12900.00, 12900, 1, 12900.00, 1, 12900, 1, 12900, 1, 12900.00, 0),
(79, 1, 29, 8, 0, 0.00, 0, 3600.00, 3600, 1, 3600.00, 1, 3600, 1, 3600, 1, 3600.00, 1),
(80, 1, 30, 2, 0, 0.00, 0, 990.00, 990, 1, 990.00, 1, 990, 1, 990, 1, 990.00, 0),
(81, 1, 31, 1, 0, 0.00, 0, 890.00, 890, 1, 890.00, 1, 890, 1, 890, 1, 890.00, 0),
(82, 1, 32, 1, 0, 0.00, 0, 790.00, 790, 1, 790.00, 1, 790, 1, 790, 1, 790.00, 0),
(83, 1, 33, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(84, 1, 34, 1, 0, 0.00, 0, 24200.00, 24200, 1, 24200.00, 1, 24200, 1, 24200, 1, 24200.00, 0),
(85, 1, 35, 8, 0, 0.00, 0, 145.00, 145, 1, 145.00, 1, 145, 1, 145, 1, 145.00, 1),
(86, 1, 36, 8, 0, 0.00, 0, 4200.00, 4200, 1, 4200.00, 1, 4200, 1, 4200, 1, 4200.00, 1),
(87, 1, 37, 1, 0, 0.00, 0, 4800.00, 4800, 1, 4800.00, 1, 4800, 1, 4800, 1, 4800.00, 0),
(88, 1, 38, 8, 0, 0.00, 0, 75.00, 75, 1, 75.00, 1, 75, 1, 75, 1, 75.00, 1),
(89, 1, 39, 1, 0, 2600.00, 150, 6500.00, 6500, 1, 6500.00, 1, 6500, 1, 6500, 1, 6500.00, 0),
(90, 1, 40, 1, 0, 0.00, 0, 1990.00, 1990, 1, 1990.00, 1, 1990, 1, 1990, 1, 1990.00, 0),
(91, 1, 41, 1, 0, 0.00, 0, 620.00, 620, 1, 620.00, 1, 620, 1, 620, 1, 620.00, 0),
(92, 1, 42, 2, 0, 0.00, 0, 890.00, 890, 1, 890.00, 1, 890, 1, 890, 1, 890.00, 0),
(93, 1, 43, 4, 0, 0.00, 0, 240.00, 240, 1, 240.00, 1, 240, 1, 240, 1, 240.00, 1),
(94, 1, 44, 6, 0, 0.00, 0, 295.00, 295, 1, 295.00, 1, 295, 1, 295, 1, 295.00, 1),
(95, 1, 45, 32, 0, 0.00, 0, 240.00, 240, 1, 240.00, 1, 240, 1, 240, 1, 240.00, 1),
(96, 1, 46, 8, 0, 0.00, 0, 210.00, 210, 1, 210.00, 1, 210, 1, 210, 1, 210.00, 1),
(97, 1, 47, 4, 0, 0.00, 0, 380.00, 380, 1, 380.00, 1, 380, 1, 380, 1, 380.00, 0),
(98, 1, 48, 3, 0, 0.00, 0, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 0),
(99, 1, 49, 4, 0, 0.00, 0, 90.00, 90, 1, 90.00, 1, 90, 1, 90, 1, 90.00, 0),
(100, 1, 50, 1, 0, 0.00, 0, 8700.00, 8700, 1, 8700.00, 1, 8700, 1, 8700, 1, 8700.00, 0),
(101, 1, 51, 1, 0, 0.00, 0, 6400.00, 6400, 1, 6400.00, 1, 6400, 1, 6400, 1, 6400.00, 0),
(102, 1, 52, 1, 0, 0.00, 0, 5600.00, 5600, 1, 5600.00, 1, 5600, 1, 5600, 1, 5600.00, 0),
(103, 1, 53, 1, 0, 0.00, 0, 920.00, 920, 1, 920.00, 1, 920, 1, 920, 1, 920.00, 0),
(104, 1, 54, 1, 0, 0.00, 0, 7890.00, 7890, 1, 7890.00, 1, 7890, 1, 7890, 1, 7890.00, 0),
(105, 1, 55, 1, 0, 2600.00, 150, 6500.00, 6500, 1, 6500.00, 1, 6500, 1, 6500, 1, 6500.00, 0),
(106, 1, 56, 8, 0, 0.00, 0, 3950.00, 3950, 1, 3950.00, 1, 3950, 1, 3950, 1, 3950.00, 1),
(107, 1, 57, 8, 0, 0.00, 0, 145.00, 145, 1, 145.00, 1, 145, 1, 145, 1, 145.00, 1),
(108, 1, 58, 2, 0, 0.00, 0, 9400.00, 9400, 1, 9400.00, 1, 9400, 1, 9400, 1, 9400.00, 0),
(109, 1, 59, 1, 0, 0.00, 0, 20900.00, 20900, 1, 20900.00, 1, 20900, 1, 20900, 1, 20900.00, 0),
(110, 1, 60, 1, 0, 0.00, 0, 3600.00, 3600, 1, 3600.00, 1, 3600, 1, 3600, 1, 3600.00, 0),
(111, 1, 61, 1, 0, 0.00, 0, 550.00, 550, 1, 550.00, 1, 550, 1, 550, 1, 550.00, 0),
(112, 1, 62, 1, 0, 0.00, 0, 6800.00, 6800, 1, 6800.00, 1, 6800, 1, 6800, 1, 6800.00, 0),
(113, 1, 63, 1, 0, 0.00, 0, 7200.00, 7200, 1, 7200.00, 1, 7200, 1, 7200, 1, 7200.00, 0),
(114, 1, 64, 1, 0, 0.00, 0, 21000.00, 21000, 1, 21000.00, 1, 21000, 1, 21000, 1, 21000.00, 0),
(115, 1, 65, 1, 0, 0.00, 0, 550.00, 550, 1, 550.00, 1, 550, 1, 550, 1, 550.00, 0),
(116, 1, 66, 2, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(117, 1, 67, 4, 0, 0.00, 0, 340.00, 340, 1, 340.00, 1, 340, 1, 340, 1, 340.00, 0),
(118, 1, 68, 1, 0, 0.00, 0, 4800.00, 4800, 1, 4800.00, 1, 4800, 1, 4800, 1, 4800.00, 0),
(119, 1, 69, 1, 0, 0.00, 0, 690.00, 690, 1, 690.00, 1, 690, 1, 690, 1, 690.00, 0),
(120, 1, 70, 1, 0, 0.00, 0, 12400.00, 12400, 1, 12400.00, 1, 12400, 1, 12400, 1, 12400.00, 0),
(121, 1, 71, 1, 0, 0.00, 0, 4200.00, 4200, 1, 4200.00, 1, 4200, 1, 4200, 1, 4200.00, 0),
(122, 1, 72, 2, 0, 0.00, 0, 4200.00, 4200, 1, 4200.00, 1, 4200, 1, 4200, 1, 4200.00, 0),
(123, 1, 73, 1, 0, 0.00, 0, 4800.00, 4800, 1, 4800.00, 1, 4800, 1, 4800, 1, 4800.00, 0),
(124, 1, 74, 1, 0, 0.00, 0, 18900.00, 18900, 1, 18900.00, 1, 18900, 1, 18900, 1, 18900.00, 0),
(125, 1, 75, 1, 0, 0.00, 0, 2800.00, 2800, 1, 2800.00, 1, 2800, 1, 2800, 1, 2800.00, 0),
(126, 1, 76, 1, 0, 0.00, 0, 22500.00, 22500, 1, 22500.00, 1, 22500, 1, 22500, 1, 22500.00, 0),
(127, 1, 77, 1, 0, 0.00, 0, 790.00, 790, 1, 790.00, 1, 790, 1, 790, 1, 790.00, 0),
(128, 1, 78, 2, 0, 0.00, 0, 1320.00, 1320, 1, 1320.00, 1, 1320, 1, 1320, 1, 1320.00, 0),
(129, 1, 79, 1, 0, 0.00, 0, 1390.00, 1390, 1, 1390.00, 1, 1390, 1, 1390, 1, 1390.00, 0),
(130, 1, 80, 1, 0, 0.00, 0, 22400.00, 22400, 1, 22400.00, 1, 22400, 1, 22400, 1, 22400.00, 0),
(131, 1, 81, 1, 0, 0.00, 0, 1600.00, 1600, 1, 1600.00, 1, 1600, 1, 1600, 1, 1600.00, 0),
(132, 1, 82, 1, 0, 2600.00, 150, 6500.00, 6500, 1, 6500.00, 1, 6500, 1, 6500, 1, 6500.00, 0),
(133, 1, 83, 2, 0, 2600.00, 150, 6500.00, 6500, 1, 6500.00, 1, 6500, 1, 6500, 1, 6500.00, 0),
(134, 1, 84, 1, 0, 2600.00, 150, 6900.00, 6900, 1, 6900.00, 1, 6900, 1, 6900, 1, 6900.00, 0),
(135, 1, 85, 1, 0, 0.00, 0, 6500.00, 6500, 1, 6500.00, 1, 6500, 1, 6500, 1, 6500.00, 0),
(136, 1, 86, 6, 0, 0.00, 0, 120.00, 120, 1, 120.00, 1, 120, 1, 120, 1, 120.00, 0),
(137, 1, 87, 5, 0, 0.00, 0, 110.00, 110, 1, 110.00, 1, 110, 1, 110, 1, 110.00, 0),
(138, 1, 88, 1, 0, 0.00, 0, 690.00, 690, 1, 690.00, 1, 690, 1, 690, 1, 690.00, 0),
(139, 1, 89, -3, 0, 0.00, 0, 1.00, 1, 1, 1.00, 1, 1, 1, 1, 1, 1.00, 0),
(140, 1, 90, 2, 0, 0.00, 0, 7800.00, 7800, 1, 7800.00, 1, 7800, 1, 7800, 1, 7800.00, 0),
(141, 1, 91, 0, 0, 0.00, 0, 990.00, 990, 1, 990.00, 1, 990, 1, 990, 1, 990.00, 0),
(142, 1, 92, 10, 0, 0.00, 0, 495.00, 495, 1, 495.00, 1, 495, 1, 495, 1, 495.00, 1),
(143, 1, 93, 2, 0, 665.00, 0, 1125.00, 1125, 1, 1125.00, 1, 1125, 1, 1125, 1, 1125.00, 0),
(144, 1, 94, 30, 0, 0.00, 0, 38.00, 38, 1, 38.00, 1, 38, 1, 38, 1, 38.00, 1),
(145, 1, 95, 36, 0, 0.00, 0, 10.00, 10, 1, 10.00, 1, 10, 1, 10, 1, 10.00, 1),
(146, 1, 96, 81, 0, 0.00, 0, 14.00, 14, 1, 14.00, 1, 14, 1, 14, 1, 14.00, 1),
(147, 1, 97, 18, 0, 0.00, 0, 14.00, 14, 1, 14.00, 1, 14, 1, 14, 1, 14.00, 1),
(148, 1, 98, 57, 0, 0.00, 0, 18.00, 18, 1, 18.00, 1, 18, 1, 18, 1, 18.00, 1),
(149, 1, 99, 37, 0, 0.00, 0, 57.00, 57, 1, 57.00, 1, 57, 1, 57, 1, 57.00, 1),
(150, 1, 100, 61, 0, 0.00, 0, 57.00, 57, 1, 57.00, 1, 57, 1, 57, 1, 57.00, 1),
(151, 1, 101, 5, 0, 0.00, 0, 40.00, 40, 1, 40.00, 1, 40, 1, 40, 1, 40.00, 1),
(152, 1, 102, 6, 0, 0.00, 0, 40.00, 40, 1, 40.00, 1, 40, 1, 40, 1, 40.00, 1),
(153, 1, 103, 17, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 1),
(154, 1, 104, 11, 0, 0.00, 0, 28.00, 28, 1, 28.00, 1, 28, 1, 28, 1, 28.00, 1),
(155, 1, 105, 3, 0, 0.00, 0, 355.00, 355, 1, 355.00, 1, 355, 1, 355, 1, 355.00, 0),
(156, 1, 106, 4, 0, 0.00, 0, 355.00, 355, 1, 355.00, 1, 355, 1, 355, 1, 355.00, 1),
(157, 1, 107, 8, 0, 0.00, 0, 24.00, 24, 1, 24.00, 1, 24, 1, 24, 1, 24.00, 1),
(158, 1, 108, 9, 0, 0.00, 0, 58.00, 58, 1, 58.00, 1, 58, 1, 58, 1, 58.00, 1),
(159, 1, 109, 7, 0, 0.00, 0, 58.00, 58, 1, 58.00, 1, 58, 1, 58, 1, 58.00, 1),
(160, 1, 110, 4, 0, 0.00, 0, 58.00, 58, 1, 58.00, 1, 58, 1, 58, 1, 58.00, 1),
(161, 1, 111, 7, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 1),
(162, 1, 112, 2, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 0),
(163, 1, 113, -7, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 0),
(164, 1, 114, 3, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 1),
(165, 1, 115, 8, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 1),
(166, 1, 116, 0, 0, 0.00, 0, 55.00, 55, 1, 55.00, 1, 55, 1, 55, 1, 55.00, 0),
(167, 1, 117, 1, 0, 0.00, 0, 58.00, 58, 1, 58.00, 1, 58, 1, 58, 1, 58.00, 0),
(168, 1, 118, 0, 0, 0.00, 0, 65.00, 65, 1, 65.00, 1, 65, 1, 65, 1, 65.00, 0),
(169, 1, 119, -1, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 0),
(170, 1, 120, 11, 0, 0.00, 0, 65.00, 65, 1, 65.00, 1, 65, 1, 65, 1, 65.00, 1),
(171, 1, 121, 1, 0, 0.00, 0, 65.00, 65, 1, 65.00, 1, 65, 1, 65, 1, 65.00, 0),
(172, 1, 122, 1, 0, 0.00, 0, 74.00, 74, 1, 74.00, 1, 74, 1, 74, 1, 74.00, 0),
(173, 1, 123, 1, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 0),
(174, 1, 124, 1, 0, 0.00, 0, 156.00, 156, 1, 156.00, 1, 156, 1, 156, 1, 156.00, 0),
(175, 1, 125, 1, 0, 0.00, 0, 330.00, 330, 1, 330.00, 1, 330, 1, 330, 1, 330.00, 0),
(176, 1, 126, 1, 0, 0.00, 0, 330.00, 330, 1, 330.00, 1, 330, 1, 330, 1, 330.00, 0),
(177, 1, 127, 5, 0, 0.00, 0, 42.00, 42, 1, 42.00, 1, 42, 1, 42, 1, 42.00, 1),
(178, 1, 128, 13, 0, 0.00, 0, 60.00, 60, 1, 60.00, 1, 60, 1, 60, 1, 60.00, 1),
(179, 1, 129, 4, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 1),
(180, 1, 130, 20, 0, 0.00, 0, 189.00, 189, 1, 189.00, 1, 189, 1, 189, 1, 189.00, 1),
(181, 1, 131, 5, 0, 0.00, 0, 115.00, 115, 1, 115.00, 1, 115, 1, 115, 1, 115.00, 0),
(182, 1, 132, 2, 0, 0.00, 0, 112.00, 112, 1, 112.00, 1, 112, 1, 112, 1, 112.00, 0),
(183, 1, 133, 2, 0, 0.00, 0, 85.00, 85, 1, 85.00, 1, 85, 1, 85, 1, 85.00, 0),
(184, 1, 134, 1, 0, 0.00, 0, 58.00, 58, 1, 58.00, 1, 58, 1, 58, 1, 58.00, 0),
(185, 1, 135, 5, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 1),
(186, 1, 136, 2, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 0),
(187, 1, 137, 3, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 0),
(188, 1, 138, -5, 0, 0.00, 0, 35.00, 35, 1, 35.00, 1, 35, 1, 35, 1, 35.00, 0),
(189, 1, 139, 15, 0, 0.00, 0, 35.00, 35, 1, 35.00, 1, 35, 1, 35, 1, 35.00, 1),
(190, 1, 140, 25, 0, 0.00, 0, 30.00, 30, 1, 30.00, 1, 30, 1, 30, 1, 30.00, 1),
(191, 1, 141, 7, 0, 0.00, 0, 27.00, 27, 1, 27.00, 1, 27, 1, 27, 1, 27.00, 1),
(192, 1, 142, 21, 0, 0.00, 0, 42.00, 42, 1, 42.00, 1, 42, 1, 42, 1, 42.00, 1),
(193, 1, 143, 10, 0, 0.00, 0, 25.00, 25, 1, 25.00, 1, 25, 1, 25, 1, 25.00, 1),
(194, 1, 144, 0, 0, 0.00, 0, 55.00, 55, 1, 55.00, 1, 55, 1, 55, 1, 55.00, 0),
(195, 1, 145, 4, 0, 0.00, 0, 172.00, 172, 1, 172.00, 1, 172, 1, 172, 1, 172.00, 0),
(196, 1, 146, 3, 0, 0.00, 0, 370.00, 370, 1, 370.00, 1, 370, 1, 370, 1, 370.00, 0),
(197, 1, 147, 22, 0, 0.00, 0, 120.00, 120, 1, 120.00, 1, 120, 1, 120, 1, 120.00, 1),
(198, 1, 148, 32, 0, 0.00, 0, 120.00, 120, 1, 120.00, 1, 120, 1, 120, 1, 120.00, 1),
(199, 1, 149, 12, 0, 0.00, 0, 30.00, 30, 1, 30.00, 1, 30, 1, 30, 1, 30.00, 1),
(200, 1, 150, 22, 0, 0.00, 0, 23.00, 23, 1, 23.00, 1, 23, 1, 23, 1, 23.00, 1),
(201, 1, 151, 20, 0, 0.00, 0, 54.00, 54, 1, 54.00, 1, 54, 1, 54, 1, 54.00, 1),
(202, 1, 152, 19, 0, 0.00, 0, 30.00, 30, 1, 30.00, 1, 30, 1, 30, 1, 30.00, 1),
(203, 1, 153, 24, 0, 0.00, 0, 30.00, 30, 1, 30.00, 1, 30, 1, 30, 1, 30.00, 1),
(204, 1, 154, 6, 0, 0.00, 0, 308.00, 308, 1, 308.00, 1, 308, 1, 308, 1, 308.00, 1),
(205, 1, 155, 1, 0, 0.00, 0, 88.00, 88, 1, 88.00, 1, 88, 1, 88, 1, 88.00, 0),
(206, 1, 156, 0, 0, 0.00, 0, 143.00, 143, 1, 143.00, 1, 143, 1, 143, 1, 143.00, 0),
(207, 1, 157, 1, 0, 0.00, 0, 390.00, 390, 1, 390.00, 1, 390, 1, 390, 1, 390.00, 0),
(208, 1, 158, 5, 0, 0.00, 0, 98.00, 98, 1, 98.00, 1, 98, 1, 98, 1, 98.00, 1),
(209, 1, 159, 7, 0, 0.00, 0, 95.00, 95, 1, 95.00, 1, 95, 1, 95, 1, 95.00, 1),
(210, 1, 160, 0, 0, 0.00, 0, 95.00, 95, 1, 95.00, 1, 95, 1, 95, 1, 95.00, 0),
(211, 1, 161, 2, 0, 0.00, 0, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 0),
(212, 1, 162, 0, 0, 0.00, 0, 180.00, 180, 1, 180.00, 1, 180, 1, 180, 1, 180.00, 0),
(213, 1, 163, 0, 0, 0.00, 0, 32.00, 32, 1, 32.00, 1, 32, 1, 32, 1, 32.00, 0),
(214, 1, 164, 3, 0, 0.00, 0, 24.00, 24, 1, 24.00, 1, 24, 1, 24, 1, 24.00, 0),
(215, 1, 165, 3, 0, 0.00, 0, 35.00, 35, 1, 35.00, 1, 35, 1, 35, 1, 35.00, 0),
(216, 1, 166, 2, 0, 0.00, 0, 22.00, 22, 1, 22.00, 1, 22, 1, 22, 1, 22.00, 0),
(217, 1, 167, 6, 0, 0.00, 0, 28.00, 28, 1, 28.00, 1, 28, 1, 28, 1, 28.00, 1),
(218, 1, 168, 12, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 1),
(219, 1, 169, 6, 0, 0.00, 0, 90.00, 90, 1, 90.00, 1, 90, 1, 90, 1, 90.00, 1),
(220, 1, 170, 1, 0, 0.00, 0, 25.00, 25, 1, 25.00, 1, 25, 1, 25, 1, 25.00, 0),
(221, 1, 171, 2, 0, 0.00, 0, 16.00, 16, 1, 16.00, 1, 16, 1, 16, 1, 16.00, 0),
(222, 1, 172, 5, 0, 0.00, 0, 18.00, 18, 1, 18.00, 1, 18, 1, 18, 1, 18.00, 1),
(223, 1, 173, 0, 0, 0.00, 0, 745.00, 745, 1, 745.00, 1, 745, 1, 745, 1, 745.00, 0),
(224, 1, 174, 0, 0, 0.00, 0, 1920.00, 1920, 1, 1920.00, 1, 1920, 1, 1920, 1, 1920.00, 0),
(225, 1, 175, 1, 0, 0.00, 0, 745.00, 745, 1, 745.00, 1, 745, 1, 745, 1, 745.00, 0),
(226, 1, 176, 0, 0, 0.00, 0, 1390.00, 1390, 1, 1390.00, 1, 1390, 1, 1390, 1, 1390.00, 0),
(227, 1, 177, 0, 0, 0.00, 0, 545.00, 545, 1, 545.00, 1, 545, 1, 545, 1, 545.00, 0),
(228, 1, 178, 1, 0, 0.00, 0, 1390.00, 1390, 1, 1390.00, 1, 1390, 1, 1390, 1, 1390.00, 0),
(229, 1, 179, 1, 0, 0.00, 0, 720.00, 720, 1, 720.00, 1, 720, 1, 720, 1, 720.00, 0),
(230, 1, 180, 0, 0, 0.00, 0, 845.00, 845, 1, 845.00, 1, 845, 1, 845, 1, 845.00, 0),
(231, 1, 181, 1, 0, 0.00, 0, 995.00, 995, 1, 995.00, 1, 995, 1, 995, 1, 995.00, 0),
(232, 1, 182, 1, 0, 0.00, 0, 2250.00, 2250, 1, 2250.00, 1, 2250, 1, 2250, 1, 2250.00, 0),
(233, 1, 183, 0, 0, 0.00, 0, 345.00, 345, 1, 345.00, 1, 345, 1, 345, 1, 345.00, 0),
(234, 1, 184, 0, 0, 0.00, 0, 345.00, 345, 1, 345.00, 1, 345, 1, 345, 1, 345.00, 0),
(235, 1, 185, 1, 0, 0.00, 0, 345.00, 345, 1, 345.00, 1, 345, 1, 345, 1, 345.00, 0),
(236, 1, 186, 1, 0, 0.00, 0, 1895.00, 1895, 1, 1895.00, 1, 1895, 1, 1895, 1, 1895.00, 0),
(237, 1, 187, 1, 0, 0.00, 0, 1490.00, 1490, 1, 1490.00, 1, 1490, 1, 1490, 1, 1490.00, 0),
(238, 1, 188, 1, 0, 0.00, 0, 795.00, 795, 1, 795.00, 1, 795, 1, 795, 1, 795.00, 0),
(239, 1, 189, 1, 0, 0.00, 0, 790.00, 790, 1, 790.00, 1, 790, 1, 790, 1, 790.00, 0),
(240, 1, 190, 1, 0, 0.00, 0, 595.00, 595, 1, 595.00, 1, 595, 1, 595, 1, 595.00, 0),
(241, 1, 191, 2, 0, 0.00, 0, 198.00, 198, 1, 198.00, 1, 198, 1, 198, 1, 198.00, 0),
(242, 1, 192, 0, 0, 0.00, 0, 2950.00, 2950, 1, 2950.00, 1, 2950, 1, 2950, 1, 2950.00, 0),
(243, 1, 193, 6, 0, 0.00, 0, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 1),
(244, 1, 194, 18, 0, 0.00, 0, 28.00, 28, 1, 28.00, 1, 28, 1, 28, 1, 28.00, 1),
(245, 1, 195, 21, 0, 0.00, 0, 28.00, 28, 1, 28.00, 1, 28, 1, 28, 1, 28.00, 1),
(246, 1, 196, 5, 0, 0.00, 0, 225.00, 225, 1, 225.00, 1, 225, 1, 225, 1, 225.00, 1),
(247, 1, 197, 4, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 1),
(248, 1, 198, 1, 0, 0.00, 0, 210.00, 210, 1, 210.00, 1, 210, 1, 210, 1, 210.00, 0),
(249, 1, 199, 5, 0, 0.00, 0, 210.00, 210, 1, 210.00, 1, 210, 1, 210, 1, 210.00, 1),
(250, 1, 200, 7, 0, 0.00, 0, 160.00, 160, 1, 160.00, 1, 160, 1, 160, 1, 160.00, 1),
(251, 1, 201, 7, 0, 0.00, 0, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 1),
(252, 1, 202, 14, 0, 0.00, 0, 115.00, 115, 1, 115.00, 1, 115, 1, 115, 1, 115.00, 1),
(253, 1, 203, 10, 0, 0.00, 0, 105.00, 105, 1, 105.00, 1, 105, 1, 105, 1, 105.00, 1),
(254, 1, 204, 8, 0, 0.00, 0, 105.00, 105, 1, 105.00, 1, 105, 1, 105, 1, 105.00, 1),
(255, 1, 205, 1, 0, 0.00, 0, 85.00, 85, 1, 85.00, 1, 85, 1, 85, 1, 85.00, 0),
(256, 1, 206, 6, 0, 0.00, 0, 75.00, 75, 1, 75.00, 1, 75, 1, 75, 1, 75.00, 1),
(257, 1, 207, 2, 0, 0.00, 0, 90.00, 90, 1, 90.00, 1, 90, 1, 90, 1, 90.00, 0),
(258, 1, 208, 3, 0, 0.00, 0, 310.00, 310, 1, 310.00, 1, 310, 1, 310, 1, 310.00, 0),
(259, 1, 209, 1, 0, 0.00, 0, 220.00, 220, 1, 220.00, 1, 220, 1, 220, 1, 220.00, 0),
(260, 1, 210, 0, 0, 0.00, 0, 220.00, 220, 1, 220.00, 1, 220, 1, 220, 1, 220.00, 0),
(261, 1, 211, 2, 0, 0.00, 0, 0.00, 0, 1, 0.00, 1, 0, 1, 0, 1, 0.00, 0),
(262, 1, 212, 1, 0, 0.00, 0, 0.00, 0, 1, 0.00, 1, 0, 1, 0, 1, 0.00, 0),
(263, 1, 213, 3, 0, 0.00, 0, 0.00, 0, 1, 0.00, 1, 0, 1, 0, 1, 0.00, 0),
(264, 1, 214, 15, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 1),
(265, 1, 215, 0, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 0),
(266, 1, 216, 12, 0, 0.00, 0, 66.00, 66, 1, 66.00, 1, 66, 1, 66, 1, 66.00, 1),
(267, 1, 217, 10, 0, 0.00, 0, 90.00, 90, 1, 90.00, 1, 90, 1, 90, 1, 90.00, 1),
(268, 1, 218, 10, 0, 0.00, 0, 56.00, 56, 1, 56.00, 1, 56, 1, 56, 1, 56.00, 1),
(269, 1, 219, 5, 0, 0.00, 0, 80.00, 80, 1, 80.00, 1, 80, 1, 80, 1, 80.00, 1),
(270, 1, 220, 19, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 1),
(271, 1, 221, 19, 0, 0.00, 0, 36.00, 36, 1, 36.00, 1, 36, 1, 36, 1, 36.00, 1),
(272, 1, 222, 1, 0, 0.00, 0, 16.00, 16, 1, 16.00, 1, 16, 1, 16, 1, 16.00, 0),
(273, 1, 223, 0, 0, 0.00, 0, 22.00, 22, 1, 22.00, 1, 22, 1, 22, 1, 22.00, 0),
(274, 1, 224, 2, 0, 0.00, 0, 22.00, 22, 1, 22.00, 1, 22, 1, 22, 1, 22.00, 0),
(275, 1, 225, 7, 0, 0.00, 0, 22.00, 22, 1, 22.00, 1, 22, 1, 22, 1, 22.00, 1),
(276, 1, 226, 8, 0, 0.00, 0, 22.00, 22, 1, 22.00, 1, 22, 1, 22, 1, 22.00, 1),
(277, 1, 227, 8, 0, 0.00, 0, 3490.00, 3490, 1, 3490.00, 1, 3490, 1, 3490, 1, 3490.00, 1),
(278, 1, 228, 1, 0, 0.00, 0, 19600.00, 19600, 1, 19600.00, 1, 19600, 1, 19600, 1, 19600.00, 0),
(279, 1, 229, 1, 0, 0.00, 0, 9900.00, 9900, 1, 9900.00, 1, 9900, 1, 9900, 1, 9900.00, 0),
(280, 1, 230, 1, 0, 0.00, 0, 1950.00, 1950, 1, 1950.00, 1, 1950, 1, 1950, 1, 1950.00, 0),
(281, 1, 231, 1, 0, 0.00, 0, 6200.00, 6200, 1, 6200.00, 1, 6200, 1, 6200, 1, 6200.00, 0),
(282, 1, 232, 2, 0, 0.00, 0, 5850.00, 5850, 1, 5850.00, 1, 5850, 1, 5850, 1, 5850.00, 0),
(283, 1, 233, 2, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(284, 1, 234, 1, 0, 0.00, 0, 2300.00, 2300, 1, 2300.00, 1, 2300, 1, 2300, 1, 2300.00, 0),
(285, 1, 235, 4, 0, 0.00, 0, 2300.00, 2300, 1, 2300.00, 1, 2300, 1, 2300, 1, 2300.00, 1),
(286, 1, 236, 1, 0, 0.00, 0, 790.00, 790, 1, 790.00, 1, 790, 1, 790, 1, 790.00, 0),
(287, 1, 237, 1, 0, 0.00, 0, 1700.00, 1700, 1, 1700.00, 1, 1700, 1, 1700, 1, 1700.00, 0),
(288, 1, 238, 0, 0, 0.00, 0, 14200.00, 14200, 1, 14200.00, 1, 14200, 1, 14200, 1, 14200.00, 0),
(289, 1, 239, 1, 0, 0.00, 0, 14200.00, 14200, 1, 14200.00, 1, 14200, 1, 14200, 1, 14200.00, 0),
(290, 1, 240, 1, 0, 0.00, 0, 23500.00, 23500, 1, 23500.00, 1, 23500, 1, 23500, 1, 23500.00, 0),
(291, 1, 241, 1, 0, 0.00, 0, 3250.00, 3250, 1, 3250.00, 1, 3250, 1, 3250, 1, 3250.00, 0),
(292, 1, 242, 2, 0, 0.00, 0, 3200.00, 3200, 1, 3200.00, 1, 3200, 1, 3200, 1, 3200.00, 0),
(293, 1, 243, 1, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(294, 1, 244, 1, 0, 0.00, 0, 1800.00, 1800, 1, 1800.00, 1, 1800, 1, 1800, 1, 1800.00, 0),
(295, 1, 245, 1, 0, 0.00, 0, 8500.00, 8500, 1, 8500.00, 1, 8500, 1, 8500, 1, 8500.00, 0),
(296, 1, 246, 1, 0, 0.00, 0, 4950.00, 4950, 1, 4950.00, 1, 4950, 1, 4950, 1, 4950.00, 0),
(297, 1, 247, 1, 0, 0.00, 0, 5200.00, 5200, 1, 5200.00, 1, 5200, 1, 5200, 1, 5200.00, 0),
(298, 1, 248, 1, 0, 0.00, 0, 6200.00, 6200, 1, 6200.00, 1, 6200, 1, 6200, 1, 6200.00, 0),
(299, 1, 249, 1, 0, 0.00, 0, 32400.00, 32400, 1, 32400.00, 1, 32400, 1, 32400, 1, 32400.00, 0),
(300, 1, 250, 1, 0, 0.00, 0, 14900.00, 14900, 1, 14900.00, 1, 14900, 1, 14900, 1, 14900.00, 0),
(301, 1, 251, 1, 0, 0.00, 0, 32900.00, 32900, 1, 32900.00, 1, 32900, 1, 32900, 1, 32900.00, 0),
(302, 1, 252, 2, 0, 0.00, 0, 6800.00, 6800, 1, 6800.00, 1, 6800, 1, 6800, 1, 6800.00, 0),
(303, 1, 253, 0, 0, 0.00, 0, 6900.00, 6900, 1, 6900.00, 1, 6900, 1, 6900, 1, 6900.00, 0),
(304, 1, 254, 1, 0, 0.00, 0, 1950.00, 1950, 1, 1950.00, 1, 1950, 1, 1950, 1, 1950.00, 0),
(305, 1, 255, 1, 0, 0.00, 0, 1950.00, 1950, 1, 1950.00, 1, 1950, 1, 1950, 1, 1950.00, 0),
(306, 1, 256, 1, 0, 0.00, 0, 2800.00, 2800, 1, 2800.00, 1, 2800, 1, 2800, 1, 2800.00, 0),
(307, 1, 257, 1, 0, 0.00, 0, 5200.00, 5200, 1, 5200.00, 1, 5200, 1, 5200, 1, 5200.00, 0),
(308, 1, 258, 6, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 1),
(309, 1, 259, 1, 0, 0.00, 0, 2400.00, 2400, 1, 2400.00, 1, 2400, 1, 2400, 1, 2400.00, 0),
(310, 1, 260, 10, 0, 0.00, 0, 3500.00, 3500, 1, 3500.00, 1, 3500, 1, 3500, 1, 3500.00, 1),
(311, 1, 261, 10, 0, 0.00, 0, 4500.00, 4500, 1, 4500.00, 1, 4500, 1, 4500, 1, 4500.00, 1),
(312, 1, 262, 0, 0, 0.00, 0, 3200.00, 3200, 1, 3200.00, 1, 3200, 1, 3200, 1, 3200.00, 0),
(313, 1, 263, 4, 0, 0.00, 0, 690.00, 690, 1, 690.00, 1, 690, 1, 690, 1, 690.00, 1),
(314, 1, 264, 2, 0, 0.00, 0, 2500.00, 2500, 1, 2500.00, 1, 2500, 1, 2500, 1, 2500.00, 0),
(315, 1, 265, 7, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 1),
(316, 1, 266, 0, 0, 0.00, 0, 29500.00, 29500, 1, 29500.00, 1, 29500, 1, 29500, 1, 29500.00, 0),
(317, 1, 267, 0, 0, 0.00, 0, 9800.00, 9800, 1, 9800.00, 1, 9800, 1, 9800, 1, 9800.00, 0),
(318, 1, 268, 2, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(319, 1, 269, 2, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(320, 1, 270, 1, 0, 0.00, 0, 5400.00, 5400, 1, 5400.00, 1, 5400, 1, 5400, 1, 5400.00, 0),
(321, 1, 271, 1, 0, 0.00, 0, 850.00, 850, 1, 850.00, 1, 850, 1, 850, 1, 850.00, 0),
(322, 1, 272, 1, 0, 0.00, 0, 2800.00, 2800, 1, 2800.00, 1, 2800, 1, 2800, 1, 2800.00, 0),
(323, 1, 273, 1, 0, 0.00, 0, 1600.00, 1600, 1, 1600.00, 1, 1600, 1, 1600, 1, 1600.00, 0),
(324, 1, 274, 0, 0, 0.00, 0, 540.00, 540, 1, 540.00, 1, 540, 1, 540, 1, 540.00, 0),
(325, 1, 275, 0, 0, 0.00, 0, 990.00, 990, 1, 990.00, 1, 990, 1, 990, 1, 990.00, 0),
(326, 1, 276, 3, 0, 80.00, 100, 160.00, 160, 1, 160.00, 1, 160, 1, 160, 1, 160.00, 0),
(327, 1, 277, 3, 0, 100.00, 95, 195.00, 195, 1, 195.00, 1, 195, 1, 195, 1, 195.00, 0),
(328, 1, 278, 3, 0, 140.00, 100, 280.00, 280, 1, 280.00, 1, 280, 1, 280, 1, 280.00, 0),
(329, 1, 279, 2, 0, 70.00, 100, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 0),
(330, 1, 280, 3, 0, 80.00, 100, 160.00, 160, 1, 160.00, 1, 160, 1, 160, 1, 160.00, 0),
(331, 1, 281, 0, 0, 30.00, 100, 60.00, 60, 1, 60.00, 1, 60, 1, 60, 1, 60.00, 0),
(332, 1, 282, 3, 0, 40.00, 100, 80.00, 80, 1, 80.00, 1, 80, 1, 80, 1, 80.00, 0),
(333, 1, 283, 0, 0, 140.00, 111, 295.00, 295, 1, 295.00, 1, 295, 1, 295, 1, 295.00, 0),
(334, 1, 284, 0, 0, 160.00, 125, 360.00, 360, 1, 360.00, 1, 360, 1, 360, 1, 360.00, 0),
(335, 1, 285, 0, 0, 180.00, 144, 440.00, 440, 1, 440.00, 1, 440, 1, 440, 1, 440.00, 0),
(336, 1, 286, 3, 0, 90.00, 111, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(337, 1, 287, 1, 0, 70.00, 100, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 0),
(338, 1, 288, 2, 0, 250.00, 96, 490.00, 490, 1, 490.00, 1, 490, 1, 490, 1, 490.00, 0),
(339, 1, 289, 1, 0, 70.00, 100, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 0),
(340, 1, 290, 0, 0, 90.00, 100, 180.00, 180, 1, 180.00, 1, 180, 1, 180, 1, 180.00, 0),
(341, 1, 291, 2, 0, 250.00, 96, 490.00, 490, 1, 490.00, 1, 490, 1, 490, 1, 490.00, 0),
(342, 1, 292, 2, 0, 300.00, 97, 590.01, 590.01, 1, 590.01, 1, 590.01, 1, 590.01, 1, 590.01, 0),
(343, 1, 293, 1, 0, 250.00, 56, 390.00, 390, 1, 390.00, 1, 390, 1, 390, 1, 390.00, 0),
(344, 1, 294, 3, 0, 140.00, 129, 320.00, 320, 1, 320.00, 1, 320, 1, 320, 1, 320.00, 0),
(345, 1, 295, 3, 0, 160.00, 100, 320.00, 320, 1, 320.00, 1, 320, 1, 320, 1, 320.00, 0),
(346, 1, 296, 1, 0, 0.00, 0, 320.00, 320, 1, 320.00, 1, 320, 1, 320, 1, 320.00, 0),
(347, 1, 297, 2, 0, 0.00, 0, 410.00, 410, 1, 410.00, 1, 410, 1, 410, 1, 410.00, 0),
(348, 1, 298, 1, 0, 0.00, 0, 175.00, 175, 1, 175.00, 1, 175, 1, 175, 1, 175.00, 0),
(349, 1, 299, 1, 0, 0.00, 0, 2800.00, 2800, 1, 2800.00, 1, 2800, 1, 2800, 1, 2800.00, 0),
(350, 1, 300, 2, 0, 0.00, 0, 2490.00, 2490, 1, 2490.00, 1, 2490, 1, 2490, 1, 2490.00, 0),
(351, 1, 301, 2, 0, 0.00, 0, 560.00, 560, 1, 560.00, 1, 560, 1, 560, 1, 560.00, 0),
(352, 1, 302, 0, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 0),
(353, 1, 303, 1, 0, 0.00, 0, 2600.00, 2600, 1, 2600.00, 1, 2600, 1, 2600, 1, 2600.00, 0),
(354, 1, 304, 1, 0, 0.00, 0, 1200.00, 1200, 1, 1200.00, 1, 1200, 1, 1200, 1, 1200.00, 0),
(355, 1, 305, 2, 0, 0.00, 0, 690.00, 690, 1, 690.00, 1, 690, 1, 690, 1, 690.00, 0),
(356, 1, 306, 1, 0, 0.00, 0, 1850.00, 1850, 1, 1850.00, 1, 1850, 1, 1850, 1, 1850.00, 0),
(357, 1, 307, 1, 0, 0.00, 0, 2200.00, 2200, 1, 2200.00, 1, 2200, 1, 2200, 1, 2200.00, 0),
(358, 1, 308, 0, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(359, 1, 309, 1, 0, 0.00, 0, 220.00, 220, 1, 220.00, 1, 220, 1, 220, 1, 220.00, 0),
(360, 1, 310, 0, 0, 0.00, 0, 140.00, 140, 1, 140.00, 1, 140, 1, 140, 1, 140.00, 0),
(361, 1, 311, 0, 0, 0.00, 0, 110.00, 110, 1, 110.00, 1, 110, 1, 110, 1, 110.00, 0),
(362, 1, 312, 1, 0, 0.00, 0, 1390.00, 1390, 1, 1390.00, 1, 1390, 1, 1390, 1, 1390.00, 0),
(363, 1, 313, 1, 0, 0.00, 0, 1590.00, 1590, 1, 1590.00, 1, 1590, 1, 1590, 1, 1590.00, 0),
(364, 1, 314, 10, 0, 0.00, 0, 189.00, 189, 1, 189.00, 1, 189, 1, 189, 1, 189.00, 1),
(365, 1, 315, 55, 0, 6.00, 0, 12.00, 12, 1, 12.00, 1, 12, 1, 12, 1, 12.00, 1),
(366, 1, 316, 65, 0, 11.50, 0, 20.00, 20, 1, 20.00, 1, 20, 1, 20, 1, 20.00, 1),
(367, 1, 317, 18, 0, 0.00, 0, 26.00, 26, 1, 26.00, 1, 26, 1, 26, 1, 26.00, 1),
(368, 1, 318, 6, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 0),
(369, 1, 319, 14, 0, 0.00, 0, 90.00, 90, 1, 90.00, 1, 90, 1, 90, 1, 90.00, 1),
(370, 1, 320, 11, 0, 0.00, 0, 145.00, 145, 1, 145.00, 1, 145, 1, 145, 1, 145.00, 1),
(371, 1, 321, 3, 0, 0.00, 0, 195.00, 195, 1, 195.00, 1, 195, 1, 195, 1, 195.00, 0),
(372, 1, 322, 3, 0, 0.00, 0, 890.00, 890, 1, 890.00, 1, 890, 1, 890, 1, 890.00, 0),
(373, 1, 323, 1, 0, 0.00, 0, 990.00, 990, 1, 990.00, 1, 990, 1, 990, 1, 990.00, 0),
(374, 1, 324, 1, 0, 0.00, 0, 32900.00, 32900, 1, 32900.00, 1, 32900, 1, 32900, 1, 32900.00, 0),
(375, 1, 325, 1, 0, 0.00, 0, 14900.00, 14900, 1, 14900.00, 1, 14900, 1, 14900, 1, 14900.00, 0),
(376, 1, 326, -1, 0, 0.00, 0, 1000.00, 1000, 1, 1000.00, 1, 1000, 1, 1000, 1, 1000.00, 0),
(377, 1, 327, -1, 0, 0.00, 0, 500.00, 500, 1, 500.00, 1, 500, 1, 500, 1, 500.00, 0),
(378, 1, 328, 1, 0, 0.00, 0, 3600.00, 3600, 1, 3600.00, 1, 3600, 1, 3600, 1, 3600.00, 0),
(379, 1, 329, 2, 0, 0.00, 0, 2400.00, 2400, 1, 2400.00, 1, 2400, 1, 2400, 1, 2400.00, 0),
(380, 1, 330, 1, 0, 0.00, 0, 1800.00, 1800, 1, 1800.00, 1, 1800, 1, 1800, 1, 1800.00, 0),
(381, 1, 331, 1, 0, 0.00, 0, 2900.00, 2900, 1, 2900.00, 1, 2900, 1, 2900, 1, 2900.00, 0),
(382, 1, 332, 1, 0, 0.00, 0, 2300.00, 2300, 1, 2300.00, 1, 2300, 1, 2300, 1, 2300.00, 0),
(383, 1, 333, 1, 0, 0.00, 0, 2500.00, 2500, 1, 2500.00, 1, 2500, 1, 2500, 1, 2500.00, 0),
(384, 1, 334, 1, 0, 0.00, 0, 2700.00, 2700, 1, 2700.00, 1, 2700, 1, 2700, 1, 2700.00, 0),
(385, 1, 335, 1, 0, 0.00, 0, 2900.00, 2900, 1, 2900.00, 1, 2900, 1, 2900, 1, 2900.00, 0),
(386, 1, 336, 51, 0, 0.00, 0, 28.00, 28, 1, 28.00, 1, 28, 1, 28, 1, 28.00, 1),
(387, 1, 337, 2, 0, 0.00, 0, 1200.00, 1200, 1, 1200.00, 1, 1200, 1, 1200, 1, 1200.00, 0),
(388, 1, 338, 2, 0, 0.00, 0, 1200.00, 1200, 1, 1200.00, 1, 1200, 1, 1200, 1, 1200.00, 0),
(389, 1, 339, 2, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(390, 1, 340, 2, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(391, 1, 341, 1, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 0),
(392, 1, 342, 1, 0, 0.00, 0, 1250.00, 1250, 1, 1250.00, 1, 1250, 1, 1250, 1, 1250.00, 0),
(393, 1, 343, 1, 0, 0.00, 0, 4900.00, 4900, 1, 4900.00, 1, 4900, 1, 4900, 1, 4900.00, 0),
(394, 1, 344, 1, 0, 0.00, 0, 6400.00, 6400, 1, 6400.00, 1, 6400, 1, 6400, 1, 6400.00, 0),
(395, 1, 345, 10, 0, 0.00, 0, 102.00, 102, 1, 102.00, 1, 102, 1, 102, 1, 102.00, 1),
(396, 1, 346, 6, 0, 0.00, 0, 142.00, 142, 1, 142.00, 1, 142, 1, 142, 1, 142.00, 1),
(397, 1, 347, 3, 0, 0.00, 0, 42.00, 42, 1, 42.00, 1, 42, 1, 42, 1, 42.00, 0),
(398, 1, 348, 3, 0, 0.00, 0, 64.00, 64, 1, 64.00, 1, 64, 1, 64, 1, 64.00, 0),
(399, 1, 349, 2, 0, 0.00, 0, 72.00, 72, 1, 72.00, 1, 72, 1, 72, 1, 72.00, 0),
(400, 1, 350, 2, 0, 0.00, 0, 112.00, 112, 1, 112.00, 1, 112, 1, 112, 1, 112.00, 0),
(401, 1, 351, 1, 0, 0.00, 0, 142.00, 142, 1, 142.00, 1, 142, 1, 142, 1, 142.00, 0),
(402, 1, 352, 2, 0, 0.00, 0, 164.00, 164, 1, 164.00, 1, 164, 1, 164, 1, 164.00, 0),
(403, 1, 353, 2, 0, 0.00, 0, 128.00, 128, 1, 128.00, 1, 128, 1, 128, 1, 128.00, 0),
(404, 1, 354, 1, 0, 0.00, 0, 730.00, 730, 1, 730.00, 1, 730, 1, 730, 1, 730.00, 0),
(405, 1, 355, 1, 0, 0.00, 0, 750.00, 750, 1, 750.00, 1, 750, 1, 750, 1, 750.00, 0),
(406, 1, 356, 1, 0, 0.00, 0, 990.00, 990, 1, 990.00, 1, 990, 1, 990, 1, 990.00, 0),
(407, 1, 357, 1, 0, 0.00, 0, 1390.00, 1390, 1, 1390.00, 1, 1390, 1, 1390, 1, 1390.00, 0),
(408, 1, 358, 10, 0, 0.00, 0, 500.00, 500, 1, 500.00, 1, 500, 1, 500, 1, 500.00, 1),
(409, 1, 359, 10, 0, 0.00, 0, 600.00, 600, 1, 600.00, 1, 600, 1, 600, 1, 600.00, 1),
(410, 1, 360, 10, 0, 0.00, 0, 700.00, 700, 1, 700.00, 1, 700, 1, 700, 1, 700.00, 1),
(411, 1, 361, 10, 0, 0.00, 0, 800.00, 800, 1, 800.00, 1, 800, 1, 800, 1, 800.00, 1),
(412, 1, 362, 10, 0, 0.00, 0, 400.00, 400, 1, 400.00, 1, 400, 1, 400, 1, 400.00, 1),
(413, 1, 363, 10, 0, 0.00, 0, 900.00, 900, 1, 900.00, 1, 900, 1, 900, 1, 900.00, 1),
(414, 1, 364, 10, 0, 0.00, 0, 1000.00, 1000, 1, 1000.00, 1, 1000, 1, 1000, 1, 1000.00, 1),
(415, 1, 365, 10, 0, 0.00, 0, 1200.00, 1200, 1, 1200.00, 1, 1200, 1, 1200, 1, 1200.00, 1),
(416, 1, 366, 20, 0, 0.00, 0, 42.00, 42, 1, 42.00, 1, 42, 1, 42, 1, 42.00, 1),
(417, 1, 367, 20, 0, 0.00, 0, 54.00, 54, 1, 54.00, 1, 54, 1, 54, 1, 54.00, 1),
(418, 1, 368, 12, 0, 0.00, 0, 690.00, 690, 1, 690.00, 1, 690, 1, 690, 1, 690.00, 1),
(419, 1, 369, 2, 0, 0.00, 0, 750.00, 750, 1, 750.00, 1, 750, 1, 750, 1, 750.00, 0),
(420, 1, 370, 2, 0, 0.00, 0, 840.00, 840, 1, 840.00, 1, 840, 1, 840, 1, 840.00, 0),
(421, 1, 371, 0, 0, 0.00, 0, 890.00, 890, 1, 890.00, 1, 890, 1, 890, 1, 890.00, 0),
(422, 1, 372, 0, 0, 0.00, 0, 995.00, 995, 1, 995.00, 1, 995, 1, 995, 1, 995.00, 0),
(423, 1, 373, 0, 0, 0.00, 0, 5890.00, 5890, 1, 5890.00, 1, 5890, 1, 5890, 1, 5890.00, 0),
(424, 1, 374, 1, 0, 32.93, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(425, 1, 375, 0, 0, 0.00, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(426, 1, 376, 1, 0, 0.00, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(427, 1, 377, 2, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(428, 1, 378, 1, 0, 0.00, 0, 238.00, 238, 1, 238.00, 1, 238, 1, 238, 1, 238.00, 0),
(429, 1, 379, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(430, 1, 380, 1, 0, 0.00, 0, 158.00, 158, 1, 158.00, 1, 158, 1, 158, 1, 158.00, 0),
(431, 1, 381, 1, 0, 0.00, 0, 78.00, 78, 1, 78.00, 1, 78, 1, 78, 1, 78.00, 0),
(432, 1, 382, 0, 0, 0.00, 0, 119.00, 119, 1, 119.00, 1, 119, 1, 119, 1, 119.00, 0),
(433, 1, 383, 0, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(434, 1, 384, 0, 0, 0.00, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(435, 1, 385, 1, 0, 0.00, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(436, 1, 386, 0, 0, 0.00, 0, 249.00, 249, 1, 249.00, 1, 249, 1, 249, 1, 249.00, 0),
(437, 1, 387, 1, 0, 0.00, 0, 115.00, 115, 1, 115.00, 1, 115, 1, 115, 1, 115.00, 0),
(438, 1, 388, 1, 0, 0.00, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(439, 1, 389, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(440, 1, 390, 0, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(441, 1, 391, 1, 0, 0.00, 0, 145.00, 145, 1, 145.00, 1, 145, 1, 145, 1, 145.00, 0),
(442, 1, 392, 2, 0, 0.00, 0, 12.00, 12, 1, 12.00, 1, 12, 1, 12, 1, 12.00, 0),
(443, 1, 393, 2, 0, 0.00, 0, 12.00, 12, 1, 12.00, 1, 12, 1, 12, 1, 12.00, 0),
(444, 1, 394, 2, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 0),
(445, 1, 395, 0, 0, 0.00, 0, 95.00, 95, 1, 95.00, 1, 95, 1, 95, 1, 95.00, 0),
(446, 1, 396, 1, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(447, 1, 397, 1, 0, 0.00, 0, 135.00, 135, 1, 135.00, 1, 135, 1, 135, 1, 135.00, 0),
(448, 1, 398, 1, 0, 0.00, 0, 225.00, 225, 1, 225.00, 1, 225, 1, 225, 1, 225.00, 0),
(449, 1, 399, 0, 0, 0.00, 0, 58.00, 58, 1, 58.00, 1, 58, 1, 58, 1, 58.00, 0),
(450, 1, 400, 1, 0, 0.00, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(451, 1, 401, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(452, 1, 402, 1, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 0),
(453, 1, 403, 1, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 0),
(454, 1, 404, 1, 0, 0.00, 0, 115.00, 115, 1, 115.00, 1, 115, 1, 115, 1, 115.00, 0),
(455, 1, 405, 1, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 0),
(456, 1, 406, 1, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(457, 1, 407, 0, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 0),
(458, 1, 408, 0, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 0),
(459, 1, 409, 1, 0, 0.00, 0, 225.00, 225, 1, 225.00, 1, 225, 1, 225, 1, 225.00, 0),
(460, 1, 410, 1, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(461, 1, 411, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(462, 1, 412, 2, 0, 0.00, 0, 235.00, 235, 1, 235.00, 1, 235, 1, 235, 1, 235.00, 0),
(463, 1, 413, 1, 0, 0.00, 0, 68.00, 68, 1, 68.00, 1, 68, 1, 68, 1, 68.00, 0),
(464, 1, 414, 0, 0, 0.00, 0, 169.00, 169, 1, 169.00, 1, 169, 1, 169, 1, 169.00, 0),
(465, 1, 415, 1, 0, 0.00, 0, 78.00, 78, 1, 78.00, 1, 78, 1, 78, 1, 78.00, 0),
(466, 1, 416, 2, 0, 0.00, 0, 12.00, 12, 1, 12.00, 1, 12, 1, 12, 1, 12.00, 0),
(467, 1, 417, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(468, 1, 418, 1, 0, 0.00, 0, 119.00, 119, 1, 119.00, 1, 119, 1, 119, 1, 119.00, 0),
(469, 1, 419, 1, 0, 0.00, 0, 145.00, 145, 1, 145.00, 1, 145, 1, 145, 1, 145.00, 0),
(470, 1, 420, 0, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(471, 1, 421, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(472, 1, 422, 0, 0, 0.00, 0, 165.00, 165, 1, 165.00, 1, 165, 1, 165, 1, 165.00, 0),
(473, 1, 423, 1, 0, 0.00, 0, 225.00, 225, 1, 225.00, 1, 225, 1, 225, 1, 225.00, 0),
(474, 1, 424, 1, 0, 0.00, 0, 225.00, 225, 1, 225.00, 1, 225, 1, 225, 1, 225.00, 0),
(475, 1, 425, 0, 0, 0.00, 0, 165.00, 165, 1, 165.00, 1, 165, 1, 165, 1, 165.00, 0),
(476, 1, 426, 1, 0, 0.00, 0, 265.00, 265, 1, 265.00, 1, 265, 1, 265, 1, 265.00, 0),
(477, 1, 427, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(478, 1, 428, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(479, 1, 429, 1, 0, 0.00, 0, 43.00, 43, 1, 43.00, 1, 43, 1, 43, 1, 43.00, 0),
(480, 1, 430, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(481, 1, 431, 0, 0, 0.00, 0, 43.00, 43, 1, 43.00, 1, 43, 1, 43, 1, 43.00, 0),
(482, 1, 432, 1, 0, 0.00, 0, 169.00, 169, 1, 169.00, 1, 169, 1, 169, 1, 169.00, 0),
(483, 1, 433, 2, 0, 0.00, 0, 12.00, 12, 1, 12.00, 1, 12, 1, 12, 1, 12.00, 0),
(484, 1, 434, 2, 0, 0.00, 0, 59.00, 59, 1, 59.00, 1, 59, 1, 59, 1, 59.00, 0),
(485, 1, 435, 1, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 0),
(486, 1, 436, 1, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(487, 1, 437, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(488, 1, 438, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(489, 1, 439, 1, 0, 0.00, 0, 125.00, 125, 1, 125.00, 1, 125, 1, 125, 1, 125.00, 0),
(490, 1, 440, 1, 0, 0.00, 0, 190.00, 190, 1, 190.00, 1, 190, 1, 190, 1, 190.00, 0),
(491, 1, 441, 1, 0, 0.00, 0, 43.00, 43, 1, 43.00, 1, 43, 1, 43, 1, 43.00, 0),
(492, 1, 442, 1, 0, 0.00, 0, 78.00, 78, 1, 78.00, 1, 78, 1, 78, 1, 78.00, 0),
(493, 1, 443, 0, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(494, 1, 444, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(495, 1, 445, 1, 0, 0.00, 0, 169.00, 169, 1, 169.00, 1, 169, 1, 169, 1, 169.00, 0),
(496, 1, 446, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(497, 1, 447, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(498, 1, 448, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(499, 1, 449, 1, 0, 0.00, 0, 225.00, 225, 1, 225.00, 1, 225, 1, 225, 1, 225.00, 0),
(500, 1, 450, 1, 0, 0.00, 0, 225.00, 225, 1, 225.00, 1, 225, 1, 225, 1, 225.00, 0),
(501, 1, 451, 1, 0, 0.00, 0, 165.00, 165, 1, 165.00, 1, 165, 1, 165, 1, 165.00, 0),
(502, 1, 452, 0, 0, 0.00, 0, 165.00, 165, 1, 165.00, 1, 165, 1, 165, 1, 165.00, 0),
(503, 1, 453, 1, 0, 0.00, 0, 159.00, 159, 1, 159.00, 1, 159, 1, 159, 1, 159.00, 0),
(504, 1, 454, 1, 0, 0.00, 0, 249.00, 249, 1, 249.00, 1, 249, 1, 249, 1, 249.00, 0),
(505, 1, 455, 0, 0, 0.00, 0, 159.00, 159, 1, 159.00, 1, 159, 1, 159, 1, 159.00, 0),
(506, 1, 456, 0, 0, 0.00, 0, 249.00, 249, 1, 249.00, 1, 249, 1, 249, 1, 249.00, 0),
(507, 1, 457, 0, 0, 0.00, 0, 18.00, 18, 1, 18.00, 1, 18, 1, 18, 1, 18.00, 0),
(508, 1, 458, 1, 0, 0.00, 0, 18.00, 18, 1, 18.00, 1, 18, 1, 18, 1, 18.00, 0),
(509, 1, 459, 1, 0, 0.00, 0, 65.00, 65, 1, 65.00, 1, 65, 1, 65, 1, 65.00, 0),
(510, 1, 460, 1, 0, 0.00, 0, 18.00, 18, 1, 18.00, 1, 18, 1, 18, 1, 18.00, 0),
(511, 1, 461, 1, 0, 0.00, 0, 20.00, 20, 1, 20.00, 1, 20, 1, 20, 1, 20.00, 0),
(512, 1, 462, 1, 0, 0.00, 0, 20.00, 20, 1, 20.00, 1, 20, 1, 20, 1, 20.00, 0),
(513, 1, 463, 1, 0, 0.00, 0, 20.00, 20, 1, 20.00, 1, 20, 1, 20, 1, 20.00, 0),
(514, 1, 464, 0, 0, 0.00, 0, 89.00, 89, 1, 89.00, 1, 89, 1, 89, 1, 89.00, 0),
(515, 1, 465, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(516, 1, 466, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(517, 1, 467, 1, 0, 0.00, 0, 135.00, 135, 1, 135.00, 1, 135, 1, 135, 1, 135.00, 0),
(518, 1, 468, 6, 0, 0.00, 0, 35.00, 35, 1, 35.00, 1, 35, 1, 35, 1, 35.00, 1),
(519, 1, 469, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(520, 1, 470, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(521, 1, 471, 1, 0, 0.00, 0, 49.00, 49, 1, 49.00, 1, 49, 1, 49, 1, 49.00, 0),
(522, 1, 472, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(523, 1, 473, 1, 0, 0.00, 0, 135.00, 135, 1, 135.00, 1, 135, 1, 135, 1, 135.00, 0),
(524, 1, 474, 1, 0, 0.00, 0, 135.00, 135, 1, 135.00, 1, 135, 1, 135, 1, 135.00, 0),
(525, 1, 475, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(526, 1, 476, 0, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(527, 1, 477, 1, 0, 0.00, 0, 78.00, 78, 1, 78.00, 1, 78, 1, 78, 1, 78.00, 0),
(528, 1, 478, 1, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(529, 1, 479, 0, 0, 0.00, 0, 99.00, 99, 1, 99.00, 1, 99, 1, 99, 1, 99.00, 0),
(530, 1, 480, 1, 0, 0.00, 0, 86.00, 86, 1, 86.00, 1, 86, 1, 86, 1, 86.00, 0),
(531, 1, 481, 0, 0, 0.00, 0, 159.00, 159, 1, 159.00, 1, 159, 1, 159, 1, 159.00, 0),
(532, 1, 482, 5, 0, 130.00, 0, 245.00, 245, 1, 245.00, 1, 245, 1, 245, 1, 245.00, 1),
(533, 1, 483, 0, 0, 18000.00, 0, 18000.00, 18000, 1, 18000.00, 1, 18000, 1, 18000, 1, 18000.00, 0),
(534, 1, 484, 10, 0, 0.00, 0, 1000.00, 1000, 1, 1000.00, 1, 1000, 1, 1000, 1, 1000.00, 1),
(535, 1, 485, 0, 0, 0.00, 0, 98.00, 98, 1, 98.00, 1, 98, 1, 98, 1, 98.00, 0),
(536, 1, 486, 3, 0, 0.00, 0, 1300.00, 1300, 1, 1300.00, 1, 1300, 1, 1300, 1, 1300.00, 0),
(537, 1, 487, 5, 0, 0.00, 0, 148.00, 148, 1, 148.00, 1, 148, 1, 148, 1, 148.00, 0),
(538, 1, 488, 15, 0, 0.00, 0, 148.00, 148, 1, 148.00, 1, 148, 1, 148, 1, 148.00, 1),
(539, 1, 489, 1, 0, 0.00, 0, 370.00, 370, 1, 370.00, 1, 370, 1, 370, 1, 370.00, 0),
(540, 1, 490, 0, 0, 0.00, 0, 45.00, 45, 1, 45.00, 1, 45, 1, 45, 1, 45.00, 0),
(541, 1, 491, 1, 0, 0.00, 0, 265.00, 265, 1, 265.00, 1, 265, 1, 265, 1, 265.00, 0),
(542, 1, 492, 1, 0, 0.00, 0, 245.00, 245, 1, 245.00, 1, 245, 1, 245, 1, 245.00, 0),
(543, 1, 493, 10, 0, 0.00, 0, 245.00, 245, 1, 245.00, 1, 245, 1, 245, 1, 245.00, 1),
(544, 1, 494, 4, 0, 0.00, 0, 1100.00, 1100, 1, 1100.00, 1, 1100, 1, 1100, 1, 1100.00, 1),
(545, 1, 495, 1, 0, 0.00, 0, 1250.00, 1250, 1, 1250.00, 1, 1250, 1, 1250, 1, 1250.00, 0),
(546, 1, 496, 9, 0, 0.00, 0, 1500.00, 1500, 1, 1500.00, 1, 1500, 1, 1500, 1, 1500.00, 1),
(547, 1, 497, 1, 0, 0.00, 0, 29500.00, 29500, 1, 29500.00, 1, 29500, 1, 29500, 1, 29500.00, 0),
(548, 1, 498, 1, 0, 0.00, 0, 12400.00, 12400, 1, 12400.00, 1, 12400, 1, 12400, 1, 12400.00, 0),
(549, 1, 499, 1, 0, 0.00, 0, 19800.00, 19800, 1, 19800.00, 1, 19800, 1, 19800, 1, 19800.00, 0),
(550, 1, 500, 6, 0, 0.00, 0, 148.00, 148, 1, 148.00, 1, 148, 1, 148, 1, 148.00, 1),
(551, 1, 501, 1, 0, 0.00, 0, 12500.00, 12500, 1, 12500.00, 1, 12500, 1, 12500, 1, 12500.00, 0),
(552, 1, 502, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(553, 1, 503, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(554, 1, 504, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(555, 1, 505, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(556, 1, 506, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(557, 1, 507, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(558, 1, 508, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(559, 1, 509, 1, 0, 0.00, 0, 14900.00, 14900, 1, 14900.00, 1, 14900, 1, 14900, 1, 14900.00, 0),
(560, 1, 510, 21, 0, 0.00, 0, 135.00, 135, 1, 135.00, 1, 135, 1, 135, 1, 135.00, 1),
(561, 1, 511, 1, 0, 0.00, 0, 3450.00, 3450, 1, 3450.00, 1, 3450, 1, 3450, 1, 3450.00, 0),
(562, 1, 512, 5, 0, 0.00, 0, 2400.00, 2400, 1, 2400.00, 1, 2400, 1, 2400, 1, 2400.00, 1),
(563, 1, 513, 2, 0, 0.00, 0, 1940.00, 1940, 1, 1940.00, 1, 1940, 1, 1940, 1, 1940.00, 0),
(564, 1, 514, 2, 0, 0.00, 0, 2378.00, 2378, 1, 2378.00, 1, 2378, 1, 2378, 1, 2378.00, 0),
(565, 1, 515, 2, 0, 0.00, 0, 1502.00, 1502, 1, 1502.00, 1, 1502, 1, 1502, 1, 1502.00, 0),
(566, 1, 516, 0, 0, 0.00, 0, 590.00, 590, 1, 590.00, 1, 590, 1, 590, 1, 590.00, 0),
(567, 1, 517, 1, 0, 0.00, 0, 2750.00, 2750, 1, 2750.00, 1, 2750, 1, 2750, 1, 2750.00, 0),
(568, 1, 518, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(569, 1, 519, 1, 0, 0.00, 0, 2850.00, 2850, 1, 2850.00, 1, 2850, 1, 2850, 1, 2850.00, 0),
(570, 1, 520, 1, 0, 0.00, 0, 2950.00, 2950, 1, 2950.00, 1, 2950, 1, 2950, 1, 2950.00, 0),
(571, 1, 521, 1, 0, 0.00, 0, 4800.00, 4800, 1, 4800.00, 1, 4800, 1, 4800, 1, 4800.00, 0),
(572, 1, 522, 1, 0, 0.00, 0, 4800.00, 4800, 1, 4800.00, 1, 4800, 1, 4800, 1, 4800.00, 0),
(573, 1, 523, 1, 0, 0.00, 0, 5800.00, 5800, 1, 5800.00, 1, 5800, 1, 5800, 1, 5800.00, 0),
(574, 1, 524, 1, 0, 0.00, 0, 6200.00, 6200, 1, 6200.00, 1, 6200, 1, 6200, 1, 6200.00, 0),
(575, 1, 525, 1, 0, 0.00, 0, 1750.00, 1750, 1, 1750.00, 1, 1750, 1, 1750, 1, 1750.00, 0),
(576, 1, 526, 1, 0, 0.00, 0, 1590.00, 1590, 1, 1590.00, 1, 1590, 1, 1590, 1, 1590.00, 0),
(577, 1, 527, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(578, 1, 528, 1, 0, 0.00, 0, 3950.00, 3950, 1, 3950.00, 1, 3950, 1, 3950, 1, 3950.00, 0),
(579, 1, 529, 1, 0, 0.00, 0, 2300.00, 2300, 1, 2300.00, 1, 2300, 1, 2300, 1, 2300.00, 0),
(580, 1, 530, 1, 0, 0.00, 0, 1790.00, 1790, 1, 1790.00, 1, 1790, 1, 1790, 1, 1790.00, 0),
(581, 1, 531, 1, 0, 0.00, 0, 1500.00, 1500, 1, 1500.00, 1, 1500, 1, 1500, 1, 1500.00, 0),
(582, 1, 532, 19, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 1),
(583, 1, 533, 5, 0, 0.00, 0, 58.00, 58, 1, 58.00, 1, 58, 1, 58, 1, 58.00, 1),
(584, 1, 534, 11, 0, 0.00, 0, 185.00, 185, 1, 185.00, 1, 185, 1, 185, 1, 185.00, 1),
(585, 1, 535, 10, 0, 0.00, 0, 185.00, 185, 1, 185.00, 1, 185, 1, 185, 1, 185.00, 1),
(586, 1, 536, 11, 0, 0.00, 0, 275.00, 275, 1, 275.00, 1, 275, 1, 275, 1, 275.00, 1),
(587, 1, 537, 11, 0, 0.00, 0, 275.00, 275, 1, 275.00, 1, 275, 1, 275, 1, 275.00, 1),
(588, 1, 538, 9, 0, 0.00, 0, 195.00, 195, 1, 195.00, 1, 195, 1, 195, 1, 195.00, 1),
(589, 1, 539, 11, 0, 0.00, 0, 32.00, 32, 1, 32.00, 1, 32, 1, 32, 1, 32.00, 1),
(590, 1, 540, 8, 0, 0.00, 0, 38.00, 38, 1, 38.00, 1, 38, 1, 38, 1, 38.00, 1),
(591, 1, 541, 7, 0, 0.00, 0, 48.00, 48, 1, 48.00, 1, 48, 1, 48, 1, 48.00, 1),
(592, 1, 542, 5, 0, 0.00, 0, 34.00, 34, 1, 34.00, 1, 34, 1, 34, 1, 34.00, 1),
(593, 1, 543, 2, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(594, 1, 544, 1, 0, 0.00, 0, 2100.00, 2100, 1, 2100.00, 1, 2100, 1, 2100, 1, 2100.00, 0),
(595, 1, 545, 3, 0, 0.00, 0, 284.00, 284, 1, 284.00, 1, 284, 1, 284, 1, 284.00, 0),
(596, 1, 546, 1, 0, 0.00, 0, 5490.00, 5490, 1, 5490.00, 1, 5490, 1, 5490, 1, 5490.00, 0),
(597, 1, 547, 1, 0, 0.00, 0, 4500.00, 4500, 1, 4500.00, 1, 4500, 1, 4500, 1, 4500.00, 1),
(598, 1, 548, 0, 0, 0.00, 0, 275.00, 275, 1, 275.00, 1, 275, 1, 275, 1, 275.00, 1),
(599, 1, 549, 8, 0, 0.00, 0, 210.00, 210, 1, 210.00, 1, 210, 1, 210, 1, 210.00, 1),
(600, 1, 550, 1, 0, 0.00, 0, 210.00, 210, 1, 210.00, 1, 210, 1, 210, 1, 210.00, 1),
(601, 1, 551, 2, 0, 0.00, 0, 340.00, 340, 1, 340.00, 1, 340, 1, 340, 1, 340.00, 1),
(602, 1, 552, 2, 0, 0.00, 0, 330.00, 330, 1, 330.00, 1, 330, 1, 330, 1, 330.00, 1),
(603, 1, 553, 1, 0, 0.00, 0, 245.00, 245, 1, 245.00, 1, 245, 1, 245, 1, 245.00, 1),
(604, 1, 554, 1, 0, 0.00, 0, 265.00, 265, 1, 265.00, 1, 265, 1, 265, 1, 265.00, 1),
(605, 1, 555, 1, 0, 0.00, 0, 240.00, 240, 1, 240.00, 1, 240, 1, 240, 1, 240.00, 1),
(606, 1, 556, 1, 0, 0.00, 0, 240.00, 240, 1, 240.00, 1, 240, 1, 240, 1, 240.00, 1),
(607, 1, 557, 1, 0, 0.00, 0, 245.00, 245, 1, 245.00, 1, 245, 1, 245, 1, 245.00, 1),
(608, 1, 558, 2, 0, 0.00, 0, 255.00, 255, 1, 255.00, 1, 255, 1, 255, 1, 255.00, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id_proveedor` int(11) NOT NULL,
  `razon_social` varchar(50) NOT NULL,
  `domicilio` varchar(30) CHARACTER SET utf8 NOT NULL COMMENT 'calle',
  `ciudad` varchar(50) CHARACTER SET utf8 NOT NULL,
  `cp` varchar(8) CHARACTER SET utf8 NOT NULL,
  `id_estado` int(11) NOT NULL,
  `telefono_local` varchar(10) NOT NULL,
  `telefono_celular` varchar(10) NOT NULL,
  `contacto` varchar(100) NOT NULL,
  `email_contacto` varchar(60) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `obser` text NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id_proveedor`, `razon_social`, `domicilio`, `ciudad`, `cp`, `id_estado`, `telefono_local`, `telefono_celular`, `contacto`, `email_contacto`, `rfc`, `fax`, `obser`, `activo`) VALUES
(23, 'Demo de proveedor', 'AAA', 'AAA', '12345', 21, '123', '123', 'AAA', 'AAA', 'HERD890308UAA', '', '', 1),
(24, 'DULCERIA SUSY S.A DE C.V. SUCURSAL 3', '104 PTE NO 1720-A LOC 10 COL. ', 'PUEBLA', '', 21, '', '2227535616', 'HECTOR', '', 'DSU910312LSO', '', 'asdasdasdasdass', 0),
(25, 'DOBLE CUADRO', 'AV. TONALTECAS 82A LA ALBERCA', 'TONALA', '45400', 14, '', '3312379510', '', 'ventas.abstractoink@gmail.com', '', '', '9:30-6:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`id`, `nombre`, `icon`, `activo`) VALUES
(1, 'Matriz', 'fa fa-cog', 1),
(2, 'SP', 'fa fa-play', 0),
(3, 'Huamantla', 'fa fa-play', 0),
(4, 'Santa Ana 1', 'fa fa-play', 0),
(5, 'Santa Ana 2', 'fa fa-play', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ticket`
--

CREATE TABLE `ticket` (
  `id_ticket` int(11) NOT NULL,
  `titulo` text CHARACTER SET utf8 NOT NULL,
  `mensaje` text CHARACTER SET utf8 NOT NULL,
  `mensaje2` text CHARACTER SET utf8 NOT NULL,
  `fuente` int(1) NOT NULL,
  `tamano` int(2) NOT NULL,
  `margensup` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ticket`
--

INSERT INTO `ticket` (`id_ticket`, `titulo`, `mensaje`, `mensaje2`, `fuente`, `tamano`, `margensup`) VALUES
(1, 'DULCIBOTANAS', '¡GRACIAS POR SU COMPRA!', 'ESTE TICKET NO ES UNA REPRESENTACIÓN FISCAL', 2, 9, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traspasos`
--

CREATE TABLE `traspasos` (
  `id` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `sucursal` int(11) NOT NULL,
  `total` float NOT NULL,
  `usuario` int(11) NOT NULL,
  `estatus` int(1) NOT NULL,
  `codigo_barras` varchar(100) NOT NULL,
  `codigo` varchar(100) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `reg` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `traspasos_detalles`
--

CREATE TABLE `traspasos_detalles` (
  `id` int(11) NOT NULL,
  `productoid` int(11) NOT NULL,
  `idtraspaso` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `cantidad_recibe` int(11) NOT NULL,
  `tipo_precio` varchar(20) NOT NULL,
  `precio` float NOT NULL,
  `subtotal` float NOT NULL,
  `estatus` int(1) NOT NULL DEFAULT 1,
  `usuario_recibe` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE `turno` (
  `id` int(11) NOT NULL,
  `sucursalId` int(11) NOT NULL DEFAULT 0,
  `fecha` date NOT NULL,
  `horaa` time NOT NULL,
  `fechacierre` date NOT NULL,
  `horac` time NOT NULL,
  `cantidad` float NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_var` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 abierto 0 cerrado',
  `user` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`id`, `sucursalId`, `fecha`, `horaa`, `fechacierre`, `horac`, `cantidad`, `nombre`, `status`, `status_var`, `user`) VALUES
(1, 1, '2021-03-26', '19:59:54', '2021-03-29', '19:42:32', 0, '', 'cerrado', 0, 'user'),
(2, 1, '2021-03-29', '19:42:40', '0000-00-00', '00:00:00', 0, '', 'cerrado', 0, 'user'),
(3, 1, '2021-03-29', '19:42:44', '2021-03-29', '19:42:53', 0, '', 'cerrado', 0, 'user'),
(4, 1, '2021-03-29', '19:42:56', '2021-03-29', '22:38:54', 0, '', 'cerrado', 0, 'user'),
(5, 1, '2021-03-29', '23:10:25', '2021-03-31', '16:34:23', 0, '', 'cerrado', 0, 'user'),
(6, 1, '2021-06-25', '16:54:02', '2021-06-25', '14:34:58', 100, 'h', 'cerrado', 0, 'user'),
(7, 2, '2021-06-25', '19:20:59', '2021-06-25', '14:35:05', 101, '102a', 'cerrado', 0, 'user'),
(8, 2, '2021-06-25', '14:35:28', '0000-00-00', '00:00:00', 100, '100', 'abierto', 1, 'user'),
(9, 1, '2021-06-25', '14:35:08', '2021-06-25', '15:36:41', 100, '100', 'cerrado', 0, 'user'),
(10, 1, '2021-06-25', '15:42:53', '0000-00-00', '00:00:00', 100, 'a', 'abierto', 1, 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `perfilId` int(11) NOT NULL,
  `personalId` int(11) NOT NULL,
  `Usuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`UsuarioID`, `perfilId`, `personalId`, `Usuario`, `contrasena`) VALUES
(1, 1, 1, 'admin', '$2y$10$.yBpfsTTEmhYhECki.qy3eL45XaLo3MinuIPgiPsjmXBYItZXHUga'),
(2, 2, 2, 'empleado', '$2y$10$/342zSSjf1Cy.k4kGMmOYeGeHMueJhrh83n9Owf1RpckBioxwZ9lu'),
(3, 2, 3, 'patiro', '$2y$10$M2RXTpW38D5b29Lb8W8NNuiQzv/BOBaA0bEbZIcsRQoL9ZzsS11xy'),
(4, 2, 4, 'felipe', '$2y$10$jj1BVrngeWRbpw9TkBiN1OiAONgwuDHtInoL7HVl/f697poz9qD9K'),
(5, 2, 5, 'carlos', '$2y$10$0/g8up8fIshncEB42tZ.MeYASrALvXbvbj3AIid.86GB8yLVw.lyG'),
(6, 2, 6, 'patricia', '$2y$10$MXqnWegzv32VJflSft0fN.m8mioY6SO2DlSW18n1gWJ2uh8wVAlpa'),
(7, 2, 7, 'meli', '$2y$10$hqh2GlGxZ9HGuUikk5ibPO9tAeriwXGK1.y3os00t3wCAvM3D2ThS'),
(8, 2, 8, 'miriam', '$2y$10$ou9m4nKAMc/fm5DGTqBMpOXd5yBQOuiSlzq1TE1zFBEeWiOUd077m'),
(9, 2, 9, 'dalia', '$2y$10$8Eki7tsD7cMy.F8gIaMSD.QwwgFqjiSX0SlFfDBmW58WXoKnTShwq');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `metodo` int(1) NOT NULL COMMENT '1 efectivo, 2 credito, 3 debito',
  `subtotal` decimal(10,2) NOT NULL,
  `descuento` double NOT NULL COMMENT '0 % 5 % 7%',
  `descuentocant` decimal(10,2) NOT NULL,
  `monto_total` decimal(10,2) NOT NULL,
  `pagotarjeta` decimal(10,2) NOT NULL,
  `efectivo` decimal(10,2) NOT NULL,
  `cambio` decimal(10,2) NOT NULL,
  `cancelado` int(11) NOT NULL,
  `hcancelacion` date NOT NULL,
  `tipo_venta` int(11) NOT NULL COMMENT 'Cuando esta una venta desde cotización',
  `factura` int(1) NOT NULL,
  `pagado_credito` int(1) NOT NULL COMMENT '0-no pagado 1-pagado',
  `reg` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `id_personal`, `id_cliente`, `sucursal`, `metodo`, `subtotal`, `descuento`, `descuentocant`, `monto_total`, `pagotarjeta`, `efectivo`, `cambio`, `cancelado`, `hcancelacion`, `tipo_venta`, `factura`, `pagado_credito`, `reg`) VALUES
(1, 1, 1, 1, 1, 100.00, 0, 0.00, 100.00, 0.00, 100.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 14:40:58'),
(2, 1, 1, 1, 1, 100.00, 0, 0.00, 100.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 14:58:35'),
(3, 1, 1, 1, 1, 100.00, 0, 0.00, 100.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 15:08:56'),
(4, 1, 1, 1, 1, 1.00, 0, 0.00, 1.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 19:45:51'),
(5, 1, 1, 1, 1, 1.00, 0, 0.00, 1.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 19:50:10'),
(6, 6, 1, 1, 1, 1.00, 0, 0.00, 1.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 20:05:54'),
(7, 6, 1, 1, 1, 1.00, 0, 0.00, 1.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 21:07:12'),
(8, 6, 1, 1, 1, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-23 21:27:56'),
(9, 6, 1, 1, 1, 1260.00, 0, 0.00, 1260.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-24 21:50:24'),
(10, 6, 1, 1, 1, 54.00, 0, 0.00, 54.00, 0.00, 100.00, 46.00, 0, '0000-00-00', 0, 0, 0, '2021-07-29 16:13:10'),
(11, 6, 1, 1, 1, 291.00, 0, 0.00, 291.00, 0.00, 500.00, 209.00, 0, '0000-00-00', 0, 0, 0, '2021-07-29 16:18:00'),
(12, 6, 1, 1, 1, 54.00, 0, 0.00, 54.00, 0.00, 100.00, 46.00, 0, '0000-00-00', 0, 0, 0, '2021-07-31 20:50:43'),
(13, 6, 1, 1, 1, 38.00, 0, 0.00, 38.00, 0.00, 50.00, 12.00, 0, '0000-00-00', 0, 0, 0, '2021-07-31 20:54:54'),
(14, 6, 1, 1, 1, 48.00, 0, 0.00, 48.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-31 22:31:45'),
(15, 6, 1, 1, 1, 3045.00, 365, 365.00, 2680.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-07-31 23:00:49'),
(16, 6, 1, 1, 1, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 1, '2021-08-01', 1, 0, 0, '2021-08-01 19:15:27'),
(17, 6, 1, 1, 1, 66.00, 0, 0.00, 66.00, 0.00, 100.00, 34.00, 0, '0000-00-00', 0, 0, 0, '2021-08-07 18:39:36'),
(18, 6, 1, 1, 1, 16.00, 0, 0.00, 16.00, 0.00, 16.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 17:42:42'),
(19, 6, 1, 1, 1, 223.00, 0, 0.00, 223.00, 0.00, 500.00, 277.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 17:55:32'),
(20, 6, 1, 1, 1, 1540.00, 77, 77.00, 1463.00, 0.00, 1500.00, 37.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 21:37:35'),
(21, 6, 1, 1, 1, 180.00, 0, 0.00, 180.00, 0.00, 200.00, 20.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 22:45:29'),
(22, 6, 1, 1, 1, 201.00, 0, 0.00, 201.00, 0.00, 201.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 22:47:43'),
(23, 6, 1, 1, 1, 84.00, 0, 0.00, 84.00, 0.00, 500.00, 416.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 22:49:51'),
(24, 6, 1, 1, 1, 14.00, 0, 0.00, 14.00, 0.00, 20.00, 6.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 22:50:53'),
(25, 6, 1, 1, 1, 45.00, 0, 0.00, 45.00, 0.00, 100.00, 55.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 22:52:05'),
(26, 6, 1, 1, 1, 90.00, 0, 0.00, 90.00, 0.00, 200.00, 110.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 22:54:25'),
(27, 6, 1, 1, 1, 45.00, 0, 0.00, 45.00, 0.00, 50.00, 5.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 22:55:17'),
(28, 6, 1, 1, 1, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-08 23:11:53'),
(29, 6, 1, 1, 1, 264.00, 24, 24.00, 240.00, 0.00, 240.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-11 17:06:11'),
(30, 1, 1, 1, 1, 66.00, 0, 0.00, 66.00, 0.00, 100.00, 34.00, 0, '0000-00-00', 0, 0, 0, '2021-08-14 21:11:42'),
(31, 6, 1, 1, 1, 775.00, 0, 0.00, 775.00, 0.00, 1000.00, 225.00, 0, '0000-00-00', 0, 0, 0, '2021-08-15 16:07:38'),
(32, 6, 1, 1, 1, 845.00, 0, 0.00, 845.00, 0.00, 1000.00, 155.00, 0, '0000-00-00', 0, 0, 0, '2021-08-15 16:11:12'),
(33, 6, 1, 1, 1, 222.00, 0, 0.00, 222.00, 0.00, 250.00, 28.00, 0, '0000-00-00', 0, 0, 0, '2021-08-15 18:26:49'),
(34, 6, 1, 1, 1, 240.00, 0, 0.00, 240.00, 0.00, 500.00, 260.00, 0, '0000-00-00', 0, 0, 0, '2021-08-15 19:08:25'),
(35, 6, 1, 1, 1, 320.00, 0, 0.00, 320.00, 0.00, 0.00, 0.00, 1, '2021-08-15', 0, 0, 0, '2021-08-15 21:04:01'),
(36, 6, 1, 1, 1, 320.00, 0, 0.00, 320.00, 0.00, 0.00, 0.00, 1, '2021-08-15', 0, 0, 0, '2021-08-15 21:04:18'),
(37, 6, 1, 1, 1, 320.00, 0, 0.00, 320.00, 0.00, 320.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-15 21:06:27'),
(38, 6, 1, 1, 1, 320.00, 0, 0.00, 320.00, 0.00, 520.00, 200.00, 0, '0000-00-00', 0, 0, 0, '2021-08-15 21:06:51'),
(39, 6, 1, 1, 1, 86.00, 0, 0.00, 86.00, 0.00, 100.00, 14.00, 0, '0000-00-00', 0, 0, 0, '2021-08-16 18:22:18'),
(40, 1, 1, 1, 1, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-21 18:55:49'),
(41, 6, 1, 1, 1, 16.00, 0, 0.00, 16.00, 0.00, 16.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-22 16:33:13'),
(42, 6, 1, 1, 1, 66.00, 0, 0.00, 66.00, 0.00, 100.00, 34.00, 0, '0000-00-00', 0, 0, 0, '2021-08-22 18:50:00'),
(43, 6, 1, 1, 1, 20.00, 0, 0.00, 20.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-22 20:21:56'),
(44, 6, 1, 1, 3, 345.00, 0, 0.00, 345.00, 0.00, 0.00, 0.00, 1, '2021-08-22', 0, 0, 0, '2021-08-22 21:31:08'),
(45, 6, 1, 1, 1, 345.00, 0, 0.00, 345.00, 0.00, 500.00, 155.00, 0, '0000-00-00', 0, 0, 0, '2021-08-22 21:34:23'),
(46, 6, 1, 1, 1, 22.00, 0, 0.00, 22.00, 0.00, 50.00, 28.00, 0, '0000-00-00', 0, 0, 0, '2021-08-22 21:49:57'),
(47, 6, 1, 1, 1, 38.00, 0, 0.00, 38.00, 0.00, 40.00, 2.00, 0, '0000-00-00', 0, 0, 0, '2021-08-22 21:50:48'),
(48, 6, 1, 1, 3, 190.00, 0, 0.00, 190.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-22 22:58:47'),
(49, 6, 1, 1, 1, 155.00, 0, 0.00, 155.00, 0.00, 0.00, 0.00, 1, '2021-08-23', 0, 0, 0, '2021-08-23 16:33:00'),
(50, 6, 1, 1, 1, 155.00, 0, 0.00, 155.00, 0.00, 500.00, 345.00, 0, '0000-00-00', 0, 0, 0, '2021-08-23 16:33:55'),
(51, 6, 1, 1, 1, 40.00, 0, 0.00, 40.00, 0.00, 200.00, 160.00, 0, '0000-00-00', 0, 0, 0, '2021-08-24 23:00:12'),
(52, 6, 1, 1, 1, 40.00, 0, 0.00, 40.00, 0.00, 100.00, 60.00, 0, '0000-00-00', 0, 0, 0, '2021-08-25 18:42:46'),
(53, 6, 1, 1, 1, 152.00, 0, 0.00, 152.00, 0.00, 152.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-26 15:32:34'),
(54, 6, 1, 1, 1, 155.00, 0, 0.00, 155.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-26 15:35:03'),
(55, 6, 1, 1, 1, 10.00, 0, 0.00, 10.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-26 19:55:04'),
(56, 6, 1, 1, 1, 24.00, 0, 0.00, 24.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-26 21:36:23'),
(57, 6, 1, 1, 1, 1000.00, 0, 0.00, 1000.00, 0.00, 1000.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 16:56:30'),
(58, 6, 1, 1, 1, 500.00, 0, 0.00, 500.00, 0.00, 500.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 16:56:57'),
(59, 6, 1, 1, 1, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 1, '2021-08-28', 0, 0, 0, '2021-08-28 16:57:56'),
(60, 6, 1, 1, 1, 3200.00, 480, 480.00, 2720.00, 0.00, 2720.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 18:14:59'),
(61, 6, 1, 1, 1, 530.00, 53, 53.00, 477.00, 0.00, 500.00, 23.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 18:40:58'),
(62, 6, 1, 1, 1, 20.00, 0, 0.00, 20.00, 0.00, 20.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 20:06:11'),
(63, 6, 1, 1, 1, 1240.00, 0, 0.00, 1240.00, 0.00, 1500.00, 260.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 21:02:53'),
(64, 6, 1, 1, 1, 1345.00, 161, 161.00, 1184.00, 0.00, 1200.00, 16.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 22:01:19'),
(65, 6, 1, 1, 1, 94.00, 0, 0.00, 94.00, 0.00, 100.00, 6.00, 0, '0000-00-00', 0, 0, 0, '2021-08-28 22:34:23'),
(66, 6, 1, 1, 1, 160.00, 0, 0.00, 160.00, 0.00, 500.00, 340.00, 0, '0000-00-00', 0, 0, 0, '2021-08-31 22:16:55'),
(67, 6, 1, 1, 1, 22.00, 0, 0.00, 22.00, 0.00, 22.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-08-31 22:31:08'),
(68, 6, 1, 1, 1, 112.00, 0, 0.00, 112.00, 0.00, 120.00, 8.00, 0, '0000-00-00', 0, 0, 0, '2021-08-31 22:41:21'),
(69, 6, 1, 1, 2, 775.00, 0, 0.00, 775.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-01 18:00:28'),
(70, 6, 1, 1, 1, 162.00, 0, 0.00, 162.00, 0.00, 200.00, 38.00, 0, '0000-00-00', 0, 0, 0, '2021-09-01 19:26:22'),
(71, 8, 1, 1, 1, 28.00, 0, 0.00, 28.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-04 19:08:30'),
(72, 8, 1, 1, 1, 745.00, 0, 0.00, 745.00, 0.00, 1000.00, 255.00, 0, '0000-00-00', 0, 0, 0, '2021-09-04 19:13:42'),
(73, 8, 1, 1, 1, 990.00, 0, 0.00, 990.00, 0.00, 1000.00, 10.00, 0, '0000-00-00', 0, 0, 0, '2021-09-04 20:42:13'),
(74, 8, 1, 1, 1, 350.00, 0, 0.00, 350.00, 0.00, 350.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-04 21:48:51'),
(75, 6, 1, 1, 3, 5890.00, 890, 890.00, 5000.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-05 20:04:44'),
(76, 8, 1, 1, 3, 2950.00, 350, 350.00, 2600.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-07 16:08:14'),
(77, 8, 1, 1, 1, 80.00, 0, 0.00, 80.00, 0.00, 500.00, 420.00, 0, '0000-00-00', 0, 0, 0, '2021-09-07 18:24:27'),
(78, 6, 1, 1, 1, 89.00, 0, 0.00, 89.00, 0.00, 200.00, 111.00, 0, '0000-00-00', 0, 0, 0, '2021-09-08 22:25:58'),
(79, 6, 1, 1, 1, 85.00, 0, 0.00, 85.00, 0.00, 500.00, 415.00, 0, '0000-00-00', 0, 0, 0, '2021-09-08 22:28:12'),
(80, 6, 1, 1, 1, 210.00, 0, 0.00, 210.00, 0.00, 210.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-09 18:12:13'),
(81, 6, 1, 1, 1, 20.00, 0, 0.00, 20.00, 0.00, 20.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-09 18:12:56'),
(82, 6, 1, 1, 1, 66.00, 0, 0.00, 66.00, 0.00, 200.00, 134.00, 0, '0000-00-00', 0, 0, 0, '2021-09-11 18:21:09'),
(83, 6, 1, 1, 1, 195.00, 0, 0.00, 195.00, 0.00, 500.00, 305.00, 0, '0000-00-00', 0, 0, 0, '2021-09-11 21:59:55'),
(84, 8, 1, 1, 1, 14200.00, 1420, 1420.00, 12780.00, 0.00, 13000.00, 220.00, 0, '0000-00-00', 0, 0, 0, '2021-09-12 20:14:23'),
(85, 8, 1, 1, 1, 490.00, 49, 49.00, 441.00, 0.00, 500.00, 59.00, 0, '0000-00-00', 0, 0, 0, '2021-09-12 20:16:28'),
(86, 8, 1, 1, 1, 309.00, 0, 0.00, 309.00, 0.00, 400.00, 91.00, 0, '0000-00-00', 0, 0, 0, '2021-09-12 20:49:36'),
(87, 8, 1, 1, 3, 18000.00, 0, 0.00, 18000.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-12 20:51:56'),
(88, 8, 1, 1, 1, 66.00, 0, 0.00, 66.00, 0.00, 100.00, 34.00, 0, '0000-00-00', 0, 0, 0, '2021-09-12 22:22:00'),
(89, 8, 1, 1, 1, 40.00, 0, 0.00, 40.00, 0.00, 0.00, 0.00, 1, '2021-09-14', 0, 0, 0, '2021-09-14 15:41:58'),
(90, 8, 1, 1, 1, 40.00, 0, 0.00, 40.00, 0.00, 50.00, 10.00, 0, '0000-00-00', 0, 0, 0, '2021-09-14 15:42:37'),
(91, 1, 57, 1, 1, 36000.80, 10.668, 4299.20, 36000.80, 0.00, 0.00, 0.00, 0, '0000-00-00', 1, 0, 0, '2021-09-14 18:27:17'),
(92, 8, 1, 1, 1, 288.00, 0, 0.00, 288.00, 0.00, 300.00, 12.00, 0, '0000-00-00', 0, 0, 0, '2021-09-14 22:14:15'),
(93, 8, 1, 1, 1, 10.00, 0, 0.00, 10.00, 0.00, 10.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 17:17:08'),
(94, 8, 1, 1, 1, 40.00, 0, 0.00, 40.00, 0.00, 40.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 20:24:53'),
(95, 8, 1, 1, 1, 32.00, 0, 0.00, 32.00, 0.00, 50.00, 18.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 20:34:10'),
(96, 8, 1, 1, 1, 180.00, 0, 0.00, 180.00, 0.00, 500.00, 320.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 20:37:28'),
(97, 8, 1, 1, 1, 96.00, 0, 0.00, 96.00, 0.00, 200.00, 104.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 20:48:47'),
(98, 8, 58, 1, 1, 1000.00, 0, 0.00, 1000.00, 0.00, 1000.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 21:00:40'),
(99, 8, 1, 1, 1, 240.00, 0, 0.00, 240.00, 0.00, 500.00, 260.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 21:07:42'),
(100, 8, 1, 1, 1, 18.00, 0, 0.00, 18.00, 0.00, 18.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-16 21:10:04'),
(101, 8, 1, 1, 1, 140.00, 0, 0.00, 140.00, 0.00, 200.00, 60.00, 0, '0000-00-00', 0, 0, 0, '2021-09-17 19:29:58'),
(102, 8, 1, 1, 1, 155.00, 0, 0.00, 155.00, 0.00, 500.00, 345.00, 0, '0000-00-00', 0, 0, 0, '2021-09-17 19:41:37'),
(103, 8, 1, 1, 1, 155.00, 0, 0.00, 155.00, 0.00, 500.00, 345.00, 0, '0000-00-00', 0, 0, 0, '2021-09-17 19:46:22'),
(104, 8, 1, 1, 1, 122.00, 0, 0.00, 122.00, 0.00, 500.00, 378.00, 0, '0000-00-00', 0, 0, 0, '2021-09-17 21:37:30'),
(105, 8, 1, 1, 1, 218.00, 0, 0.00, 218.00, 0.00, 500.00, 282.00, 0, '0000-00-00', 0, 0, 0, '2021-09-17 22:00:29'),
(106, 8, 1, 1, 1, 124.00, 0, 0.00, 124.00, 0.00, 500.00, 376.00, 0, '0000-00-00', 0, 0, 0, '2021-09-18 15:18:45'),
(107, 8, 1, 1, 1, 124.00, 0, 0.00, 124.00, 0.00, 500.00, 376.00, 0, '0000-00-00', 0, 0, 0, '2021-09-18 15:38:52'),
(108, 8, 1, 1, 1, 86.00, 0, 0.00, 86.00, 0.00, 100.00, 14.00, 0, '0000-00-00', 0, 0, 0, '2021-09-18 15:48:22'),
(109, 8, 1, 1, 1, 218.00, 0, 0.00, 218.00, 0.00, 500.00, 282.00, 0, '0000-00-00', 0, 0, 0, '2021-09-18 17:14:34'),
(110, 1, 1, 1, 1, 742.00, 0, 0.00, 742.00, 0.00, 0.00, 0.00, 1, '2021-09-18', 0, 0, 0, '2021-09-18 17:31:35'),
(111, 8, 1, 1, 1, 90.00, 0, 0.00, 90.00, 0.00, 100.00, 10.00, 0, '0000-00-00', 0, 0, 0, '2021-09-18 20:53:01'),
(112, 8, 1, 1, 1, 56.00, 0, 0.00, 56.00, 0.00, 100.00, 44.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 17:26:51'),
(113, 8, 1, 1, 3, 427.00, 0, 0.00, 427.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 17:30:55'),
(114, 8, 1, 1, 1, 530.00, 0, 0.00, 530.00, 0.00, 540.00, 10.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 17:55:58'),
(115, 8, 1, 1, 1, 48.00, 0, 0.00, 48.00, 0.00, 100.00, 52.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 18:06:30'),
(116, 8, 1, 1, 1, 110.00, 0, 0.00, 110.00, 0.00, 500.00, 390.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 18:08:35'),
(117, 8, 1, 1, 1, 28.00, 0, 0.00, 28.00, 0.00, 28.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 18:48:27'),
(118, 8, 1, 1, 3, 1798.00, 0, 0.00, 1798.00, 0.00, 0.00, 0.00, 1, '2021-09-19', 0, 0, 0, '2021-09-19 19:30:41'),
(119, 8, 1, 1, 3, 960.00, 0, 0.00, 960.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 20:23:36'),
(120, 8, 1, 1, 1, 22.00, 0, 0.00, 22.00, 0.00, 52.00, 30.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 20:53:42'),
(121, 8, 1, 1, 1, 22.00, 0, 0.00, 22.00, 0.00, 52.00, 30.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 20:54:36'),
(122, 8, 1, 1, 3, 544.00, 39, 39.00, 505.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 20:57:25'),
(123, 8, 1, 1, 1, 960.00, 0, 0.00, 960.00, 0.00, 1000.00, 40.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 21:18:45'),
(124, 8, 1, 1, 1, 156.00, 0, 0.00, 156.00, 0.00, 200.00, 44.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 21:36:53'),
(125, 8, 1, 1, 2, 544.00, 49, 49.00, 495.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 21:51:32'),
(126, 8, 1, 1, 3, 1390.00, 140, 140.00, 1250.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 22:26:50'),
(127, 8, 1, 1, 1, 73.00, 0, 0.00, 73.00, 0.00, 500.00, 427.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 22:32:21'),
(128, 1, 1, 1, 1, 295.00, 0, 0.00, 295.00, 0.00, 300.00, 5.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 22:38:06'),
(129, 8, 1, 1, 3, 1798.00, 0, 0.00, 1798.00, 0.00, 1798.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 22:58:18'),
(130, 8, 1, 1, 1, 742.00, 0, 0.00, 742.00, 0.00, 1000.00, 258.00, 0, '0000-00-00', 0, 0, 0, '2021-09-19 23:07:25'),
(131, 1, 1, 1, 1, 140.00, 0, 0.00, 140.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-21 19:16:30'),
(132, 6, 1, 1, 1, 500.00, 0, 0.00, 500.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-23 21:27:35'),
(133, 6, 1, 1, 1, 1500.00, 0, 0.00, 1500.00, 0.00, 1500.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-24 16:32:03'),
(134, 1, 1, 1, 1, 57.00, 0, 0.00, 57.00, 0.00, 500.00, 443.00, 0, '0000-00-00', 0, 0, 0, '2021-09-24 20:29:19'),
(135, 1, 1, 1, 1, 28.00, 0, 0.00, 28.00, 0.00, 28.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-24 20:30:47'),
(136, 1, 1, 1, 1, 76.00, 0, 0.00, 76.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-24 20:32:11'),
(137, 1, 1, 1, 1, 76.00, 0, 0.00, 76.00, 0.00, 76.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-24 20:33:36'),
(138, 1, 1, 1, 1, 84.00, 0, 0.00, 84.00, 0.00, 200.00, 116.00, 0, '0000-00-00', 0, 0, 0, '2021-09-24 20:35:33'),
(139, 6, 1, 1, 1, 1050.00, 105, 105.00, 945.00, 0.00, 1000.00, 55.00, 0, '0000-00-00', 0, 0, 0, '2021-09-24 23:10:34'),
(140, 6, 1, 1, 1, 142.00, 0, 0.00, 142.00, 0.00, 200.00, 58.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 15:36:06'),
(141, 6, 1, 1, 1, 306.00, 0, 0.00, 306.00, 0.00, 500.00, 194.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 15:38:36'),
(142, 6, 1, 1, 1, 73.00, 0, 0.00, 73.00, 0.00, 100.00, 27.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 17:38:47'),
(143, 6, 1, 1, 1, 20.00, 0, 0.00, 20.00, 0.00, 20.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 19:44:27'),
(144, 6, 1, 1, 1, 415.00, 0, 0.00, 415.00, 0.00, 500.00, 85.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 21:13:29'),
(145, 6, 1, 1, 3, 240.00, 0, 0.00, 240.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 22:17:27'),
(146, 6, 1, 1, 1, 626.00, 0, 0.00, 626.00, 0.00, 700.00, 74.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 22:38:29'),
(147, 6, 1, 1, 1, 24.00, 0, 0.00, 24.00, 0.00, 24.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 22:41:40'),
(148, 6, 1, 1, 1, 84.00, 0, 0.00, 84.00, 0.00, 84.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 22:41:59'),
(149, 6, 1, 1, 1, 156.00, 0, 0.00, 156.00, 0.00, 500.00, 344.00, 0, '0000-00-00', 0, 0, 0, '2021-09-25 22:57:24'),
(150, 8, 1, 1, 1, 143.00, 0, 0.00, 143.00, 0.00, 500.00, 357.00, 0, '0000-00-00', 0, 0, 0, '2021-09-26 16:33:47'),
(151, 8, 1, 1, 1, 20.00, 0, 0.00, 20.00, 0.00, 20.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-26 16:36:13'),
(152, 8, 1, 1, 1, 262.00, 0, 0.00, 262.00, 0.00, 262.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-28 18:14:28'),
(153, 8, 1, 1, 1, 197.00, 0, 0.00, 197.00, 0.00, 200.00, 3.00, 0, '0000-00-00', 0, 0, 0, '2021-09-29 18:10:41'),
(154, 8, 1, 1, 1, 350.00, 0, 0.00, 350.00, 0.00, 500.00, 150.00, 0, '0000-00-00', 0, 0, 0, '2021-09-29 18:26:52'),
(155, 8, 1, 1, 1, 28.00, 0, 0.00, 28.00, 0.00, 28.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-09-29 18:29:02'),
(156, 8, 1, 1, 1, 125.00, 0, 0.00, 125.00, 0.00, 200.00, 75.00, 0, '0000-00-00', 0, 0, 0, '2021-09-29 22:04:03'),
(157, 8, 1, 1, 2, 2225.00, 0, 0.00, 2225.00, 0.00, 0.00, 0.00, 1, '2021-10-01', 0, 0, 0, '2021-10-01 19:58:00'),
(158, 8, 1, 1, 1, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-01 19:58:12'),
(159, 8, 1, 1, 2, 2225.00, 0, 0.00, 2225.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-01 19:59:43'),
(160, 8, 1, 1, 1, 0.00, 0, 0.00, 0.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-01 20:05:21'),
(161, 8, 1, 1, 1, 40.00, 0, 0.00, 40.00, 0.00, 40.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-02 18:40:51'),
(162, 8, 1, 1, 1, 6900.00, 690, 690.00, 6210.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-02 20:15:42'),
(163, 8, 1, 1, 1, 554.00, 0, 0.00, 554.00, 0.00, 600.00, 46.00, 0, '0000-00-00', 0, 0, 0, '2021-10-02 22:15:38'),
(164, 8, 1, 1, 2, 5450.00, 230, 230.00, 5220.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-03 18:48:22'),
(165, 8, 1, 1, 2, 181.00, 0, 0.00, 181.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-03 19:10:50'),
(166, 1, 1, 1, 1, 1940.00, 0, 0.00, 1940.00, 0.00, 2000.00, 60.00, 0, '0000-00-00', 0, 0, 0, '2021-10-03 19:17:31'),
(167, 8, 1, 1, 1, 86.00, 0, 0.00, 86.00, 0.00, 500.00, 414.00, 0, '0000-00-00', 0, 0, 0, '2021-10-03 19:19:52'),
(168, 1, 1, 1, 2, 1580.00, 108, 108.00, 1472.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-03 21:52:50'),
(169, 1, 1, 1, 1, 30.00, 0, 0.00, 30.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-03 21:53:29'),
(170, 1, 58, 1, 1, 5500.00, 15.3846, 1000.00, 5500.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 1, 0, 0, '2021-10-03 22:19:11'),
(171, 8, 1, 1, 1, 155.00, 0, 0.00, 155.00, 0.00, 155.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-04 19:32:30'),
(172, 8, 1, 1, 1, 48.00, 0, 0.00, 48.00, 0.00, 50.00, 2.00, 0, '0000-00-00', 0, 0, 0, '2021-10-08 18:30:18'),
(173, 8, 1, 1, 1, 52.00, 0, 0.00, 52.00, 0.00, 52.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-08 18:31:24'),
(174, 8, 1, 1, 1, 24.00, 0, 0.00, 24.00, 0.00, 24.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-08 20:40:16'),
(175, 8, 1, 1, 2, 453.00, 0, 0.00, 453.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-08 21:32:53'),
(176, 9, 1, 1, 1, 166.00, 0, 0.00, 166.00, 0.00, 200.00, 34.00, 0, '0000-00-00', 0, 0, 0, '2021-10-09 17:53:54'),
(177, 9, 1, 1, 1, 38.00, 0, 0.00, 38.00, 0.00, 100.00, 62.00, 0, '0000-00-00', 0, 0, 0, '2021-10-09 18:19:43'),
(178, 1, 1, 1, 1, 145.00, 0, 0.00, 145.00, 0.00, 200.00, 55.00, 0, '0000-00-00', 0, 0, 0, '2021-10-09 22:42:23'),
(179, 8, 1, 1, 1, 193.00, 0, 0.00, 193.00, 0.00, 500.00, 307.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 17:52:54'),
(180, 8, 1, 1, 1, 242.00, 0, 0.00, 242.00, 0.00, 500.00, 258.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 17:57:55'),
(181, 9, 1, 1, 1, 453.00, 32, 32.00, 421.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 19:09:16'),
(182, 8, 1, 1, 1, 133.00, 0, 0.00, 133.00, 0.00, 150.00, 17.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 19:42:49'),
(183, 9, 1, 1, 1, 44.00, 0, 0.00, 44.00, 0.00, 45.00, 1.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 20:10:35'),
(184, 8, 1, 1, 1, 351.00, 0, 0.00, 351.00, 0.00, 500.00, 149.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 20:36:36'),
(185, 8, 1, 1, 1, 2073.00, 0, 0.00, 2073.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 20:43:13'),
(186, 9, 1, 1, 2, 924.00, 0, 0.00, 924.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 21:36:47'),
(187, 8, 1, 1, 1, 121.00, 0, 0.00, 121.00, 0.00, 200.00, 79.00, 0, '0000-00-00', 0, 0, 0, '2021-10-10 22:06:52'),
(188, 1, 1, 1, 1, 2076.00, 0, 0.00, 2076.00, 0.00, 2076.00, 0.00, 1, '2021-10-18', 0, 0, 0, '2021-10-14 17:29:54'),
(189, 1, 1, 1, 1, 390.00, 78, 78.00, 312.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-14 17:40:28'),
(190, 8, 1, 1, 1, 245.00, 0, 0.00, 245.00, 0.00, 500.00, 255.00, 0, '0000-00-00', 0, 0, 0, '2021-10-14 17:45:18'),
(191, 8, 1, 1, 1, 430.00, 0, 0.00, 430.00, 0.00, 500.00, 70.00, 0, '0000-00-00', 0, 0, 0, '2021-10-14 17:46:26'),
(192, 8, 1, 1, 1, 280.00, 0, 0.00, 280.00, 0.00, 500.00, 220.00, 0, '0000-00-00', 0, 0, 0, '2021-10-14 19:48:40'),
(193, 8, 1, 1, 1, 120.00, 0, 0.00, 120.00, 0.00, 150.00, 30.00, 0, '0000-00-00', 0, 0, 0, '2021-10-14 19:58:29'),
(194, 8, 1, 1, 1, 120.00, 0, 0.00, 120.00, 0.00, 120.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-14 20:16:39'),
(195, 8, 1, 1, 1, 66.00, 0, 0.00, 66.00, 0.00, 100.00, 34.00, 0, '0000-00-00', 0, 0, 0, '2021-10-15 19:57:35'),
(196, 9, 1, 1, 1, 202.00, 0, 0.00, 202.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-16 16:24:20'),
(197, 9, 1, 1, 1, 287.00, 0, 0.00, 287.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-16 18:13:37'),
(198, 9, 1, 1, 1, 365.00, 0, 0.00, 365.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-16 18:15:01'),
(199, 9, 1, 1, 1, 365.00, 0, 0.00, 365.00, 0.00, 500.00, 135.00, 0, '0000-00-00', 0, 0, 0, '2021-10-16 18:16:24'),
(200, 9, 1, 1, 1, 12.00, 0, 0.00, 12.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-16 18:23:01'),
(201, 8, 1, 1, 1, 369.00, 0, 0.00, 369.00, 0.00, 369.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-16 19:47:32'),
(202, 9, 1, 1, 1, 326.00, 0, 0.00, 326.00, 0.00, 400.00, 74.00, 0, '0000-00-00', 0, 0, 0, '2021-10-16 20:29:47'),
(203, 8, 1, 1, 1, 45.00, 0, 0.00, 45.00, 0.00, 50.00, 5.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 18:54:16'),
(204, 8, 1, 1, 1, 18.00, 0, 0.00, 18.00, 0.00, 50.00, 32.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 18:55:43'),
(205, 8, 1, 1, 1, 46.00, 0, 0.00, 46.00, 0.00, 50.00, 4.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 18:57:45'),
(206, 9, 1, 1, 1, 22.00, 0, 0.00, 22.00, 0.00, 50.00, 28.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 19:01:59'),
(207, 9, 1, 1, 3, 3385.00, 339, 339.00, 3046.00, 0.00, 0.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 19:41:05'),
(208, 8, 1, 1, 1, 80.00, 0, 0.00, 80.00, 0.00, 80.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 19:57:12'),
(209, 9, 1, 1, 1, 142.00, 15, 15.00, 127.00, 0.00, 127.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 20:12:04'),
(210, 8, 1, 1, 1, 45.00, 0, 0.00, 45.00, 0.00, 500.00, 455.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 20:25:58'),
(211, 8, 1, 1, 1, 250.00, 0, 0.00, 250.00, 0.00, 400.00, 150.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 20:51:08'),
(212, 8, 1, 1, 1, 22.00, 0, 0.00, 22.00, 0.00, 50.00, 28.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 21:23:29'),
(213, 9, 1, 1, 1, 3265.00, 391, 391.00, 2874.00, 0.00, 2900.00, 26.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 22:46:58'),
(214, 8, 1, 1, 1, 38.00, 0, 0.00, 38.00, 0.00, 50.00, 12.00, 0, '0000-00-00', 0, 0, 0, '2021-10-17 22:50:27'),
(215, 1, 1, 1, 1, 2321.00, 0, 0.00, 2321.00, 0.00, 2321.00, 0.00, 0, '0000-00-00', 0, 0, 0, '2021-10-18 17:44:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_temporal`
--

CREATE TABLE `ventas_temporal` (
  `id_venta` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `sucursal` int(11) NOT NULL,
  `metodo` int(1) NOT NULL COMMENT '1 efectivo, 2 credito, 3 debito',
  `subtotal` double NOT NULL,
  `descuento` double NOT NULL COMMENT '0 % 5 % 7%',
  `descuentocant` double NOT NULL,
  `monto_total` double NOT NULL,
  `pagotarjeta` float NOT NULL,
  `efectivo` float NOT NULL,
  `cambio` float NOT NULL,
  `cancelado` int(11) NOT NULL,
  `hcancelacion` date NOT NULL,
  `tipo_venta` int(11) NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1,
  `reg` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas_temporal`
--

INSERT INTO `ventas_temporal` (`id_venta`, `id_personal`, `id_cliente`, `sucursal`, `metodo`, `subtotal`, `descuento`, `descuentocant`, `monto_total`, `pagotarjeta`, `efectivo`, `cambio`, `cancelado`, `hcancelacion`, `tipo_venta`, `activo`, `reg`) VALUES
(1, 6, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '2021-07-24 00:35:11'),
(2, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, '0000-00-00', 0, 0, '2021-08-28 16:47:02'),
(3, 1, 1, 1, 1, 155, 0, 0, 155, 0, 50, 0, 0, '0000-00-00', 0, 0, '2021-08-28 16:51:33');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_abono`
--

CREATE TABLE `venta_abono` (
  `id` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `idpersona` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `fecha` datetime NOT NULL,
  `activo` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE `venta_detalle` (
  `id_detalle_venta` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `tipo` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`id_detalle_venta`, `id_venta`, `id_producto`, `cantidad`, `precio`, `tipo`) VALUES
(2, 1, 1, 1, 100.00, 0),
(3, 2, 1, 1, 100.00, 0),
(4, 3, 1, 1, 100.00, 0),
(5, 4, 89, 1, 1.00, 0),
(6, 5, 89, 1, 1.00, 0),
(7, 6, 89, 1, 1.00, 0),
(8, 7, 89, 1, 1.00, 0),
(9, 9, 45, 6, 210.00, 0),
(10, 10, 141, 2, 27.00, 0),
(11, 11, 101, 2, 40.00, 0),
(12, 11, 102, 1, 40.00, 0),
(13, 11, 99, 2, 57.00, 0),
(14, 11, 100, 1, 57.00, 0),
(15, 12, 141, 2, 27.00, 0),
(16, 13, 94, 1, 38.00, 0),
(17, 14, 107, 2, 24.00, 0),
(18, 15, 93, 1, 1125.00, 0),
(19, 15, 174, 1, 1920.00, 0),
(20, 17, 216, 1, 66.00, 0),
(21, 18, 222, 1, 16.00, 0),
(22, 19, 158, 1, 98.00, 0),
(23, 19, 130, 1, 125.00, 0),
(24, 20, 116, 28, 55.00, 0),
(25, 21, 217, 1, 90.00, 0),
(26, 21, 113, 2, 45.00, 0),
(27, 22, 113, 1, 45.00, 0),
(28, 22, 114, 2, 45.00, 0),
(29, 22, 216, 1, 66.00, 0),
(30, 23, 97, 6, 14.00, 0),
(31, 24, 97, 1, 14.00, 0),
(32, 25, 114, 1, 45.00, 0),
(33, 26, 113, 1, 45.00, 0),
(34, 26, 114, 1, 45.00, 0),
(35, 27, 113, 1, 45.00, 0),
(36, 29, 216, 4, 66.00, 0),
(37, 30, 216, 1, 66.00, 0),
(38, 31, 44, 1, 295.00, 0),
(39, 31, 45, 2, 240.00, 0),
(40, 32, 180, 1, 845.00, 0),
(41, 33, 216, 2, 66.00, 0),
(42, 33, 217, 1, 90.00, 0),
(43, 34, 219, 3, 80.00, 0),
(44, 35, 296, 1, 320.00, 0),
(45, 36, 296, 1, 320.00, 0),
(46, 37, 296, 1, 320.00, 0),
(47, 38, 296, 1, 320.00, 0),
(48, 39, 222, 4, 16.00, 0),
(49, 39, 225, 1, 22.00, 0),
(50, 41, 222, 1, 16.00, 0),
(51, 42, 216, 1, 66.00, 0),
(52, 43, 95, 2, 10.00, 0),
(53, 44, 183, 1, 345.00, 0),
(54, 45, 183, 1, 345.00, 0),
(55, 46, 224, 1, 22.00, 0),
(56, 47, 223, 1, 22.00, 0),
(57, 47, 222, 1, 16.00, 0),
(58, 48, 308, 1, 190.00, 0),
(59, 49, 19, 1, 155.00, 0),
(60, 50, 19, 1, 155.00, 0),
(61, 51, 316, 2, 20.00, 0),
(62, 52, 316, 2, 20.00, 0),
(63, 53, 225, 2, 22.00, 0),
(64, 53, 98, 2, 18.00, 0),
(65, 53, 107, 1, 24.00, 0),
(66, 53, 302, 1, 48.00, 0),
(67, 54, 18, 1, 155.00, 0),
(68, 55, 95, 1, 10.00, 0),
(69, 56, 107, 1, 24.00, 0),
(70, 57, 326, 1, 1000.00, 0),
(71, 58, 327, 1, 500.00, 0),
(72, 60, 262, 1, 3200.00, 0),
(73, 61, 293, 1, 390.00, 0),
(74, 61, 287, 1, 140.00, 0),
(75, 62, 316, 1, 20.00, 0),
(76, 63, 318, 1, 45.00, 0),
(77, 63, 319, 1, 90.00, 0),
(78, 63, 320, 1, 145.00, 0),
(79, 63, 45, 4, 240.00, 0),
(80, 64, 101, 10, 40.00, 0),
(81, 64, 138, 15, 35.00, 0),
(82, 64, 142, 10, 42.00, 0),
(83, 65, 97, 2, 14.00, 0),
(84, 65, 98, 2, 18.00, 0),
(85, 65, 95, 3, 10.00, 0),
(86, 66, 101, 4, 40.00, 0),
(87, 67, 223, 1, 22.00, 0),
(88, 68, 194, 4, 28.00, 0),
(89, 69, 44, 1, 295.00, 0),
(90, 69, 43, 2, 240.00, 0),
(91, 70, 316, 3, 20.00, 0),
(92, 70, 336, 3, 28.00, 0),
(93, 70, 98, 1, 18.00, 0),
(94, 71, 336, 1, 28.00, 0),
(95, 72, 173, 1, 745.00, 0),
(96, 73, 323, 1, 990.00, 0),
(97, 74, 18, 2, 155.00, 0),
(98, 74, 101, 1, 40.00, 0),
(99, 75, 373, 1, 5890.00, 0),
(100, 76, 192, 1, 2950.00, 0),
(101, 77, 282, 1, 80.00, 0),
(102, 78, 384, 1, 89.00, 0),
(103, 79, 194, 1, 28.00, 0),
(104, 79, 100, 1, 57.00, 0),
(105, 80, 139, 6, 35.00, 0),
(106, 81, 95, 2, 10.00, 0),
(107, 82, 216, 1, 66.00, 0),
(108, 83, 321, 1, 195.00, 0),
(109, 84, 238, 1, 14200.00, 0),
(110, 85, 482, 2, 245.00, 0),
(111, 86, 163, 3, 32.00, 0),
(112, 86, 164, 2, 24.00, 0),
(113, 86, 166, 1, 22.00, 0),
(114, 86, 156, 1, 143.00, 0),
(115, 87, 483, 1, 18000.00, 0),
(116, 88, 216, 1, 66.00, 0),
(117, 89, 316, 2, 20.00, 0),
(118, 90, 316, 2, 20.00, 0),
(119, 92, 163, 1, 32.00, 0),
(120, 93, 95, 1, 10.00, 0),
(121, 94, 316, 2, 20.00, 0),
(122, 95, 163, 1, 32.00, 0),
(123, 96, 219, 2, 80.00, 0),
(124, 96, 316, 1, 20.00, 0),
(125, 97, 405, 1, 48.00, 0),
(126, 97, 408, 1, 48.00, 0),
(127, 98, 326, 1, 1000.00, 0),
(128, 99, 147, 2, 120.00, 0),
(129, 100, 457, 1, 18.00, 0),
(130, 101, 287, 1, 140.00, 0),
(131, 102, 19, 1, 155.00, 0),
(132, 103, 19, 1, 155.00, 0),
(133, 104, 221, 1, 36.00, 0),
(134, 104, 399, 1, 58.00, 0),
(135, 104, 195, 1, 28.00, 0),
(136, 105, 390, 1, 190.00, 0),
(137, 105, 195, 1, 28.00, 0),
(138, 108, 317, 1, 26.00, 0),
(139, 108, 316, 3, 20.00, 0),
(140, 109, 353, 1, 128.00, 0),
(141, 109, 217, 1, 90.00, 0),
(142, 110, 131, 1, 115.00, 0),
(143, 110, 117, 1, 58.00, 0),
(144, 110, 147, 1, 120.00, 0),
(145, 110, 195, 1, 28.00, 0),
(146, 111, 319, 1, 90.00, 0),
(147, 112, 336, 2, 28.00, 0),
(148, 113, 130, 1, 189.00, 0),
(149, 113, 217, 1, 90.00, 0),
(150, 114, 293, 1, 390.00, 0),
(151, 114, 287, 1, 140.00, 0),
(152, 115, 220, 1, 48.00, 0),
(153, 116, 94, 1, 38.00, 0),
(154, 116, 349, 1, 72.00, 0),
(155, 117, 336, 1, 28.00, 0),
(156, 119, 45, 4, 240.00, 0),
(157, 120, 225, 1, 22.00, 0),
(158, 121, 225, 1, 22.00, 0),
(159, 122, 105, 1, 355.00, 0),
(160, 122, 130, 1, 189.00, 0),
(161, 123, 45, 4, 240.00, 0),
(162, 124, 216, 2, 66.00, 0),
(163, 124, 315, 2, 12.00, 0),
(164, 125, 105, 1, 355.00, 0),
(165, 125, 130, 1, 189.00, 0),
(166, 126, 176, 1, 1390.00, 0),
(167, 127, 490, 1, 45.00, 0),
(168, 127, 336, 1, 28.00, 0),
(169, 128, 283, 1, 295.00, 0),
(170, 129, 284, 1, 360.00, 0),
(171, 129, 285, 1, 440.00, 0),
(172, 129, 455, 2, 159.00, 0),
(173, 129, 296, 1, 320.00, 0),
(174, 129, 290, 2, 180.00, 0),
(175, 130, 131, 1, 115.00, 0),
(176, 130, 117, 1, 58.00, 0),
(177, 130, 147, 1, 120.00, 0),
(178, 130, 195, 2, 28.00, 0),
(179, 130, 144, 1, 55.00, 0),
(180, 130, 281, 1, 60.00, 0),
(181, 130, 289, 1, 140.00, 0),
(182, 130, 485, 1, 98.00, 0),
(183, 130, 316, 2, 20.00, 0),
(184, 131, 279, 1, 140.00, 0),
(185, 132, 327, 1, 500.00, 0),
(186, 133, 496, 1, 1500.00, 0),
(187, 134, 100, 1, 57.00, 0),
(188, 135, 194, 1, 28.00, 0),
(189, 136, 119, 1, 48.00, 0),
(190, 136, 336, 1, 28.00, 0),
(191, 137, 336, 1, 28.00, 0),
(192, 137, 119, 1, 48.00, 0),
(193, 138, 336, 3, 28.00, 0),
(194, 139, 372, 1, 995.00, 0),
(195, 139, 144, 1, 55.00, 0),
(196, 140, 346, 1, 142.00, 0),
(197, 141, 346, 2, 142.00, 0),
(198, 141, 223, 1, 22.00, 0),
(199, 142, 318, 1, 45.00, 0),
(200, 142, 97, 2, 14.00, 0),
(201, 143, 316, 1, 20.00, 0),
(202, 144, 319, 3, 90.00, 0),
(203, 144, 320, 1, 145.00, 0),
(204, 145, 321, 1, 195.00, 0),
(205, 145, 318, 1, 45.00, 0),
(206, 146, 108, 7, 58.00, 0),
(207, 146, 210, 1, 220.00, 0),
(208, 147, 107, 1, 24.00, 0),
(209, 148, 336, 3, 28.00, 0),
(210, 149, 216, 1, 66.00, 0),
(211, 149, 217, 1, 90.00, 0),
(212, 150, 395, 1, 95.00, 0),
(213, 150, 405, 1, 48.00, 0),
(214, 151, 316, 1, 20.00, 0),
(215, 152, 216, 2, 66.00, 0),
(216, 152, 118, 1, 65.00, 0),
(217, 152, 121, 1, 65.00, 0),
(218, 153, 481, 1, 159.00, 0),
(219, 153, 94, 1, 38.00, 0),
(220, 154, 298, 2, 175.00, 0),
(221, 155, 336, 1, 28.00, 0),
(222, 156, 215, 1, 125.00, 0),
(223, 157, 184, 1, 345.00, 0),
(224, 157, 371, 1, 890.00, 0),
(225, 157, 30, 1, 990.00, 0),
(226, 159, 184, 1, 345.00, 0),
(227, 159, 371, 1, 890.00, 0),
(228, 159, 30, 1, 990.00, 0),
(229, 161, 316, 2, 20.00, 0),
(230, 162, 253, 1, 6900.00, 0),
(231, 163, 147, 2, 120.00, 0),
(232, 163, 169, 2, 90.00, 0),
(233, 163, 168, 1, 45.00, 0),
(234, 163, 464, 1, 89.00, 0),
(235, 164, 235, 2, 2300.00, 0),
(236, 164, 271, 1, 850.00, 0),
(237, 165, 19, 1, 155.00, 0),
(238, 165, 317, 1, 26.00, 0),
(239, 166, 513, 1, 1940.00, 0),
(240, 167, 480, 1, 86.00, 0),
(241, 168, 275, 1, 990.00, 0),
(242, 168, 516, 1, 590.00, 0),
(243, 169, 95, 3, 10.00, 0),
(244, 171, 18, 1, 155.00, 0),
(245, 172, 316, 1, 20.00, 0),
(246, 172, 336, 1, 28.00, 0),
(247, 173, 336, 1, 28.00, 0),
(248, 173, 107, 1, 24.00, 0),
(249, 174, 315, 2, 12.00, 0),
(250, 175, 375, 1, 89.00, 0),
(251, 175, 414, 1, 169.00, 0),
(252, 175, 383, 1, 99.00, 0),
(253, 175, 541, 2, 48.00, 0),
(254, 176, 533, 1, 58.00, 0),
(255, 176, 532, 1, 48.00, 0),
(256, 176, 166, 1, 22.00, 0),
(257, 176, 94, 1, 38.00, 0),
(258, 177, 94, 1, 38.00, 0),
(259, 178, 420, 1, 49.00, 0),
(260, 178, 541, 2, 48.00, 0),
(261, 179, 422, 1, 165.00, 0),
(262, 179, 336, 1, 28.00, 0),
(263, 180, 223, 2, 22.00, 0),
(264, 180, 336, 4, 28.00, 0),
(265, 180, 316, 2, 20.00, 0),
(266, 180, 97, 1, 14.00, 0),
(267, 180, 539, 1, 32.00, 0),
(268, 181, 113, 9, 45.00, 0),
(269, 181, 119, 1, 48.00, 0),
(270, 182, 542, 1, 34.00, 0),
(271, 182, 479, 1, 99.00, 0),
(272, 183, 316, 1, 20.00, 0),
(273, 183, 107, 1, 24.00, 0),
(274, 184, 476, 1, 99.00, 0),
(275, 184, 443, 1, 49.00, 0),
(276, 184, 467, 1, 135.00, 0),
(277, 184, 542, 2, 34.00, 0),
(278, 185, 274, 2, 540.00, 0),
(279, 185, 425, 1, 165.00, 0),
(280, 185, 386, 1, 249.00, 0),
(281, 185, 456, 1, 249.00, 0),
(282, 185, 451, 1, 165.00, 0),
(283, 185, 452, 1, 165.00, 0),
(284, 186, 18, 1, 155.00, 0),
(285, 186, 102, 1, 40.00, 0),
(286, 186, 139, 1, 35.00, 0),
(287, 186, 194, 1, 28.00, 0),
(288, 186, 532, 1, 48.00, 0),
(289, 186, 108, 1, 58.00, 0),
(290, 186, 222, 1, 16.00, 0),
(291, 186, 318, 1, 45.00, 0),
(292, 186, 487, 1, 148.00, 0),
(293, 186, 141, 1, 27.00, 0),
(294, 186, 94, 1, 38.00, 0),
(295, 186, 94, 4, 38.00, 0),
(296, 186, 336, 1, 28.00, 0),
(297, 186, 336, 3, 28.00, 0),
(298, 186, 166, 1, 22.00, 0),
(299, 187, 431, 2, 43.00, 0),
(300, 187, 468, 1, 35.00, 0),
(301, 188, 548, 6, 275.00, 0),
(302, 188, 545, 1, 284.00, 0),
(303, 188, 346, 1, 142.00, 0),
(304, 189, 407, 1, 125.00, 0),
(305, 189, 538, 1, 195.00, 0),
(306, 189, 540, 1, 38.00, 0),
(307, 189, 539, 1, 32.00, 0),
(308, 190, 482, 1, 245.00, 0),
(309, 191, 482, 1, 245.00, 0),
(310, 191, 535, 1, 185.00, 0),
(311, 192, 535, 1, 185.00, 0),
(312, 192, 160, 1, 95.00, 0),
(313, 193, 147, 1, 120.00, 0),
(314, 194, 147, 1, 120.00, 0),
(315, 195, 225, 1, 22.00, 0),
(316, 195, 224, 1, 22.00, 0),
(317, 195, 226, 1, 22.00, 0),
(318, 196, 225, 1, 22.00, 0),
(319, 196, 225, 4, 22.00, 0),
(320, 196, 226, 1, 22.00, 0),
(321, 196, 224, 1, 22.00, 0),
(322, 196, 541, 1, 48.00, 0),
(323, 197, 19, 1, 155.00, 0),
(324, 197, 216, 2, 66.00, 0),
(325, 198, 216, 2, 66.00, 0),
(326, 198, 19, 1, 155.00, 0),
(327, 198, 94, 1, 38.00, 0),
(328, 198, 316, 2, 20.00, 0),
(329, 199, 19, 1, 155.00, 0),
(330, 199, 316, 2, 20.00, 0),
(331, 199, 216, 2, 66.00, 0),
(332, 199, 94, 1, 38.00, 0),
(333, 200, 315, 1, 12.00, 0),
(334, 201, 162, 1, 180.00, 0),
(335, 201, 130, 1, 189.00, 0),
(336, 202, 382, 1, 119.00, 0),
(337, 202, 336, 1, 28.00, 0),
(338, 202, 164, 1, 24.00, 0),
(339, 202, 19, 1, 155.00, 0),
(340, 203, 318, 1, 45.00, 0),
(341, 204, 98, 1, 18.00, 0),
(342, 205, 222, 1, 16.00, 0),
(343, 205, 95, 3, 10.00, 0),
(344, 206, 225, 1, 22.00, 0),
(345, 207, 323, 2, 990.00, 0),
(346, 207, 177, 1, 545.00, 0),
(347, 207, 542, 1, 34.00, 0),
(348, 207, 97, 37, 14.00, 0),
(349, 207, 550, 1, 210.00, 0),
(350, 207, 316, 1, 20.00, 0),
(351, 207, 316, 2, 20.00, 0),
(352, 207, 94, 1, 38.00, 0),
(353, 208, 316, 4, 20.00, 0),
(354, 209, 351, 1, 142.00, 0),
(355, 210, 318, 1, 45.00, 0),
(356, 211, 310, 1, 140.00, 0),
(357, 211, 311, 1, 110.00, 0),
(358, 212, 225, 1, 22.00, 0),
(359, 213, 371, 1, 890.00, 0),
(360, 213, 372, 1, 995.00, 0),
(361, 213, 368, 2, 690.00, 0),
(362, 214, 540, 1, 38.00, 0),
(363, 215, 548, 6, 275.00, 0),
(364, 215, 545, 1, 284.00, 0),
(365, 215, 346, 1, 142.00, 0),
(366, 215, 482, 1, 245.00, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle_temporal`
--

CREATE TABLE `venta_detalle_temporal` (
  `id_detalle_venta` int(11) NOT NULL,
  `id_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` float NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `tipo` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`categoriaId`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClientesId`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `constraint_fk_04` (`id_proveedor`),
  ADD KEY `compra_fk_sucursal` (`idsucursal`);

--
-- Indices de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD PRIMARY KEY (`id_detalle_compra`),
  ADD KEY `constraint_fk_08` (`id_compra`),
  ADD KEY `constraint_fk_09` (`id_producto`);

--
-- Indices de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cotizacion_fk_usuario` (`usuario`);

--
-- Indices de la tabla `cotizacion_partidas`
--
ALTER TABLE `cotizacion_partidas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `partidas_producto` (`productoid`),
  ADD KEY `partidas_fk_cotizacion` (`idcotizacion`);

--
-- Indices de la tabla `cotizacion_terminos`
--
ALTER TABLE `cotizacion_terminos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcotizacion` (`idcotizacion`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`EstadoId`);

--
-- Indices de la tabla `gastos`
--
ALTER TABLE `gastos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idmarca`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`MenuId`);

--
-- Indices de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD PRIMARY KEY (`MenusubId`),
  ADD KEY `fk_menu_sub_menu_idx` (`MenuId`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id_nota`);

--
-- Indices de la tabla `password`
--
ALTER TABLE `password`
  ADD PRIMARY KEY (`idpass`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`perfilId`);

--
-- Indices de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD PRIMARY KEY (`Perfil_detalleId`),
  ADD KEY `fk_Perfiles_detalles_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_Perfiles_detalles_menu_sub1_idx` (`MenusubId`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`personalId`);

--
-- Indices de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD PRIMARY KEY (`personalmenuId`),
  ADD KEY `personal_fkpersona` (`personalId`),
  ADD KEY `personal_fkmenu` (`MenuId`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`productoid`);

--
-- Indices de la tabla `productos_sucursal_precios`
--
ALTER TABLE `productos_sucursal_precios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalles_producto_fk_sucursal` (`idsucursal`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`id_proveedor`),
  ADD KEY `constraint_fk_27` (`id_estado`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`id_ticket`);

--
-- Indices de la tabla `traspasos`
--
ALTER TABLE `traspasos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `traspaso_fk_usuario` (`usuario`),
  ADD KEY `traspaso_fk_sucursal` (`sucursal`);

--
-- Indices de la tabla `traspasos_detalles`
--
ALTER TABLE `traspasos_detalles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalles_fk_traspasos` (`idtraspaso`),
  ADD KEY `detalles_fk_productos` (`productoid`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`),
  ADD KEY `fk_usuarios_Perfiles1_idx` (`perfilId`),
  ADD KEY `fk_usuarios_personal1_idx` (`personalId`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`),
  ADD UNIQUE KEY `id_venta_UNIQUE` (`id_venta`),
  ADD KEY `constraint_fk_30` (`id_cliente`),
  ADD KEY `constraint_fk_31` (`id_personal`),
  ADD KEY `fk_venta_sucursal` (`sucursal`);

--
-- Indices de la tabla `ventas_temporal`
--
ALTER TABLE `ventas_temporal`
  ADD PRIMARY KEY (`id_venta`);

--
-- Indices de la tabla `venta_abono`
--
ALTER TABLE `venta_abono`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abono_fk_ventas` (`id_venta`),
  ADD KEY `abono_fk_personal` (`idpersona`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`id_detalle_venta`),
  ADD UNIQUE KEY `id_detalle_venta_UNIQUE` (`id_detalle_venta`),
  ADD KEY `constraint_fk_15` (`id_producto`),
  ADD KEY `constraint_fk_16` (`id_venta`);

--
-- Indices de la tabla `venta_detalle_temporal`
--
ALTER TABLE `venta_detalle_temporal`
  ADD PRIMARY KEY (`id_detalle_venta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `categoriaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClientesId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  MODIFY `id_detalle_compra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `cotizacion_partidas`
--
ALTER TABLE `cotizacion_partidas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de la tabla `cotizacion_terminos`
--
ALTER TABLE `cotizacion_terminos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `EstadoId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `gastos`
--
ALTER TABLE `gastos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idmarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `MenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  MODIFY `MenusubId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id_nota` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `password`
--
ALTER TABLE `password`
  MODIFY `idpass` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `perfilId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  MODIFY `Perfil_detalleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
  MODIFY `personalId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  MODIFY `personalmenuId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `productoid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=559;

--
-- AUTO_INCREMENT de la tabla `productos_sucursal_precios`
--
ALTER TABLE `productos_sucursal_precios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=609;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `ticket`
--
ALTER TABLE `ticket`
  MODIFY `id_ticket` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `traspasos`
--
ALTER TABLE `traspasos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `traspasos_detalles`
--
ALTER TABLE `traspasos_detalles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT de la tabla `ventas_temporal`
--
ALTER TABLE `ventas_temporal`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `venta_abono`
--
ALTER TABLE `venta_abono`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=367;

--
-- AUTO_INCREMENT de la tabla `venta_detalle_temporal`
--
ALTER TABLE `venta_detalle_temporal`
  MODIFY `id_detalle_venta` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compra_fk_sucursal` FOREIGN KEY (`idsucursal`) REFERENCES `sucursales` (`id`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra_detalle`
--
ALTER TABLE `compra_detalle`
  ADD CONSTRAINT `fk_compra_compra` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`),
  ADD CONSTRAINT `fk_compra_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cotizacion`
--
ALTER TABLE `cotizacion`
  ADD CONSTRAINT `cotizacion_fk_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`UsuarioID`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `cotizacion_partidas`
--
ALTER TABLE `cotizacion_partidas`
  ADD CONSTRAINT `partidas_fk_cotizacion` FOREIGN KEY (`idcotizacion`) REFERENCES `cotizacion` (`id`),
  ADD CONSTRAINT `partidas_producto` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `cotizacion_terminos`
--
ALTER TABLE `cotizacion_terminos`
  ADD CONSTRAINT `cotizacion_terminos_ibfk_1` FOREIGN KEY (`idcotizacion`) REFERENCES `cotizacion` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `menu_sub`
--
ALTER TABLE `menu_sub`
  ADD CONSTRAINT `fk_menu_sub_menu` FOREIGN KEY (`MenuId`) REFERENCES `menu` (`MenuId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles_detalles`
--
ALTER TABLE `perfiles_detalles`
  ADD CONSTRAINT `fk_Perfiles_detalles_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Perfiles_detalles_menu_sub1` FOREIGN KEY (`MenusubId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal_menu`
--
ALTER TABLE `personal_menu`
  ADD CONSTRAINT `personal_fkmenu` FOREIGN KEY (`MenuId`) REFERENCES `menu_sub` (`MenusubId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `personal_fkpersona` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `productos_sucursal_precios`
--
ALTER TABLE `productos_sucursal_precios`
  ADD CONSTRAINT `detalles_producto_fk_producto` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON DELETE NO ACTION,
  ADD CONSTRAINT `detalles_producto_fk_sucursal` FOREIGN KEY (`idsucursal`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `traspasos`
--
ALTER TABLE `traspasos`
  ADD CONSTRAINT `traspaso_fk_sucursal` FOREIGN KEY (`sucursal`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION,
  ADD CONSTRAINT `traspaso_fk_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`UsuarioID`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `traspasos_detalles`
--
ALTER TABLE `traspasos_detalles`
  ADD CONSTRAINT `detalles_fk_productos` FOREIGN KEY (`productoid`) REFERENCES `productos` (`productoid`) ON DELETE NO ACTION,
  ADD CONSTRAINT `detalles_fk_traspasos` FOREIGN KEY (`idtraspaso`) REFERENCES `traspasos` (`id`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_Perfiles1` FOREIGN KEY (`perfilId`) REFERENCES `perfiles` (`perfilId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`personalId`) REFERENCES `personal` (`personalId`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`ClientesId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_presonal` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`personalId`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_venta_sucursal` FOREIGN KEY (`sucursal`) REFERENCES `sucursales` (`id`) ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_abono`
--
ALTER TABLE `venta_abono`
  ADD CONSTRAINT `abono_fk_personal` FOREIGN KEY (`idpersona`) REFERENCES `personal` (`personalId`) ON DELETE NO ACTION,
  ADD CONSTRAINT `abono_fk_ventas` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD CONSTRAINT `fk_detalle_producto` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`productoid`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`id_venta`) REFERENCES `ventas` (`id_venta`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
