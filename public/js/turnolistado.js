var base_url = $('#base_url').val();
var table;
$(document).ready(function($) {
	table = $('#data-tables-turnos').DataTable();
	loadtable();
});
function loadtable(){
	table.destroy();
	table = $('#data-tables-turnos').DataTable({
        stateSave: true,
        responsive: !0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url+"index.php/ListaTurnos/getlistturnos",
            type: "post",
            "data": {
                'personal':0
            },
        },
        "columns": [
            {"data": "id"},
            {"data": "sucursal"},
            {"data": "fecha"},
            {"data": "horaa"},
            {"data": "horac",},
            {"data": "nombre"},
            {"data": "status",
                render:function(data,type,row){
                    var html='';
                    if(row.status=='abierto'){
                        html='<span class="badge badge-success">Abierto</span>';
                    }else if(row.status=='cerrado'){
                        html='<span class="badge badge-danger">Cerrado</span>';
                    }
                    return html;
                }
            },
            
            {"data": null,
                 render:function(data,type,row){
                    $('.tooltipped').tooltip();
                    var html='<button class="btn btn_orange" onclick="consultarturn('+row.id+','+row.sucursalId+')">\
                                    <i class="fa fa-book"></i>\
                              </button>';
                    
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50,100]]
        
    });
}
function consultarturn(id,suc){
    $("#iframeri").modal();
    $.ajax({
      type:'POST',
      url:base_url+'Ventas/consultarturno',
      data:{
        id:id,
        suc:suc
      },
      async:false,
      success:function(data){
        //var array = $.parseJSON(data);
        $('#tbCorte').html(data);
        
      }
    });
    
  }