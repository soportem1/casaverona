var base_url = $('#base_url').val();
var tabla;
var tabla_entrenamiento;
var aux_idempleado=0;
$(document).ready(function() {
    table();
    // eliminar
    $('#sieliminar').click(function(event) {
      var idp =$('#hddIdpro').val();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/deleteproductos',
            data: {id:idp},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                console.log(data);
                toastr.success('Hecho!', 'eliminado Correctamente');
                reload_registro();
            }
        });
    });

    $('#imprimiretiqueta').click(function(event) {
      var idp=$('#idproetiqueta').val();
      var nump=$('#numprint').val();
      $('#iframeetiqueta').html('<iframe src="'+base_url+'Etiquetas?id='+idp+'&page='+nump+'&print=true"></iframe>');
    });
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
    var sucu=$('#idsucursal').val();
    $('.tabla_productos').html('<table class="table table-striped" id="data_tables">\
        <thead>\
          <tr>\
            <th></th>\
            <th>Código</th>\
            <th>Nombre</th>\
            <th>Precio</th>\
            <th>Existencia</th>\
            <th></th>\
          </tr>\
        </thead>\
        <tbody>\
        </tbody>\
      </table>');
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado",
            type: "post",
            //"data":{empleado:$('#empleado_busqueda').val()},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"productoid"},
            {"data":"codigo"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    //if(sucu==1){
                    //    html='<button class="btn btn-raised btn-icon btn_amarillo" onclick="modal_precio('+row.productoid+')"><i class="fa fa-money"></i></button>';
                    //}else{
                        html=row.precioventa;
                    //}
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    //if(sucu==1){
                        //html='<button class="btn btn-raised btn-icon btn_orange" onclick="modal_existencia('+row.productoid+')"><i class="fa fa-cubes"></i></button>';
                    //}else{
                        html=row.stock;
                    //}    
                    return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                var cm="'";
                    if(sucu==1){
                        html+='<div class="form-group">\
                            <div class="btn-group" role="group" aria-label="Basic example">\
                                <a class="btn btn-raised btn-icon btn_amarillo" href="'+base_url+'Productos/productosadd?id='+row.productoid+'-1"><i class="fa fa-eye"></i></a>\
                                <button class="btn btn-raised btn-icon btn_amarillo" onclick="etiquetas('+row.productoid+','+row.codigo+','+cm+row.nombre+cm+','+cm+row.categoria+cm+','+row.precioventa+')"><i class="fa fa-barcode"></i></button>\
                                <a class="btn btn-raised btn-icon btn_orange" href="'+base_url+'Productos/productosadd?id='+row.productoid+'-0"><i class="ft-edit"></i></a>\
                                <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="productodelete('+row.productoid+');"><i class="ft-trash-2"></i></button>\
                            </div>\
                        </div>';
                    }else{
                        html+='';
                    }
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function etiquetas(id,codigo,nombre,categoria,precio){
    $('#ecodigo').html(codigo);
    $('#eproducto').html(nombre);
    $('#ecategoria').html(categoria);
    $('#eprecio').html(precio);
    $('#idproetiqueta').val(id);
    $("#modaletiquetas").modal();
    $('#iframeetiqueta').html('<iframe src="'+base_url+'Etiquetas?id='+id+'&page=1"></iframe>'); 
}
function productodelete(id){
    $('#hddIdpro').val(id);
    $('#eliminacion').modal();  
}

function productosall(){
    $("#modalproductos").modal();
    $('#iframeproductos').html('<iframe src="'+base_url+'Visorpdf?filex=Productosall&iden=id&id=0" style="height: 500px "></iframe>'); 
}
function modal_precio(id){
    $('#precio_sucu').modal();  
    // ajax
    $.ajax({
        type:'POST',
        url: base_url+'Productos/get_precios_sucu',
        data: {id:id},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.tabla_precios').html(data);
        }
    });
}

function modal_existencia(id){
    $('#existencia_sucu').modal();  
    // ajax
    $.ajax({
        type:'POST',
        url: base_url+'Productos/get_existencias_sucu',
        data: {id:id},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.tabla_existencias').html(data);
        }
    });
}

