var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});
function table(){
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Listado_traspasos/getlistado",
            type: "post",
            //"data":{idsucursal:$('#idsucursal').val()},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = row.fecha+' '+row.hora;
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html ='<button type="button" class="btn btn-raised btn-icon btn_amarillo" onclick="ver_resumen('+row.id+')"><i class="fa fa-eye"></i></button>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function ver_resumen(id){
    $('#ver_resumen_modal').modal();
    tablas_producto(id);
}


function tablas_producto(id){
    var html='<table class="table table-striped" id="data_tables_productos" style="width: 100%">\
                <thead>\
                  <tr>\
                    <th>Nombre</th>\
                    <th>Cantidad</th>\
                  </tr>\
                </thead>\
                <tbody id="data_tables_productos_tbody">\
                </tbody>\
              </table>';
    $('.tabla_productos').html(html);
    $.ajax({
        type:'POST',
        url: base_url+'Listado_traspasos/tabla_produtos',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('#data_tables_productos_tbody').html(data);
        }
    });

}