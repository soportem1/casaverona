var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Personal/getlistado",
            type: "post",
            //"data":{empleado:$('#empleado_busqueda').val()},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"personalId"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html=row.nombre+' '+row.apellidos;
                return html;
                }
            },
            {"data":"Usuario"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                var cm="'";
                    html+='<div class="form-group">\
                        <div class="btn-group" role="group" aria-label="Basic example">\
                            <a class="btn btn-raised btn-icon btn_orange" href="'+base_url+'Personal/Personaladd?id='+row.personalId+'"><i class="ft-edit"></i></a>\
                            <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="eliminar_update('+row.personalId+');"><i class="ft-trash-2"></i></button>\
                        </div>\
                    </div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function eliminar_update(id){
    $('#eliminar_registro').modal();
    $('#id_per').val(id);
}

function personaldelete(){
    var id=$('#id_per').val();
    $.ajax({
            type:'POST',
            url: base_url+'Personal/deletepersonal',
            data: {id:id},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                toastr.success('eliminado Correctamente','Hecho!' );
                tabla.ajax.reload();
                $('#eliminar_registro').modal('hide');
            }
        });
}