var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Cotizaciones/getlistado",
            type: "post",
            //"data":{empleado:$('#empleado_busqueda').val()},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.cliente==0){
                        html=row.contacto;
                    }else{
                        html=row.nombre;
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var cant=parseFloat(row.total);
                    var html ='$'+cant.toFixed(2);
                return html;
                }
            },
            {"data":"fecha"},
            {"data":"Usuario"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = '';
                    if(row.estatus==0){
                        html = '<span class="badge btn_orange border_eti">Pendiente</span>';
                    }else if(row.estatus==1){
                        html = '<span class="badge bg-success border_eti">Aceptada</span>';
                    }else if(row.estatus==2){
                        html = '<span class="badge btn-danger border_eti">Rechazada</span>';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                var cm="'";
                    html+='<div class="form-group">\
                        <div class="btn-group" role="group" aria-label="Basic example">\
                            <a class="btn btn-raised btn-icon btn_orange" href="'+base_url+'Cotizaciones/registro/'+row.id+'"><i class="ft-edit"></i></a>\
                            <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="eliminar_update('+row.id+');"><i class="ft-trash-2"></i></button>\
                            <a class="btn btn-raised btn-icon btn_amarillo" target="block_" href="'+base_url+'Cotizaciones/documento/'+row.id+'"><i class="ft-printer"></i></a>';
                        if(row.estatus==0){
                    html+='<a class="btn btn-raised btn-icon btn-success white idcoti_'+row.id+'" title="Aceptar"\
                            data-uncluir="'+row.uncluir+'" data-total="'+row.total+'"\
                            data-subtotal="'+row.subtotal+'" data-iva="'+row.iva+'"\
                            onclick="modal_aceptar('+row.id+')"><i class="ft-check-square"></i></a>\
                            <a class="btn btn-raised btn-icon btn-danger white" title="Rechazar" onclick="modal_cancelar('+row.id+')"><i class="ft-x-square"></i></a>';
                        }
                 html+='</div>\
                    </div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function eliminar_update(id){
    $('#eliminar_registro').modal();
    $('#id_aux').val(id);
}

function delete_registro(){
    var id=$('#id_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'Cotizaciones/deleteregistro',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            tabla.ajax.reload();
            $('#eliminar_registro').modal('hide');
        }
    });
}
var id_cancelar=0;
function modal_cancelar(id){
    $('#cancelar_registro').modal();
    id_cancelar=id;
}

function rechazar_registro(){
    var id=id_cancelar;
    $.ajax({
        type:'POST',
        url: base_url+'Cotizaciones/rechazarregistro',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('Rechazada con éxito','Hecho!' );
            tabla.ajax.reload();
            $('#cancelar_registro').modal('hide');
        }
    });
}
var id_aceptar=0;
function modal_aceptar(id){
    $('#aceptar_registro').modal();
    id_aceptar=id;
    tablas_producto(id);
    var uncluir = $('.idcoti_'+id).data('uncluir');
    if(uncluir=='on'){
        $('.subtotal_p').css('display','block');
        $('.iva_p').css('display','block');
    }else{
        $('.subtotal_p').css('display','none');
        $('.iva_p').css('display','none');
    }
    $('.subtotal_p_total').html($('.idcoti_'+id).data('subtotal'));
    $('.iva_p_total').html($('.idcoti_'+id).data('iva'));
    $('.total_p_total').html($('.idcoti_'+id).data('total'));
    $('.total_p_total2').html($('.idcoti_'+id).data('total'));
    
}
function tablas_producto(id){
    var html='<table class="table table-striped" id="data_tables_productos" style="width: 100%">\
                <thead>\
                  <tr>\
                    <th>Nombre</th>\
                    <th>Cantidad</th>\
                    <th>Stock</th>\
                    <th>Precio</th>\
                    <th>Subtotal</th>\
                  </tr>\
                </thead>\
                <tbody id="data_tables_productos_tbody">\
                </tbody>\
              </table>';
    $('.tabla_productos').html(html);
    $.ajax({
        type:'POST',
        url: base_url+'Cotizaciones/tabla_produtos',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('#data_tables_productos_tbody').html(data);
        }
    });

}

function descuento_cotizacion(){
    var descuento = $('#descuento').val();
    var desc=parseFloat(descuento);
    var entre=parseFloat(desc/100); 
    var total = $('.total_p_total2').text();
    var ttl=parseFloat(total*entre);
    var resta =parseFloat(total-ttl);
    $('.total_p_total').html(resta.toFixed(2));
    $('.cantdescuento').html(ttl.toFixed(2));    
}

function aceptar_registro(){
    $.ajax({
        type:'POST',
        url: base_url+'Cotizaciones/registrar_cotizacion',
        data: {id:id_aceptar,mpago:$('#mpago option:selected').val(),
                total_p_total:$('.total_p_total').text(),
                descuento:$('#descuento').val(),
                cantdescuento:$('.cantdescuento').text(),
        },
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            if(array.validar==0){
                registrar_productos(array.id_venta,id_aceptar);
                toastr.success('Guardado Correctamente','Hecho!');
                tabla.ajax.reload();
                $('#aceptar_registro').modal('hide');
                $('#aceptar_cotizacion').modal('hide');
            }else{
                toastr.error('Uno de los productos le faltan existencias', '¡Atención!');
            }
        }
    });
}

function registrar_productos(id,idcot){
    $.ajax({
        type:'POST',
        url: base_url+'Cotizaciones/ingresarventapro',
        data: {id:id,idcot:idcot},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
        }
    });
}

function aceptar_cotizacion(){
    $('#aceptar_cotizacion').modal();
}