var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
    var sucursal = $('#idsucursal option:selected').val();
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Listacompras/getlistado",
            type: "post",
            "data":{fechain:$('#productoscin').val(),fechafin:$('#productoscfin').val(),sucursal:sucursal
            },
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id_compra"},
            {"data":"reg"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var cant=parseFloat(row.monto_total);
                    var html ='$'+cant.toFixed(2);
                return html;
                }
            },
            {"data":"sucursal"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = '<a class="btn btn-raised btn-icon btn_orange white" onclick="modal_detalles('+row.id_compra+')" title="Ticket"><i class="fa fa-eye"></i></a>\
                                <a class="btn btn-raised btn-icon btn_amarillo white" onclick="ticket('+row.id_compra+')" title="Ticket"><i class="fa fa-book"></i></a>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function modal_detalles(id){
    $('#modal_detalle').modal();
    $.ajax({
        type:'POST',
        url: base_url+'Listacompras/get_tabla_detalles',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('.tbody_lcompras_detalle').html(data);
        }
    });
    
}


  function ticket(id){
    $("#iframeri").modal();
    $('.iframereporte').html('<iframe src="' + base_url + 'Ticket/ticket_compras/'+id+'" class="iframeprintc1" id="iframeprintc1"></iframe>');
    //$('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+id+'"></iframe>');
  }