var base_url = $('#base_url').val();
var sucursal_aux = $('#sucursal_aux').val();
var tabla;
var tipo_aux=0;
var id_aux_pass=0;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Gastos/getlistado",
            type: "post",
            //"data":{empleado:$('#empleado_busqueda').val()},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    return  '$'+ Number.parseFloat(row.monto).toFixed(2);
                }
            },
            {"data":"fecha_gas"},
            {"data":"Usuario"},
            {"data":"sucursal"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                var cm="'";
                    if(sucursal_aux==1){
                        html+='<div class="form-group">\
                            <div class="btn-group" role="group" aria-label="Basic example">\
                                <a class="btn btn-raised btn-icon btn_orange gas_'+row.id+'"\
                                    data-idgasto="'+row.id+'"\
                                    data-gasto="'+row. nombre+'"\
                                    data-monto="'+row.monto+'"\
                                    data-fecha="'+row.fecha+'"\
                                    data-idsucursal="'+row.idsucursal+'"\
                                   style="color:white !important" onclick="registro_modal('+row.id+')"><i class="ft-edit"></i></a>\
                                <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="eliminar_update('+row.id+');"><i class="ft-trash-2"></i></button>\
                            </div>\
                        </div>';
                    }else{
                        html+='<div class="form-group">\
                            <div class="btn-group" role="group" aria-label="Basic example">\
                                <a class="btn btn-raised btn-icon btn_orange gas_'+row.id+'"\
                                    data-idgasto="'+row.id+'"\
                                    data-gasto="'+row. nombre+'"\
                                    data-monto="'+row.monto+'"\
                                    data-fecha="'+row.fecha+'"\
                                    data-idsucursal="'+row.idsucursal+'"\
                                   style="color:white !important" onclick="registro_modal_pass('+row.id+',1)"><i class="ft-edit"></i></a>\
                                <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="eliminar_update_pass('+row.id+',0);"><i class="ft-trash-2"></i></button>\
                            </div>\
                        </div>';
                    }
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}
function eliminar_update(id){
    $('#eliminar_registro').modal();
    $('#id_aux').val(id);
}

function delete_registro(){
    var id=$('#id_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'Gastos/deleteregistro',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            tabla.ajax.reload();
            $('#eliminar_registro').modal('hide');
        }
    });
}
function registro_modal(id){
	$('#registro_modal').modal();
    if(id==0){
        $('#idgasto').val(0);
        $('#gasto').val('');
        $('#monto').val('');
        $('#fecha').val('');
    }else{
        $('#idgasto').val($('.gas_'+id).data('idgasto'));
        $('#gasto').val($('.gas_'+id).data('gasto'));
        $('#monto').val($('.gas_'+id).data('monto'));
        $('#fecha').val($('.gas_'+id).data('fecha'));
        $('#idsucursal option:selected').removeAttr('selected');
        $("#idsucursal option[value='"+$('.gas_'+id).data('idsucursal')+"']").attr("selected", true);
    }
}

function registrar(){
    var form_register = $('#formgastos');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            monto:{
              required: true
            },
            fecha:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#formgastos").valid();
    if($valid) {
        $('.btn_registro').attr('disabled',true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Gastos/registro_gasto',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                var id=data;
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    reload_registro();
                    $('#idgasto').val(0);
                    $('#gasto').val('');
                    $('#monto').val('');
                    $('#fecha').val('');
                    $('.btn_registro').attr('disabled',false);
                }, 1000); 
                $('#registro_modal').modal('hide');
                $('.vd_red').remove();
            }
        });
    }   
}

function registro_modal_pass(id,tipo){
    $('#modal_verificar').modal();  
    id_aux_pass=id; 
    tipo_aux=tipo;
}
function eliminar_update_pass(id,tipo){
    $('#modal_verificar').modal();  
    id_aux_pass=id; 
    tipo_aux=tipo;
}
function verificar_pass(){
    var pass=$('#passw').val();
    if(pass!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Gastos/verificar',
            data: {pass:pass},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                if(data==1){
                    if(tipo_aux==1){
                        registro_modal(id_aux_pass);
                    }else{
                        eliminar_update(id_aux_pass);
                    }  
                    $('#modal_verificar').modal('hide');  
                }else{
                    toastr.error('Contraseña incorrecta', '!Atención!');
                }
            }
        });
    }else{
        toastr.error('El campo esta vacío', '!Atención!');
    }
}