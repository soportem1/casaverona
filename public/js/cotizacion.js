var base_url = $('#base_url').val();
var cont_aux=0;
var proselected_length=0;
var proselected=0;
var contpar_aux=0;
$(document).ready(function () {
    //=====================================================================================================
    $('#guardar_btn').click(function(event) {
        var form_register = $('#form_regitro');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                cliente:{
                  required: true
                },  
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        var $valid = $("#form_regitro").valid();
        console.log($valid);
        if($valid) {
            $('.btn_registro').attr('disabled',true);
            //var check_iva = '';
            /*
            if($('#check_iva').is(':checked')){
                check_iva = 'on';
            }else{
                check_iva = '';
            }
            var datos = $('#form_regitro').serialize()+'&uncluir='+check_iva+'&subtotal='+$('.subtotal_p_total').text()+'&iva='+$('.iva_p_total').text()+'&total='+$('.total_p_total').text();
            */
            var datos = $('#form_regitro').serialize()+'&total='+$('.total_p_total').text();
            $.ajax({
                type:'POST',
                url: base_url+'Cotizaciones/registrar',
               // data: {id:id,nom:nom,ape:ape,perf:perf,mail:email,tcel:telcel,tcasa:telcasa,call:calle,next:noexte,est:estado,nint:noint,col:colonia,mun:municipio,cp:cop},
                data: datos,
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    var idpro=data;
                    toastr.success('Hecho!', 'Guardado Correctamente');
                    //=====================
                    //=====================
                    
                    setTimeout (function(){ 
                    	location.href=base_url+'Cotizaciones';
                    }, 2000);
                    guardar_terminos(idpro);
                    guardar_partidas(idpro);
                }
            });
        }
    });
    /// Folio consecutivo
    folio();
    var terminos_aux=$('#terminos_aux').val();
    var idcoti=$('#idcoti').val();
    if(terminos_aux==1){
      tabla_terminos_codiciones(idcoti);
    }else{
      agregar_terminos();
    } 
    var partidas_aux=$('#partidas_aux').val();
    if(partidas_aux==1){
      tabla_partidas(idcoti);
    }
    // Partidas
    $( "#productos" ).focus();
    $( "#productos" ).keypress(function(e) {
        if (e.which==13) {
            productos_buscar(1);
        }else{
            setTimeout(function(){ 
                productos_buscar(0);
            }, 900);
            
        }
    });
    $(document).keydown(function(e) {
        console.log(e.which)
        switch(e.which) { 
            case 38:
                navigate('up');
            break;
            case 40:
                navigate('down');
            break;
            case 13:
                navigate('enter');

            break;
        }
    });
    //incluir_iva(); 
    ///////////////////////////////
    $('#vcliente').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un cliente',
        ajax: {
            url: base_url+'Cotizaciones/searchcli',
            dataType: "json",
            data: function (params) {
            var query = {
                search: params.term,
                type: 'public'
            }
            return query;
        },
        processResults: function(data){
            var clientes=data;
            var itemscli = [];
            data.forEach(function(element) {
                itemscli.push({
                    id: element.ClientesId,
                    text: element.Nom
                });
            });
            return {
                results: itemscli
            };          
        },  
      }
    });
});

function pad (str, max) { str = str.toString(); return str.length < max ? pad("0" + str, max) : str; }

function folio(){
    var num_emp=$('#num_folio').val();
    var anio=$('#anio').val();
    $('#folio').val('SP'+anio+pad(num_emp,4)); 
    $('#folio_aux').val('SP-'+anio+'-'+num_emp);
}
function agregar_terminos(){
    add_terminos(0,'');
}
var cont=1;
function add_terminos(id,nombre){
    var html='<div class="row row_'+cont+'">\
                <div class="col-md-1" align="right">\
                  <p>'+cont+'.-</p>\
                </div>\
                <div class="col-md-10">\
                  <input type="hidden" id="idterminox" value="'+id+'">\
                  <input type="text" class="form-control" id="nombre_terminox" value="'+nombre+'">\
                </div>\
                <div class="col-md-1">';
                if(cont==1){
                  html+='<a class="btn btn-round btn_orange" style="color:white" onclick="agregar_terminos()"><i class="fa fa-plus-square"></i></a>';
                }else{
                  html+='<a class="btn btn-round btn_amarillo" style="color:white" onclick="delete_terminos('+cont+','+id+')"><i class="fa fa-minus-square"></i></a>';
                }
                html+='</div>\
              </div>';
    $('.tabla_terminos2').append(html);
    cont++;
}
function delete_terminos(cont,id){
    if(id==0){
        $('.row_'+cont).remove();
    }else{
        $('#eliminar_terminos').modal();
        $('#id_termino_aux').val(id)
        cont_aux=cont;
    }
}

function delete_registro_termino(){
    var id=$('#id_termino_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'Cotizaciones/deleteregistro_termino',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            $('#eliminar_terminos').modal('hide');
            $('.row_'+cont_aux).remove();

        }
    });
}

function guardar_terminos(id){
    var DATAP= [];
    var TABLAP = $(".tabla_terminos2 > div");                  
    TABLAP.each(function(){         
          item = {};
          item ["idcotizacion"]=id;
          item ["idtermino"]=$(this).find("input[id*='idterminox']").val();
          item ["termino"]=$(this).find("input[id*='nombre_terminox']").val();
          DATAP.push(item);
    });
    INFOP  = new FormData();
    aInfop   = JSON.stringify(DATAP);
    INFOP.append('data', aInfop);
    $.ajax({
        data: INFOP,
        type: 'POST',
        url : base_url+'Cotizaciones/insert_terminos',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data2){
                //toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                //toastr.error('Error', '500');
            }
        },
        success: function(data2){
          
        }
    }); 
}

function tabla_terminos_codiciones(id){
    $.ajax({
        type:'POST',
        url: base_url+"Cotizaciones/get_terminos_condiciones",
        data: {id:id},
        success:function(data){
            var array = $.parseJSON(data);
            if(array.length>0) {
                array.forEach(function(element){
                  add_terminos(element.id,element.nombre);
                });
            }else{
                agregar_terminos();
            }   
        }
    });
}
/// Partidas
function productos_buscar(tipo){
    var productos_b=$('#productos').val();
    if(productos_b!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Cotizaciones/buscar_producto',
            data:{
                producto:productos_b,
                idsucursal:1
                },
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){ 
                    console.log(data); 
                    var array = $.parseJSON(data);
                    if (tipo==0) {
                        $('.producto_buscar_t').html('');
                        array.forEach(function(item) {
                            var html='<a data-nombre="'+item.nombre+'" \
                                        data-precioventa="'+item.precioventa+'" \
                                        data-especial1_precio="'+item.especial1_precio+'" \
                                        data-especial2_precio="'+item.especial2_precio+'" \
                                        data-mediomayoreo="'+item.mediomayoreo+'" \
                                        data-mayoreo="'+item.mayoreo+'" \
                                        onclick="agregar_producto('+item.productoid+')" \
                                        class="btn btn_orange_borde pro_'+item.productoid+' proselected" style="width: 100%;">'+item.codigo+' / '+item.nombre+'</a><br>';
                            $('.producto_buscar_t').append(html);
                        });
                        proselected_length=$('.proselected').length;
                        proselected=-1;

                    }else{
                        if (array.length==1) {
                            array.forEach(function(item) {
                                add_producto(0,item.productoid,item.nombre,0,item.precioventa,item.especial1_precio,item.especial2_precio,item.mediomayoreo,item.mayoreo,0);
                            });
                        }else{

                        }
                    }
            }
        });
    }else{
        setTimeout(function(){ 
            $('.producto_buscar_t').html('');
        }, 1000);
    }
}

function agregar_producto(idpro){
    var nombre=$('.pro_'+idpro).data('nombre');
    var precio=$('.pro_'+idpro).data('precioventa');

    var especial1_precio=$('.pro_'+idpro).data('especial1_precio');
    var especial2_precio=$('.pro_'+idpro).data('especial2_precio');
    var mediomayoreo=$('.pro_'+idpro).data('mediomayoreo');
    var mayoreo=$('.pro_'+idpro).data('mayoreo');

    add_producto(0,idpro,nombre,0,precio,especial1_precio,especial2_precio,mediomayoreo,mayoreo,0);
    setTimeout(function(){ 
        $('.producto_buscar_t').html('');
    }, 100);
    $('#productos').val('');
}


var cont2=0;
function add_producto(idpartida,idpro,nombre,cantidad,precio,especial1_precio,especial2_precio,mediomayoreo,mayoreo,subtotal){
    var html='';
    var selecttxt='<select id="tipo_preciox" class="form-control tipo_precio_'+cont2+'" onchange="tipo_precio_select('+cont2+')">\
                    <option value="1-'+precio+'">Precio venta: '+precio+'</option>\
                    <option value="2-'+especial1_precio+'">Precio especial 1: '+especial1_precio+'</option>\
                    <option value="3-'+especial2_precio+'">Precio especial 2: '+especial2_precio+'</option>\
                    <option value="4-'+mediomayoreo+'">Precio medio mayoreo: '+mediomayoreo+'</option>\
                    <option value="5-'+mayoreo+'">Precio mayoreo: '+mayoreo+'</option>\
                </select>';
    html='<tr class="row_p_'+cont2+'">\
          <td>'+nombre+'\
            <input type="hidden" id="idpartidax" value="'+idpartida+'">\
            <input type="hidden" id="productoidx" value="'+idpro+'">\
          </td>\
          <td><input type="number" id="cantidadx" class="form-control cantidad_s_'+cont2+'" oninput="ope_subtotal('+cont2+')" value="'+cantidad+'"></td>\
          <td>'+selecttxt+'\
          </td>\
          <td><input type="number" id="preciox" class="form-control preciox_'+cont2+'" value="'+precio+'"></td>\
          <td><span class="subtotal_'+cont2+'">$'+subtotal+'</span>\
             <input type="hidden" id="subtotalx" class="subtotalx_'+cont2+'" value="'+subtotal+'">\
          </td>\
          <td><button type="button" class="btn btn-raised btn-icon btn_crema" onclick="remove_producto('+cont2+','+idpartida+')""><i class="ft-trash-2"></i></button></td>\
        </tr>';
    $('#data_tables_productos').append(html);    
    cont2++;
    suma_total_ope();
    setTimeout(function(){ 
        $('#productos').val('');
        $('.producto_buscar_t').html('');
    }, 100);
}

function remove_producto(cont,idp){
    if(idp==0){
        $('.row_p_'+cont).remove();
    }else{
        $('#eliminar_partida').modal();
        $('#id_partida_aux').val(idp)
        contpar_aux=cont;
    }
    suma_total_ope();
}

function delete_registro_partidas(){
    var id=$('#id_partida_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'Cotizaciones/deleteregistro_partida',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            $('#eliminar_partida').modal('hide');
            $('.row_p_'+contpar_aux).remove();
            suma_total_ope();
        }
    });
}


function navigate(direction) {
    if(proselected_length>0){
        if(direction == 'up') {//subir
            $(".proselected").removeClass('selected');
            if (proselected==0 ||proselected==-1) {
                proselected=0;
            }else{
                proselected--;
            }
            
        
        } else if (direction == 'down') {//bajar
            $(".proselected").removeClass('selected');
            proselected++;
            if(proselected>=proselected_length){
                proselected=(proselected_length-1);
            }
        }else if (direction == 'enter') {
            $(".proselected.selected").click();
        }
    }
    
    //console.log(proselected);
    var valor=$(".proselected").eq(proselected).addClass('selected');
    //console.log(valor);
}

function tipo_precio_select(id){
    var precio = $('.tipo_precio_'+id+' option:selected').val(); 
    cadena = precio.split('-');
    $('.preciox_'+id).val(cadena[1]);

}
function ope_subtotal(id){
    var cantidad=$('.cantidad_s_'+id).val();
    var cant=parseFloat(cantidad);
    var precio=$('.preciox_'+id).val();
    var prec=parseFloat(precio);
    var suma_total=parseFloat(cant*prec);
    $('.subtotalx_'+id).val(suma_total);
    var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(suma_total);
    $('.subtotal_'+id).html(monto);
    suma_total_ope();
}

function replaceAll( text, busca, reemplaza ){
  while (text.toString().indexOf(busca) != -1)
      text = text.toString().replace(busca,reemplaza);
  return text;
}

function guardar_partidas(id){
    var DATAP= [];
    var TABLAP = $("#data_tables_productos tbody > tr");                  
    TABLAP.each(function(){         
          item = {};
          item ["idcotizacion"]=id;
          item ["idpartida"]=$(this).find("input[id*='idpartidax']").val();
          item ["productoid"]=$(this).find("input[id*='productoidx']").val();
          item ["cantidad"]=$(this).find("input[id*='cantidadx']").val();
          item ["precio"]=$(this).find("input[id*='preciox']").val();
          item ["subtotal"]=$(this).find("input[id*='subtotalx']").val();
          item ["tipo_precio"]=$(this).find("select[id*='tipo_preciox'] option:selected").val();
          DATAP.push(item);
    });
    INFOP  = new FormData();
    aInfop   = JSON.stringify(DATAP);
    INFOP.append('data', aInfop);
    $.ajax({
        data: INFOP,
        type: 'POST',
        url : base_url+'Cotizaciones/insert_partidas',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data2){
                //toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                //toastr.error('Error', '500');
            }
        },
        success: function(data2){
          
        }
    }); 
}

function tabla_partidas(id){
    $.ajax({
        type:'POST',
        url: base_url+"Cotizaciones/get_partidas",
        data: {id:id},
        success:function(data){
            var array = $.parseJSON(data);
            if(array.length>0) {
                array.forEach(function(element){
                  add_producto(element.id,element.productoid,element.nombre,element.cantidad,element.precio,element.especial1_precio,element.especial2_precio,element.mediomayoreo,element.mayoreo,element.subtotal);
                });
            }   
        }
    });
}

/*
function incluir_iva(){
    if($('#check_iva').is(':checked')){
        $('.subtotal_p').css('display','block');
        $('.iva_p').css('display','block');
    }else{
        $('.subtotal_p').css('display','none');
        $('.iva_p').css('display','none');
    }
}
*/

function suma_total_ope(){
    var TABLA   = $("#data_tables_productos tbody> tr");
    var addtp=0;
    TABLA.each(function(){   
        var total_precio = $(this).find("input[id*='subtotalx']").val();
        var vstotal = total_precio;
        addtp += Number(vstotal);   
    });
    /*
    if($('#check_iva').is(':checked')){
        var cantidad=parseFloat(addtp);
        var iva=parseFloat(cantidad*.16);
        var suma=parseFloat(cantidad+iva); 
        $('.subtotal_p_total').html(cantidad);
        $('.iva_p_total').html(iva);
        $('.total_p_total').html(Number(suma).toFixed(2));
    }else{
    */
        $('.total_p_total').html(Number(addtp).toFixed(2));
    //}

}