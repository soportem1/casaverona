var base_url = $('#base_url').val();
var edi_cate=0;
var edi_marca=0;
$(document).ready(function () {
    //=====================================================================================================
     $('#savepr').click(function(event) {
        var form_register = $('#formproductos');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);
        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                codigo:{
                  required: true
                },
                nombre:{
                    required: true
                },
                pstock:{
                    min: 0,
                    required: true,
                },
                preciocompra:{
                    required: true
                },
                porcentaje:{
                    required: true
                },
                precioventa:{
                    required: true
                },  
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });

        var $valid = $("#formproductos").valid();
        console.log($valid);
        if($valid) {
            $('.btn_registro').attr('disabled',true);
            var datos = $('#formproductos').serialize();
            $.ajax({
                type:'POST',
                url: 'registra_productos',
               // data: {id:id,nom:nom,ape:ape,perf:perf,mail:email,tcel:telcel,tcasa:telcasa,call:calle,next:noexte,est:estado,nint:noint,col:colonia,mun:municipio,cp:cop},
                data: datos,
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    var idpro=data;
                    detalles_sucu(idpro);
                    toastr.success('Hecho!', 'Guardado Correctamente');
                    //=====================
                    	if ($('#imgpro')[0].files.length > 0) {
    		            	var inputFileImage = document.getElementById('imgpro');
    						var file = inputFileImage.files[0];
    						var data = new FormData();
    						data.append('img',file);
    						data.append('carpeta',$('#carpetap').val());
    						data.append('idpro',idpro);
    						$.ajax({
    							url:'imgpro',
    							type:'POST',
    							contentType:false,
    							data:data,
    							processData:false,
    							cache:false,
    							success: function(data) {
    								var array = $.parseJSON(data);
    					                if (array.ok=true) {
    					                	$(".fileinput").fileinput("clear");
    					                }else{
    					                	toastr.error('Error', data.msg);
    					                }
    							},
    							error: function(jqXHR, textStatus, errorThrown) {
    					                var data = JSON.parse(jqXHR.responseText);
    					                console.log(data);
    					                if (data.ok=='true') {
    					                	$(".fileinput").fileinput("clear");
    					                }else{
    					                	toastr.error('Error', data.msg);
    					                }
    					              
    					            }
    						});
    		        	}
                    //=====================
                    setInterval(function(){ 
                    	location.href=base_url+'Productos';
                    }, 3000);
                    
                }
            });
        }
    });
    $('#generacodigo').click(function(event) {
        generacodigo(1);
        generacodigo(2);
    });
    paq();
});

function calcular(id){
    var costo = $('.preciocompra_c_'+id).val();
    var porcentaje = $('.porcentaje_c_'+id).val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('.precioventa_c_'+id).val(cantitotal.toFixed(2));
    $('.preciommayoreo_c_'+id).val(cantitotal.toFixed(2));
    $('.cpmmayoreo_c_'+id).val('1');
    $('.preciomayoreo_c_'+id).val(cantitotal.toFixed(2));
    $('.cpmayoreo_c_'+id).val('1');
    $('.especial1_precio_c_'+id).val(cantitotal.toFixed(2));
    $('.especial1_cantidad_c_'+id).val(1);
    $('.especial2_precio_c_'+id).val(cantitotal.toFixed(2));
    $('.especial2_cantidad_c_'+id).val(1);
}
/*
function calcular(){
	var costo = $('#preciocompra').val();
    var porcentaje = $('#porcentaje').val();
    var porcentaje2 = porcentaje/100;
    var costo2 = costo*porcentaje2;
    var cantitotal = parseFloat(costo)+parseFloat(costo2);
    $('#precioventa').val(cantitotal.toFixed(2));
    $('#preciommayoreo').val(cantitotal.toFixed(2));
    $('#cpmmayoreo').val('1');
    $('#preciomayoreo').val(cantitotal.toFixed(2));
    $('#cpmayoreo').val('1');
    $('#especial1_precio').val(cantitotal.toFixed(2));
    $('#especial1_cantidad').val(1);
    $('#especial2_precio').val(cantitotal.toFixed(2));
    $('#especial2_cantidad').val(1);
}
*/

function buscarproducto(){
    var search=$('#buscarpro').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: 'Productos/buscarpro',
            data: {
                buscar: $('#buscarpro').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadospro2').html(data);
            }
        });
        $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
}
function modal_categoria(){
    edi_cate=0;
    $("#modalcategoria").modal();
    reload_categoria();
}

function editar_categoria(id){
    edi_cate=1;
    $('#idcategoria_categoria').val($('.cate_'+id).data('idcategoria'));
    $('#nombre_categoria').val($('.cate_'+id).data('nombre'));
}

function reload_categoria(){
    tabla_cate=$("#table_datos_categoria").DataTable();
    tabla_cate.destroy();
    table_categoria();
}
function table_categoria(){
    tabla_cate=$("#table_datos_categoria").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_categoria",
            type: "post",
            error: function(){
               $("#table_datos_categoria").css("display","none");
            }
        },
        "columns": [
            {"data":"categoria"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idcategoria="'+row.categoriaId+'"\
                            data-nombre="'+row.categoria+'"\
                            class="btn btn-raised btn-icon btn_orange cate_'+row.categoriaId+'" onclick="editar_categoria('+row.categoriaId+')"><i class="ft-edit"></i></button>\
                        <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="eliminar_registro_cate('+row.categoriaId+');"><i class="ft-trash-2"></i></button>';             
                   return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function guarda_registro_categoria(){
    var form_register = $('#form_registro_categoria');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_categoria").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registro_categoria',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                
                var id=data;
                $('#nombre_categoria').val();
                if(edi_cate==1){
                    $("#categoria option[value='"+id+"']").attr("selected", true);
                    $("#categoria option[value='"+id+"']").text($('#nombre_categoria').val());
                }else{
                    $('#pcategoria').prepend("<option value='"+id+"' selected>"+$('#nombre_categoria').val()+"</option>");    
                }
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    reload_categoria();
                    $('#idcategoria_categoria').val(0);
                    $('#nombre_categoria').val('');
                }, 1000); 
        
            }
        });
    }   
}

function eliminar_registro_cate(id){
    $('#id_cate').val(id);
    $('#elimina_registro_modal_cate').modal();
}
function delete_registro_cate(){
    var idp=$('#id_cate').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Productos/deleteregistro_cate",
        data:{id:idp},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('Hecho!', 'Elimando Correctamente');
            $('#pcategoria option[value="'+idp+'"]').remove();
            $('#elimina_registro_modal_cate').modal('hide');
            setTimeout(function(){ 
               reload_categoria(); 
            }, 1000); 
        }
    });  
}

function modal_marcas(){
    edi_marca=0;
    $('#modal_marca').modal();
    reload_marca();
}

function reload_marca(){
    tabla_marca=$("#table_datos_marcas").DataTable();
    tabla_marca.destroy();
    table_marcas();
}
function table_marcas(){
    tabla_marca=$("#table_datos_marcas").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": false,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Productos/getlistado_marca",
            type: "post",
            error: function(){
               $("#table_datos_marcas").css("display","none");
            }
        },
        "columns": [
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='<button type="button"\
                            data-idmarca="'+row.idmarca+'"\
                            data-nombre="'+row.nombre+'"\
                            class="btn btn-raised btn-icon btn_orange marc_'+row.idmarca+'" onclick="editar_marca('+row.idmarca+')"><i class="ft-edit"></i></button>\
                        <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="eliminar_registro_marca('+row.idmarca+');"><i class="ft-trash-2"></i></button>';             
                    return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[5, 10, 25], [5, 10, 25]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function editar_marca(id){
    edi_marca=1;
    $('#idmarca_marca').val($('.marc_'+id).data('idmarca'));
    $('#nombre_marca').val($('.marc_'+id).data('nombre'));
}

function eliminar_registro_marca(id){
    $('#id_marc').val(id);
    $('#elimina_registro_modal_marca').modal();
}
function delete_registro_marca(){
    var idp=$('#id_marc').val(); 
    $.ajax({
        type:'POST',
        url: base_url+"Productos/deleteregistro_marca",
        data:{id:idp},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('Hecho!', 'Elimando Correctamente');
            $('#marca option[value="'+idp+'"]').remove();
            $('#elimina_registro_modal_marca').modal('hide');
            setTimeout(function(){ 
               reload_marca(); 
            }, 1000); 
        }
    });  
}

//===========================================================
function guarda_registro_marca(){
    var form_register = $('#form_registro_marca');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_registro_marca").valid();
    if($valid) {
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Productos/registro_marca',
            data: datos,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                
                var id=data;
                $('#nombre_marca').val();
                if(edi_marca==1){
                    $("#marca option[value='"+id+"']").attr("selected", true);
                    $("#marca option[value='"+id+"']").text($('#nombre_marca').val());
                }else{
                    $('#marca').prepend("<option value='"+id+"' selected>"+$('#nombre_marca').val()+"</option>");    
                }
                toastr.success('Hecho!', 'Guardado Correctamente');
                setTimeout(function(){ 
                    reload_marca();
                    $('#idmarca_marca').val(0);
                    $('#nombre_marca').val('');
                }, 1000); 
        
            }
        });
    }   
}

function detalles_sucu(id){
    var DATAP= [];
    var TABLAP = $(".row_detalles .tab-content > .tab-pane");                  
    TABLAP.each(function(){         
          item = {};
          item ["productoid"]=id;
          item ["iddetalles"]=$(this).find("input[id*='iddetalles']").val();
          item ["idsucursal"]=$(this).find("input[id*='idsucursal']").val();
          item ["stock"]=$(this).find("input[id*='pstock']").val();
          item ["stock_paq"]=$(this).find("input[id*='stock_paq']").val();
          item ["preciocompra"]=$(this).find("input[id*='preciocompra']").val();
          item ["porcentaje"]=$(this).find("input[id*='porcentaje']").val();
          item ["precioventa"]=$(this).find("input[id*='precioventa']").val();
          /*
          item ["especial1_precio"]=$(this).find("input[id*='especial1_precio']").val();
          item ["especial1_cantidad"]=$(this).find("input[id*='especial1_cantidad']").val();
          item ["especial2_precio"]=$(this).find("input[id*='especial2_precio']").val();
          item ["especial2_cantidad"]=$(this).find("input[id*='especial2_cantidad']").val();
          item ["mediomayoreo"]=$(this).find("input[id*='preciommayoreo']").val();
          item ["canmediomayoreo"]=$(this).find("input[id*='cpmmayoreo']").val();
          item ["mayoreo"]=$(this).find("input[id*='preciomayoreo']").val();
          item ["canmayoreo"]=$(this).find("input[id*='cpmayoreo']").val();
          item ["precio_paq"]=$(this).find("input[id*='precio_paquete']").val();
          */
          item ["especial1_precio"]=$(this).find("input[id*='precioventa']").val();
          item ["especial1_cantidad"]=1;
          item ["especial2_precio"]=$(this).find("input[id*='precioventa']").val();
          item ["especial2_cantidad"]=1;
          item ["mediomayoreo"]=$(this).find("input[id*='precioventa']").val();
          item ["canmediomayoreo"]=1;
          item ["mayoreo"]=$(this).find("input[id*='precioventa']").val();
          item ["canmayoreo"]=1;
          item ["precio_paq"]=$(this).find("input[id*='precioventa']").val();
          DATAP.push(item);
    });
    INFOP  = new FormData();
    aInfop   = JSON.stringify(DATAP);
    INFOP.append('data', aInfop);
    $.ajax({
        data: INFOP,
        type: 'POST',
        url : base_url+'Productos/insert_productos_sucursal_precios',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data2){
                //toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                //toastr.error('Error', '500');
            }
        },
        success: function(data2){
          
        }
    }); 
}
function incluir_tx(){
    if($('.incluir').is(':checked')){
       $('.preciocompra').val($('.preciocompra1').val());
       $('.porcentaje').val($('.porcentaje1').val());
       $('.precioventa').val($('.precioventa1').val());
       $('.especial1_precio').val($('.especial1_precio1').val());
       $('.especial1_cantidad').val($('.especial1_cantidad1').val());
       $('.especial2_precio').val($('.especial2_precio1').val());
       $('.especial2_cantidad').val($('.especial2_cantidad1').val());
       $('.preciommayoreo').val($('.preciommayoreo1').val());
       $('.cpmmayoreo').val($('.cpmmayoreo1').val());
       $('.preciomayoreo').val($('.preciomayoreo1').val());
       $('.cpmayoreo').val($('.cpmayoreo1').val());
    }
    
}
function generacodigo(tipo){
    var d = new Date();
    var dia=d.getDate();//1 31
    var dias=d.getDay();//0 6
    var mes = d.getMonth();//0 11
    var yy = d.getFullYear();//9999
    var hr = d.getHours();//0 24
    var min = d.getMinutes();//0 59
    var seg = d.getSeconds();//0 59
    var yyy = 18;
    var ram = Math.floor((Math.random() * 99) + 1);
    var codigo=seg+''+min+''+hr+''+yyy+''+ram+''+mes+''+dia+''+dias;
    //var condigo0=condigo.substr(0,13);
    //console.log(codigo);
    if(tipo==1){
        $('#pcodigo').val(codigo);    
    }
    if(tipo==2){
        if($('#pag').is(':checked')==true){
            $('#codigo_pag').val(codigo);
        }
    }
    
}

function paq(){
    if($('#pag').is(':checked')==true){
        $('.input_pag').show('show');
    }else{
        $('.input_pag').hide('show');
    }
}







