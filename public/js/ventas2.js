var base_url = $('#base_url').val();
var proselected_length=0;
var proselected=0;
$(document).ready(function() {
	//$( "#productos" ).focus();
    $( "#productos" ).keypress(function(e) {
        if (e.which==13) {
            productos_buscar(1);
        }else{
            setTimeout(function(){ 
                productos_buscar(0);
            }, 900);
            
        }
    });
    $(document).keydown(function(e) {
        console.log(e.which)
        switch(e.which) { 
            case 38:
                navigate('up');
            break;
            case 40:
                navigate('down');
            break;
            case 13:
                navigate('enter');

            break;
        }
    });
});
function productos_buscar(tipo){
    var productos_b=$('#productos').val();
    if(productos_b!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Ventas/searchproductopost',
            data:{
                search:productos_b
            },
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){ 
                    console.log(data); 
                    var array = $.parseJSON(data);
                    if (tipo==0) {
                        $('.producto_buscar_t').html('');
                        array.forEach(function(item) {
                            var html='<a data-nombre="'+item.nombre+'" \
                                        data-precioventa="'+item.precioventa+'" \
                                        data-especial1_precio="'+item.especial1_precio+'" \
                                        data-especial2_precio="'+item.especial2_precio+'" \
                                        data-mediomayoreo="'+item.mediomayoreo+'" \
                                        data-mayoreo="'+item.mayoreo+'" \
                                        onclick="addproducto2('+item.productoid+')" \
                                        class="btn btn_orange_borde pro_'+item.productoid+' proselected" style="width: 100%;">'+item.codigo+' / '+item.nombre+'</a><br>';
                            $('.producto_buscar_t').append(html);
                        });
                        proselected_length=$('.proselected').length;
                        proselected=-1;

                    }else{
                        if (array.length==1) {
                            array.forEach(function(item) {
                                addproducto2(item.productoid);
                            });
                        }else{

                        }
                    }
            }
        });
    }else{
        setTimeout(function(){ 
            $('.producto_buscar_t').html('');
        }, 1000);
    }
}
function navigate(direction) {
    
    if(proselected_length>0){
        if(direction == 'up') {//subir
            $(".proselected").removeClass('selected');
            if (proselected==0 ||proselected==-1) {
                proselected=0;
            }else{
                proselected--;
            }
            
        
        } else if (direction == 'down') {//bajar
            $(".proselected").removeClass('selected');
            proselected++;
            if(proselected>=proselected_length){
                proselected=(proselected_length-1);
            }
        }else if (direction == 'enter') {
            $(".proselected.selected").click();
        }
    }
    
    //console.log(proselected);
    var valor=$(".proselected").eq(proselected).addClass('selected');
    //console.log(valor);
}
function addproducto2(pro){
	if ($('#vcantidad').val()>0) {
		
		$.ajax({
	        type:'POST',
	        url: 'Ventas/addproducto',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: pro
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	console.log(data);
	                $('#class_productos').html(data);
	            }
	        });
		$('#vcantidad').val(1);
		$('#producto').html('');
		$('.producto_buscar_t').html('');
	    //$("#vproducto").val(0).change();
	    //$('#vproducto').select2('open').on('focus');
	}
    calculartotal();
}