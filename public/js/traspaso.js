var base_url = $('#base_url').val();
var proselected_length=0;
var proselected=0;
var contpar_aux=0;
$(document).ready(function() {

    $( "#productos" ).focus();
    $( "#productos" ).keypress(function(e) {
        if (e.which==13) {
            productos_buscar(1);
        }else{
            setTimeout(function(){ 
                productos_buscar(0);
            }, 900);
            
        }
    });
    $(document).keydown(function(e) {
        console.log(e.which)
        switch(e.which) { 
            case 38:
                navigate('up');
            break;
            case 40:
                navigate('down');
            break;
            case 13:
                navigate('enter');

            break;
        }
    });

    ///===========  Guardar Traspaso=============== 
    /*
    $('#guardar_btn').click(function(event) {
        var validar = 0;
        var TABLAP = $("#data_tables_productos tbody > tr");                  
        TABLAP.each(function(){         
            validar = 1;
        });
        if(validar==1){
            $('.btn_registro').attr('disabled',true);
            var datos = 'id='+$('#idtraspaso').val()+'&sucursal='+$('#idsucursal option:selected').val()+'&fecha='+$('#fecha').val()+'&hora='+$('#hora').val()+'&total='+$('.total_p_total').text();
            $.ajax({
                type:'POST',
                url: base_url+'Traspasos/registrar',
               // data: {id:id,nom:nom,ape:ape,perf:perf,mail:email,tcel:telcel,tcasa:telcasa,call:calle,next:noexte,est:estado,nint:noint,col:colonia,mun:municipio,cp:cop},
                data: datos,
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(){
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                    var idtrans=data;
                    toastr.success('Hecho!', 'Guardado Correctamente');
                    //=====================
                    //=====================
                    var fecha=$('#fecha').val();//2021-04-08
                    var fecha_aux=fecha.split("-")
                    var codigo_barras='SUC'+$('#idsucursal option:selected').val()+idtrans+fecha_aux[2]+fecha_aux[1]+fecha_aux[0];
                    JsBarcode("#barcode",codigo_barras);
                    setTimeout (function(){ 
                        var id_traspaso_aux=$('#idtraspaso').val();
                        if(id_traspaso_aux==0){
                            agregar_codigo(idtrans,codigo_barras);

                        }
                    }, 1000);
                    setTimeout (function(){ 
                        location.href=base_url+'Traspasos';
                    }, 2000);
                    guardar_traspasos(idtrans);
                    
                }
            });
        }else{
            toastr.error('No has agregado almenos un traspaso', '¡Atención!');
        }
    });
    */
    var idtraspaso=$('#idtraspaso').val();
    var tras_detalle_aux=$('#tras_detalle_aux').val();
    if(tras_detalle_aux==1){
      tabla_traspasos_detalles(idtraspaso);
    }

    

});

function varlidar_existencia(){
    var validar = 0;
    var DATAP= [];
    var TABLAP = $("#data_tables_productos tbody > tr");                  
    TABLAP.each(function(){         
          item = {};
          item ["productoid"]=$(this).find("input[id*='productoidx']").val();
          item ["cantidad"]=$(this).find("input[id*='cantidadx']").val();
          DATAP.push(item);
          validar = 1;
    });
    if(validar==1){
        INFOP  = new FormData();
        aInfop   = JSON.stringify(DATAP);
        INFOP.append('data', aInfop);
        $.ajax({
            data: INFOP,
            type: 'POST',
            url : base_url+'Traspasos/validar_exisntecias',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    //toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    //toastr.error('Error', '500');
                }
            },
            success: function(data){
                if(data==0){
                    $('.btn_registro').attr('disabled',true);
                    var datos = 'id='+$('#idtraspaso').val()+'&sucursal='+$('#idsucursal option:selected').val()+'&fecha='+$('#fecha').val()+'&hora='+$('#hora').val()+'&total='+$('.total_p_total').text();
                    $.ajax({
                        type:'POST',
                        url: base_url+'Traspasos/registrar',
                       // data: {id:id,nom:nom,ape:ape,perf:perf,mail:email,tcel:telcel,tcasa:telcasa,call:calle,next:noexte,est:estado,nint:noint,col:colonia,mun:municipio,cp:cop},
                        data: datos,
                        async: false,
                        statusCode:{
                            404: function(data){
                                toastr.error('Error!', 'No Se encuentra el archivo');
                            },
                            500: function(){
                                toastr.error('Error', '500');
                            }
                        },
                        success:function(data){
                            var idtrans=data;
                            toastr.success('Hecho!', 'Guardado Correctamente');
                            //=====================
                            //=====================
                            var fecha=$('#fecha').val();//2021-04-08
                            var fecha_aux=fecha.split("-")
                            var codigo_barras='SUC'+$('#idsucursal option:selected').val()+idtrans+fecha_aux[2]+fecha_aux[1]+fecha_aux[0];
                            JsBarcode("#barcode",codigo_barras);
                            setTimeout (function(){ 
                                var id_traspaso_aux=$('#idtraspaso').val();
                                if(id_traspaso_aux==0){
                                    agregar_codigo(idtrans,codigo_barras);

                                }
                            }, 1000);
                            setTimeout (function(){ 
                                location.href=base_url+'Traspasos';
                            }, 2000);
                            guardar_traspasos(idtrans);
                            
                        }
                    });
              
                }else{
                    toastr.error('Uno de los productos le faltan existencias', '¡Atención!');
                }


            }
        });
    }else{
        toastr.error('No has agregado almenos un traspaso', '¡Atención!');
    } 
}




function productos_buscar(tipo){
    var productos_b=$('#productos').val();
    if(productos_b!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Traspasos/buscar_producto',
            data:{
                producto:productos_b,
                idsucursal:$('#idsucursal option:selected').val()
                },
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){ 
                    console.log(data); 
                    var array = $.parseJSON(data);
                    if (tipo==0) {
                        $('.producto_buscar_t').html('');
                        array.forEach(function(item) {
                            var html='<a data-nombre="'+item.nombre+'" \
                                        data-precioventa="'+item.precioventa+'" \
                                        data-especial1_precio="'+item.especial1_precio+'" \
                                        data-especial2_precio="'+item.especial2_precio+'" \
                                        data-mediomayoreo="'+item.mediomayoreo+'" \
                                        data-mayoreo="'+item.mayoreo+'" \
                                        onclick="agregar_producto('+item.productoid+')" \
                                        class="btn btn_orange_borde pro_'+item.productoid+' proselected" style="width: 100%;">'+item.codigo+' / '+item.nombre+'</a><br>';
                            $('.producto_buscar_t').append(html);
                        });
                        proselected_length=$('.proselected').length;
                        proselected=-1;

                    }else{
                        if (array.length==1) {
                            array.forEach(function(item) {
                                add_producto(0,item.productoid,item.nombre,0,item.precioventa,item.especial1_precio,item.especial2_precio,item.mediomayoreo,item.mayoreo,0);
                            });
                        }else{

                        }
                    }
            }
        });
    }else{
        setTimeout(function(){ 
            $('.producto_buscar_t').html('');
        }, 1000);
    }
}

function agregar_producto(idpro){
    var nombre=$('.pro_'+idpro).data('nombre');
    var precio=$('.pro_'+idpro).data('precioventa');

    var especial1_precio=$('.pro_'+idpro).data('especial1_precio');
    var especial2_precio=$('.pro_'+idpro).data('especial2_precio');
    var mediomayoreo=$('.pro_'+idpro).data('mediomayoreo');
    var mayoreo=$('.pro_'+idpro).data('mayoreo');

    add_producto(0,idpro,nombre,0,precio,especial1_precio,especial2_precio,mediomayoreo,mayoreo,0);
    setTimeout(function(){ 
        $('.producto_buscar_t').html('');
    }, 100);
    $('#productos').val('');
}

var cont=0;
function add_producto(idtrasp,idpro,nombre,cantidad,precio,especial1_precio,especial2_precio,mediomayoreo,mayoreo,subtotal){
    var html='';
    var disabled_input='';
    if(idtrasp==0){
        disabled_input='';
    }else{
        disabled_input='disabled';
    }
    var selecttxt='<select id="tipo_preciox" class="form-control tipo_precio_'+cont+'" onchange="tipo_precio_select('+cont+')" '+disabled_input+'>\
                    <option value="1-'+precio+'">Precio venta: '+precio+'</option>\
                    <option value="2-'+especial1_precio+'">Precio especial 1: '+especial1_precio+'</option>\
                    <option value="3-'+especial2_precio+'">Precio especial 2: '+especial2_precio+'</option>\
                    <option value="4-'+mediomayoreo+'">Precio medio mayoreo: '+mediomayoreo+'</option>\
                    <option value="5-'+mayoreo+'">Precio mayoreo: '+mayoreo+'</option>\
                </select>';
    html='<tr class="row_p_'+cont+'">\
          <td>'+nombre+'\
            <input type="hidden" id="idtrasdetallex" value="'+idtrasp+'">\
            <input type="hidden" class="productoidx_'+cont+'" id="productoidx" value="'+idpro+'">\
          </td>\
          <td><input type="number" id="cantidadx" class="form-control cantidad_s_'+cont+'" oninput="ope_subtotal('+cont+')" value="'+cantidad+'" '+disabled_input+'></td>\
          <td>'+selecttxt+'\
          </td>\
          <td><input type="number" id="preciox" class="form-control preciox_'+cont+'" value="'+precio+'" '+disabled_input+'></td>\
          <td><span class="subtotal_'+cont+'">$'+subtotal+'</span>\
             <input type="hidden" id="subtotalx" class="subtotalx_'+cont+'" value="'+subtotal+'">\
          </td>\
          <td><button type="button" class="btn btn-raised btn-icon btn_crema" onclick="remove_producto('+cont+','+idtrasp+')""><i class="ft-trash-2"></i></button></td>\
        </tr>';
    $('#data_tables_productos').append(html);    
    cont++;
    suma_total_ope();
    setTimeout(function(){ 
        $('#productos').val('');
        $('.producto_buscar_t').html('');
    }, 300);
}
var cantidad_aux=0;
var productoidx_aux=0;
function remove_producto(cont,idp){
    if(idp==0){
        $('.row_p_'+cont).remove();
    }else{
        $('#eliminar_traspaso').modal();
        $('#id_traspaso_aux').val(idp)
        contpar_aux=cont;
        cantidad_aux=$('.cantidad_s_'+cont).val();
        productoidx_aux=$('.productoidx_'+cont).val();
    }
    suma_total_ope();
}

function delete_registro_transpaso(){
    var id=$('#id_traspaso_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/deleteregistro_traspaso',
        data: {id:id,cantidad:cantidad_aux,productoid:productoidx_aux},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            $('#eliminar_traspaso').modal('hide');
            $('.row_p_'+contpar_aux).remove();
            suma_total_ope();
        }
    });
}

function navigate(direction) {
    if(proselected_length>0){
        if(direction == 'up') {//subir
            $(".proselected").removeClass('selected');
            if (proselected==0 ||proselected==-1) {
                proselected=0;
            }else{
                proselected--;
            }
            
        
        } else if (direction == 'down') {//bajar
            $(".proselected").removeClass('selected');
            proselected++;
            if(proselected>=proselected_length){
                proselected=(proselected_length-1);
            }
        }else if (direction == 'enter') {
            $(".proselected.selected").click();
        }
    }
    
    //console.log(proselected);
    var valor=$(".proselected").eq(proselected).addClass('selected');
    //console.log(valor);
}

function ope_subtotal(id){
    var cantidad=$('.cantidad_s_'+id).val();
    var cant=parseFloat(cantidad);
    var precio=$('.preciox_'+id).val();
    var prec=parseFloat(precio);
    var suma_total=parseFloat(cant*prec);
    $('.subtotalx_'+id).val(suma_total);
    var monto=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(suma_total);
    $('.subtotal_'+id).html(monto);
    suma_total_ope();
}

function suma_total_ope(){
    var TABLA   = $("#data_tables_productos tbody> tr");
    var addtp=0;
    TABLA.each(function(){   
        var total_precio = $(this).find("input[id*='subtotalx']").val();
        var vstotal = total_precio;
        addtp += Number(vstotal);   
    });
    $('.total_p_total').html(addtp);
}

function guardar_traspasos(id){
    var DATAP= [];
    var TABLAP = $("#data_tables_productos tbody > tr");                  
    TABLAP.each(function(){         
          item = {};
          item ["idtraspaso"]=id;
          item ["idtrasdetalle"]=$(this).find("input[id*='idtrasdetallex']").val();
          item ["productoid"]=$(this).find("input[id*='productoidx']").val();
          item ["cantidad"]=$(this).find("input[id*='cantidadx']").val();
          item ["precio"]=$(this).find("input[id*='preciox']").val();
          item ["subtotal"]=$(this).find("input[id*='subtotalx']").val();
          item ["tipo_precio"]=$(this).find("select[id*='tipo_preciox'] option:selected").val();
          DATAP.push(item);
    });
    INFOP  = new FormData();
    aInfop   = JSON.stringify(DATAP);
    INFOP.append('data', aInfop);
    $.ajax({
        data: INFOP,
        type: 'POST',
        url : base_url+'Traspasos/insert_trapasos',
        processData: false, 
        contentType: false,
        async: false,
        statusCode:{
            404: function(data2){
                //toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                //toastr.error('Error', '500');
            }
        },
        success: function(data2){
          
        }
    }); 
}

function tabla_traspasos_detalles(id){
    $.ajax({
        type:'POST',
        url: base_url+"Traspasos/get_traspasos_detalles",
        data: {id:id,idsucursal:$('#idsucursal_aux').val()},
        success:function(data){
            var array = $.parseJSON(data);
            if(array.length>0) {
                array.forEach(function(element){
                  add_producto(element.id,element.productoid,element.nombre,element.cantidad,element.precio,element.especial1_precio,element.especial2_precio,element.mediomayoreo,element.mayoreo,element.subtotal);
                });
            }   
        }
    });
}

function agregar_codigo(id,codigo){
    canvas = document.getElementsByTagName("canvas");
    var dataURL =canvas[0].toDataURL();
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/add_img_codigo_barras',
        data: {id:id,codigo_barras:dataURL,codigo:codigo},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
        }
    });
    
}
