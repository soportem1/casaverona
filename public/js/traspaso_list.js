var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        //"info":     false,
        //"paging": false,
        "ajax": {
            "url": base_url+"Traspasos/getlistado",
            type: "post",
            "data":{idsucursal:$('#idsucursal').val()},
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = row.fecha+' '+row.hora;
                return html;
                }
            },
            {"data":"nombre"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var cant=parseFloat(row.total);
                    var html ='$'+cant.toFixed(2);
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html ='<button type="button" class="btn btn-raised btn-icon btn_amarillo" onclick="ver_resumen('+row.id+')"><i class="fa fa-eye"></i></button>';
                return html;
                }
            },
            {"data":"Usuario"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = '';
                    if(row.estatus==0){
                        html = '<span class="badge bg-danger border_eti">Pendiente</span>';
                    }else if(row.estatus==1){
                        html = '<span class="badge bg-success border_eti">Realizado</span>';
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                    html+='<div class="form-group">\
                        <div class="btn-group" role="group" aria-label="Basic example">';
                        if(row.estatus==0){
                        html+='<a class="btn btn-raised btn-icon btn_orange" href="'+base_url+'Traspasos/registro/'+row.id+'"><i class="ft-edit"></i></a>\
                            <button type="button" class="btn btn-raised btn-icon btn_crema" onclick="eliminar_update('+row.id+');"><i class="ft-trash-2"></i></button>';
                        }
                        html+='<a type="button" class="btn btn-raised btn-icon btn_amarillo" target="block_" href="'+base_url+'Traspasos/documento/'+row.id+'"><i class="ft-printer"></i></a>\
                        </div>\
                    </div>';
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

function eliminar_update(id){
    $('#eliminar_registro').modal();
    $('#id_aux').val(id);
}

function delete_registro(){
    var id=$('#id_aux').val();
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/deleteregistro',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('eliminado Correctamente','Hecho!' );
            tabla.ajax.reload();
            $('#eliminar_registro').modal('hide');
        }
    });
}

function ver_resumen(id){
    $('#ver_resumen_modal').modal();
    tablas_producto(id);
}


function tablas_producto(id){
    var html='<table class="table table-striped" id="data_tables_productos" style="width: 100%">\
                <thead>\
                  <tr>\
                    <th>Nombre</th>\
                    <th>Cantidad</th>\
                    <th>Precio</th>\
                    <th>Subtotal</th>\
                  </tr>\
                </thead>\
                <tbody id="data_tables_productos_tbody">\
                </tbody>\
              </table>';
    $('.tabla_productos').html(html);
    $.ajax({
        type:'POST',
        url: base_url+'Traspasos/tabla_produtos',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            $('#data_tables_productos_tbody').html(data);
        }
    });

}