$(document).ready(function(){
	$('#chkFecha').change(function(){
		if(document.getElementById("chkFecha").checked){
			f = new Date();
			if(f.getDate()<10){
				dia = "0"+f.getDate();
			}else{dia = f.getDate();
			}
			if(f.getMonth()<10){
				mes = "0"+(f.getMonth()+1);
			}else{
				mes = (f.getMonth+1);
			}
			fecha = f.getFullYear()+"-"+mes+"-"+dia;
			var fecha_actual = $('#fecha_actual').val();
			$('#txtInicio').val(fecha_actual);
			$('#txtFin').val(fecha_actual);
			$('#txtInicio').attr('disabled','disabled');
			$('#txtFin').attr('disabled','disabled');
		}
		else{
			$('#txtInicio').val('');
			$('#txtFin').val('');
			$('#txtInicio').removeAttr('disabled');
			$('#txtFin').removeAttr('disabled');
		}
	});
	$('#btnBuscar').click(function(){
		var pro_ven=0;
		if($('#checkproducto').is(':checked')){
			pro_ven=1;
        }else{
            pro_ven=0;
        }
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.idsucursal = $('#idsucursal option:selected').val();
		params.tipo_venta = $('#tipo_venta option:selected').val();
		params.pro_ven = pro_ven;
		if(params.fecha1 != '' && params.fecha2 != ''){
            var tipo = $('#tipo_venta option:selected').val();
            if(tipo!=4 && tipo!=5 && tipo!=0){
                $.ajax({
					type:'POST',
					url:'Corte_caja/corte',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbCorte').html(array.tabla);
						$('#tbCorte2').html(array.tabla2);
						$('#rowventas').html(array.totalventas);
						$('#totalutilidades').html(array.totalutilidad);
						$('#dgastos').html(''+array.dgastos);
						$('#dTotal').html(''+array.dTotal);
	                    $('#dSubtotal').html(''+array.dSubtotal);
	                    if($('#checkproducto').is(':checked')){ 
	                        $('.mas_vendindos').html(array.mas_vendindos);
	                    }else{
	                    	$('.mas_vendindos').html('');
	                    }   
					}
				});
				$('.corte_texto').css('display','none');
				$('.ventas_texto').css('display','block');
				$('#tbCorte_pedido').html('');
				$('#total_pedido').html('');
				$('#total_pedido_anticipo').html('');  
            }else if(tipo==5){
                $.ajax({
					type:'POST',
					url:'Corte_caja/corte_pedidos',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbCorte_pedido').html(array.tabla);
						$('#total_pedido').html(array.total);
						$('#total_pedido_anticipo').html(array.anticipo);
						$('#total_abono').html(parseFloat(array.abono, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());

					}
				});
				$('.corte_texto').css('display','block');
				$('.ventas_texto').css('display','none');
			}else if(tipo==0){
                $.ajax({
					type:'POST',
					url:'Corte_caja/corte_pedidos',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbCorte_pedido').html(array.tabla);
						$('#total_pedido').html(array.total);
						$('#total_pedido_anticipo').html(array.anticipo);  
					}
				});
				$.ajax({
					type:'POST',
					url:'Corte_caja/corte',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbCorte').html(array.tabla);
						$('#tbCorte2').html(array.tabla2);
						$('#rowventas').html(array.totalventas);
						$('#totalutilidades').html(array.totalutilidad);
						$('#dgastos').html(''+array.dgastos);
						$('#dTotal').html(''+array.dTotal);
	                    $('#dSubtotal').html(''+array.dSubtotal);
	                    if($('#checkproducto').is(':checked')){ 
	                        $('.mas_vendindos').html(array.mas_vendindos);
	                    }else{
	                    	$('.mas_vendindos').html('');
	                    }   
					}
				});
				$('.corte_texto').css('display','block');
				$('.ventas_texto').css('display','block');
            }else{
                $.ajax({
					type:'POST',
					url:'Corte_caja/corte_credito',
					data:params,
					async:false,
					success:function(data){
						console.log(data);
						var array = $.parseJSON(data);
						$('#tbCorte').html(array.tabla);
						$('#tbCorte2').html(array.tabla2);
						$('#rowventas').html(array.totalventas);
						$('#totalutilidades').html(array.totalutilidad);
						$('#dTotal').html(''+array.dTotal);
	                    $('#dSubtotal').html(''+array.dSubtotal);
	                    if($('#checkproducto').is(':checked')){ 
	                        $('.mas_vendindos').html(array.mas_vendindos);
	                    }else{
	                    	$('.mas_vendindos').html('');
	                    }   
					}
				});
				$('.corte_texto').css('display','none');
				$('.ventas_texto').css('display','block');
				$('#tbCorte_pedido').html('');
				$('#total_pedido').html('');
				$('#total_pedido_anticipo').html(''); 
            }
				

		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
});