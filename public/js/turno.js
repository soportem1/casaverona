var base_url = $('#base_url').val();
$(document).ready(function() {	
	$('#btnabrirt').click(function(){
			$.ajax({
				type:'POST',
				url:base_url+'Ventas/abrirturno',
				data:{
					cantidad: $('#cantidadt').val(),
					nombre: $('#nombredelturno').val(),
					suc:$('#idsucursal option:selected').val()
				},
				async:false,
				success:function(data){
					toastr.success('Hecho!', 'Turno Abierto');
					$('#modalturno').modal('hide');
					consultarturno();
				}
			});
	});
	$('#cerrarturno').click(function(){
			$.ajax({
				type:'POST',
				url:base_url+'Ventas/cerrarturno',
				data:{
					id: $('#idturno').val()
				},
				async:false,
				success:function(data){
					toastr.success('Turno cerrado', 'Hecho!');
					//document.getElementById("cantidadt").disabled = false;
					//document.getElementById("nombredelturno").disabled = false;
					document.getElementById("btnabrirt").disabled = false;
					document.getElementById("btncerrar").disabled = true;
					document.getElementById("cerrarturno").disabled = true;
					//$('#cantidadt').val('');
					//$('#nombredelturno').val('');
					$('#horainicial').val('');

					$('#nombredelturno').val('').prop('readonly', false);
					$('#txtInicio').val('').prop('readonly', false);
					$('#cantidadt').val('').prop('readonly', false);
					//alert(data);
				}
			});
		
	});
	$('#btncerrar').click(function(){
		params = {};
		params.fecha1 = $('#txtInicio').val();
		params.fecha2 = $('#txtFin').val();
		params.idsucursal=$('#idsucursal option:selected').val();
		if(params.fecha1 != '' && params.fecha2 != ''){
			$.ajax({
				type:'POST',
				url:base_url+'Corte_caja/corte',
				data:params,
				async:false,
				success:function(data){
					console.log(data);
					var array = $.parseJSON(data);
					$('#tbCorte').html(array.tabla);
					$('#ddescuento').html(array.descuento);
					/*$('#cRealizadas').html(array.cRealizadas);
					$('#cContado').html(array.cContado);
					$('#cCredito').html(array.cCredito);
					$('#cTerminal').html(array.cTerminal);
					$('#cCheque').html(array.cCheque);
					$('#cTransferencia').html(array.cTransferencia);
					$('#cPagos').html(array.cPagos);
					$('#dContado').html('$ '+array.dContado);
					$('#dCredito').html('$ '+array.dCredito);
					$('#dTerminal').html('$ '+array.dTerminal);
					$('#dCheque').html('$ '+array.dCheque);
					$('#dTransferencia').html('$ '+array.dTransferencia);
					$('#dPagos').html('$ '+array.dPagos);*/
					$('#dTotal3').html(new Intl.NumberFormat('es-MX').format(array.dTotal));
                    //$('#dImpuestos').html(''+array.dImpuestos);
                    $('#dSubtotal').html(''+array.dSubtotal);




                    var cantidadt=$('#cantidadt').val();
                    $('#val2').html(cantidadt);
                    var caja=parseFloat(cantidadt)+parseFloat(array.dTotal);
                    $('#stotal2').html(new Intl.NumberFormat('es-MX').format(caja));
				}
			});
		}
		else{
			toastr.error('No existen fechas validas', 'Error');
			
		}
	});
	consultarturno();
});

function imprimir(){
      window.print();
}
function consultarturno(){
	var suc=$('#idsucursal option:selected').val();
	$.ajax({
	      type:'POST',
	      url:base_url+'Turno/consultarturnosuc',
	      data:{sucursal:suc},
	      async:false,
	      success:function(data){
	        	var array = $.parseJSON(data);
	        	console.log(array);
	        	var count=$.each(array, function(index, item) {
	            	if(item.status_var==1){
	            		$('#idturno').val(item.id);
			            $('#txtInicio').val(item.fecha).prop('readonly', true);
			            $('#horainicial').val(item.horaa).prop('readonly', true);
			            //$('#').val(item.horac);
			            $('#cantidadt').val(item.cantidad).prop('readonly', true);
			            $('#nombredelturno').val(item.nombre).prop('readonly', true);
			            $('#btnabrirt').prop('disabled', true);
			            $('#btncerrar').prop('disabled', false);
			            $('#cerrarturno').prop('disabled', false);
	            	}else{
	            		$('#idturno').val(0);
			            $('#txtInicio').val('').prop('readonly', false);
			            $('#horainicial').val('').prop('readonly', true);
			            
			            $('#cantidadt').val('').prop('readonly', false);
			            $('#nombredelturno').val('').prop('readonly', false);
			            $('#btnabrirt').prop('disabled', false);
			            $('#btncerrar').prop('disabled', true);
			            $('#cerrarturno').prop('disabled', true);
			            $('#tbCorte').html('');
	            	}
	            });
	            console.log(count.length);
	            if(count.length==0){
	            	$('#idturno').val(0);
			      $('#txtInicio').val('').prop('readonly', false);
			      $('#horainicial').val('').prop('readonly', false);			            
			      $('#cantidadt').val('').prop('readonly', false);
			      $('#nombredelturno').val('').prop('readonly', false);
			      $('#btnabrirt').prop('disabled', false);
			      $('#btncerrar').prop('disabled', true);
			      $('#cerrarturno').prop('disabled', true);
			      $('#tbCorte').html('');
	            }
	      }
    	});
}