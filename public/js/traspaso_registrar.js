var base_url = $('#base_url').val();
$(document).ready(function() {
    $( "#productos" ).keypress(function(e) {
        if (e.which==13) {
            buscar_producto_tras();
        }else{

        }
    });
});


function buscar_producto_tras(){
    var productos_b=$('#productos').val();
    if(productos_b!=''){
        get_traspaso(productos_b);
    }else{
        toastr.error('El campo esta vacío', '¡Atención!');
    }
}
function get_traspaso(codigo){
    $.ajax({
        type:'POST',
        url: base_url+'Registrar_Traspasos/get_producto_traspaso',
        data: {codigo:codigo},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            if(array.validar==1){
                $('.btn_aceptar').css('display','none');
                $('.info_traspaso').html(array.traspaso);
            }else{
                toastr.error('Este código, erróneo', '¡Atención!');
            }
        }
    });
}

var id_traspaso=0;
function cancelar_traspaso_modal(id){
    $('#traspaso_partida_modal').modal()
    id_traspaso=id;
}
function verificar_pass2(){
    var passw2=$('#passw2').val();
    if(passw2!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Registrar_Traspasos/verificar',
            data: {pass:passw2},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                if(data==1){
                    $('#traspaso_partida_modal').modal('hide');  
                    $('#eliminar_traspaso').modal();
                }else{
                    toastr.error('Contraseña incorrecta', '!Atención!');
                }
            }
        });
    }else{
        toastr.error('El campo esta vacío', '!Atención!');
    }
}
function cancelar_traspaso(){
    $.ajax({
        type:'POST',
        url: base_url+'Registrar_Traspasos/deleteregistro_traspaso',
        data: {id:id_traspaso},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            toastr.success('Registro cancelado','Hecho!' );
            $('#eliminar_traspaso').modal('hide')
            setTimeout(function(){ 
                $('.row_tras_'+id_traspaso).remove();
            }, 1000);
        }
    });
}
function aceptar_traspaso(id){
    var checked_aux=1;
    if($('.check_tras_'+id).is(':checked')){
        checked_aux=2;

    }else{
        checked_aux=1;
    }
    $.ajax({
        type:'POST',
        url: base_url+'Registrar_Traspasos/aceptar_registro_traspaso',
        data: {id:id,checked:checked_aux},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
        }
    });
}
var id_tras=0;
function modificar_cantidad(id,cant){
    var cant_aux=$('.cantidad_tras_aux_'+id).val();
    if(cant_aux==1){
        $('.cantidad_tras_'+id).val(cant);
        $('#traspaso_cantidad_modal').modal();
    }
    id_tras=id;
}
function verificar_pass(){
    var pass=$('#passw').val();
    if(pass!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Registrar_Traspasos/verificar',
            data: {pass:pass},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                if(data==1){
                    $('.cantidad_tras_aux_'+id_tras).val(0);
                    $('#traspaso_cantidad_modal').modal('hide');  
                }else{
                    toastr.error('Contraseña incorrecta', '!Atención!');
                }
            }
        });
    }else{
        toastr.error('El campo esta vacío', '!Atención!');
    }
}

function guardar_traspaso(id){
    
    var aux_check=1;
    var DATAP= [];
    var TABLAP = $("#tabla_productos tbody > tr");                  
    TABLAP.each(function(){ 
        if($(this).find("input[class*='check_tras']").is(':checked')){
          item = {};
          item ["idtraspaso_detalle"]=$(this).find("input[id*='idtraspasodx']").val();
          item ["productoidx"]=$(this).find("input[id*='productoidx']").val();
          item ["cantidad"]=$(this).find("input[id*='cantidad_trasx']").val();
          DATAP.push(item);
        }else{
            aux_check=0;
        }    
    });
    INFOP  = new FormData();
    aInfop   = JSON.stringify(DATAP);
    INFOP.append('data', aInfop);
    INFOP.append('idtraspaso', id);
    if(aux_check==1){
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            data: INFOP,
            type: 'POST',
            url : base_url+'Registrar_Traspasos/update_partidas',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    //toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    //toastr.error('Error', '500');
                }
            },
            success: function(data){
                toastr.success('Guardado Correctamente', 'Hecho!');
                location.href=base_url+'Registrar_Traspasos';
            }
        });
    }else{
        toastr.error('Favor de validar todos los productos', '!Atención!');
    }
}