var base_url = $('#base_url').val();
var proselected_length=0;
var proselected=0;
$(document).ready(function() {	
	$('#cproveedor').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un proveedor',
	  	ajax: {
	    	url: 'Compras/searchpro',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var itemspro = [];
	    	data.forEach(function(element) {
                itemspro.push({
                    id: element.id_proveedor,
                    text: element.razon_social
                });
            });
            return {
                results: itemspro
            };	    	
	    },  
	  }
	});

	$( "#productos" ).focus();
    $( "#productos" ).keypress(function(e) {
        if (e.which==13) {
            productos_buscar(1);
        }else{
            setTimeout(function(){ 
                productos_buscar(0);
            }, 900);
            
        }
    });
    $(document).keydown(function(e) {
        console.log(e.which)
        switch(e.which) { 
            case 38:
                navigate('up');
            break;
            case 40:
                navigate('down');
            break;
            case 13:
                navigate('enter');

            break;
        }
    });

	$('#ingresaventa').click(function(event) {
		    var total_aux   = $("#productosv tbody > tr");
		    var num_aux=0;
			total_aux.each(function(){ 
			    num_aux=1; 
			});      
            if(num_aux==1){          
                $.ajax({
                    type:'POST',
                    url: 'Compras/ingresarcompra',
                    data: {
                    	uss: $('#ssessius').val(),
                        prov: $('#cproveedor').val(),
                        desc: $('#mdescuento').val(),
                        total: $('#vtotal').val(),
                        idsucursal: $('#idsucursal').val()
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                    	var idcompra=data;
                    	var DATA  = [];
            			var TABLA   = $("#productosv tbody > tr");
            				TABLA.each(function(){         
				                item = {};
				                item ["idcompra"] = idcompra;
				                item ["producto"] = $(this).find("input[id*='vsproid']").val();
				                item ["cantidad"] = $(this).find("input[id*='vscanti']").val();
				                item ["precio"] = $(this).find("input[id*='vsprecio']").val();
                                item ["provedor_id"] = $(this).find("input[id*='vsprovedor_id']").val();
                                
				                item ["idsucursal"] = $('#idsucursal').val()
				                DATA.push(item);
				            });
            				INFO  = new FormData();
            				aInfo   = JSON.stringify(DATA);
            				INFO.append('data', aInfo);
				            $.ajax({
				                data: INFO,
				                type: 'POST',
				                url : 'Compras/ingresarcomprapro',
				                processData: false, 
				                contentType: false,
				                success: function(data){
				                }
				            });
				            toastr.success('Compra guardado correctamente', '¡Éxito!');
				            limpiar();
				            $('#vcantidad').val('')
							//$('#vproducto').html('');
							$('#cprecio').val('');
						    //$("#vproducto").val(0).change();
						    $('#vtotal').val(0);
                            $('#total_p_total_texto').val('$0.00');
                            setTimeout(function(){ 
                                window.location.href = base_url+"Listacompras";
                            }, 1000);
                    }
                });
            }else{   
            	toastr.error('Falta agregar una compra', '¡Atención!');
            }
        });
	


});

function addproducto(idproducto){
    var prove = $('#cproveedor').val();
	if ($('#vcantidad').val()>0 && $('#cprecio').val()>0){
        if(prove!=null){
    		$.ajax({
    	        type:'POST',
    	        url: 'Compras/addproducto',
    	        data: {
    	            cant: $('#vcantidad').val(),
    	            prod: idproducto,
    	            prec: $('#cprecio').val(),
                    prov: $('#cproveedor option:selected').val(),
    	            },
    	            async: false,
    	            statusCode:{
    	                404: function(data){
    	                    toastr.error('Error!', 'No Se encuentra el archivo');
    	                },
    	                500: function(){
    	                    toastr.error('Error', '500');
    	                }
    	            },
    	            success:function(data){
    	                var array = $.parseJSON(data);
    	                console.log(array);
    	                var selecttotal = Number((parseFloat(array.cant)*parseFloat(array.precio)).toFixed(2));
    	                var producto='<tr class="producto_'+array.id+'">\
    	                                <td><input type="hidden" name="vsproid" id="vsproid" value="'+array.id+'">'+array.codigo+'</td>\
    	                                <td><input type="number" name="vscanti" id="vscanti" value="'+array.cant+'" readonly style="background: transparent;border: 0px;width: 80px;"></td>\
    	                                <td>'+array.proveedor+'\
                                            <input type="hidden" name="vsprovedor_id" id="vsprovedor_id" value="'+array.provedor_id+'">\
                                        </td>\
                                        <td>'+array.producto+'</td>\
    	                                <td>$ <input type="text" name="vsprecio" id="vsprecio" value="'+Number(array.precio).toFixed(2)+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>\
    	                                <td>$ <input type="text" class="vstotal" name="vstotal" id="vstotal" value="'+Number(selecttotal).toFixed(2)+'" readonly style="background: transparent;border: 0px;width: 100px;"></td>\
    	                                <td><a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro('+array.id+')"><i class="ft-trash font-medium-3"></i></a></td>\
    	                            </tr>';
    	                $('#class_productos').append(producto);
                        setTimeout(function(){ 
                            $('#productos').val('');
                            $('.producto_buscar_t').html('');
                        }, 300);
    	            }
    	        });
    		$('#vcantidad').val('')
    		//$('#vproducto').html('');
    		$('#cprecio').val('');
    	    //$("#vproducto").val(0).change();
        }else{
            toastr.error('Falta capturar el campo de proveedor', '¡Atención!');
        }
	}else{
        toastr.error('Falta agregar una cantidad o precio compra', '¡Atención!');
    }





	
    calculartotal();
}
function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    
    $('#vtotal').val(Number(addtp).toFixed(2));
    $('#total_p_total_texto').val('$'+Number(addtp).toFixed(2));
}
function limpiar(){
	$('#class_productos').html('');
}
function deletepro(id){
	$('.producto_'+id).remove();
}

function productos_buscar(tipo){
    var productos_b=$('#productos').val();
    if(productos_b!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Compras/buscar_producto',
            data:{
                producto:productos_b,
                idsucursal:$('#idsucursal option:selected').val()
                },
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){ 
                    console.log(data); 
                    var array = $.parseJSON(data);
                    if (tipo==0) {
                        $('.producto_buscar_t').html('');
                        array.forEach(function(item) {
                            var html='<a onclick="addproducto('+item.productoid+')" \
                                        class="btn btn_orange_borde pro_'+item.productoid+' proselected" style="width: 100%;">'+item.codigo+' / '+item.nombre+'</a><br>';
                            $('.producto_buscar_t').append(html);
                        });
                        proselected_length=$('.proselected').length;
                        proselected=-1;

                    }else{
                        if (array.length==1) {
                            array.forEach(function(item) {
                                addproducto(item.productoid);
                            });
                        }else{

                        }
                    }
            }
        });
    }else{
        setTimeout(function(){ 
            $('.producto_buscar_t').html('');
        }, 1000);
    }
}

function navigate(direction) {
    if(proselected_length>0){
        if(direction == 'up') {//subir
            $(".proselected").removeClass('selected');
            if (proselected==0 ||proselected==-1) {
                proselected=0;
            }else{
                proselected--;
            }
            
        
        } else if (direction == 'down') {//bajar
            $(".proselected").removeClass('selected');
            proselected++;
            if(proselected>=proselected_length){
                proselected=(proselected_length-1);
            }
        }else if (direction == 'enter') {
            $(".proselected.selected").click();
        }
    }
    
    //console.log(proselected);
    var valor=$(".proselected").eq(proselected).addClass('selected');
    //console.log(valor);
}

function campo_vacio(){
    var productos = $('#productos').val();
    if(productos==''){
        $('.producto_buscar_t').html('');   
    }
}