var base_url = $('#base_url').val();
var tabla;
$(document).ready(function() {
	table();
});

function reload_registro(){
    tabla.destroy();
    table();
}

function table(){
	tabla=$("#data_tables").DataTable({
        "bProcessing": true,
        "serverSide": true,
        "searching": true,
        responsive: !0,
        "ajax": {
            "url": base_url+"Pedidos/getlistado",
            type: "post",
            error: function(){
               $("#data_tables").css("display","none");
            }
        },
        "columns": [
            {"data":"id"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html='';
                    if(row.cliente==0){
                        html=row.personacontacto;
                    }else{
                        html=row.nombre;
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var cant=parseFloat(row.total);
                    var html ='$'+cant.toFixed(2);
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var ant=parseFloat(row.anticipo);
                    var html ='$'+ant.toFixed(2);
                return html;
                }
            },
            {"data":"fechapedido"},
            {"data":"fechaentrega"},
            {"data":"Usuario"},
            {"data": null,
                "render": function ( data, type, row, meta ){
                    var html = '';
                    if(row.activo==1){
                        if(row.estatus==0){
                            html = '<a class="badge btn_orange border_eti idregs_'+row.id+'" data-estatus="'+row.estatus+'" onclick="modal_estatus('+row.id+')" style="color:white">Pendiente</a>';
                        }else if(row.estatus==1){
                            html = '<a class="badge bg-info border_eti idregs_'+row.id+'" data-estatus="'+row.estatus+'" onclick="modal_estatus('+row.id+')" style="color:white">Proceso</a>';
                        }else if(row.estatus==2){
                            html = '<a class="badge btn-success border_eti idregs_'+row.id+'" data-estatus="'+row.estatus+'" onclick="modal_estatus('+row.id+')" style="color:white">Entregada</a>';
                        }else if(row.estatus==3){
                            html = '<a class="badge btn-danger border_eti idregs_'+row.id+'" data-estatus="'+row.estatus+'" onclick="modal_estatus('+row.id+')" style="color:white">Saldado</a>';
                        }
                    }else{
                       
                    }
                return html;
                }
            },
            {"data": null,
                "render": function ( data, type, row, meta ){
                var html='';
                if(row.activo==1){
                    if(row.estatus!=2){
                        var cm="'";
                            html+='<div class="form-group">\
                                <div class="btn-group" role="group" aria-label="Basic example">\
                                    <a class="btn btn-raised btn-icon btn_orange" href="'+base_url+'Pedidos/formulario/'+row.id+'"><i class="ft-edit"></i></a>\
                                    <a class="btn btn-raised btn-icon btn_amarillo" target="block_" href="'+base_url+'Pedidos/documento/'+row.id+'"><i class="ft-printer"></i></a>';
                            html+='<a class="btn btn-raised btn-icon btn_amarillo idreg_'+row.id+'" data-total="'+row.total+'" data-anticipo="'+row.anticipo+'" data-cliente="'+row.nombre+'"  data-fechapedido="'+row.fechapedido+'"  data-fechaentrega="'+row.fechaentrega+'" data-cantdescuento="'+row.cantdescuento+'"  onclick="modal_pago('+row.id+')"><b>$</b></a>\
                                    <a class="btn btn-raised btn-icon btn-danger white" title="Rechazar" onclick="modal_cancelar('+row.id+')"><i class="ft-x-square"></i></a>';
                                
                         html+='</div>\
                        </div>';
                    }else{
                        var cm="'";
                            html+='<div class="form-group">\
                                <div class="btn-group" role="group" aria-label="Basic example">\
                                    <a class="btn btn-raised btn-icon btn_amarillo" target="block_" href="'+base_url+'Pedidos/documento/'+row.id+'"><i class="ft-printer"></i></a>';
                            html+='<a class="btn btn-raised btn-icon btn_amarillo idreg_'+row.id+'" data-total="'+row.total+'" data-anticipo="'+row.anticipo+'" data-cliente="'+row.nombre+'"  data-fechapedido="'+row.fechapedido+'"  data-fechaentrega="'+row.fechaentrega+'" data-cantdescuento="'+row.cantdescuento+'"  onclick="modal_pago('+row.id+')"><b>$</b></a>';
                                
                         html+='</div>\
                        </div>';
                    }
                }else{
                    html = '<a class="badge btn-danger" style="color:white">Cancelado</a>';
                }
                return html;
                }
            },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50], [10, 25, 50]],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Mostrar _MENU_ entradas",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    
    });
}

var resta_aux=0;
var idpedido=0;
function modal_pago(id){
    $('#pagos_registro').modal();
    $('.cliente_txt').html($('.idreg_'+id).data('cliente'));
    $('.fecha_pedido_txt').html($('.idreg_'+id).data('fechapedido'));
    $('.fecha_entrega_txt').html($('.idreg_'+id).data('fechaentrega'));
    var ttl=parseFloat($('.idreg_'+id).data('total'));
    var html1 = '$'+parseFloat(ttl, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();// '$'+ttl.toFixed(2);
    var ant=parseFloat($('.idreg_'+id).data('anticipo'));
    var html2 = '$'+parseFloat(ant, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString(); //'$'+ant.toFixed(2);
    var desc=parseFloat($('.idreg_'+id).data('cantdescuento'));
    var html3 = '$'+parseFloat(desc, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString(); //'$'+ant.toFixed(2);
    $('.total_t').html(html1);
    $('.anticipo_t').html(html2);
    $('.descuento_t').html(html3);
    var ttl2=parseFloat(ttl);
    var ant2=parseFloat(ant);
    var resta=ttl2-ant2; 
    $('.restante_t').html('$'+parseFloat(resta, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
    tabla_pagos(id);
    idpedido=id;
}

function tabla_pagos(id){
    $.ajax({
        type:'POST',
        url: base_url+'Pedidos/pagos_pedido',
        data: {id:id},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array=$.parseJSON(data);
            $('#tbody_pago').html(array.tabla);
            $('.total_abono').html('<b>Abono: $'+parseFloat(array.suma_pagos, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+'</b>');
            var resta_total=parseFloat(array.resta_total);
            $('.restante_t').html('$'+parseFloat(resta_total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            resta_aux=resta_total;
            if(resta_total<=0){
                $('.txt_abono').css('display','none');
                $.ajax({
                    type:'POST',
                    url: base_url+'Pedidos/update_saldado',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        if(data==1){
                            tabla.ajax.reload();
                            toastr.success('Hecho!', 'Pedido saldado');
                        }    
                    }
                });
            }
        }
    });
}

function guardar_pago(){
   var pago = $('#pago').val();
   var fecha = $('#fecha').val();
   if(pago!=''){
        if(pago>resta_aux){
            toastr.error('La cantidad es mayor a lo que resta','Atención');
        }else{
            if(fecha!=''){
                $('.btn_pago').attr('disabled',true);
                $.ajax({
                    type:'POST',
                    url: base_url+'Pedidos/registrar_pago',
                    data: {idpedido:idpedido,pago:pago,fecha:fecha},
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                        setTimeout (function(){ 
                            $('.btn_pago').attr('disabled',false);
                            $('#pago').val('');
                            tabla_pagos(idpedido);
                        }, 2000);
                        toastr.success('Hecho!', 'Guardado Correctamente');
                    }
                });
            }else{
                toastr.error('El campo de fecha esta vacío','Atención');
            }
        }
   }else{
        toastr.error('El campo de pago esta vacío','Atención');
   }
   
}

var idpedido_est=0;
function modal_estatus(id){
    idpedido_est=id;
    var estu=$('.idregs_'+id).data('estatus');
    if(estu==1){
        $('#proceso').prop('checked', true);
        $('#entrgado').prop('checked', false);
        $('#proceso').prop('disabled', false);
        $('#entrgado').prop('disabled', false);
    }else if(estu==2){
        $('#proceso').prop('checked', true);
        $('#entrgado').prop('checked', true);
        $('#proceso').prop('disabled', true);
        $('#entrgado').prop('disabled', true);
    }else{
        $('#proceso').prop('checked', false);
        $('#entrgado').prop('checked', false);
        $('#proceso').prop('disabled', false);
        $('#entrgado').prop('disabled', false);
    }
    $('#editar_estatus').modal();
}

function estatus_proceso(){
    var estatus=0;
    if($('#proceso').is(':checked')){
        estatus=1;
    }else{
        estatus=0;
        $('#entrgado').prop('checked', false);
    }
    $.ajax({
        type:'POST',
        url: base_url+'Pedidos/update_estatus',
        data: {idpedido:idpedido_est,estatus:estatus},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            tabla.ajax.reload();
            toastr.success('Hecho!', 'Actualizado Correctamente');
        }
    }); 
}

function estatus_entregada(){
    var estatus=0;
    if($('#entrgado').is(':checked')){
        estatus=2;
        $('#proceso').prop('checked', true);
        $('#proceso').prop('disabled', true);
        $('#entrgado').prop('disabled', true);
    }else{
        estatus=1;
    }

    $.ajax({
        type:'POST',
        url: base_url+'Pedidos/update_estatus',
        data: {idpedido:idpedido_est,estatus:estatus},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            tabla.ajax.reload();
            toastr.success('Hecho!', 'Actualizado Correctamente');
        }
    });
    
}

function modal_cancelar(id){
    idpedido_est=id;
    $('#cancelar_registro').modal();
}

function cancelar_pedido(){
    $.ajax({
        type:'POST',
        url: base_url+'Pedidos/update_registro',
        data: {idpedido:idpedido_est},
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            tabla.ajax.reload();
            toastr.success('Hecho!', 'Cancelado Correctamente');
            $('#cancelar_registro').modal('hide');
        }
    }); 
}