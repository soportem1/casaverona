var base_url = $('#base_url').val();
$(document).ready(function() {

    sucursales_grafica_txt();
    setTimeout(function(){
        sucursales_grafica(); 
    }, 1000);
    setInterval(function(){ 
        sucursales_grafica_txt();
        setTimeout(function(){
            sucursales_grafica(); 
        }, 1000);
    },  60000);
    
});

function sucursales_grafica_txt(){
    $('.col_texto').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_sucursales_all_texto',
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){ 
            $('.col_texto').html(data);
        }
    });

}

function sucursales_grafica(){
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/get_sucursales_all',
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){ 
            var array = $.parseJSON(data);
            array.forEach(function(item) {
                graficas(item.id,item.nombre);
            });
        }
    });
}

function graficas(id,nemesuc){
    var label_pro=productos_vendidos_txt(id);
    //console.log(label_pro);
    var data_pro=productos_vendidos_num(id);
    if (label_pro.length != 0){
        var ctx = $("#chart_"+id);
        // Chart Options
        var chartOptions = {
            // Elements options apply to all of the options unless overridden in a dataset
            // In this case, we are setting the border of each bar to be 2px wide and green
            elements: {
                rectangle: {
                    borderWidth: 2,
                    borderColor: 'rgb(0, 255, 0)',
                    borderSkipped: 'bottom'
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            responsiveAnimationDuration: 500,
            legend: {
                position: 'top',
            },
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        color: "#f2b327",
                        drawTicks: false,
                    },
                    scaleLabel: {
                        display: true,
                    }
                }],
            },
            title: {
                display: true,
                text: nemesuc
            }
        };

        // Chart Data
        var chartData = {
            labels: label_pro,
            datasets: [{
                label: "Productos",
                data: data_pro,
                backgroundColor: "#f2b327",
                hoverBackgroundColor: "#f2752c",
                borderColor: "transparent"
            }]
        };

        var config = {
            type: 'bar',

            // Chart Options
            options: chartOptions,

            data: chartData
        };
        // Create the chart
        var lineChart = new Chart(ctx, config);
    }
}   

function productos_vendidos_txt(id){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/sucursal_all',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            var cont = 1;
            array.forEach(function(item){
                pro.push(cont);
                cont++ 
            });
        }
    });
    return pro;
}

function productos_vendidos_num(id){
    var pro =[];
    $.ajax({
        type:'POST',
        url: base_url+'Inicio/sucursal_all',
        data: {id:id},
        async: false,
        statusCode:{
            404: function(data){
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(){
                toastr.error('Error', '500');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            array.forEach(function(item) {
                pro.push(item.total_cantidad);
            });
        }
    });
    return pro;
}